<?php

namespace Rebond\Controller;

use Rebond\App;
use Rebond\Enums\Cms\Component;
use Rebond\Enums\Cms\Menu;
use Rebond\Enums\Cms\SideNav;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Status;
use Rebond\Models\Cms\Page;
use Rebond\Repository\Cms\GadgetRepository;
use Rebond\Repository\Cms\LayoutRepository;
use Rebond\Repository\Cms\PageRepository;
use Rebond\Repository\Cms\TemplateRepository;
use Rebond\Services\Auth;
use Rebond\Services\Cache;
use Rebond\Services\Core\SiteService;
use Rebond\Services\Cms\PageService;
use Rebond\Services\Lang;
use Rebond\Services\Log;
use Rebond\Services\Nav;
use Rebond\Services\Renderer;
use Rebond\Services\Session;
use Rebond\Services\Template;

class CmsController
{
    /* @var App */
    protected $app;
    /* @var Page */
    protected $page;
    /* @var Cache */
    protected $cache;
    /* @var array */
    protected $gadgets;
    /* @var int */
    protected $cacheTime;
    /** @var Template */
    protected $tplMaster;
    /* @var Template */
    protected $tplLayout;

    /**
     * @param App $app
     * @param string $friendlyUrl
     */
    public function run(App $app, $friendlyUrl)
    {
        if ($app->getSite()->getStatus() == Status::INACTIVE) {
            $renderer = new Renderer($app);
            echo $renderer->inactive();
            Session::close();
        }

        $this->app = $app;

        $this->page = $this->getPage($friendlyUrl);
        $this->cache = new Cache($app);
        $this->tplMaster = new Template(Template::SITE, ['www']);
        $this->tplLayout = new Template(Template::SITE, ['www']);
        $this->cacheTime = $this->app->getSite()->getCacheTime();

        $layout = LayoutRepository::loadById($this->page->getLayoutId());

        $this->loadGadgets();
        $this->fillEmptyColumns($layout->getColumns());

        // template
        $template = TemplateRepository::loadById($this->page->getTemplateId());

        $this->loadNavHeader($template);
        $this->loadBreadcrumbs($template);
        $this->loadNavSide($template);
        $this->loadFooterNav($template);

        $this->tplMaster->set('layout', $this->tplLayout->render($layout->getFilename()));

        $this->loadHeader();
        $this->loadCssAndJs();

        return $this->tplMaster->render($template->getFilename());
    }

    /**
     * @param string $friendlyUrl
     * @return Page
     * @throws \Exception
     */
    private function getPage($friendlyUrl)
    {
        // retrieve page
        $page = PageRepository::loadByUrl($friendlyUrl);

        // this should never occur
        if (!isset($page)) {
            throw new \Exception($friendlyUrl, Code::PAGE_NOT_FOUND);
        }

        // redirection
        if ($page->getRedirect() != '' && $page->getRedirect() != $friendlyUrl) {
            $friendlyUrl = Nav::readCmsRequest($page->getRedirect());
            return $this->getPage($friendlyUrl);
        }

        // permission
        Auth::isAuthorized($this->app->getUser(), $page->getPermission(), true, $this->app->getSite()->getSignInUrl());

        $this->app->setUrl($friendlyUrl);
        return $page;
    }

    private function loadGadgets()
    {
        $pageInfo = 'page: ' . $this->page->getTitle() . ' [' . $this->page->getId() . ']';

        // page components
        $options = [];
        $options['where'][] = 'gadget.status = 1';
        $options['order'][] = 'gadget.display_order';
        $gadgets = GadgetRepository::loadAllByPageId($this->page->getId(), $options);

        if (count($gadgets) == 0) {
            return;
        }

        $this->gadgets = [];

        foreach ($gadgets as $gadget) {
            $component = $gadget->getComponent();

            if (!isset($component)) {
                Log::log(Code::COMPONENT_NOT_FOUND,
                    $pageInfo . ', ' .
                    'component: ' . $gadget->getComponentId(),
                    __FILE__, __LINE__);
                continue;
            }
            $moduleName = $component->getModule()->getName();
            $moduleInfo = $moduleName . ' [' . $component->getModuleId() . ']';

            $gadgetPath = '\Own\Gadgets\App\\' . $moduleName . 'Gadget';
            $componentMethod = $component->getMethod();
            $componentInfo = $component->getModule()
                    ->getName() . '_' . $component->getTitle() . '[' . $component->getId() . ']';
            $componentCache = $component->getModule()->getName() . '_' . $component->getMethod();

            if (!method_exists($gadgetPath, $componentMethod)) {
                Log::log(Code::GADGET_NOT_FOUND,
                    $pageInfo . ', ' .
                    'module: ' . $moduleName . ', ' .
                    'component: ' . $componentInfo, __FILE__,
                    __LINE__);
                continue;
            }

            if ($component->getStatus() != Status::ACTIVE || $component->getModule()->getStatus() != Status::ACTIVE) {
                Log::log(Code::GADGET_NOT_FOUND,
                    $pageInfo . ', ' .
                    'component status: ' . $componentInfo . ': ' . $component->getStatus() . ',' .
                    'module status: ' . $moduleInfo . ': ' . $component->getModule()->getStatus(),
                    __FILE__, __LINE__);
                continue;
            }

            // look for cache
            if ($component->getCanBeCached()) {
                $html = $this->cache->getGadgetCache($componentCache,
                    $gadget->getFilterId() . '_' . $gadget->getCustomFilter());
                if (isset($html)) {
                    $this->tplLayout->add('column' . $gadget->getCol(), $html);
                    continue;
                }
            }

            $gadgetClass = $this->getGadget($moduleName, $gadgetPath);

            // get filter
            if (in_array($component->getType(), [Component::SINGLE_ELEMENT, Component::FILTERED_CARDS])) {
                if ($gadget->getFilterId() == 0) {
                    Log::log(Code::EMPTY_FILTER,
                        $pageInfo . ', ' .
                        'component: ' . $componentInfo, __FILE__, __LINE__);
                    continue;
                } else {
                    $render = $gadgetClass->$componentMethod($gadget->getFilterId());
                }
            } else {
                $render = ($component->getType() == Component::CUSTOM_LISTING)
                    ? $gadgetClass->$componentMethod($gadget->getCustomFilter())
                    : $gadgetClass->$componentMethod();
            }

            $this->tplLayout->add('column' . $gadget->getCol(), $render);

            if ($component->getCanBeCached()) {
                $this->cache->saveGadgetCache($componentCache,
                    $gadget->getFilterId() . '_' . $gadget->getCustomFilter(),
                    $render);
            }
        }
    }

    /** @param int $columns */
    private function fillEmptyColumns($columns)
    {
        $url = $this->app->getConfig('admin-url');
        $isAdmin = $this->app->getUser()->hasAccess('admin.page', true);

        for ($i = 1; $i <= $columns; $i++) {
            if ($this->tplLayout->varIsSet('column' . $i) === false) {
                $emptyGadget = $isAdmin
                    ? '<div class="status-error">' . Lang::lang('gadget_none',
                        [$i, $url, $this->page->getId()]) . '</div>'
                    : '';
                $this->tplLayout->set('column' . $i, $emptyGadget);
            }
        }
    }

    private function loadNavHeader(\Rebond\Models\Cms\Template $template)
    {
        if ($template->getMenu() == Menu::NONE) {
            return;
        }

        // Get cache
        $navHeader = $this->cache->getCache('navHeader', $template->getMenu() . '_' . $template->getMenuLevel());

        if (!isset($navHeader)) {
            $navHeader = PageRepository::buildHeaderNav($template->getMenuLevel(), $template->getMenu());
            $this->cache->saveCache('navHeader', $template->getMenu() . '_' . $template->getMenuLevel(), $navHeader);
        }

        $tpl = new Template(Template::SITE, ['www']);
        $tpl->set('title', $this->app->getSite()->getTitle());
        $tpl->set('nav', PageService::renderNav($navHeader, $this->page->getFullUrl()));
        $this->tplLayout->set('navHeader', $tpl->render('nav-header'));
    }

    private function loadBreadcrumbs(\Rebond\Models\Cms\Template $template)
    {
        if (!$template->getInBreadcrumb()) {
            return;
        }

        // Get cache
        $breadcrumb = $this->cache->getCache('breadcrumb', $this->page->getId());

        if (!isset($breadcrumb)) {
            $breadcrumb = PageService::renderBreadcrumb($this->page->getId());
            $this->cache->saveCache('breadcrumb', $this->page->getId(), $breadcrumb);
        }

        $this->tplLayout->set('breadcrumb', $breadcrumb);
    }

    private function loadNavSide(\Rebond\Models\Cms\Template $template)
    {
        if ($template->getSideNav() == SideNav::NONE) {
            return;
        }

        $currentId = null;

        // Get cache
        $navSide = $this->cache->getCache('navSide', $template->getSideNav() . '_' . $template->getSideNavLevel());

        if (!isset($navSide)) {
            switch ($template->getSideNav()) {
                case SideNav::PARENT_1:
                    $currentId = $this->page->getParentId();
                    break;
                case SideNav::CHILDREN:
                    $currentId = $this->page->getId();
                    break;
                case SideNav::HOME:
                    $currentId = 1;
                    break;
                case SideNav::PARENT_2:
                    $currentId = PageRepository::loadByParent($this->page->getParentId(), 1);
                    break;
                case SideNav::PARENT_3:
                    $currentId = PageRepository::loadByParent($this->page->getParentId(), 2);
                    break;
            }
            $navSide = PageRepository::buildSideNav($currentId, $template->getSideNavLevel());
            $this->cache->saveCache('navSide', $template->getSideNav() . '_' . $template->getSideNavLevel(), $navSide);
        }

        $tpl = new Template(Template::SITE, ['www']);
        $tpl->set('nav', PageService::renderNav($navSide, $this->page->getFullUrl()));
        $this->tplLayout->set('navSide', $tpl->render('nav-side'));
    }

    private function loadFooterNav(\Rebond\Models\Cms\Template $template)
    {
        if (!$template->getInFooter()) {
            return;
        }

        $navFooter = $this->cache->getCache('navFooter', $template->getFooterLevel());

        if (!isset($navFooter)) {
            $navFooter = PageRepository::buildFooterNav($template->getFooterLevel());
            $this->cache->saveCache('navFooter', $template->getFooterLevel(), $navFooter);
        }

        $tpl = new Template(Template::SITE, ['www']);
        $tpl->set('nav', PageService::renderNav($navFooter, $this->page->getFullUrl()));
        $this->tplLayout->set('navFooter', $tpl->render('nav-footer'));
    }

    private function loadHeader()
    {
        if ($this->app->getSite()->getKeywords() != '') {
            $this->tplMaster->addMeta('keywords', $this->app->getSite()->getKeywords());
        }
        if ($this->app->getSite()->getDescription() != '') {
            $this->tplMaster->addMeta('description', $this->app->getSite()->getDescription());
        }

        $this->tplMaster->set('title', $this->page->getTitle());
        $this->tplMaster->set('site', $this->app->getSite()->getTitle());
        $this->tplMaster->set('skin', $this->app->getSkin());
        $this->tplMaster->set('siteUrl', $this->app->getConfig('site-url'));
        $this->tplMaster->set('adminUrl', $this->app->getConfig('admin-url'));
        $this->tplMaster->set('bodyClass', $this->page->getClass());
        $this->tplMaster->set('notifications', Session::renderSiteResp());
        $this->tplMaster->set('signedUser', $this->app->getUser());
    }

    private function loadCssAndJs()
    {
        $ga = SiteService::renderGoogleAnalytics($this->app->getSite()->getGoogleAnalytics());
        $this->tplMaster->set('footer', $ga);

        $this->tplMaster->addJs('/node_modules/jquery/dist/jquery.min.js');
        $this->tplMaster->addJs('/js/lang-' . $this->app->getCurrentLang() . '.js');

        if ($this->app->getSite()->getJs() != '') {
            $jsList = explode(',', $this->app->getSite()->getJs());
            foreach ($jsList as $js) {
                $this->tplMaster->addJs($js);
            }
        }

        if ($this->page->getJs() != '') {
            $jsList = explode(',', $this->page->getJs());
            foreach ($jsList as $js) {
                $this->tplMaster->addJs($js);
            }
        }

        $this->tplMaster->addCss('https://fonts.googleapis.com/css?family=Work+Sans');

        if (!$this->app->isDebug()) {
            $this->tplMaster->addCss('/css/min-' . $this->app->getSkin() . '.css');
            $this->tplMaster->addJs('/js/min.js');
        } else {
            $this->tplMaster->addCss('/node_modules/normalize-css/normalize.css');
            $this->tplMaster->addCss('/node_modules/components-font-awesome/css/font-awesome.min.css');
            $this->tplMaster->addCss('/node_modules/rebond-assets/css/rebond.css');
            $this->tplMaster->addCss('/node_modules/rebond-assets-site/css/rebond-site.css');
            $this->tplMaster->addCss('/css/skin/' . $this->app->getSkin() . '/own.css', false);
            $this->tplMaster->addJs('/node_modules/rebond-assets/js/rebond.js');
            $this->tplMaster->addJs('/node_modules/rebond-assets-site/js/rebond-site.js');
            $this->tplMaster->addJs('/js/own.js', false);
        }

        if ($this->app->getSite()->getCss() != '') {
            $cssList = explode(',', $this->app->getSite()->getCss());
            foreach ($cssList as $css) {
                $this->tplMaster->addCss($css);
            }
        }
        if ($this->page->getCss() != '') {
            $cssList = explode(',', $this->page->getCss());
            foreach ($cssList as $css) {
                $this->tplMaster->addCss($css);
            }
        }
    }

    /**
     * @param string $moduleName
     * @param string $gadgetPath
     * @return string
     */
    private function getGadget($moduleName, $gadgetPath)
    {
        if (!array_key_exists($moduleName, $this->gadgets)) {
            $this->gadgets[$moduleName] = new $gadgetPath($this->app, $moduleName);
        }
        return $this->gadgets[$moduleName];
    }
}
