<?php

namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Models\DateTime;
use Rebond\Repository\Core\UserRepository;
use Rebond\Repository\Data;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\File;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Session;
use Rebond\Services\Template;

class ToolsController extends BaseAdminController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        Auth::isAdminAuthorized($this->signedUser, 'admin.tools', true, '/');
    }

    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('tools'));
    }

    // Overview page
    public function index()
    {
        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'tools']);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('index'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function logs()
    {
        $now = new DateTime();
        $logPath = $this->app->getPath('own-log-file');
        $defaultLog = $now->format('Y-m') . '-error';

        if (!file_exists($logPath . $defaultLog . '.json')) {
            File::save($logPath . $defaultLog . '.json', '[]');
        }

        $selectedLog = Converter::stringKey('log', 'get', $defaultLog);

        // action
        if (Form::isSubmitted('btnClear')) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.tools.edit', true, '/tools/logs');
            if (File::delete($logPath, $selectedLog . '.json')) {
                Session::adminSuccess('file_deleted', '/tools/logs', [$selectedLog]);
            } else {
                Session::adminError('error_file_not_found', [$selectedLog]);
            }
        }

        $logs = File::readJson($logPath . $selectedLog . '.json');
        $logs = array_reverse($logs);

        foreach ($logs as &$log) {
            $log['user'] = UserRepository::loadById($log['userId'], true);
        }

        // view
        $this->setTpl();

        // filter
        $logFiles = File::getFiles($this->app->getPath('own-log-file'));
        $logFiles = array_reverse($logFiles);

        $logList = [];
        foreach ($logFiles as $logFile) {
            $logNoExt = \Rebond\Services\File::getNoExtension($logFile);
            $logList[] = [
                'file' => $logNoExt,
                'selected' => ($logNoExt == $selectedLog) ? ' selected' : ''
            ];
        }


        $tplFilter = new Template(Template::MODULE_ADMIN, ['admin', 'log']);
        $tplFilter->set('selectedLog', $selectedLog);
        $tplFilter->set('logList', $logList);

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['admin', 'log']);
        $tplMain->set('selectedLog', $selectedLog);
        $tplMain->set('logs', $logs);

        // layout
        $this->tplLayout->set('column1', $tplFilter->render('filter'));
        $this->tplLayout->set('column2', $tplMain->render('table'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));
        $this->tplMaster->set('jsLauncher', 'toolsLogs');
        return $this->tplMaster->render('tpl-default');
    }

    public function siteInfo()
    {
        $query = 'select version() as v';
        $db = new Data($this->app);
        $dbVersion = $db->selectColumn($query);

        $composer = File::readJson($this->app->getFullPath() . '/composer.lock');
        $folders = File::getFolders($this->app->getConfig('admin') . 'static');
        $bowers = [];
        foreach ($folders as $folder) {
            $bowerFile = $this->app->getConfig('admin') . 'node_modules/' . $folder . '/bower.json';
            if (file_exists($bowerFile)) {
                $file = File::readJson($bowerFile);
                $bowers[$file['name']] = Converter::stringKey('version', $file, 'n/a');
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'tools']);
        $tplMain->set('dbVersion', $dbVersion);
        $tplMain->set('composer', $composer['packages']);
        $tplMain->set('bowers', $bowers);
        $tplMain->set('currentLang', $this->app->getCurrentLang());
        $tplMain->set('env', $this->app->getCurrentLang());
        $tplMain->set('isDebug', $this->app->isDebug());
        $tplMain->set('logSql', $this->app->getSite()->getLogSql());
        $tplMain->set('cookieDomain', $this->app->getConfig('cookie-domain'));
        $tplMain->set('cookies', $_COOKIE);
        $tplMain->set('servers', $_SERVER);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('site-info'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function phpInfo()
    {
        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'tools']);

        ob_start();
        phpinfo();
        $info = ob_get_contents();
        ob_end_clean();
        $info = preg_replace('%^.*<body>(.*)</body>.*$%ms', '$1', $info);

        $tplMain->set('info', $info);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('phpinfo'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }
}
