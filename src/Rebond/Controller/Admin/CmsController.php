<?php
namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Enums\Core\Status;
use Rebond\Forms\Cms\ComponentForm;
use Rebond\Forms\Cms\LayoutForm;
use Rebond\Forms\Cms\ModuleForm;
use Rebond\Forms\Cms\TemplateForm;
use Rebond\Repository\Cms\ComponentRepository;
use Rebond\Repository\Cms\LayoutRepository;
use Rebond\Repository\Cms\ModuleRepository;
use Rebond\Repository\Cms\TemplateRepository;
use Rebond\Services\Auth;
use Rebond\Services\Cms\ModuleService;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Session;
use Rebond\Services\Template;

class CmsController extends BaseAdminController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        Auth::isAdminAuthorized($this->signedUser, 'admin.cms', true, '/');
    }

    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('cms'));
    }

    // Homepage with basic information
    public function index()
    {
            // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'cms']);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('index'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function template()
    {
        $templates = TemplateRepository::loadAll();

        // view
        $this->setTpl();

        // filter
        $tplFilter = new Template(Template::MODULE_ADMIN, ['cms', 'template']);
        $tplFilter->set('count', count($templates));

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['cms', 'template']);
        $tplMain->set('items', $templates);

        // layout
        $this->tplLayout->set('column1', $tplFilter->render('filter'));
        $this->tplLayout->set('column2', $tplMain->render('table'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));
        return $this->tplMaster->render('tpl-default');
    }

    public function templateEdit()
    {
        // check
        $id = Converter::intKey('id');

        $template = TemplateRepository::loadById($id, true);
        $form = new TemplateForm($template);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.cms.edit', true, '/cms/template');
            if ($form->setFromPost()->validate()->isValid()) {
                $template->save();
                Session::adminSuccess('saved', '/cms/template');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['cms', 'template']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.cms.edit')) {
            $tplMain->set('item', $form);
            $this->tplLayout->set('column1', $tplMain->render('editor'));
        } else {
            $tplMain->set('item', $template);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function layout()
    {
        $layouts = LayoutRepository::loadAll();

        // view
        $this->setTpl();
        // filter
        $tplFilter = new Template(Template::MODULE_ADMIN, ['cms', 'layout']);
        $tplFilter->set('count', count($layouts));

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['cms', 'layout']);
        $tplMain->set('items', $layouts);

        // layout
        $this->tplLayout->set('column1', $tplFilter->render('filter'));
        $this->tplLayout->set('column2', $tplMain->render('table'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));
        return $this->tplMaster->render('tpl-default');
    }

    public function layoutEdit()
    {
        // check
        $id = Converter::intKey('id');

        $layout = LayoutRepository::loadById($id, true);
        $form = new LayoutForm($layout);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.cms.edit', true, '/cms/layout');
            if ($form->setFromPost()->validate()->isValid()) {
                $layout->save();
                Session::adminSuccess('saved', '/cms/layout');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['cms', 'layout']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.cms.edit')) {
            $tplMain->set('item', $form);
            $this->tplLayout->set('column1', $tplMain->render('editor'));
        } else {
            $tplMain->set('item', $layout);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function module()
    {
        // action
        if (Form::isSubmitted('btnAdd')) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.cms.edit', true, '/cms/module');
            $moduleId = Converter::intKey('moduleId', 'post');
            $module = ModuleRepository::loadById($moduleId);
            if (isset($module)) {
                $module->setStatus(Status::ACTIVE);
                $module->save();
                Session::adminSuccess('added', '/cms/module');
            } else {
                Session::set('adminError', Lang::lang('module_must_select'));
            }
        }

        $options = [];
        $options['where'][] = 'module.status IN (0,1)';
        $activeModules = ModuleRepository::loadAll($options);

        // view
        $this->setTpl();

        // filter
        $tplFilter = new Template(Template::MODULE_ADMIN, ['cms', 'module']);
        $tplFilter->set('count', count($activeModules));

        $options = [];
        $options['where'][] = 'module.status = 2';
        $newModules = ModuleRepository::loadAll($options);
        $tplFilter->set('items', $newModules);

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['cms', 'module']);
        $tplMain->set('items', $activeModules);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.cms.edit')) {
            $this->tplLayout->set('column1', $tplFilter->render('filter'));
            $this->tplLayout->set('column2', $tplMain->render('table'));
        } else {
            $this->tplLayout->set('column1', $tplFilter->render('filter-config'));
            $this->tplLayout->set('column2', $tplMain->render('table-config'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));
        return $this->tplMaster->render('tpl-default');
    }

    public function moduleEdit()
    {
        // check
        $id = Converter::intKey('id');

        $module = ModuleRepository::loadById($id);
        if (!isset($module)) {
            Session::adminError('item_not_found', '/cms/module', [Lang::lang('module'), $id]);
        }

        $form = new ModuleForm($module);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.cms.edit', true, '/cms/module');
            if ($form->setFromPost()->validate()->isValid()) {
                $module->save();
                Session::adminSuccess('saved', '/cms/module');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['cms', 'module']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.cms.edit')) {
            $tplMain->set('item', $form);
            $this->tplLayout->set('column1', $tplMain->render('editor'));
        } else {
            $tplMain->set('item', $module);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function component()
    {
        // view
        $this->setTpl();

        // layout
        $this->tplLayout->set('column1', Template::loading());

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'cmsComponent');
        return $this->tplMaster->render('tpl-default');
    }

    public function componentEdit()
    {
        // check
        $id = Converter::intKey('id');

        $component = ComponentRepository::loadById($id, true);
        $form = new ComponentForm($component);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.cms.edit', true, '/cms/component');
            if ($form->setFromPost()->validate()->isValid()) {
                $component->save();
                Session::adminSuccess('saved', '/cms/component');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['cms', 'component']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.cms.edit', false)) {
            $tplMain->set('item', $form);
            $this->tplLayout->set('column1', $tplMain->render('editor'));
        } else {
            $tplMain->set('item', $component);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'cmsComponentEdit');
        return $this->tplMaster->render('tpl-default');
    }
}
