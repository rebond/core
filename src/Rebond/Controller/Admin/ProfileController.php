<?php
namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Forms\Cms\UserSettingsForm;
use Rebond\Forms\Core\FeedbackForm;
use Rebond\Forms\Core\UserForm;
use Rebond\Models\Cms\UserSettings;
use Rebond\Models\Core\Feedback;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\Core\UserSecurityService;
use Rebond\Services\Core\UserService;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Session;
use Rebond\Services\Template;

class ProfileController extends BaseAdminController
{
    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('profile'));
    }

    public function signIn()
    {
        // action
        $form = new UserForm($this->signedUser);
        $form->signIn();

        if ($form->getModel()->getIsAdmin()) {
            Session::redirect('/');
        }

        if (Auth::isAuth($form->getModel())) {
            Session::setAndRedirect('siteError', Lang::lang('access_non_authorized'), $this->app->getConfig('site-url'));
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'user']);
        $tplMain->set('item', $form);

        // master
        $this->tplMaster->set('column1', $tplMain->render('sign-in'));
        $this->tplMaster->set('jsLauncher', 'profile');
        return $this->tplMaster->render('tpl-signin');
    }

    public function index()
    {
        // auth
        Auth::isAdminAuthorized($this->signedUser, null, true, '/profile/sign-in');

        $userForm = new UserForm($this->signedUser);

        if (Form::isSubmitted()) {
            if (UserService::createOrEdit($userForm, false, false)) {
                Session::allSuccess('saved', '/profile');
            };
        }

        // view
        $this->setTpl();

        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'user']);
        $tplMain->set('item', $userForm);
        $tplMain->set('profile', true);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('editor'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'profile');
        return $this->tplMaster->render('tpl-default');
    }

    public function changePassword()
    {
        // auth
        Auth::isAdminAuthorized($this->signedUser, null, true, '/profile/sign-in');

        $userForm = new UserForm($this->signedUser);

        // action
        if (Form::isSubmitted('btnPasswordReset')) {
            if ($userForm->changePassword(false, true)) {
                Session::allSuccess('password_changed', '/profile');
            }
        }

        // view
        $this->setTpl();

        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'user']);
        $tplMain->set('item', $userForm);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('editor-password'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'profile');
        return $this->tplMaster->render('tpl-default');
    }

    public function security()
    {
        // auth
        Auth::isAdminAuthorized($this->signedUser, null, true, '/profile/sign-in');

        $api = Converter::stringKey('api', 'get', '');

        $items = UserSecurityService::getAllSecureByUserId(
            $this->signedUser->getId(), $api, '/profile/security');

        // view
        $this->setTpl();

        $tplFilter = new Template(Template::MODULE_ADMIN, ['core', 'userSecurity']);
        $tplFilter->set('count', count($items));
        $tplFilter->set('url', '/profile/security?');


        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'userSecurity']);
        $tplMain->set('items', $items);

        // layout
        $this->tplLayout->set('column1', $tplFilter->render('filter'));
        $this->tplLayout->set('column2', $tplMain->render('table'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));
        $this->tplMaster->set('jsLauncher', 'userSecurity');
        return $this->tplMaster->render('tpl-default');
    }

    public function settings()
    {
        // auth
        Auth::isAdminAuthorized($this->signedUser, null, true, '/profile/sign-in');

        // check
        $userSetting = $this->signedUser->getUserSettings();
        if (!isset($userSetting)) {
            $userSetting = new UserSettings();
            $userSetting->setId($this->signedUser->getId());
        }

        $form = new UserSettingsForm($userSetting);

        // action
        if (Form::isSubmitted()) {
            if ($form->setFromPost()->validate()->isValid()) {
                $userSetting->save();
                Session::adminSuccess('saved', '/profile/settings');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        $tplMain = new Template(Template::MODULE_ADMIN, ['cms', 'userSettings']);
        $tplMain->set('item', $form);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('editor'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'profile');
        return $this->tplMaster->render('tpl-default');
    }

    public function feedback()
    {
        // auth
        Auth::isAdminAuthorized($this->signedUser, null, true, '/profile/sign-in');

        // check
        $feedback = new Feedback();
        $feedback->setStatus(\Rebond\Enums\Core\Status::INACTIVE);
        $feedback->setUserId($this->signedUser->getId());
        $feedback->setTitle($this->signedUser->getFullName() . '-' . date('Y-m-d'));

        $form = new FeedbackForm($feedback);

        // action
        if (Form::isSubmitted()) {
            if ($form->setFromPost()->validate()->isValid()) {
                $feedback->save();
                Session::adminSuccess('feedback_sent', '/profile/feedback');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'feedback']);
        $tplMain->set('item', $form);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('editor'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'profile');
        return $this->tplMaster->render('tpl-default');
    }

    public function help()
    {
        // auth
        Auth::isAdminAuthorized($this->signedUser, null, true, '/profile/sign-in');

        // view
        $this->setTpl();

        $tplMain = new Template(Template::ADMIN, ['admin', 'profile']);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('help'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'profileHelp');
        return $this->tplMaster->render('tpl-default');
    }

    public function signOut()
    {
        UserService::signOut($this->signedUser);
    }
}
