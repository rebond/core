<?php
namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Ratio;
use Rebond\Forms\Core\FolderForm;
use Rebond\Forms\Core\MediaForm;
use Rebond\Repository\Core\FolderRepository;
use Rebond\Repository\Core\MediaRepository;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\Core\MediaLinkService;
use Rebond\Models\DateTime;
use Rebond\Services\File;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Log;
use Rebond\Services\Media;
use Rebond\Services\Session;
use Rebond\Services\Template;

class MediaController extends BaseAdminController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        Auth::isAdminAuthorized($this->signedUser, 'admin.media', true, '/');
    }

    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('media'));
    }

    // Listing of medium
    public function index()
    {
        // view
        $this->setTpl();

        // nav
        $tplTree = new Template(Template::ADMIN, ['admin', 'media']);
        $tree = FolderRepository::buildTree();
        $tplTree->set('tree', $tree);

        // layout
        $this->tplLayout->set('column1', Template::loading());

        // master
        $this->tplMaster->set('navSide', $tplTree->render('tree'));
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'mediaView');
        $this->tplMaster->addJs('/node_modules/rebond-assets-admin/js/rebond-folder.js');
        return $this->tplMaster->render('tpl-default');
    }

    public function edit()
    {
        // action
        $id = Converter::intKey('id');

        $media = MediaRepository::loadById($id);
        if (!isset($media)) {
            Session::adminError('item_not_found', '/media', [Lang::lang('media'), $id]);
        }

        $form = new MediaForm($media);

        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.media.edit', true, '/media/edit');
            $oldUpload = $media->getUpload();
            $form->setFromPost();
            // extension removed in form
            $media->setUpload($media->getUpload() . '.' . $media->getExtension());
            if ($form->validate()->isValid()) {
                File::rename($media->getPath(), $oldUpload, $media->getUpload());
                $media->save();
                Log::log(Code::TRACK, 'media saved [' . $media->getId() . ':' . $media->getTitle() . ']', __FILE__, __LINE__);
                Session::adminSuccess('saved', '/media');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        // media
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'media']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.media.edit')) {
            $tplMain->set('item', $form);
            $tplMain->set('url', $this->app->getConfig('media-url') . $media->getUpload());
            $tplMain->set('uploadMax', ini_get('upload_max_filesize') . 'B');
            $this->tplLayout->set('column1', $tplMain->render('editor'));
        } else {
            $tplMain->set('item', $media);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        //master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'mediaEdit');
        $this->tplMaster->addJs('/node_modules/rebond-assets-admin/js/dropzone.js');
        return $this->tplMaster->render('tpl-default');
    }

    public function crop()
    {
        // auth
        Auth::isAdminAuthorized($this->signedUser, 'admin.media.edit', true, '/media');

        // action
        $id = Converter::intKey('id');
        $x = Converter::intKey('x', 'post');
        $y = Converter::intKey('y', 'post');
        $w = Converter::intKey('w', 'post');
        $h = Converter::intKey('h', 'post');

        $media = MediaRepository::loadById($id);
        if (!isset($media)) {
            Session::adminError('item_not_found', '/media', [Lang::lang('media'), $id]);
        }

        // action
        if (Form::isSubmitted()) {
            $file = Media::showFromModel($media, 920, 900, Ratio::KEEP_MIN);
            $filename = File::getFilename($file);
            $upload = Media::crop($this->app->getConfig('media'), $media->getPath(), $filename, $x, $y, $w, $h);

            // add Media
            // copy media by setting MediaId to 0
            $fullPathFile = $this->app->getConfig('media') . $media->getPath() . $upload;
            $media->setId(0);
            $media->setOriginalFilename($upload);
            $media->setWidth($w);
            $media->setHeight($h);
            $media->setFileSize(filesize($fullPathFile));
            $media->setUpload($upload);
            $media->setCreatedDate('now');

            $media->save();
            Session::adminSuccess('saved', '/media');
        }

        // view
        $this->setTpl();

        // media
        $tplMain = new Template(Template::ADMIN, ['admin', 'media']);
        $tplMain->set('model', $media);

        // layout
        $this->tplLayout->add('column1', $tplMain->render('crop'));

        //master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'mediaCrop');
        $this->tplMaster->addCss('/node_modules/cropper/dist/cropper.css');
        $this->tplMaster->addJs('/node_modules/cropper/dist/cropper.js');
        return $this->tplMaster->render('tpl-default');
    }

    // Upload multiple media
    public function upload()
    {
        // auth
        Auth::isAdminAuthorized($this->signedUser, 'admin.media.upload', true, '/media');

        // view
        $this->setTpl();

        $folderId = Converter::intKey('id', 'get', 1);

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'media']);
        $tplMain->set('folderId', $folderId);
        $tplMain->set('uploadMax', ini_get('upload_max_filesize') . 'B');

        // layout
        $this->tplLayout->set('column1', $tplMain->render('upload'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'mediaUpload');
        $this->tplMaster->addJs('/node_modules/rebond-assets-admin/js/dropzone.js');
        return $this->tplMaster->render('tpl-default');
    }

    public function folderEdit()
    {
        // check
        $id = Converter::intKey('id');
        $pid = Converter::intKey('pid');

        $folder = FolderRepository::loadById($id, true);

        if ($pid != 0) {
            $folder->setParentId($pid);
        }

        $form = new FolderForm($folder);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.media.edit', true, '/media');
            if ($form->setFromPost()->validate()->isValid()) {
                $folder->save();
                Log::log(Code::TRACK, 'folder saved [' . $folder->getId() . ':' . $folder->getTitle() . ']', __FILE__, __LINE__);
                Session::adminSuccess('saved', '/media');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'folder']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.media.edit')) {
            $tplMain->set('item', $form);
            $this->tplLayout->set('column1', $tplMain->render('editor'));
        } else {
            $tplMain->set('item', $folder);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function textEditor()
    {
        // check
        $info = null;
        $generate = Converter::boolKey('generate');
        $imageListPath = $this->app->getConfig('admin') . 'js/image-list.js';

        // action
        if ($generate) {
            $url = $this->app->getConfig('media-url');
            $list = '[';
            $options = [];
            $options['clearSelect'] = true;
            $options['select'][] = MediaRepository::getList(['id', 'title', 'path', 'upload']);
            $options['where'][] = 'media.status = 1';
            $options['where'][] = 'media.extension IN ("png", "gif", "jpg", "jpeg")';
            $options['order'][] = 'media.title';
            $medium = MediaRepository::loadAll($options);
            foreach ($medium as $media) {
                $list .= '{title:"' . $media->getTitle() . '", value:"' . $url . $media->getPath() . $media->getUpload() . '"},' . chr(10);
            }
            if (count($medium) > 0) {
                $list = substr($list, 0, -2);
            }
            $list .= ']';
            File::save($imageListPath, $list);
            Session::adminSuccess('photo_generated', '/media/text-editor', [count($medium)]);
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'media']);
        $tplMain->set('info', $info);

        if (file_exists($imageListPath)) {
            $tplMain->set('date', DateTime::createFromTimestamp(filemtime($imageListPath)));
            $tplMain->set('link', '/js/image-list.js');
        }

        // layout
        $this->tplLayout->set('column1', $tplMain->render('text-editor'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function inUse()
    {
        $mediaId = Converter::intKey('id');
        $media = MediaRepository::loadById($mediaId);

        if (!isset($media)) {
            return $this->index();
        }

        // find all entities which use the medium
        $mediaLinks = MediaLinkService::getLinkedMedia($mediaId);

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'media']);
        $tplMain->set('media', $media);

        $layout = 'in-use-empty';

        if (count($mediaLinks['apps']) + count($mediaLinks['groupedItems']) > 0) {
            $layout = 'in-use';
            $tplMain->set('apps', $mediaLinks['apps']);
            $tplMain->set('groupedItems', $mediaLinks['groupedItems']);
        }

        // layout
        $this->tplLayout->set('column1', $tplMain->render($layout));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }
}
