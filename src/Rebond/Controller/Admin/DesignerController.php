<?php
namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Models\DateTime;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\File;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Session;
use Rebond\Services\Template;
use Rebond\Services\Validate;

class DesignerController extends BaseAdminController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        Auth::isAdminAuthorized($this->signedUser, 'admin.designer', true, '/');
    }

    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('designer'));
    }

    public function index()
    {
        // view
        $this->setTpl();

        // main
        $tplDefault = new Template(Template::ADMIN, ['admin', 'designer']);

        // layout
        $this->tplLayout->set('column1', $tplDefault->render('index'));
        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function css()
    {
        $filePath = $this->app->getConfig('site') . '/css/skin/';

        $skins = File::getFolders($filePath);

        $files = [];
        foreach ($skins as $skin) {
            $files[$skin] = [];
            $cssFiles = File::getFiles($filePath . $skin);
            foreach ($cssFiles as $css) {
                if (File::getExtension($css) != 'css') {
                    continue;
                }
                $files[$skin][] = $css;
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'designer']);
        $tplMain->set('files', $files);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('css-listing'));
        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function cssEdit()
    {
        // check
        $skin = Converter::stringKey('skin');
        $file = Converter::stringKey('file');

        $fileContent = Converter::stringKey('fileContent', 'post');
        $filePath = $this->app->getConfig('site') . 'css/skin/' . $skin . '/' . $file;

        if (empty($skin) || empty($file)) {
            Session::redirect('/designer/css');
        }

        if (!file_exists($filePath)) {
            Session::adminError('error_file_not_found', '/designer/css', [$filePath]);
        }

        // action
        if (Form::isSubmitted()) {
            if (File::getExtension($file) != 'css') {
                Session::adminError('error_file_invalid', '/designer/css', [$file]);
            }
            if (!copy($filePath, File::getNoExtension($filePath) . '-' . (new DateTime())->format('string') . '.bak')) {
                Session::adminError('error_item_not_saved', '/designer/css', [$file]);
            }
            file_put_contents($filePath, $fileContent);
            Session::adminSuccess('saved', '/designer/css');
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'designer']);
        $tplMain->set('skin', $skin);
        $tplMain->set('file', $file);
        $tplMain->set('filePath', $filePath);
        $tplMain->set('editable', File::getExtension($file) == 'css');

        // layout
        $this->tplLayout->set('column1', $tplMain->render('css-form'));
        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'designerCss');
        $this->tplMaster->addCss('/node_modules/codemirror/lib/codemirror.css');
        $this->tplMaster->addJs('/node_modules/codemirror/lib/codemirror.js');
        $this->tplMaster->addJs('/node_modules/codemirror/mode/css/css.js');
        return $this->tplMaster->render('tpl-default');
    }

    public function tpl()
    {
        // template folders
        $mainTemplates = File::getViews(false);

        // template modules
        $appTemplates = File::getViews(true);

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'designer']);
        $tplMain->set('mainTemplates', $mainTemplates);
        $tplMain->set('appTemplates', $appTemplates);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('template-listing'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function tplEdit()
    {
        // check
        $folder = Converter::stringKey('folder');
        $module = Converter::stringKey('module');
        $template = Converter::stringKey('template');
        $save = Converter::stringKey('save', 'post');

        if (!isset($template)) {
            Session::adminError('error_file_not_found', '/designer/tpl', [$template]);
        }

        $validator = Validate::validate('folder', $folder, ['alphaNumSafe' => true]);
        if (!$validator->isValid()) {
            Session::setAndRedirect('adminError', $validator->getMessage(), '/designer/tpl');
        }

        $validator = Validate::validate('file', $template, ['alphaNumSafe' => true]);
        if (!$validator->isValid()) {
            Session::setAndRedirect('adminError', $validator->getMessage(), '/designer/tpl');
        }

        $templatePath = $this->app->getPath('own-view')
            . $folder . '/' . (isset($module) ? $module . '/' : '') . $template;

        if (!file_exists($templatePath)) {
            Session::adminError('error_file_not_found', '/designer/tpl', [$templatePath]);
        }

        // action
        if (isset($save)) {
            $templatePost = Converter::stringKey('template', 'post');
            $fileContent = Converter::stringKey('fileContent', 'post');
            $folderPost = Converter::stringKey('folder', 'post');
            $modulePost = Converter::stringKey('module', 'post');

            $filePathPost = $this->app->getPath('own-view')
                . $folderPost . '/' . (isset($modulePost) ? $modulePost . '/' : '') . $templatePost;

            if (!file_exists($filePathPost) || File::getExtension($templatePost) != 'tpl') {
                Session::adminError('error_file_invalid', '/designer/tpl', [$filePathPost]);
            }
            if (!copy($filePathPost, File::getNoExtension($filePathPost) . '-' . (new DateTime())->format('string') . '.bak')) {
                Session::adminError('error_item_not_saved', '/designer/tpl', [$templatePost]);
            }
            file_put_contents($filePathPost, $fileContent);
            Session::adminSuccess('saved', '/designer/tpl');
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'designer']);
        $tplMain->set('template', $template);
        $tplMain->set('folder', $folder);
        $tplMain->set('module', $module);
        $tplMain->set('templatePath', $templatePath);
        $tplMain->set('editable', File::getExtension($template) == 'tpl');

        // layout
        $this->tplLayout->set('column1', $tplMain->render('template-form'));
        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'designerTpl');

        $this->tplMaster->addCss('/node_modules/codemirror/lib/codemirror.css');
        $this->tplMaster->addJs('/node_modules/codemirror/lib/codemirror.js');
        $this->tplMaster->addJs('/node_modules/codemirror/mode/htmlmixed/htmlmixed.js');
        $this->tplMaster->addJs('/node_modules/codemirror/mode/xml/xml.js');
        $this->tplMaster->addJs('/node_modules/codemirror/mode/clike/clike.js');
        $this->tplMaster->addJs('/node_modules/codemirror/mode/php/php.js');
        return $this->tplMaster->render('tpl-default');
    }
}
