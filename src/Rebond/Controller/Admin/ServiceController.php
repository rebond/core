<?php

namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Enums\Cms\Component;
use Rebond\Enums\Cms\Version;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Ratio;
use Rebond\Enums\Core\Result;
use Rebond\Enums\Core\Status;

use Rebond\Models\Cms\Page;
use Rebond\Models\Cms\UserSettings;
use Rebond\Models\FilterInfo;
use Rebond\Models\Search;
use Rebond\Repository\Cms\ComponentRepository;
use Rebond\Repository\Cms\ContentRepository;
use Rebond\Repository\Cms\FilterRepository;
use Rebond\Repository\Cms\GadgetRepository;
use Rebond\Repository\Cms\ModuleRepository;
use Rebond\Repository\Cms\PageRepository;
use Rebond\Repository\Core\FolderRepository;
use Rebond\Repository\Core\LogRepository;
use Rebond\Repository\Core\MediaRepository;
use Rebond\Repository\Core\RolePermissionRepository;
use Rebond\Repository\Core\UserRepository;
use Rebond\Repository\Core\UserRoleRepository;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\Core\FolderService;
use Rebond\Services\Core\MediaLinkService;
use Rebond\Services\Core\UserService;
use Rebond\Services\File;
use Rebond\Services\Lang;
use Rebond\Services\Log;
use Rebond\Services\Media;
use Rebond\Services\Nav;
use Rebond\Services\Template;

class ServiceController extends BaseAdminController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->app->setAjax(true);
    }

    public function deleteBackup()
    {
        if (!$this->hasPrivilege('admin.dev')) {
            return $this->noPrivilege('admin.dev');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // file selected
        $file = Converter::stringKey('file', 'post');

        $backupPath = $this->app->getPath('own-backup-file');

        if (isset($file)) {
            $filePath = $this->app->getPath('own-backup-file') . $file;
            if (!file_exists($filePath)) {
                $json['message'] = Lang::lang('backup_not_found');
                return json_encode($json);
            }
            $files = File::getFiles($backupPath);
            if (count($files) < 2) {
                $json['message'] = Lang::lang('backup_one_mininum');
                return json_encode($json);
            }
            if (stripos($filePath, 'launch') !== false) {
                $json['message'] = Lang::lang('backup_launch_not_deletable');
                return json_encode($json);
            }
            unlink($filePath);
            $json['result'] = Result::SUCCESS;
            $json['message'] = Lang::lang('deleted');
            return json_encode($json);
        }

        // all files in backup folder
        $files = File::getFiles($backupPath);
        foreach ($files as $file) {
            unlink($this->app->getPath('own-cache-file') . $file);
        }

        $json['result'] = Result::SUCCESS;
        $json['message'] = Lang::lang('backups_removed');
        return json_encode($json);
    }

    public function deleteBin()
    {
        if (!$this->hasPrivilege('admin.dev')) {
            return $this->noPrivilege('admin.dev');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // file selected
        $module = Converter::stringKey('module', 'post');
        $folder = Converter::stringKey('folder', 'post');
        $skin = Converter::stringKey('skin', 'post');
        $contentId = Converter::intKey('contentId', 'post');
        $file = Converter::stringKey('file', 'post');

        // module content & template
        if (isset($module, $contentId)) {
            $appData = '\Own\Repository\App\\' . $module . 'Repository';
            $item = $appData::loadById($contentId);
            if (!isset($item)) {
                $json['message'] = Lang::lang('item_not_found', [$module, $contentId]);
                return json_encode($json);
            }

            $item->delete();
            $json['result'] = Result::SUCCESS;
            $json['message'] = Lang::lang('deleted');
            return json_encode($json);
        }

        if (isset($file)) {
            $toDelete = null;
            if (isset($skin)) {
                $toDelete = $this->app->getConfig('site') . '/css/skin/' . $skin . '/';
            } else if (isset($module)) {
                $toDelete = $this->app->getPath('own-view') . 'App/' . $module . '/';
            } else if (isset($folder)) {
                $toDelete = $this->app->getPath('own-view') . $folder . '/';
            }
            if (isset($toDelete)) {
                if (!file_exists($toDelete . $file)) {
                    $json['message'] = Lang::lang('error_file_not_found', [$file]);
                    return json_encode($json);
                }
                if (!File::delete($toDelete, $file)) {
                    $json['message'] = Lang::lang('error_file_not_deleted', [$file]);
                    return json_encode($json);
                }
                $json['result'] = Result::SUCCESS;
                $json['message'] = Lang::lang('deleted');
                return json_encode($json);
            }
        }

        $json['message'] = Lang::lang('error_parameters_invalid');
        return json_encode($json);
    }

    public function deleteCache()
    {
        if (!$this->hasPrivilege('admin.config.edit')) {
            return $this->noPrivilege('admin.config.edit');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // file selected
        $file = Converter::stringKey('file', 'post');
        $cachePath = $this->app->getPath('own-cache-file');

        if (isset($file)) {
            $filePath = $cachePath . $file;
            if (!file_exists($filePath)) {
                $json['message'] = Lang::lang('error_file_not_found', [$file]);
                return json_encode($json);
            }
            unlink($filePath);
            $json['result'] = Result::SUCCESS;
            $json['message'] = Lang::lang('cache_deleted');
            return json_encode($json);
        }

        // all files in cache folder
        $files = File::getFiles($cachePath);
        foreach ($files as $file) {
            unlink($cachePath . $file);
        }

        $json['result'] = Result::SUCCESS;
        $json['message'] = Lang::lang('cache_all_deleted');
        return json_encode($json);
    }

    public function deleteFolder()
    {
        if (!$this->hasPrivilege('admin.media.edit')) {
            return $this->noPrivilege('admin.media.edit');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $id = Converter::intKey('id', 'post');

        if (!isset($id)) {
            $json['message'] = Lang::lang('error_parameters_invalid');
            return json_encode($json);
        }

        if ($id == 1) {
            $json['message'] = Lang::lang('folder_cannot_delete_root');
            return json_encode($json);
        }

        // check item
        $folder = FolderRepository::loadById($id);

        if (!isset($folder)) {
            $json['message'] = Lang::lang('item_not_found', [Lang::lang('folder'), $id]);
            return json_encode($json);
        }

        // find all children folders
        $folderChildren = FolderRepository::getChildren($id);
        if ($folderChildren) {
            foreach ($folderChildren as $folderChild) {
                FolderRepository::deleteById($folderChild);
                MediaRepository::moveToRoot($folderChild);
            }
        }
        // remove folders
        FolderRepository::deleteById($id);
        MediaRepository::moveToRoot($id);

        Log::log(Code::TRACK, 'folder deleted [' . $folder->getId() . ':' . $folder->getTitle() . ']', __FILE__,
            __LINE__);

        $json['result'] = Result::SUCCESS;
        $json['message'] = Lang::lang('folder_children_deleted');
        return json_encode($json);
    }

    public function deleteMedia()
    {
        if (!$this->hasPrivilege('admin.media.edit')) {
            return $this->noPrivilege('admin.media.edit');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $mediaId = Converter::intKey('id', 'post', 0);

        // get media
        $media = MediaRepository::loadById($mediaId);
        if (!isset($media)) {
            $json['message'] = Lang::lang('item_not_found', [Lang::lang('media'), $mediaId]);
            return json_encode($json);
        }

        // find all entities which use the medium
        $count = MediaLinkService::getLinkedMedia($mediaId, true);

        if ($count > 0) {
            $json['message'] = Lang::lang('error_media_in_use') . ' <a href="/media/in-use?id=' . $mediaId . '">' . $count . ' ' . Lang::lang('items') . '</a>';
            return json_encode($json);
        }

        $result = MediaRepository::deleteById($media->getId());
        if ($result == Result::ERROR) {
            $json['message'] = Lang::lang('error_item_not_deleted', [$media->getTitle()]);
            return json_encode($json);
        }

        File::deleteAllMedia($media->getPath(), $media->getUpload(), true);

        Log::log(Code::TRACK, 'media deleted [' . $media->getId() . ':' . $media->getTitle() . ']', __FILE__, __LINE__);

        $json['result'] = Result::SUCCESS;
        $json['message'] = Lang::lang('deleted');
        return json_encode($json);
    }

    public function deletePage()
    {
        if (!$this->hasPrivilege('admin.page.edit')) {
            return $this->noPrivilege('admin.page.edit');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $id = Converter::intKey('id', 'post', 0);

        if ($id < Page::FIRST_ID_OF_EDITABLE) {
            $json['message'] = Lang::lang('page_protected');
            return json_encode($json);
        }

        // Check item
        $page = PageRepository::loadById($id);

        if (!isset($page)) {
            $json['message'] = Lang::lang('error_page_invalid', [$id]);
            return json_encode($json);
        }

        // Find all children pages
        $pageChildren = PageRepository::getPagesIdToDelete($id);
        if ($pageChildren) {
            foreach ($pageChildren as $pageChild) {
                PageRepository::deleteById($pageChild);
                GadgetRepository::deleteByPageId($pageChild);
            }
        }

        Log::log(Code::TRACK, 'page deleted [' . $page->getId() . ':' . $page->getTitle() . ']', __FILE__, __LINE__);

        $json['result'] = Result::SUCCESS;
        $json['message'] = Lang::lang('page_children_deleted');
        return json_encode($json);
    }

    public function getMedia()
    {
        if (!$this->hasPrivilege('admin.media')) {
            return $this->noPrivilege('admin.media');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $id = Converter::intKey('id', 'post', 0);

        $mediaModel = MediaRepository::loadById($id);
        if (!isset($mediaModel)) {
            $json['message'] = Lang::lang('error_media_invalid');
            return json_encode($json);
        }

        $json['result'] = Result::SUCCESS;
        $json['html'] = Media::showFromModel($mediaModel, 512, 512, Ratio::KEEP_MAX);
        return json_encode($json);
    }

    public function imageList()
    {
        if (!$this->hasPrivilege('admin.content.edit')) {
            return $this->noPrivilege('admin.content.edit');
        }

        $mediaList = $this->app->getConfig('admin') . 'js/image-list.js';
        if (file_exists($mediaList)) {
            return file_get_contents($mediaList);
        }
        return [];
    }

    public function listComponent()
    {
        if (!$this->hasPrivilege('admin.cms')) {
            return $this->noPrivilege('admin.cms');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $hashParam = Converter::stringKey('hash', 'post', 'no');

        $hash = explode('/', $hashParam);
        $status = (isset($hash[0]) && $hash[0] != '') ? $hash[0] : 'active';
        $page = (isset($hash[1]) && $hash[1] != '') ? (int)$hash[1] : 1;

        $options = [];
        if ($status == 'active') {
            $options['where'][] = 'component.status IN (0,1)';
        } else {
            $options['where'][] = 'component.status = 2';
        }
        $componentCount = ComponentRepository::count($options);

        // get user settings
        $userSettings = $this->signedUser->getUserSettings();
        $maxByPage = isset($userSettings) ? (int)$userSettings->getPagingValue() : 10;

        $page = Nav::getCurrentPage($page, $componentCount, $maxByPage);

        // add select, join, order and limit
        $options['select'][] = ModuleRepository::getList([], 'component_module');
        $options['join'][] = 'cms_module component_module ON component_module.id = component.module_id';
        $options['order'][] = 'component_module.title, component.title';
        $options['limit'][] = (($page - 1) * $maxByPage) . ', ' . $maxByPage;

        $components = ComponentRepository::loadAll($options);

        $tpl = new Template(Template::MODULE_ADMIN, ['cms', 'component']);
        $tpl->set('items', $components);

        $tplFilter = new Template(Template::MODULE_ADMIN, ['cms', 'component']);
        $tplFilter->set('paging', Nav::renderPaging($componentCount, '#' . $status . '/', $maxByPage, $page));
        $tplFilter->set('itemsPerPage', Nav::renderItemsPerPage($maxByPage));
        $tplFilter->set('status', $status);

        $tplLayout = new Template(Template::ADMIN, ['admin']);
        $tplLayout->set('column1', $tplFilter->render('filter'));
        $tplLayout->set('column2', $tpl->render('table'));

        $json['result'] = Result::SUCCESS;
        $json['message'] = Lang::lang('list_updated') . ' (' . count($components) . ')';
        $json['html'] = $tplLayout->render('layout-2-row');
        return json_encode($json);
    }

    public function listContent()
    {
        if (!$this->hasPrivilege('admin.content')) {
            return $this->noPrivilege('admin.content');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $hashParam = Converter::stringKey('hash', 'post');

        $hash = explode('/', $hashParam);
        $moduleName = (isset($hash[0]) && $hash[0] != '') ? ucfirst($hash[0]) : '';
        $version = (isset($hash[1]) && $hash[1] != '') ? $hash[1] : 'published';
        $page = (isset($hash[2]) && $hash[2] != '') ? (int)$hash[2] : 1;
        $order = (isset($hash[3]) && $hash[3] != '') ? $hash[3] : 'published_date-desc';

        if ($moduleName == '') {
            $tplMain = new Template(Template::ADMIN, ['admin', 'content']);
            $json['result'] = Result::NO_CHANGE;
            $json['html'] = $tplMain->render('index');
            return json_encode($json);
        }

        $module = ModuleRepository::loadByName($moduleName);
        if (!isset($module)) {
            $tplMain = new Template(Template::ADMIN, ['admin', 'content']);

            $json['result'] = Result::NO_CHANGE;
            $json['html'] = $tplMain->render('index');
            return json_encode($json);
        }

        if (!(in_array($order, [
            'title-asc',
            'published_date-asc',
            'created_date-asc',
            'title-desc',
            'published_date-desc',
            'created_date-desc'
        ]))) {
            $order = 'published_date-desc';
        }

        // check
        $appData = '\Own\Repository\App\\' . $moduleName . 'Repository';

        // get user settings
        $userSettings = $this->signedUser->getUserSettings();
        $maxByPage = isset($userSettings) ? (int)$userSettings->getContentPagingValue() : 10;

        $countItems = $appData::countByVersion($version, false);

        $page = Nav::getCurrentPage($page, $countItems, $maxByPage);

        $options = [];
        $options['limit'][] = (($page - 1) * $maxByPage) . ', ' . $maxByPage;
        $options['order'][] = 'content.' . str_replace('-', ' ', $order);

        $genericModels = $appData::loadByVersion($version, false, $options);

        // view

        // filter
        $tplFilter = new Template(Template::ADMIN, ['admin', 'content']);
        $tplFilter->set('paging',
            Nav::renderPaging($countItems, '#' . $moduleName . '/' . $version . '/', $maxByPage, $page, '/' . $order));
        $tplFilter->set('itemsPerPage', Nav::renderItemsPerPage($maxByPage));
        $tplFilter->set('module', $moduleName);
        $tplFilter->set('version', $version);
        $tplFilter->set('order', $order);

        // content
        $tpl = new Template(Template::MODULE_ADMIN, ['app', Converter::toSnakeCase($moduleName)]);
        $tpl->set('items', $genericModels);
        $tpl->set('version', $version);
        $tpl->set('filter', $module->getHasFilter());
        $tpl->set('workflow', $module->getWorkflow());

        $json['result'] = Result::SUCCESS;
        $json['tabon'] = $version;
        $json['module'] = $moduleName;
        $json['message'] = Lang::lang('list_updated') . ' (' . count($genericModels) . ')';
        $json['html'] = $tplFilter->render('filter') . $tpl->render('admin-app-table');
        return json_encode($json);
    }

    public function listMedia()
    {
        if (!$this->hasPrivilege('admin.media')) {
            return $this->noPrivilege('admin.media');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $hashParam = Converter::stringKey('hash', 'post');

        $hash = explode('/', $hashParam);
        $folderId = (isset($hash[0]) && $hash[0] != '') ? (int)$hash[0] : 1;
        $page = (isset($hash[1]) && $hash[1] != '') ? (int)$hash[1] : 1;
        $type = (isset($hash[2]) && $hash[2] != '') ? $hash[2] : 'all';
        $order = (isset($hash[3]) && $hash[3] != '') ? $hash[3] : 'modified_date-desc';
        $selectable = (isset($hash[4]) && $hash[4] == 0) ? 0 : 1;


        if (!(in_array($type, ['all', 'images', 'videos', 'documents']))) {
            $type = 'all';
        }
        if (!(in_array($order, [
            'title-asc',
            'extension-asc',
            'modified_date-asc',
            'title-desc',
            'extension-desc',
            'modified_date-desc'
        ]))) {
            $order = 'modified_date-desc';
        }

        $options = [];
        $options['where'][] = 'media.status = 1';
        $options['where'][] = ['media.is_selectable = ?', $selectable];
        if ($folderId != 0) {
            $options['where'][] = ['media.folder_id = ?', $folderId];
        }
        if ($type != 'all') {
            switch ($type) {
                case 'images':
                    $options['where'][] = 'media.extension IN ("jpg", "jpeg", "png")';
                    break;
                case 'videos':
                    $options['where'][] = 'media.extension IN ("avi", "mov")';
                    break;
                case 'documents':
                    $options['where'][] = 'media.extension IN ("pdf", "xls", "xlsx", "doc", "docx")';
                    break;
            }
        }
        $mediaCount = MediaRepository::count($options);

        // get user settings
        $userSettings = $this->signedUser->getUserSettings();
        $mediaView = 'grid';
        $maxByPage = 10;
        if (isset($userSettings)) {
            $mediaView = $userSettings->getMediaViewValue();
            $maxByPage = (int)$userSettings->getMediaPagingValue();
        }

        $page = Nav::getCurrentPage($page, $mediaCount, $maxByPage);

        // add paging and order
        $options['order'][] = 'media.' . str_replace('-', ' ', $order);
        $options['limit'][] = (($page - 1) * $maxByPage) . ', ' . $maxByPage;

        $media = MediaRepository::loadAll($options);

        $tpl = new Template(Template::MODULE_ADMIN, ['core', 'media']);
        $tpl->set('items', $media);

        $tplFilter = new Template(Template::MODULE_ADMIN, ['core', 'media']);
        $urlExtension = '/' . $type . '/' . $order . '/' . $selectable;
        $tplFilter->set('paging',
            Nav::renderPaging($mediaCount, '#' . $folderId . '/', $maxByPage, $page, $urlExtension));
        $tplFilter->set('itemsPerPage', Nav::renderItemsPerPage($maxByPage));
        $tplFilter->set('mediaView', $mediaView);
        $tplFilter->set('folderId', $folderId);
        $tplFilter->set('order', $order);
        $tplFilter->set('type', $type);
        $tplFilter->set('selectable', $selectable);

        $tplLayout = new Template(Template::ADMIN, ['admin']);
        $tplLayout->set('column1', $tplFilter->render('filter-full'));
        $tplLayout->set('column2', $tpl->render($mediaView));

        $json['result'] = Result::SUCCESS;
        $json['message'] = Lang::lang('list_updated') . ' (' . count($media) . ')';
        $json['html'] = $tplLayout->render('layout-2-row');
        return json_encode($json);
    }

    public function listQuickView()
    {
        if (!$this->hasPrivilege('admin.quickview')) {
            return $this->noPrivilege('admin.quickview');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $hashParam = Converter::stringKey('hash', 'post');

        $hash = explode('/', $hashParam);
        $moduleName = isset($hash[0]) ? $hash[0] : '';
        $page = isset($hash[1]) ? (int)$hash[1] : 1;

        if ($moduleName == '') {
            $tplLayout = new Template(Template::ADMIN, ['admin']);
            $tplMain = new Template(Template::ADMIN, ['admin', 'quickview']);
            $tplLayout->set('column1', $tplMain->render('index'));
            $json['result'] = Result::NO_CHANGE;
            $json['html'] = $tplLayout->render('layout-1-col');
            return json_encode($json);
        }

        // check
        $busData = '\Own\Repository\Bus\\' . $moduleName . 'Repository';

        // method check
        if (!method_exists($busData, 'count')) {
            $tplLayout = new Template(Template::ADMIN, ['admin']);
            $tplMain = new Template(Template::ADMIN, ['admin', 'quickview']);
            $tplLayout->set('column1', $tplMain->render('quickview-non-persistent'));
            $json['result'] = Result::NO_CHANGE;
            $json['html'] = $tplLayout->render('layout-1-col');
            return json_encode($json);
        }

        // get user settings
        $userSettings = $this->signedUser->getUserSettings();
        $maxByPage = isset($userSettings) ? (int)$userSettings->getPagingValue() : 10;

        $countItems = $busData::count();

        $page = Nav::getCurrentPage($page, $countItems, $maxByPage);

        $options = [];
        $options['limit'][] = (($page - 1) * $maxByPage) . ', ' . $maxByPage;

        $genericModels = $busData::loadAll($options);

        // view

        // filter
        $tplFilter = new Template(Template::ADMIN, ['admin', 'quickview']);
        $tplFilter->set('paging', Nav::renderPaging($countItems, '#' . $moduleName . '/', $maxByPage, $page));
        $tplFilter->set('itemsPerPage', Nav::renderItemsPerPage($maxByPage));
        $tplFilter->set('entity', $moduleName);

        // content
        $tpl = new Template(Template::MODULE_ADMIN, ['bus', $moduleName]);
        $tpl->set('items', $genericModels);

        $tplLayout = new Template(Template::ADMIN, ['admin']);
        $tplLayout->set('column1', $tplFilter->render('filter'));
        $tplLayout->set('column2', $tpl->render('admin-bus-table'));

        $json['result'] = Result::SUCCESS;
        $json['module'] = $moduleName;
        $json['message'] = Lang::lang('list_updated') . ' (' . count($genericModels) . ')';
        $json['html'] = $tplLayout->render('layout-2-row');
        return json_encode($json);
    }

    public function listUser()
    {
        if (!$this->hasPrivilege('admin.user')) {
            return $this->noPrivilege('admin.user');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $hashParam = Converter::stringKey('hash', 'post');

        $hash = explode('/', $hashParam);
        $status = (isset($hash[0]) && $hash[0] != '') ? $hash[0] : 'active';
        $page = (isset($hash[1]) && $hash[1] != '') ? (int)$hash[1] : 1;
        $order = (isset($hash[2]) && $hash[2] != '') ? $hash[2] : 'email-asc';

        $options = [];
        if ($status == 'active') {
            $options['where'][] = 'user.status IN (0,1)';
        } else {
            $options['where'][] = 'user.status = 2';
        }
        $userCount = UserRepository::count($options);

        if (!(in_array($order, [
            'email-asc',
            'first_name-asc',
            'last_name-asc',
            'created_date-asc',
            'email-desc',
            'first_name-desc',
            'last_name-desc',
            'created_date-desc'
        ]))) {
            $order = 'email-asc';
        }

        // get user settings
        $userSettings = $this->signedUser->getUserSettings();
        $maxByPage = isset($userSettings) ? (int)$userSettings->getPagingValue() : 10;

        $page = Nav::getCurrentPage($page, $userCount, $maxByPage);

        // add paging and order
        $options['limit'][] = (($page - 1) * $maxByPage) . ', ' . $maxByPage;
        $options['order'][] = 'user.' . str_replace('-', ' ', $order);

        $users = UserRepository::loadAll($options);

        $tpl = new Template(Template::MODULE_ADMIN, ['core', 'user']);
        $tpl->set('items', $users);

        $tplFilter = new Template(Template::MODULE_ADMIN, ['core', 'user']);
        $tplFilter->set('paging', Nav::renderPaging($userCount, '#' . $status . '/', $maxByPage, $page));
        $tplFilter->set('itemsPerPage', Nav::renderItemsPerPage($maxByPage));
        $tplFilter->set('status', $status);
        $tplFilter->set('order', $order);

        $tplLayout = new Template(Template::ADMIN, ['admin']);
        $tplLayout->set('column1', $tplFilter->render('filter-full'));
        $tplLayout->set('column2', $tpl->render('table'));

        $json['result'] = Result::SUCCESS;
        $json['message'] = Lang::lang('list_updated') . ' (' . count($users) . ')';
        $json['html'] = $tplLayout->render('layout-2-row');
        return json_encode($json);
    }

    public function search()
    {
        if (!$this->hasPrivilege('admin')) {
            return $this->noPrivilege('admin');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $search = Converter::stringKey('search', 'post');
        if (!isset($search)) {
            $json['message'] = Lang::lang('error_parameters_invalid');
            return json_encode($json);
        }

        $searchResults = [];

        // search content
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = ModuleRepository::getList(['id', 'title']);
        $options['where'][] = 'module.status = 1';
        $options['where'][] = 'module.has_content = 1';
        $modules = ModuleRepository::loadAll($options);

        $options2 = [];
        $options2['clearSelect'] = true;
        $options2['where'][] = ['LOWER(content.title) LIKE ?', '%' . strtolower($search) . '%'];
        $options2['where'][] = ['content.version IN (?)', [0, 1, 3]];
        $options2['limit'][] = '0, 10';

        foreach ($modules as $module) {
            $appData = '\Own\Repository\App\\' . $module->getName() . 'Repository';

            $options2['select'][] = $appData::getList(['app_id']);
            $options2['select'][] = ContentRepository::getList(['title', 'version']);

            $items = $appData::loadAll($options2);
            if (isset($items) && count($items) > 0) {
                foreach ($items as $item) {
                    $result = new Search();
                    $result->setType($module->getTitle());
                    $result->setTitle($item->getTitle());
                    $result->setStatus($item->getVersion());
                    $result->setStatusValue($item->getVersionValue());
                    $result->setLink('/content/edit/?module=' . $module->getName() . '&id=' . $item->getAppId());
                    $searchResults[] = $result;
                }
            }
        }

        // Search users
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = UserRepository::getList(['id', 'email', 'first_name', 'last_name', 'status']);
        $options['where'][] = ['LOWER(user.email) LIKE ?', '%' . strtolower($search) . '%'];
        $users = UserRepository::loadAll($options);
        foreach ($users as $user) {
            $result = new Search();
            $result->setType(Lang::lang('user'));
            $result->setTitle($user->getFullName());
            $result->setStatus($user->getStatus());
            $result->setStatusValue($user->getStatusValue());
            $result->setLink('/user/edit?id=' . $user->getId());
            $searchResults[] = $result;
        }

        // search media
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = MediaRepository::getList(['id', 'title', 'status']);
        $options['where'][] = ['LOWER(media.title) LIKE ?', '%' . strtolower($search) . '%'];
        $media = MediaRepository::loadAll($options);
        foreach ($media as $medium) {
            $result = new Search();
            $result->setType(Lang::lang('media'));
            $result->setTitle($medium->getTitle());
            $result->setStatus($medium->getStatus());
            $result->setStatusValue($medium->getStatusValue());
            $result->setLink('/media/edit?id=' . $medium->getId());
            $searchResults[] = $result;
        }

        // view
        $tpl = new Template(Template::ADMIN, ['admin']);
        $tpl->set('results', $searchResults);

        $json['result'] = Result::SUCCESS;
        $json['message'] = $tpl->render('search');
        return json_encode($json);
    }

    public function selectMedia()
    {
        if (!$this->hasPrivilege('admin.content.edit')) {
            return $this->noPrivilege('admin.content.edit');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $field = Converter::stringKey('field', 'post');
        $hashParam = Converter::stringKey('hash', 'post');
        $search = Converter::stringKey('search', 'post');

        if (!isset($field, $hashParam)) {
            $json['message'] = Lang::lang('error_parameters_invalid');
            return json_encode($json);
        }

        $hash = explode('/', substr($hashParam, 1));

        $folderId = (isset($hash[0]) && $hash[0] != '') ? (int)$hash[0] : 0;
        $page = (isset($hash[1]) && $hash[1] != '') ? (int)$hash[1] : 1;
        $type = (isset($hash[2]) && $hash[2] != '') ? $hash[2] : 'all';
        $order = (isset($hash[3]) && $hash[3] != '') ? $hash[3] : 'modified_date-desc';

        if (!(in_array($type, ['all', 'images', 'videos', 'documents']))) {
            $type = 'all';
        }
        if (!(in_array($order, [
            'title-asc',
            'extension-asc',
            'modified_date-asc',
            'title-desc',
            'extension-desc',
            'modified_date-desc'
        ]))) {
            $order = 'modified_date-desc';
        }

        $options = [];
        $options['where'][] = 'media.status = 1';
        $options['where'][] = 'media.is_selectable = 1';
        if ($folderId != 0) {
            $options['where'][] = ['media.folder_id = ?', $folderId];
        }
        if (isset($search) && $search != '') {
            $options['where'][] = ['media.title LIKE ?', '%' . $search . '%'];
        }
        if ($type != 'all') {
            switch ($type) {
                case 'images':
                    $options['where'][] = 'media.extension IN ("jpg", "jpeg", "png")';
                    break;
                case 'videos':
                    $options['where'][] = 'media.extension IN ("avi", "mov")';
                    break;
                case 'documents':
                    $options['where'][] = 'media.extension IN ("pdf", "xls", "xlsx", "doc", "docx")';
                    break;
            }
        }
        $mediaCount = MediaRepository::count($options);

        // get user settings
        $userSettings = $this->signedUser->getUserSettings();
        $mediaView = 'grid';
        $maxByPage = 10;
        if (isset($userSettings)) {
            $mediaView = $userSettings->getMediaViewValue();
            $maxByPage = (int)$userSettings->getMediaPagingValue();
        }

        $page = Nav::getCurrentPage($page, $mediaCount, $maxByPage);

        $folders = FolderService::renderList();

        // add paging and order
        $options['order'][] = 'media.' . str_replace('-', ' ', $order);
        $options['limit'][] = (($page - 1) * $maxByPage) . ', ' . $maxByPage;

        $media = MediaRepository::loadAll($options);

        $tpl = new Template(Template::MODULE_ADMIN, ['core', 'media']);
        $tpl->set('items', $media);
        $tpl->set('field', $field);

        $tplFilter = new Template(Template::MODULE_ADMIN, ['core', 'media']);
        $tplFilter->set('paging',
            Nav::renderPaging($mediaCount, '#' . $folderId . '/', $maxByPage, $page, '/' . $type . '/' . $order));
        $tplFilter->set('folders', $folders);
        $tplFilter->set('itemsPerPage', Nav::renderItemsPerPage($maxByPage));
        $tplFilter->set('mediaView', $mediaView);
        $tplFilter->set('order', $order);
        $tplFilter->set('type', $type);
        $tplFilter->set('folderId', $folderId);
        $tplFilter->set('hash', '#' . $folderId . '/' . $page . '/' . $type . '/' . $order);
        $tplFilter->set('search', $search);

        $json['result'] = Result::SUCCESS;
        $json['message'] = Lang::lang('list_updated') . ' (' . count($media) . ')';
        $json['html'] = $tplFilter->render('filter-modal');
        $json['html'] .= $tpl->render($mediaView . '-modal');
        return json_encode($json);
    }

    public function selectGadgetFilter()
    {
        if (!$this->hasPrivilege('admin.page.edit')) {
            return $this->noPrivilege('admin.page.edit');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $id = Converter::intKey('id', 'post', 0);
        $search = Converter::stringKey('search', 'post');

        $gadget = GadgetRepository::loadById($id);
        if (!isset($gadget)) {
            $json['message'] = Lang::lang('item_not_found', [Lang::lang('gadget'), $id]);
            return json_encode($json);
        }

        $results = [];

        if ($gadget->getComponent()->getType() == Component::SINGLE_ELEMENT) {
            $appData = '\Own\Repository\App\\' . $gadget->getComponent()->getModule()->getName() . 'Repository';
            $options = [];
            $options['where'][] = ['content.version IN (?)', [0, 1, 3]];
            if (isset($search) && $search != '') {
                $options['where'][] = ['content.title LIKE ?', '%' . $search . '%'];
            }
            $options['order'][] = 'content.title';
            $items = $appData::loadAll($options, false);
            if (isset($items) && count($items) > 0) {
                foreach ($items as $item) {
                    $result = new FilterInfo();
                    $result->setId($item->getContentGroup());
                    $result->setTitle($item->getTitle());
                    $result->setAppId($item->getAppId());
                    $results[] = $result;
                }
            }
        } else if ($gadget->getComponent()->getType() == Component::FILTERED_CARDS) {
            $options = [];
            $options['where'][] = ['filter.module_id = ?', $gadget->getComponent()->getModuleId()];
            if ($search != '') {
                $options['where'][] = ['filter.title LIKE ?', '%' . $search . '%'];
            }
            $options['order'][] = 'filter.title';
            $filters = FilterRepository::loadAll($options);
            if (isset($filters) && count($filters) > 0) {
                foreach ($filters as $filter) {
                    $result = new FilterInfo();
                    $result->setId($filter->getId());
                    $result->setTitle($filter->getTitle());
                    $result->setAppId($filter->getId());
                    $results[] = $result;
                }
            }
        }

        $tpl = new Template(Template::MODULE_ADMIN, ['cms', 'gadget']);
        $tpl->set('id', $gadget->getId());
        $tpl->set('results', $results);
        $tpl->set('search', $search);
        $json['result'] = Result::SUCCESS;
        $json['html'] = $tpl->render('filter-modal');
        return json_encode($json);
    }

    public function updateGadget()
    {
        if (!$this->hasPrivilege('admin.page.edit')) {
            return $this->noPrivilege('admin.page.edit');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $id = Converter::intKey('id', 'post');
        $property = Converter::stringKey('property', 'post');
        $value = Converter::intKey('value', 'post');

        if (!isset($id, $property, $value)) {
            $json['message'] = Lang::lang('error_parameters_invalid');
            return json_encode($json);
        }

        // Check item
        $gadget = GadgetRepository::loadById($id);
        if (!isset($gadget)) {
            $json['message'] = Lang::lang('error_gadget_invalid', [$id]);
            return json_encode($json);
        }

        // Save item
        switch ($property) {
            case 'column':
                $gadget->setCol($value);
                break;
            case 'displayOrder':
                $gadget->setDisplayOrder($value);
                break;
            case 'filter':
                $gadget->setFilterId($value);
                break;
        }

        $gadget->save();

        Log::log(Code::TRACK,
            'gagdet updated [page:' . $gadget->getPageId() . ', component:' . $gadget->getComponentId() . ']', __FILE__,
            __LINE__);

        $json['result'] = Result::SUCCESS;
        $json['message'] = Lang::lang('saved');
        return json_encode($json);
    }

    public function updateStatus()
    {
        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $package = Converter::stringKey('package', 'post');
        $entity = Converter::stringKey('entity', 'post');
        $id = Converter::intKey('id', 'post');
        $id2 = Converter::intKey('id2', 'post');
        $status = Converter::intKey('status', 'post');

        if (!isset($package, $entity, $id, $status)) {
            $json['message'] = Lang::lang('error_parameters_invalid');
            return json_encode($json);
        }

        if (!$this->checkPrivilegeByEntity($entity)) {
            return $this->noPrivilege($entity);
        }

        // check item
        $namespace = (in_array($package, ['App', 'Bus'])) ? '\Own' : '\Rebond';
        $genericData = $namespace . '\Repository\\' . $package . '\\' . $entity . 'Repository';

        $newStatus = Status::INACTIVE;

        // active
        if ($status == Status::INACTIVE) {
            $newStatus = Status::ACTIVE;
            // inactive or undelete
        } else if (in_array($status, [Status::ACTIVE, Status::DELETED])) {
            $newStatus = Status::INACTIVE;
            // delete
        } else if ($status === Status::TO_DELETE) {
            $newStatus = Status::DELETED;
        }

        // block
        if ($entity == 'User') {
            if ($this->signedUser->getId() == $id) {
                return json_encode([
                    'result' => Result::ERROR,
                    'message' => Lang::lang('error_status_own')
                ]);
            }
            if (!UserService::isEditable($this->signedUser, $id)) {
                return json_encode([
                    'result' => Result::ERROR,
                    'message' => Lang::lang('error_dev_update')
                ]);
            }
        }

        $update = 'saved';

        // update status
        if ($id2 == 0) { // single id table
            if ($newStatus == Status::INACTIVE || $newStatus == Status::ACTIVE) {
                $genericData::updateStatus($id, $newStatus);
            } else if ($newStatus == Status::DELETED) {
                $update = 'deleted';

                switch ($entity) {
                    case 'Feedback':
                    case 'Gadget':
                    case 'Log':
                    case 'MediaLink':
                    case 'User':
                        $genericData::deleteById($id);
                        break;
                    case 'Component':
                        GadgetRepository::deleteByComponentId($id);
                        $genericData::updateStatus($id, $newStatus);
                        break;
                    case 'Filter':
                        ContentRepository::clearFilter($id);
                        $genericData::deleteById($id);
                        break;
                    case 'Layout':
                        $options = [];
                        $options['clearSelect'] = true;
                        $options['select'][] = PageRepository::getList(['id']);
                        $options['join'][] = 'cms_layout layout ON layout.id = page.layout_id';
                        $options['where'][] = ['layout.id = ?', $id];
                        $pages = PageRepository::loadAll($options);
                        if (isset($pages) && count($pages) > 0) {
                            $json['result'] = Result::ERROR;
                            $json['message'] = Lang::lang('error_item_in_use', [
                                Lang::lang('layout'),
                                count($pages),
                                Lang::lang('pages')
                            ]);
                            return json_encode($json);
                        }
                        $genericData::deleteById($id);
                        break;
                    case 'Module':
                        $options = [];
                        $options['clearSelect'] = true;
                        $options['select'][] = ComponentRepository::getList(['id']);
                        $options['where'][] = ['component.module_id = ?', $id];
                        $components = ComponentRepository::loadAll($options);
                        if (count($components) > 0) {
                            foreach ($components as $component) {
                                ComponentRepository::updateStatus($component->getId(), $newStatus);
                                GadgetRepository::deleteByComponentId($component->getId());
                            }
                        }
                        $genericData::updateStatus($id, $newStatus);
                        break;
                    case 'Permission':
                        RolePermissionRepository::deleteByPermissionId($id);
                        $genericData::deleteById($id);
                        break;
                    case 'Role':
                        $userRoles = UserRoleRepository::loadAllByRoleId($id);
                        if (isset($userRoles) && count($userRoles) > 0) {
                            $json['result'] = Result::ERROR;
                            $json['message'] = Lang::lang('error_item_in_use', [
                                Lang::lang('role'),
                                count($userRoles),
                                Lang::lang('users')
                            ]);
                            return json_encode($json);
                        }
                        UserRoleRepository::deleteByRoleId($id);
                        RolePermissionRepository::deleteByRoleId($id);
                        $genericData::deleteById($id);
                        break;
                    case 'Template':
                        $options = [];
                        $options['clearSelect'] = true;
                        $options['select'][] = PageRepository::getList(['id']);
                        $options['join'][] = 'cms_template template ON template.id = page.template_id';
                        $options['where'][] = ['template.id = ?', $id];
                        $pages = PageRepository::loadAll($options);
                        if (isset($pages) && count($pages) > 0) {
                            $json['result'] = Result::ERROR;
                            $json['message'] = Lang::lang('error_item_in_use', [
                                Lang::lang('template'),
                                count($pages),
                                Lang::lang('users')
                            ]);
                            return json_encode($json);
                        }
                        $genericData::deleteById($id);
                        break;
                    default:
                        Log::log(Code::ENTITY_NOT_FOUND, $entity, __FILE__, __LINE__);
                        return json_encode([
                            'result' => Result::ERROR,
                            'message' => Lang::lang('item_not_found', [$entity, $id])
                        ]);
                }
            }
        } else { // double id table
            if ($newStatus == Status::DELETED) {
                $genericData::deleteByIds($id, $id2);
                $update = 'deleted';
            } else {
                $genericData::updateStatus($id, $id2, $newStatus);
            }
        }

        Log::log(Code::TRACK,
            'update status [package:' . $package . ', entity:' . $entity . ', id:' . $id . ',' . $id2 . ', status:' . $status . ']',
            __FILE__, __LINE__);

        return json_encode([
            'result' => Result::SUCCESS,
            'message' => Lang::lang($update)
        ]);
    }

    public function updateUserSettings()
    {
        if (!$this->hasPrivilege(null)) {
            return $this->noPrivilege('admin');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $value = Converter::intKey('value', 'post');
        $type = Converter::stringKey('type', 'post');

        if (!isset($type, $value)) {
            $json['message'] = Lang::lang('error_parameters_invalid');
            return json_encode($json);
        }

        // Check item
        $userSettings = $this->signedUser->getUserSettings();
        if (!isset($userSettings)) {
            $userSettings = new UserSettings();
            $userSettings->setId($this->signedUser->getId());
        }

        // Save item
        switch ($type) {
            case 'mediaView':
                $userSettings->setMediaView($value);
                break;
            case 'mediaPaging':
                $userSettings->setMediaPaging($value);
                break;
            case 'contentPaging':
                $userSettings->setContentPaging($value);
                break;
            case 'paging':
                $userSettings->setPaging($value);
                break;
        }
        $userSettings->save();

        $json['result'] = Result::SUCCESS;
        $json['message'] = Lang::lang('saved');
        return json_encode($json);
    }

    public function updateVersion()
    {
        if (!$this->hasPrivilege('admin.content.edit')) {
            return $this->noPrivilege('admin.content.edit');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        // check
        $entity = Converter::stringKey('entity', 'post');
        $appId = Converter::intKey('appId', 'post');
        $version = Converter::intKey('version', 'post');

        if (!isset($entity, $appId, $version)) {
            $json['message'] = Lang::lang('error_parameters_invalid');
            return json_encode($json);
        }

        // load model
        $appData = '\Own\Repository\App\\' . $entity . 'Repository';
        $model = $appData::loadById($appId);

        if (!isset($model)) {
            $json['message'] = Lang::lang('item_not_found', [$entity, $appId]);
            return json_encode($json);
        }

        $json['value'] = $model->getVersion();
        $appData::updateVersion($model, $version);

        $json['result'] = Result::SUCCESS;
        $json['entity'] = $entity;
        $json['message'] = $version == Version::DELETED
            ? Lang::lang('deleted')
            : Lang::lang('version_updated');
        return json_encode($json);
    }

    public function upload()
    {
        if (!$this->hasPrivilege('admin.media.upload')) {
            return $this->noPrivilege('admin.media.upload');
        }

        $json = [];
        $json['result'] = Result::ERROR;

        $folderId = Converter::intKey('folderId', 'post');
        $replaceId = Converter::intKey('replaceId', 'post');

        $resultUpload = Media::upload('file', true, $folderId, $replaceId);

        if ($resultUpload['result'] == Result::ERROR) {
            Log::log(Code::MEDIA_NOT_SAVED, $resultUpload['message'], __FILE__, __LINE__);
            $json['message'] = $resultUpload['message'];
            return json_encode($json);
        }

        $json['result'] = Result::SUCCESS;
        $json['id'] = $resultUpload['mediaId'];

        $text = (isset($folderId)) ? 'media_uploaded' : 'medium_replaced';
        $json['message'] = Lang::lang($text, [$resultUpload['filename']]);

        return json_encode($json);
    }

    /**
     * @param string $entity
     * @throws bool
     */
    private function checkPrivilegeByEntity($entity)
    {
        $map = [
            'Component' => 'admin.cms.edit',
            'Feedback' => 'admin.dev',
            'Content' => 'admin.dev',
            'Filter' => 'admin.content.filter',
            'Folder' => 'admin.media.edit',
            'Gadget' => 'admin.page.edit',
            'Layout' => 'admin.cms.edit',
            'Log' => 'admin.tools.edit',
            'Media' => 'admin.media.edit',
            'Module' => 'media.cms.edit',
            'MediaLink' => 'media.config.edit',
            'Page' => 'admin.page.edit',
            'Permission' => 'admin.config.edit',
            'Role' => 'admin.config.edit',
            'RolePermission' => 'admin.config.edit',
            'Site' => 'admin.config.edit',
            'Template' => 'admin.cms.edit',
            'User' => 'admin.user.edit',
            'UserRole' => 'admin.user.edit',
            'UserSecurity' => 'admin.user.security',
            'UserSettings' => 'admin.dev',
        ];
        if (!array_key_exists($entity, $map)) {
            return false;
        }
        return self::hasPrivilege($map[$entity]);
    }

    /**
     * @param string $permission
     * @return bool
     */
    private function hasPrivilege($permission)
    {
        return Auth::isAdminAuthorized($this->signedUser, $permission);
    }

    /**
     * @param string $permission
     * @return string
     */
    private function noPrivilege($permission)
    {
        Log::log(
            Code::NOT_ENOUGH_PRIVILEGE,
            'permission: ' . $permission . ', user: ' . $this->signedUser->getFullName(),
            __FILE__,
            __LINE__
        );
        return json_encode(
            [
                'result' => Result::ERROR,
                'message' => Lang::lang('access_non_authorized')
            ]
        );
    }
}
