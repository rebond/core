<?php

namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Enums\Cms\Version;
use Rebond\Enums\Core\Code;
use Rebond\Forms\Cms\FilterForm;
use Rebond\Models\Cms\Module;
use Rebond\Repository\Cms\FilterRepository;
use Rebond\Repository\Cms\ModuleRepository;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Log;
use Rebond\Services\Session;
use Rebond\Services\Template;

class ContentController extends BaseAdminController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        Auth::isAdminAuthorized($this->signedUser, 'admin.content', true, '/');
    }

    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('content'));
    }

    public function index()
    {
        // view
        $this->setTpl();

        // header
        $options = [];
        $options['where'][] = 'module.status = 1';
        $options['where'][] = 'module.has_content = 1';
        $options['order'][] = 'module.title';

        $modules = ModuleRepository::loadAll($options);
        $items = [];
        foreach ($modules as $module) {
            $items[$module->getName()] = $module->getTitle();
        }
        $tplHeader = new Template(Template::ADMIN, ['admin', 'content']);
        $tplHeader->set('modules', $items);

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'content']);

        // layout
        $this->tplLayout->set('column1', $tplHeader->render('filter-version'));
        $this->tplLayout->set('column2', $tplMain->render('index'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));
        $this->tplMaster->set('jsLauncher', 'contentList');
        return $this->tplMaster->render('tpl-default');
    }

    public function edit()
    {
        // check
        $moduleName = Converter::stringKey('module');
        $id = Converter::intKey('id');
        $module = $this->findModule($moduleName);

        // action
        $appData = '\Own\Repository\App\\' . $module->getName() . 'Repository';
        $appForm = '\Own\Forms\App\\' . $module->getName() . 'Form';

        $model = $appData::loadById($id, true);
        $model->loadModule();

        $form = new $appForm($model);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.content.edit', true, '/content/edit');
            $model->setAuthorId($this->signedUser->getId());
            if ($form->setFromPost()->validate()->isValid()) {
                $model->save();
                $status = ($module->getWorkflow() == 0) ? 'published' : 'pending';
                Session::adminSuccess('saved', '/content/#' . $moduleName . '/' . $status);
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        $tplMain = new Template(Template::MODULE_ADMIN, ['app', Converter::toSnakeCase($moduleName)]);

        // main
        $tplMain->set('filter', $module->getHasFilter());

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.content.edit', false)) {
            $tplMain->set('item', $form);
            $this->tplLayout->set('column1', $tplMain->render('admin-app-editor'));
        } else {
            $tplMain->set('item', $model);
            $this->tplLayout->set('column1', $tplMain->render('admin-app-view'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'contentEditor');
        $this->tplMaster->addJs('/node_modules/tinymce/tinymce.min.js');
        return $this->tplMaster->render('tpl-default');
    }

    public function trail()
    {
        // check
        $moduleName = Converter::stringKey('module');
        $id = Converter::intKey('id');
        $module = $this->findModule($moduleName);

        $appData = '\Own\Repository\App\\' . $module->getName() . 'Repository';
        $item = $appData::loadCurrent($id, false);
        $items = $appData::loadTrail($id);

        // view
        $this->setTpl();

        $tplMain = new Template(Template::MODULE_ADMIN, ['app', Converter::toSnakeCase($moduleName)]);
        $tplMain->set('item', $item);
        $tplMain->set('items', $items);
        $tplMain->set('filter', $module->getHasFilter());

        // layout
        $this->tplLayout->set('column1', $tplMain->render('admin-app-trail'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'contentList');
        return $this->tplMaster->render('tpl-default');
    }

    public function view()
    {
        // check
        $moduleName = Converter::stringKey('module');
        $id = Converter::intKey('id');
        $module = $this->findModule($moduleName);

        $appData = '\Own\Repository\App\\' . $module->getName() . 'Repository';
        $item = $appData::loadById($id);
        if (!isset($item)) {
            Session::adminError('item_not_found', '/content/', [$moduleName, $id]);
        }

        // view
        $this->setTpl();

        $tplMain = new Template(Template::MODULE_ADMIN, ['app', Converter::toSnakeCase($moduleName)]);
        $tplMain->set('item', $item);
        $tplMain->set('filter', $module->getHasFilter());

        // layout
        $this->tplLayout->set('column1', $tplMain->render('admin-app-view'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function delete()
    {
        // auth
        Auth::isAdminAuthorized($this->signedUser, 'admin.content.edit', true, '/content');

        // check
        $moduleName = Converter::stringKey('module');
        $id = Converter::intKey('id');
        $module = $this->findModule($moduleName);

        $appData = '\Own\Repository\App\\' . $module->getName() . 'Repository';
        $model = $appData::loadById($id);
        if (!isset($model)) {
            Session::adminError('item_not_found', '/content/', [$moduleName, $id]);
        }

        $appData::updateVersion($model, Version::DELETED);
        Session::adminSuccess('deleted', '/content/#' . $moduleName . '/published/');
    }

    public function filter()
    {
        $options = [];
        $options['select'][] = ModuleRepository::getList([], 'filter_module');
        $options['join'][] = 'cms_module filter_module ON filter_module.id = filter.module_id';
        $options['order'][] = 'filter_module.title, filter.display_order, filter.title';
        $filters = FilterRepository::loadAll($options);

        // view
        $this->setTpl();

        // filter
        $tplFilter = new Template(Template::MODULE_ADMIN, ['cms', 'filter']);
        $tplFilter->set('count', count($filters));

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['cms', 'filter']);
        $tplMain->set('items', $filters);

        // layout
        $this->tplLayout->set('column1', $tplFilter->render('filter'));
        $this->tplLayout->set('column2', $tplMain->render('table'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));
        return $this->tplMaster->render('tpl-default');
    }

    public function filterEdit()
    {
        // check
        $id = Converter::intKey('id');

        $filter = FilterRepository::loadById($id, true);
        $form = new FilterForm($filter);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.content.filter', true, '/content/filter');
            if ($form->setFromPost()->validate()->isValid()) {
                $filter->save();

                Session::adminSuccess('saved', '/content/filter');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['cms', 'filter']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.content.filter', false)) {
            $tplMain->set('item', $form);
            $this->tplLayout->set('column1', $tplMain->render('editor'));
        } else {
            $tplMain->set('item', $filter);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    /**
     * Find module
     * @param string $moduleName
     * @return Module
     */
    private function findModule($moduleName)
    {
        if ($moduleName == '') {
            Session::redirect('/content');
        }

        $module = ModuleRepository::loadByName($moduleName);
        if (!isset($module)) {
            Session::adminError('item_not_found', '/content/', [Lang::lang('module'), $moduleName]);
        }
        return $module;
    }
}
