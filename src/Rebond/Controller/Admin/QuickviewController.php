<?php
namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Forms\AbstractForm;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\File;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Session;
use Rebond\Services\Template;

class QuickviewController extends BaseAdminController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        Auth::isAdminAuthorized($this->signedUser, 'admin.quickview', true, '/');
    }

    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('quick_view'));
    }

    public function index()
    {
        // view
        $this->setTpl();

        // nav
        $entities = File::getFiles('Own/Models/Bus/');
        foreach ($entities as &$entity) {
            $entity = File::getNoExtension($entity);
        }

        $tplNav = new Template(Template::ADMIN, ['admin', 'quickview']);
        $tplNav->set('entities', $entities);

        // main

        // layout
        $this->tplLayout->set('column1', Template::loading());

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
       
        // master
        $this->tplMaster->set('navSide', $tplNav->render('nav'));
        $this->tplMaster->set('page', 'quickview');
        $this->tplMaster->set('jsLauncher', 'quickview');
        return $this->tplMaster->render('tpl-default');
    }

    public function edit()
    {
        // check
        $module = Converter::stringKey('module');
        $id = Converter::intKey('id');
        $id2 = Converter::intKey('id2');

        if($module == '') {
            Session::redirect('/quickview');
        }

        $busModel = '\Own\Models\Bus\\' . $module . '';
        $busData = '\Own\Repository\Bus\\' . $module . 'Repository';
        $busForm = '\Own\Forms\Bus\\' . $module . 'Form';

        if ($id == 0) {
            $entity = new $busModel();
        } else {
            if ($id2 != 0) {
                $entity = $busData::loadById($id, $id2);
            } else {
                $entity = $busData::loadById($id);
            }
            if(!isset($entity)){
                $entity = new $busModel();
            }
        }

        /* @var $form AbstractForm */
        $form = new $busForm($entity);

        // action
        if (Form::isSubmitted()) {
            if ($form->setFromPost()->validate()->isValid()) {
                $entity->save();
                Session::adminSuccess('saved', '/quickview/#' . $module);
            }
            Session::set('adminError', $form->getValidation()->getMessage());
        }

        // view
        $this->setTpl();

        // nav
        $entities = File::getFolders('Own/Bus/');

        $tplNav = new Template(Template::ADMIN, ['admin', 'quickview']);
        $tplNav->set('entities', $entities);
        $tplNav->set('active', $module);

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['bus', $module]);
        $tplMain->set('item', $form);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('admin-bus-editor'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('navSide', $tplNav->render('nav'));
        $this->tplMaster->set('page', 'quickview');
        $this->tplMaster->addJs('/node_modules/tinymce/tinymce.min.js');
        $this->tplMaster->set('jsLauncher', 'quickedit');
        return $this->tplMaster->render('tpl-default');
    }
}
