<?php

namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\ResponseFormat;
use Rebond\Forms\Cms\GadgetForm;
use Rebond\Forms\Cms\PageForm;
use Rebond\Models\Cms\Gadget;
use Rebond\Models\Cms\Page;
use Rebond\Repository\Cms\GadgetRepository;
use Rebond\Repository\Cms\LayoutRepository;
use Rebond\Repository\Cms\PageRepository;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Log;
use Rebond\Services\Nav;
use Rebond\Services\Session;
use Rebond\Services\Template;

class PageController extends BaseAdminController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        Auth::isAdminAuthorized($this->signedUser, 'admin.page', true, '/');
    }

    public function index()
    {
        $tplMain = new Template(Template::ADMIN, ['admin', 'page']);

        return $this->response(
            'tpl-default', [
            'title' => Lang::lang('page'),
            'jsLauncher' => 'page',
            'navSide' => $this->renderNavSide(),
            'js' => '/node_modules/rebond-assets-admin/js/rebond-page.js'
        ],
            'layout-1-col', [
                'column1' => $tplMain->render('index')
            ]
        );
    }

    // Page edition / New page
    public function edit()
    {
        // check
        $id = Converter::intKey('id');
        $pid = Converter::intKey('pid', 'get', 1);

        $page = PageRepository::loadById($id);
        if (!isset($page)) {
            $page = new Page();
            $title = Lang::lang('page_new');
        } else {
            $title = Lang::lang('edit') . ' ' . $page->getTitle();
        }

        if ($page->getId() != Nav::HOME_PAGE && $page->getParentId() == Nav::HOME_PAGE) {
            $page->setParentId($pid);
        }

        $form = new PageForm($page);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.page.edit', true, '/page');
            if ($form->setFromPost()->validate()->isValid()) {
                // set new friendly url hidden
                $page->setFriendlyUrl(strtolower($page->getFriendlyUrl()));
                $fuh = PageRepository::fullFriendlyUrl($page->getParentId());
                $page->setFriendlyUrlPath($fuh);
                $page->save();
                // update children pages
                PageRepository::updateChildrenFriendlyUrl($page->getId(), $page->getFriendlyUrlPath(),
                    $page->getFriendlyUrl());
                Log::log(Code::TRACK, 'page saved [' . $page->getId() . ':' . $page->getTitle() . ']', __FILE__,
                    __LINE__);
                Session::adminSuccess('saved', '/page');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['cms', 'page']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.page.edit')) {
            $tplMain->set('title', $title);
            $tplMain->set('isDev', $this->signedUser->getIsDev());
            $tplMain->set('siteUrl', rtrim($this->app->getConfig('site-url'), '/'));
            $tplMain->set('item', $form);
            $column1 = $tplMain->render('editor');
        } else {
            $tplMain->set('item', $page);
            $column1 = $tplMain->render('view');
        }

        return $this->response(
            'tpl-default', [
            'title' => Lang::lang('page'),
            'jsLauncher' => 'page',
            'navSide' => $this->renderNavSide(),
            'js' => '/node_modules/rebond-assets-admin/js/rebond-page.js'
        ],
            'layout-1-col', [
                'column1' => $column1
            ]
        );
    }

    public function gadget()
    {
        // check
        $id = Converter::intKey('id');
        $page = PageRepository::loadById($id);
        if (!isset($page)) {
            Session::redirect('/page');
        }

        $gadget = new Gadget();
        $gadget->setPageId($page->getId());

        $form = new GadgetForm($gadget);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.page.edit', true, '/page/gadget?id=' . $id);
            if ($form->setFromPost()->validate()->isValid()) {
                $gadget->save();
                Log::log(Code::TRACK,
                    'gadget added [' . $gadget->getId() . ', page:' . $page->getId() . ':' . $page->getTitle() . ']',
                    __FILE__,
                    __LINE__);
                Session::adminSuccess('saved', '/page/gadget?id=' . $id);
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // form
        $layout = LayoutRepository::loadById($page->getLayoutId());
        $columns = $layout->getColumns();
        $columnOptions = [];
        for ($i = 1; $i <= $columns; $i++) {
            $columnOptions[$i] = Lang::lang('column') . ' ' . $i;
        }
        $tplForm = new Template(Template::MODULE_ADMIN, ['cms', 'gadget']);
        $tplForm->set('item', $form);
        $tplForm->set('layoutFilename', $layout->getFilename());
        $tplForm->set('title', Lang::lang('content_add'));
        $tplForm->set('pageTitle', $page->getTitle());
        $tplForm->set('columnOptions', $columnOptions);
        $tplForm->set('selectedColumn', $gadget->getCol());

        // active listing
        $options = [];
        $options['join'][] = 'cms_page page ON page.id = gadget.page_id';
        $options['join'][] = 'cms_component component ON component.id = gadget.component_id';
        $options['join'][] = 'cms_module module ON module.id = component.module_id';
        $options['where'][] = ['page.id = ?', $page->getId()];
        $options['where'][] = 'module.status = 1';
        $options['where'][] = 'component.status = 1';
        $options['order'][] = 'gadget.col';
        $options['order'][] = 'gadget.display_order';
        $gadgets = GadgetRepository::loadAll($options);
        foreach ($gadgets as $gadget) {
            $gadget->prepareFilter();
        }

        // inactive listing
        $options = [];
        $options['join'][] = 'cms_page page ON page.id = gadget.page_id';
        $options['join'][] = 'cms_component component ON component.id = gadget.component_id';
        $options['join'][] = 'cms_module module ON module.id = component.module_id';
        $options['where'][] = ['page.id = ?', $page->getId()];
        $options['where'][] = 'module.status = 0 OR component.status = 0';
        $options['order'][] = 'gadget.col';
        $options['order'][] = 'gadget.display_order';
        $inactiveGadgets = GadgetRepository::loadAll($options);
        foreach ($inactiveGadgets as $gadget) {
            $gadget->prepareFilter();
        }

        $tplListing = new Template(Template::MODULE_ADMIN, ['cms', 'gadget']);
        $tplListing->set('items', $gadgets);
        $tplListing->set('inactiveItems', $inactiveGadgets);
        $tplListing->set('columnOptions', $columnOptions);

        return $this->response(
            'tpl-default', [
            'title' => Lang::lang('page'),
            'jsLauncher' => 'pageGadget',
            'navSide' => $this->renderNavSide(),
            'js' => '/node_modules/rebond-assets-admin/js/rebond-page.js'
        ],
            'layout-2-row', [
                'column1' => $tplForm->render('editor-selector'),
                'column2' => $tplListing->render('table')
            ]
        );
    }

    /**
     * Render the side navigation
     * @return string
     */
    private function renderNavSide()
    {
        if ($this->getFormat() == ResponseFormat::HTML) {
            $tplTree = new Template(Template::ADMIN, ['admin', 'page']);
            $tree = PageRepository::buildTree();
            $tplTree->set('tree', $tree);
            return $tplTree->render('tree');
        }
        return null;
    }
}
