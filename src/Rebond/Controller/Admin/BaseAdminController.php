<?php
namespace Rebond\Controller\Admin;

use Rebond\Controller\AbstractController;
use Rebond\Enums\Core\ResponseFormat;
use Rebond\Services\Core\SiteService;
use Rebond\Services\Lang;
use Rebond\Services\Menu;
use Rebond\Services\Nav;
use Rebond\Services\Session;
use Rebond\Services\Template;

class BaseAdminController extends AbstractController
{
    protected $tplMenu;

    protected function setBaseTpl($format = ResponseFormat::HTML)
    {
        $this->tplLayout = new Template(Template::ADMIN, ['admin']);

        if ($format == ResponseFormat::JSON) {
            return;
        }

        $this->tplMaster = new Template(Template::ADMIN, ['admin']);
        $this->tplMenu = new Template(Template::ADMIN, ['admin']);

        $ga = SiteService::renderGoogleAnalytics($this->app->getSite()->getGoogleAnalytics());
        $this->tplMaster->add('footer', $ga);

        $this->tplMenu->set('mainMenu', Menu::buildMenu($this->signedUser));
        $this->tplMenu->set('subMenu', Menu::buildSubMenu($this->app, $this->signedUser, Nav::getUrlSegment(1)));
        $this->tplMenu->set('mainActive', Nav::getUrlSegment(1));
        $this->tplMenu->set('subActive', Nav::getUrlSegment(2));

        $this->tplMaster->addCss('/node_modules/normalize-css/normalize.css');
        $this->tplMaster->addCss('/node_modules/jquery-ui-dist/jquery-ui.css');
        $this->tplMaster->addCss('/node_modules/components-font-awesome/css/font-awesome.min.css');
        $this->tplMaster->addCss('https://fonts.googleapis.com/css?family=Work+Sans');
        $this->tplMaster->addCss('/node_modules/rebond-assets/css/rebond.css');
        $this->tplMaster->addCss('/node_modules/rebond-assets-admin/css/rebond-admin.css');
        $this->tplMaster->addCss('/css/own.css', false);

        $this->tplMaster->addJs('/node_modules/jquery/dist/jquery.min.js');
        $this->tplMaster->addJs('/js/lang-' . $this->app->getCurrentLang() . '.js');
        $this->tplMaster->addJs('/node_modules/jquery-ui-dist/jquery-ui.js');
        $this->tplMaster->addJs('/node_modules/rebond-assets/js/rebond.js', false);
        $this->tplMaster->addJs('/node_modules/rebond-assets-admin/js/rebond-admin.js', false);
        $this->tplMaster->addJs('/js/own.js', false);

        $this->tplMaster->set('menu', $this->tplMenu->render('menu'));
        $this->tplMaster->set('signedUser', $this->signedUser);
        $this->tplMaster->set('site', $this->app->getSite()->getTitle());
        $this->tplMaster->set('title', Lang::lang('admin'));
        $this->tplMaster->set('siteUrl', $this->app->getConfig('site-url'));
        $this->tplMaster->set('notifications', Session::renderAdminResp());

        $tplLocale = new Template(Template::ADMIN, ['admin']);
        $tplLocale->set('langList', $this->app->getLangList());
        $tplLocale->set('current', $this->app->getCurrentLang());
        $this->tplMaster->set('langSelector', $tplLocale->render('locale'));
    }
}
