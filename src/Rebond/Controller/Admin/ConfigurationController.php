<?php
namespace Rebond\Controller\Admin;

use Rebond\App;
use Rebond\Enums\Core\Code;
use Rebond\Forms\Core\MediaLinkForm;
use Rebond\Forms\Core\PermissionForm;
use Rebond\Forms\Core\RoleForm;
use Rebond\Forms\Core\SiteForm;
use Rebond\Models\Core\RolePermission;
use Rebond\Models\Core\Site;
use Rebond\Models\DateTime;
use Rebond\Repository\Core\MediaLinkRepository;
use Rebond\Repository\Core\PermissionRepository;
use Rebond\Repository\Core\RolePermissionRepository;
use Rebond\Repository\Core\RoleRepository;
use Rebond\Repository\Core\SiteRepository;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\Core\SiteService;
use Rebond\Services\File;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Session;
use Rebond\Services\Template;

class ConfigurationController extends BaseAdminController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        Auth::isAdminAuthorized($this->signedUser, 'admin.config', true, '/');
    }

    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('configuration'));
    }

    public function index()
    {
        // view
        $this->setTpl();

        // main
        $tplDefault = new Template(Template::ADMIN, ['admin', 'configuration']);

        // layout
        $this->tplLayout->set('column1', $tplDefault->render('index'));
        
        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function site()
    {
        $site = SiteRepository::loadById(Site::MAIN);
        if (!isset($site)) {
            throw new \Exception(Site::MAIN, Code::SITE_NOT_FOUND);
        }

        $siteForm = new SiteForm($site);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', true, '/configuration');
            if ($siteForm->setFromPost()->validate()->isValid()) {
                $siteForm->getModel()->save();
                SiteService::updateIsCms($siteForm->getModel()->getIsCms());
                Session::adminSuccess('saved', '/configuration/site');
            } else {
                Session::set('adminError', $siteForm->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'site']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', false)) {
            $tplMain->set('item', $siteForm);
            $this->tplLayout->set('column1', $tplMain->render('editor'));
        } else {
            $tplMain->set('item', $site);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function mediaLink()
    {
        $mediaLinks = MediaLinkRepository::loadAll();

        // view
        $this->setTpl();

        // filter
        $tplFilter = new Template(Template::MODULE_ADMIN, ['core', 'mediaLink']);
        $tplFilter->set('count', count($mediaLinks));

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'mediaLink']);
        $tplMain->set('items', $mediaLinks);

        // layout
        $this->tplLayout->set('column1', $tplFilter->render('filter'));
        $this->tplLayout->set('column2', $tplMain->render('table'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));
        return $this->tplMaster->render('tpl-default');
    }

    public function mediaLinkEdit()
    {
        // check
        $id = Converter::intKey('id');

        $mediaLink = MediaLinkRepository::loadById($id, true);
        $form = new MediaLinkForm($mediaLink);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', true, '/configuration/media-link');
            if ($form->setFromPost()->validate()->isValid()) {
                $mediaLink->save();
                Session::adminSuccess('saved', '/configuration/media-link');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'mediaLink']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', false)) {
            $tplMain->set('item', $form);
            $this->tplLayout->set('column1', $tplMain->render('editor'));
        } else {
            $tplMain->set('item', $mediaLink);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        // master
        $this->tplMaster->set('jsLauncher', 'configMediaLinkEdit');
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function lang()
    {
        $xmlLangList = File::getFiles($this->app->getPath('own-lang-file'));

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'configuration']);
        $tplMain->set('xmlLangList', $xmlLangList);
        $tplMain->set('langList', $this->app->getLangList());
        $tplMain->set('current', $this->app->getCurrentLang());

        // layout
        $this->tplLayout->set('column1', $tplMain->render('lang'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function langEdit()
    {
        // check
        $file = Converter::stringKey('f');
        $fileContent = Converter::stringKey('fileContent', 'post');

        $filePath = $this->app->getPath('own-lang-file') . $file;

        if ($file == '') {
            Session::redirect('/configuration/lang');
        }

        if (!file_exists($filePath)) {
            Session::adminError('error_file_not_found', '/configuration/lang', [$filePath]);
        }

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', true, '/configuration/lang');
            file_put_contents($filePath, $fileContent);
            Session::adminSuccess('saved', '/configuration/lang');
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'configuration']);
        $tplMain->set('file', $file);
        $tplMain->set('filePath', $filePath);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('lang-form'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'configLang');
        $this->tplMaster->addCss('/node_modules/codemirror/lib/codemirror.css');
        $this->tplMaster->addJs('/node_modules/codemirror/lib/codemirror.js');
        $this->tplMaster->addJs('/node_modules/codemirror/mode/yaml/yaml.js');
        return $this->tplMaster->render('tpl-default');
    }

    public function cache()
    {
        // view
        $this->setTpl();

        // main
        $cachePath = $this->app->getPath('own-cache-file');
        $cacheTime = $this->app->getSite()->getCacheTime();
        $now = time();

        $files = File::getFiles($cachePath);
        $fileData = [];

        foreach ($files as $file) {
            $expiry = filemtime($cachePath . $file) + $cacheTime;
            if ($expiry < $now) {
                File::delete($cachePath, $file);
                continue;
            }
            $expiryDate = DateTime::createFromTimestamp($expiry);
            $fileData[] = [
              'name' => $file,
              'expiry' => $expiryDate->format(),
            ];
        }

        $tplMain = new Template(Template::ADMIN, ['admin', 'configuration']);
        $tplMain->set('fileData', $fileData);
        $tplMain->set('cache', $this->app->getSite()->getCacheTime());

        // layout
        $this->tplLayout->set('column1', $tplMain->render('cache'));
        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        $this->tplMaster->set('jsLauncher', 'configCache');
        return $this->tplMaster->render('tpl-default');
    }

    public function generatedPhotos()
    {
        $mediaList = File::getFiles($this->app->getConfig('media'));
        $media = [];
        $generated = [];

        $pattern = '/-(.*).(.*)/';
        foreach ($mediaList as $ml) {
            if (!preg_match($pattern, $ml)) {
                if (!isset($media[$ml])) {
                    $media[$ml] = [];
                }
            } else {
                $mainMedia = File::getMainMedia($ml);
                $media[$mainMedia][] = $ml;
                $generated[] = $ml;
            }
        }

        if (Form::isSubmitted('btnCleanup')) {
            foreach ($generated as $photo) {
                File::deleteAllMedia('', $photo, true);
            }
            Session::adminSuccess('generated_photos_deleted', '/configuration/generated-photos');
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::ADMIN, ['admin', 'configuration']);
        $tplMain->set('path', $this->app->getConfig('media-url'));
        $tplMain->set('media', $media);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('generated-photos'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function roles()
    {
        $options = [];
        $options['where'][] = 'role.status IN (0,1)';
        $options['order'][] = 'role.title';
        $roles = RoleRepository::loadAll($options);

        // view
        $this->setTpl();

        // filter
        $tplFilter = new Template(Template::MODULE_ADMIN, ['core', 'role']);
        $tplFilter->set('count', count($roles));

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'role']);
        $tplMain->set('items', $roles);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', false)) {
            $this->tplLayout->set('column1', $tplFilter->render('filter'));
            $this->tplLayout->set('column2', $tplMain->render('table'));
        } else {
            $this->tplLayout->set('column1', $tplFilter->render('filter-config'));
            $this->tplLayout->set('column2', $tplMain->render('table-config'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));
        return $this->tplMaster->render('tpl-default');
    }

    public function roleEdit()
    {
        // check
        $id = Converter::intKey('id');
        $role = RoleRepository::loadById($id, true);
        $roleform = new RoleForm($role);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', true, '/configuration/role-edit?id=' . $id);
            if ($roleform->setFromPost()->validate()->isValid()) {
                $role->save();
                Session::adminSuccess('saved', '/configuration/roles');
            } else {
                Session::set('adminError', $roleform->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'role']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', false)) {
            $tplMain->set('item', $roleform);
            $this->tplLayout->set('column1', $tplMain->render('editor'));
        } else {
            $tplMain->set('item', $role);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function rolePermissions()
    {
        // check
        $id = Converter::intKey('id');
        $permissionIds = Converter::arrayKey('permission' ,'post');

        $role = RoleRepository::loadById($id);
        if (!isset($role)) {
            Session::adminError('item_not_found', '/configuration/roles', [Lang::lang('role'), $id]);
        }

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', true, '/configuration/role-permissions/?id=' . $id);
            RolePermissionRepository::deleteByRoleId($id);
            $newPermissions = [];
            if (isset($permissionIds)) {
                foreach ($permissionIds as $permissionId) {
                    $rolePermission = new RolePermission();
                    $rolePermission->setRoleId($role->getId());
                    $rolePermission->setPermissionId($permissionId);
                    $newPermissions[] = $rolePermission;
                }
            }
            RolePermissionRepository::saveAll($newPermissions);
            Session::adminSuccess('saved', '/configuration/roles');
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'role']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', false)) {
            $tplMain->set('item', new RoleForm($role));
            $this->tplLayout->set('column1', $tplMain->render('editor-permission'));
        } else {
            $allPermissions = PermissionRepository::loadAll();
            $permissions = PermissionRepository::loadByRoleId($role->getId());
            $tplMain->set('allPermissions', $allPermissions);
            $tplMain->set('permissions', $permissions);
            $this->tplLayout->set('column1', $tplMain->render('view-permission'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function permissions()
    {
        $options = [];
        $options['where'][] = 'permission.status IN (0,1)';
        $options['order'][] = 'permission.title';
        $permissions = PermissionRepository::loadAll($options);

        // view
        $this->setTpl();

        // filter
        $tplFilter = new Template(Template::MODULE_ADMIN, ['core', 'permission']);
        $tplFilter->set('count', count($permissions));

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'permission']);
        $tplMain->set('items', $permissions);

        // layout
        $this->tplLayout->set('column1', $tplFilter->render('filter'));
        $this->tplLayout->set('column2', $tplMain->render('table'));
        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-row'));

        return $this->tplMaster->render('tpl-default');
    }

    public function permissionEdit()
    {
        // check
        $id = Converter::intKey('id');
        $permission = PermissionRepository::loadById($id, true);
        $form = new PermissionForm($permission);

        // action
        if (Form::isSubmitted()) {
            Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', true, '/configuration/permission-edit?id=' . $id);
            if ($form->setFromPost()->validate()->isValid()) {
                $permission->save();
                Session::adminSuccess('saved', '/configuration/permissions');
            } else {
                Session::set('adminError', $form->getValidation()->getMessage());
            }
        }

        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::MODULE_ADMIN, ['core', 'permission']);

        // layout
        if (Auth::isAdminAuthorized($this->signedUser, 'admin.config.edit', false)) {
            $tplMain->set('item', $form);
            $this->tplLayout->set('column1', $tplMain->render('editor'));
        } else {
            $tplMain->set('item', $permission);
            $this->tplLayout->set('column1', $tplMain->render('view'));
        }

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }
}
