<?php
namespace Rebond\Controller;

use Rebond\App;
use Rebond\Enums\Core\ResponseFormat;
use Rebond\Enums\Core\Status;
use Rebond\Services\Core\SiteService;
use Rebond\Services\Renderer;
use Rebond\Services\Session;
use Rebond\Services\Template;

class BaseSiteController extends AbstractController
{
    public function __construct(App $app)
    {
        if ($app->getSite()->getStatus() == Status::INACTIVE) {
            $renderer = new Renderer($app);
            echo $renderer->inactive();
            Session::close();
        }
        parent::__construct($app);
    }

    protected function setBaseTpl($format = ResponseFormat::HTML)
    {
        $this->tplLayout = new Template(Template::SITE, ['www']);

        if ($format == ResponseFormat::JSON) {
            return;
        }

        $this->tplMaster = new Template(Template::SITE, ['www']);
        if ($this->app->getSite()->getKeywords() != '') {
            $this->tplMaster->addMeta('keywords', $this->app->getSite()->getKeywords());
        }
        if ($this->app->getSite()->getDescription() != '') {
            $this->tplMaster->addMeta('description', $this->app->getSite()->getDescription());
        }

        $this->tplMaster->set('site', $this->app->getSite()->getTitle());
        $this->tplMaster->set('title', $this->app->getSite()->getTitle());
        $this->tplMaster->set('signedUser', $this->signedUser);
        $this->tplMaster->set('adminUrl', $this->app->getConfig('admin-url'));

        $ga = SiteService::renderGoogleAnalytics($this->app->getSite()->getGoogleAnalytics());
        $this->tplMaster->set('footer', $ga);
        $this->tplMaster->set('siteUrl', $this->app->getConfig('site-url'));
        $this->tplMaster->set('notifications', Session::renderSiteResp());

        $this->tplMaster->addJs('/node_modules/jquery/dist/jquery.min.js');
        $this->tplMaster->addJs('/node_modules/jquery-ui-dist/jquery-ui.js');
        $this->tplMaster->addJs('/js/lang-' . $this->app->getCurrentLang() . '.js');

        $this->tplMaster->addCss('https://fonts.googleapis.com/css?family=Work+Sans');
        $this->tplMaster->addCss('/node_modules/jquery-ui-dist/jquery-ui.css');

        if (!$this->app->isDebug()) {
            $this->tplMaster->addCss('/css/min-' . $this->app->getSkin() . '.css');
            $this->tplMaster->addJs('/js/min.js', false);
        } else {
            $this->tplMaster->addCss('/node_modules/normalize-css/normalize.css');
            $this->tplMaster->addCss('/node_modules/rebond-assets/css/rebond.css');
            $this->tplMaster->addCss('/node_modules/rebond-assets-site/css/rebond-site.css');
            $this->tplMaster->addCss('/node_modules/components-font-awesome/css/font-awesome.min.css');
            $this->tplMaster->addCss('/css/skin/' . $this->app->getSkin() . '/own.css');
            $this->tplMaster->addJs('/node_modules/rebond-assets/js/rebond.js', false);
            $this->tplMaster->addJs('/node_modules/rebond-assets-site/js/rebond-site.js', false);
            $this->tplMaster->addJs('/js/own.js', false);
        }
    }
}

