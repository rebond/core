<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Generator;

use Rebond\App;
use Rebond\Services\Converter;
use Rebond\Services\Data;
use Rebond\Services\File;
use Rebond\Services\Template;

class MysqlGenerator extends AbstractGenerator
{
    const MAX_LENGTH = 10000;
    private $properties;
    private $table;
    private $db;
    private $maxLength;

    /**
     * Constructor
     * @param App $app
     * @param string $package
     * @param string $entity
     * @param string $sqlEntity
     * @param array $properties
     */
    public function __construct(App $app, $package, $entity, $sqlEntity, array $properties)
    {
        parent::__construct($app, $package, $entity);
        $this->properties = $properties;
        $this->table = strtolower($package) . '_' . $sqlEntity;
        $this->db = new \Rebond\Repository\Data($app);
        $this->maxLength = self::MAX_LENGTH;
    }

    public function produce()
    {
        $this->addInfo(self::INFO_TITLE, 'MySQL scripts');
        $this->produceScript();
    }

    public function produceScript()
    {
        $scriptTpl = $this->createTemplate('mysql');

        $scriptTpl->set('table', $this->table);

        $fieldList = [];
        $multipleKey = [];

        foreach ($this->properties as $property => $options) {
            if ($this->ignoreColumn($options))
                continue;

            $type = $options['type'];
            $sqlType = $this->getDefaultMysqlType(Converter::stringKey('mysql', $options, null), $type);

            $maxLength = $this->getMaxLength($options);

            $autoIncrement = '';
            if ($type == 'primaryKey') {
                $autoIncrement = Converter::boolKey('isAutoIncrement', $options, true) ? 'AUTO_INCREMENT' : '';
                $property = ($this->package == 'App') ? 'app_id' : 'id';
                $primaryKey = $property;
                $scriptTpl->set('primaryKey', $primaryKey);
            }
            if ($type == 'multipleKey') {
                $multipleKey[] = $property . '_id';
            }
            $fieldList[] = $this->fieldList('field', null, $property, $type, $sqlType, $maxLength, $autoIncrement);
        }

        $scriptTpl->set('multipleKey', $multipleKey);
        $scriptTpl->set('fieldList', implode(',', $fieldList));

        $scriptPath = $this->getNamespace(false) . 'scripts/' . strtolower($this->package) . '/' . lcfirst($this->entity) . '.sql';
        File::save($scriptPath, $scriptTpl->render('table'));

        $this->addInfo(self::INFO_SUCCESS, 'SQL scripts created');

        $existQuery = 'SHOW TABLES LIKE ';
        $exist = count($this->db->selectRow($existQuery . $this->db->pdo()->quote($this->table)));

        if ($exist == 0) {
           Data::runScript($scriptPath);
            $this->addInfo(self::INFO_SUCCESS, 'SQL table created');
        } else if ($exist > 0 && $this->db->count('SELECT COUNT(*) FROM ' . $this->table) == 0) {
            $this->db->execute('DROP TABLE ' . $this->table);
            Data::runScript($scriptPath);
            $this->addInfo(self::INFO_SUCCESS, 'SQL table recreated');
        } else {
            $sqlCols = $this->db->select('SHOW COLUMNS FROM ' . $this->table)->fetchAll();
            $updatedColumns = $this->updateColumns($sqlCols);
            if ($updatedColumns > 0) {
                $this->addInfo(self::INFO_WARNING, $updatedColumns . ' columns updated');
            }
        }
    }

    /**
     * @param array $sqlCols
     * @return int
     */
    private function updateColumns(array $sqlCols)
    {
        $updatedColumns = 0;
        foreach ($this->properties as $property => $options) {
            if ($this->ignoreColumn($options)) {
                continue;
            }

            $type = $options['type'];
            $sqlType = $this->getDefaultMysqlType(Converter::stringKey('mysql', $options, null), $type);

            $default = Converter::stringKey('default', $options, null);
            $default = isset($default) ? ' DEFAULT ' . $default : '';

            $maxLength = $this->getMaxLength($options);

            $oldProperty = Converter::stringKey('oldName', $options, null);
            $update = Converter::boolKey('update', $options, false);
            $remove = Converter::boolKey('remove', $options, false);

            $autoIncrement = '';
            if ($type == 'primaryKey') {
                $autoIncrement = Converter::boolKey('isAutoIncrement', $options, true) ? 'AUTO_INCREMENT' : '';
                $property = ($this->package == 'App') ? 'app_id' : 'id';
            } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) {
                $oldProperty = ($oldProperty != null) ? $property . 'Id'  : null;
                $property .= '_id';
            }

            if (isset($oldProperty) && in_array($oldProperty, ['', '_id'])) {
                $temp = Converter::toCamelCase($property);
                if ($temp != $property) {
                    $oldProperty = $temp;
                }
            }

            $sqlColumn = $this->sqlColumn($sqlCols, $property);
            $oldSqlColumn = $this->sqlColumn($sqlCols, $oldProperty);

            // remove column
            if ($remove) {
                if (!isset($sqlColumn)) {
                    $this->addInfo(self::INFO_WARNING, 'Column has already been removed: <b>' . $sqlColumn . '</b>');
                } else {
                    $sqlRemove = $this->fieldList('column-remove', $this->table, $sqlColumn);
                    $this->db->execute($sqlRemove);
                    $this->addInfo(self::INFO_SUCCESS, 'Column removed: <b>' . $sqlColumn . '</b>');
                    $updatedColumns++;

                }
                continue;
            }

            // rename column
            if (isset($oldProperty)) {
                if (!isset($oldSqlColumn) && isset($sqlColumn)) {
                    $this->addInfo(self::INFO_ERROR, 'Column <b>' . $oldProperty . '</b> has already been renamed to <b>' . $property . '</b>');
                } else if (!isset($oldSqlColumn)) {
                    $this->addInfo(self::INFO_ERROR, 'Column <b>' . $oldProperty . '</b> cannot be rename because it does not exist');
                } else if (isset($sqlColumn)) {
                    $this->addInfo(self::INFO_ERROR, 'Column <b>' . $oldProperty . '</b> cannot be rename to <b>' . $property . '</b> because <b>' . $property . '</b> does not exist');
                } else {
                    $sqlUpdate = $this->fieldList('column-rename', $this->table, $property, $type, $sqlType, $maxLength, $autoIncrement, $default, $oldProperty);
                    $this->db->execute($sqlUpdate);
                    $this->addInfo(self::INFO_SUCCESS, 'Column renamed from <b>' . $oldProperty . '</b> to <b>' . $property . '</b>');
                    $updatedColumns++;
                }
                continue;
            }
            // update column
            if ($update) {
                if (!isset($sqlColumn)) {
                    $this->addInfo(self::INFO_ERROR, 'Column cannot be updated because it does not exist: <b>' . $property . '</b>');
                } else {
                    $sqlUpdate = $this->fieldList('column-update', $this->table, $property, $type, $sqlType, $maxLength, $autoIncrement, $default);
                    $this->db->execute($sqlUpdate);
                    $this->addInfo(self::INFO_SUCCESS, 'Column updated: <b>' . $property . '</b>');
                    $updatedColumns++;
                }
                continue;
            }
            // add column
            if (!isset($sqlColumn)) {
                $sqlAdd = $this->fieldList('column-add', $this->table, $property, $type, $sqlType, $maxLength, $autoIncrement, $default);
                $this->db->execute($sqlAdd);
                $this->addInfo(self::INFO_SUCCESS, 'Column added: <b>' . $property . '</b>');
                $updatedColumns++;
                continue;
            }
        }
        return $updatedColumns;
    }

    /**
     * @param array $options
     * @return int
     */
    private function getMaxLength(array $options)
    {
        $validations = Converter::arrayKey('validation', $options, []);
        foreach ($validations as $key => $value) {
            if ($key == 'maxLength') {
                return $value;
            }
        }
        return $this->maxLength;
    }

    /**
     * @param $sqlType
     * @param $type
     * @return string
     */
    private function getDefaultMysqlType($sqlType, $type)
    {
        if (isset($sqlType)) {
            return $sqlType;
        }

        if (in_array($type, ['integer', 'primaryKey', 'foreignKey', 'singleKey', 'multipleKey', 'media'])) {
            return 'INT UNSIGNED';
        }
        return $type;
    }

    /**
     * @param array $sqlCols
     * @param $property
     * @return string
     */
    private function sqlColumn(array $sqlCols, $property)
    {
        if (!isset($property) || $property == '') {
            return null;
        }

        foreach ($sqlCols as $column) {
            if (strcasecmp($column['Field'], $property) == 0) {
                return $column['Field'];
            }
        }
        return null;
    }

    /**
     * @param $options
     * @return bool
     */
    private function ignoreColumn($options)
    {
        $type = $options['type'];
        $isPersistent = Converter::boolKey('isPersistent', $options, true);
        if (!$isPersistent || in_array($type, ['foreignKeyLink', 'singleKeyLink'])) {
            return true;
        }
        return false;
    }

    /**
     * @param string $template
     * @param string $table
     * @param string $property
     * @param string $type
     * @param string $sqlType
     * @param int $maxLength
     * @param string $autoIncrement
     * @param string $default = null

     * @param string $oldProperty = null
     * @return string
     */
    private function fieldList($template, $table, $property, $type = null, $sqlType = null, $maxLength = 0, $autoIncrement = '', $default = null, $oldProperty = null)
    {
        $tpl = new Template(Template::ADMIN, ['generator', 'mysql', 'parts']);
        $tpl->set('property', $property);
        $tpl->set('type', $type);
        $tpl->set('sqlType', $sqlType);
        $tpl->set('maxLength', $maxLength);
        $tpl->set('autoIncrement', $autoIncrement);

        if (isset($default)) {
            $tpl->set('default', $default);
        }

        if (isset($table)) {
            $tpl->set('table', $table);
        }

        if (isset($oldProperty)) {
            $tpl->set('oldProperty', $oldProperty);
        }

        return $tpl->render($template);
    }
}
