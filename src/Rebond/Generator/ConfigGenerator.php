<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Generator;

use Rebond\App;
use Rebond\Enums\Core\Code;
use Rebond\Services\Converter;
use Rebond\Services\File;
use Rebond\Services\Log;
use Rebond\Services\Session;
use Rebond\Services\Template;
use Symfony\Component\Yaml\Parser;

class ConfigGenerator
{
    /* @var App */
    private $app;
    /* @var int */
    private $env;

    /**
     * @param App $app
     * @param int $env
     */
    public function __construct(App $app, $env)
    {
        $this->app = $app;
        $this->env = $env;
    }

    /**
     * Generate config file
     * @param string $env
     * @throws \Exception
     */
    public function generate()
    {
        $configFile = $this->app->getPath('own-file') . 'config.yaml';

        if (!file_exists($configFile)) {
            throw new \Exception($configFile, Code::CONFIG_NOT_FOUND);
        }

        $yaml = new Parser();
        $conf = $yaml->parse(file_get_contents($configFile));

        $server = $this->findServer($conf['servers']);

        $configTpl = new Template(Template::ADMIN, ['generator', 'config']);
        $configTpl->set('license', '// auto-generated by Rebond, changes to this files will be lost if the configuration changes');
        $configTpl->set('host', substr($this->getHost(), strlen($server['adminDomain']) + 1));
        $configTpl->set('siteFolder', $server['siteFolder']);
        $configTpl->set('adminFolder', $server['adminFolder']);
        $configTpl->set('siteDomain', $server['siteDomain']);
        $configTpl->set('adminDomain', $server['adminDomain']);
        $configTpl->set('salt', $server['salt']);

        $configTpl->set('dbHost', $server['db']['host']);
        $configTpl->set('dbPort', $server['db']['port']);
        $configTpl->set('dbUsername', $server['db']['username']);
        $configTpl->set('dbPassword', $server['db']['password']);
        $configTpl->set('dbName', $server['db']['name']);

        $configTpl->set('mailType', $server['mail']['type']);
        $configTpl->set('mailEmail', $server['mail']['email']);
        if ($server['mail']['type'] == 'smtp') {
            $configTpl->set('mailHost', $server['mail']['host']);
            $configTpl->set('mailPort', $server['mail']['port']);
            $configTpl->set('mailOption', $server['mail']['option']);
            $configTpl->set('mailPassword', $server['mail']['password']);
        } else {
            $configTpl->set('mailHost', '');
            $configTpl->set('mailPort', '');
            $configTpl->set('mailOption', '');
            $configTpl->set('mailPassword', '');
        }

        $langList = [];
        foreach ($conf['languages'] as $lang => $locale) {
            $tpl = new Template(Template::ADMIN, ['generator', 'config']);
            $tpl->set('lang', $lang);
            $tpl->set('locale', $locale);
            $langList[] = $tpl->render('lang');
        }
        $configTpl->set('lang', implode('', $langList));

        $settings = [];
        foreach ($conf['settings'] as $key => $value) {
            $tpl = new Template(Template::ADMIN, ['generator', 'config']);
            $tpl->set('key', $key);
            $tpl->set('value', $value);
            $settings[] = $tpl->render('setting');
        }
        $configTpl->set('setting', implode('', $settings));
        $configPath = $this->app->getPath('generated-cache') . 'Config.php';
        File::save($configPath, str_replace('<#php', '<?php', $configTpl->render('config')));
        Log::log(Code::LANG_GENERATED, 'Generation of the config file [' . $this->env . ']', __FILE__, __LINE__);
        Session::allSuccess('cache_generated', '/');
    }

    /**
     * @param $servers
     * @return array|null
     * @throws \Exception
     */
    private function findServer($servers)
    {
        $default = Converter::arrayKey('default', $servers);
        if (!isset($default)) {
            throw new \Exception('Common environment not found', Code::CONFIG_ENV_NOT_FOUND);
        }

        $found = null;
        if ($this->env === false) {
            foreach ($servers as $server => $options) {
                if (isset($options['domain']) && strpos($this->getHost(), $options['domain']) !== false) {
                    $found = $servers[$server];
                    break;
                }
            }
        } else {
            $found = Converter::arrayKey($this->env, $servers);
        }

        return !isset($found)
            ? $default
            : array_replace_recursive($default, $found);
    }

    /** @return string */
    private function getHost()
    {
        return isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
    }
}
