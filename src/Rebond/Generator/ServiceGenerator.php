<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Generator;

use Rebond\Services;

class ServiceGenerator extends AbstractGenerator
{
    public function produce()
    {
        $this->addInfo(self::INFO_TITLE, 'Service class');
        $this->produceService();
    }

    private function produceService()
    {
        // Service template
        $serviceTpl = $this->createTemplate('service');

        $serviceTpl->set('namespace', $this->getNamespace(false, false));

        $servicePath = $this->getNamespace(false) . 'Services/' . $this->package . '/' . $this->entity . 'Service.php';
        if (!file_exists($servicePath)) {
            $render = str_replace('<#php', '<?php', $serviceTpl->render('service'));
            Services\File::save($servicePath, $render);
            $this->addInfo(self::INFO_SUCCESS, 'Service class created');
        } else {
            $this->addInfo(self::INFO_WARNING , 'Service class already exists');
        }
    }
}
