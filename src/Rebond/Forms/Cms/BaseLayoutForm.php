<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Layout;
use Rebond\Repository\Cms\LayoutRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseLayoutForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $titleValidator;
    /* @var string */
    protected $titleBuilder;
    /* @var array */
    protected $summaryValidator;
    /* @var string */
    protected $summaryBuilder;
    /* @var array */
    protected $filenameValidator;
    /* @var string */
    protected $filenameBuilder;
    /* @var array */
    protected $columnsValidator;
    /* @var string */
    protected $columnsBuilder;
    /* @var array */
    protected $statusValidator;
    /*
     * @param Layout $model
     * @param string $unique
     */
    public function __construct(Layout $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->titleValidator = ['string' => true, 'alphaNumUnderscore' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->titleBuilder = 'string';
        $this->summaryValidator = ['string' => true, 'maxLength' => 200];
        $this->summaryBuilder = 'string';
        $this->filenameValidator = ['string' => true, 'alphaNumDash' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->filenameBuilder = 'string';
        $this->columnsValidator = ['integer' => true, 'required' => true];
        $this->columnsBuilder = 'integer';
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseLayoutForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $value = Converter::stringKey('title' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitle($value);
            }
        }
        if (!isset($properties) || in_array('summary', $properties)) {
            $value = Converter::stringKey('summary' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setSummary($value);
            }
        }
        if (!isset($properties) || in_array('filename', $properties)) {
            $value = Converter::stringKey('filename' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setFilename($value);
            }
        }
        if (!isset($properties) || in_array('columns', $properties)) {
            $value = Converter::intKey('columns' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setColumns($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('title', $properties)) {
            $fields['title'] = $this->validateTitle();
        }
        if (!isset($properties) || in_array('summary', $properties)) {
            $fields['summary'] = $this->validateSummary();
        }
        if (!isset($properties) || in_array('filename', $properties)) {
            $fields['filename'] = $this->validateFilename();
        }
        if (!isset($properties) || in_array('columns', $properties)) {
            $fields['columns'] = $this->validateColumns();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateTitle()
    {
        return Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
    }

    public function buildTitle()
    {
        return Form::buildField('title' . $this->unique, $this->titleBuilder, $this->getModel()->getTitle());
    }

    public function validateSummary()
    {
        return Validate::validate('summary', $this->getModel()->getSummary(), $this->summaryValidator);
    }

    public function buildSummary()
    {
        return Form::buildField('summary' . $this->unique, $this->summaryBuilder, $this->getModel()->getSummary());
    }

    public function validateFilename()
    {
        return Validate::validate('filename', $this->getModel()->getFilename(), $this->filenameValidator);
    }

    public function buildFilename()
    {
        return Form::buildField('filename' . $this->unique, $this->filenameBuilder, $this->getModel()->getFilename());
    }

    public function validateColumns()
    {
        return Validate::validate('columns', $this->getModel()->getColumns(), $this->columnsValidator);
    }

    public function buildColumns()
    {
        return Form::buildField('columns' . $this->unique, $this->columnsBuilder, $this->getModel()->getColumns());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
