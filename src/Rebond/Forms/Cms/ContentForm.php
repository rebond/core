<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Content;
use Rebond\Repository\Cms\FilterRepository;
use Rebond\Services\Form;

class ContentForm extends BaseContentForm
{
    /**
     * @param Content $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }

    public function buildFilter()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = FilterRepository::getList(['id', 'title']);
        $options['where'][] = ['filter.module_id = ?', $this->getModel()->getModuleId()];
        $options['order'][] = 'filter.display_order, filter.title';
        $items = FilterRepository::loadAll($options);
        return Form::buildItemList('filterId' . $this->unique, $items, 'id', 'title', $this->getModel()->getFilterId());
    }
}
