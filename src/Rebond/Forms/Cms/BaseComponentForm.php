<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Component;
use Rebond\Repository\Cms\ComponentRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseComponentForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $titleValidator;
    /* @var string */
    protected $titleBuilder;
    /* @var array */
    protected $moduleValidator;
    /* @var array */
    protected $summaryValidator;
    /* @var string */
    protected $summaryBuilder;
    /* @var array */
    protected $methodValidator;
    /* @var string */
    protected $methodBuilder;
    /* @var array */
    protected $canBeCachedValidator;
    /* @var array */
    protected $typeValidator;
    /* @var array */
    protected $statusValidator;
    /*
     * @param Component $model
     * @param string $unique
     */
    public function __construct(Component $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->titleValidator = ['string' => true, 'alphaNumUnderscore' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->titleBuilder = 'string';
        $this->moduleValidator = ['foreignKey' => true];
        $this->summaryValidator = ['string' => true, 'required' => true, 'maxLength' => 200];
        $this->summaryBuilder = 'string';
        $this->methodValidator = ['string' => true, 'alphaNum' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->methodBuilder = 'string';
        $this->canBeCachedValidator = ['bool' => true];
        $this->typeValidator = ['enum' => true];
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseComponentForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $value = Converter::stringKey('title' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitle($value);
            }
        }
        if (!isset($properties) || in_array('moduleId', $properties)) {
            $value = Converter::intKey('moduleId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setModuleId($value);
            }
        }
        if (!isset($properties) || in_array('summary', $properties)) {
            $value = Converter::stringKey('summary' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setSummary($value);
            }
        }
        if (!isset($properties) || in_array('method', $properties)) {
            $value = Converter::stringKey('method' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setMethod($value);
            }
        }
        if (!isset($properties) || in_array('canBeCached', $properties)) {
            $value = Converter::boolKey('canBeCached' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setCanBeCached($value);
            }
        }
        if (!isset($properties) || in_array('type', $properties)) {
            $value = Converter::intKey('type' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setType($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('title', $properties)) {
            $fields['title'] = $this->validateTitle();
        }
        if (!isset($properties) || in_array('moduleId', $properties)) {
            $fields['module'] = $this->validateModule();
        }
        if (!isset($properties) || in_array('summary', $properties)) {
            $fields['summary'] = $this->validateSummary();
        }
        if (!isset($properties) || in_array('method', $properties)) {
            $fields['method'] = $this->validateMethod();
        }
        if (!isset($properties) || in_array('type', $properties)) {
            $fields['type'] = $this->validateType();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateTitle()
    {
        return Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
    }

    public function buildTitle()
    {
        return Form::buildField('title' . $this->unique, $this->titleBuilder, $this->getModel()->getTitle());
    }

    public function validateModule()
    {
        return Validate::validate('module', $this->getModel()->getModuleId(), $this->moduleValidator);
    }

    public function buildModule()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Cms\ModuleRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Cms\ModuleRepository::loadAll($options);
        return Form::buildItemList('moduleId' . $this->unique, $models, 'id', 'title', $this->getModel()->getModuleId(), $this->moduleValidator['foreignKey']);
    }

    public function validateSummary()
    {
        return Validate::validate('summary', $this->getModel()->getSummary(), $this->summaryValidator);
    }

    public function buildSummary()
    {
        return Form::buildField('summary' . $this->unique, $this->summaryBuilder, $this->getModel()->getSummary());
    }

    public function validateMethod()
    {
        return Validate::validate('method', $this->getModel()->getMethod(), $this->methodValidator);
    }

    public function buildMethod()
    {
        return Form::buildField('method' . $this->unique, $this->methodBuilder, $this->getModel()->getMethod());
    }

    public function buildCanBeCached()
    {
        return Form::buildBoolean('canBeCached' . $this->unique, 'can_be_cached', $this->getModel()->getCanBeCached());
    }

    public function validateType()
    {
        return Validate::validate('type', $this->getModel()->getType(), $this->typeValidator);
    }

    public function buildType()
    {
        return Form::buildList('type' . $this->unique, $this->getModel()->getTypeList(), $this->getModel()->getType());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
