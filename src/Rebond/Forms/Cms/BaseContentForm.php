<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Content;
use Rebond\Repository\Cms\ContentRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseContentForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $titleValidator;
    /* @var string */
    protected $titleBuilder;
    /* @var array */
    protected $moduleValidator;
    /* @var array */
    protected $appIdValidator;
    /* @var string */
    protected $appIdBuilder;
    /* @var array */
    protected $contentGroupValidator;
    /* @var string */
    protected $contentGroupBuilder;
    /* @var array */
    protected $filterValidator;
    /* @var array */
    protected $authorValidator;
    /* @var array */
    protected $publisherValidator;
    /* @var array */
    protected $urlFriendlyTitleValidator;
    /* @var string */
    protected $urlFriendlyTitleBuilder;
    /* @var array */
    protected $useExpirationValidator;
    /* @var array */
    protected $goLiveDateValidator;
    /* @var string */
    protected $goLiveDateBuilder;
    /* @var array */
    protected $expiryDateValidator;
    /* @var string */
    protected $expiryDateBuilder;
    /* @var array */
    protected $publishedDateValidator;
    /* @var string */
    protected $publishedDateBuilder;
    /* @var array */
    protected $versionValidator;
    /*
     * @param Content $model
     * @param string $unique
     */
    public function __construct(Content $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->titleValidator = ['string' => true, 'required' => true, 'maxLength' => 80];
        $this->titleBuilder = 'string';
        $this->moduleValidator = ['foreignKey' => true];
        $this->appIdValidator = ['integer' => true];
        $this->appIdBuilder = 'integer';
        $this->contentGroupValidator = ['integer' => true];
        $this->contentGroupBuilder = 'integer';
        $this->filterValidator = ['foreignKey' => false];
        $this->authorValidator = ['foreignKey' => true];
        $this->publisherValidator = ['foreignKey' => false];
        $this->urlFriendlyTitleValidator = ['string' => true, 'alphaNumDash' => true, 'maxLength' => 80];
        $this->urlFriendlyTitleBuilder = 'string';
        $this->useExpirationValidator = ['bool' => true];
        $this->goLiveDateValidator = ['datetime' => true];
        $this->goLiveDateBuilder = 'datetime';
        $this->expiryDateValidator = ['datetime' => true];
        $this->expiryDateBuilder = 'datetime';
        $this->publishedDateValidator = ['datetime' => true];
        $this->publishedDateBuilder = 'datetime';
        $this->versionValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseContentForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $value = Converter::stringKey('title' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitle($value);
            }
        }
        if (!isset($properties) || in_array('moduleId', $properties)) {
            $value = Converter::intKey('moduleId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setModuleId($value);
            }
        }
        if (!isset($properties) || in_array('appId', $properties)) {
            $value = Converter::intKey('appId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setAppId($value);
            }
        }
        if (!isset($properties) || in_array('contentGroup', $properties)) {
            $value = Converter::intKey('contentGroup' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setContentGroup($value);
            }
        }
        if (!isset($properties) || in_array('filterId', $properties)) {
            $value = Converter::intKey('filterId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setFilterId($value);
            }
        }
        if (!isset($properties) || in_array('authorId', $properties)) {
            $value = Converter::intKey('authorId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setAuthorId($value);
            }
        }
        if (!isset($properties) || in_array('publisherId', $properties)) {
            $value = Converter::intKey('publisherId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setPublisherId($value);
            }
        }
        if (!isset($properties) || in_array('urlFriendlyTitle', $properties)) {
            $value = Converter::stringKey('urlFriendlyTitle' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setUrlFriendlyTitle($value);
            }
        }
        if (!isset($properties) || in_array('useExpiration', $properties)) {
            $value = Converter::boolKey('useExpiration' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setUseExpiration($value);
            }
        }
        if (!isset($properties) || in_array('goLiveDate', $properties)) {
            $value = Converter::dateKey('goLiveDate' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setGoLiveDate($value);
            }
        }
        if (!isset($properties) || in_array('expiryDate', $properties)) {
            $value = Converter::dateKey('expiryDate' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setExpiryDate($value);
            }
        }
        if (!isset($properties) || in_array('publishedDate', $properties)) {
            $value = Converter::dateKey('publishedDate' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setPublishedDate($value);
            }
        }
        if (!isset($properties) || in_array('version', $properties)) {
            $value = Converter::intKey('version' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setVersion($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('title', $properties)) {
            $fields['title'] = $this->validateTitle();
        }
        if (!isset($properties) || in_array('moduleId', $properties)) {
            $fields['module'] = $this->validateModule();
        }
        if (!isset($properties) || in_array('appId', $properties)) {
            $fields['appId'] = $this->validateAppId();
        }
        if (!isset($properties) || in_array('contentGroup', $properties)) {
            $fields['contentGroup'] = $this->validateContentGroup();
        }
        if (!isset($properties) || in_array('filterId', $properties)) {
            $fields['filter'] = $this->validateFilter();
        }
        if (!isset($properties) || in_array('authorId', $properties)) {
            $fields['author'] = $this->validateAuthor();
        }
        if (!isset($properties) || in_array('publisherId', $properties)) {
            $fields['publisher'] = $this->validatePublisher();
        }
        if (!isset($properties) || in_array('urlFriendlyTitle', $properties)) {
            $fields['urlFriendlyTitle'] = $this->validateUrlFriendlyTitle();
        }
        if (!isset($properties) || in_array('goLiveDate', $properties)) {
            $fields['goLiveDate'] = $this->validateGoLiveDate();
        }
        if (!isset($properties) || in_array('expiryDate', $properties)) {
            $fields['expiryDate'] = $this->validateExpiryDate();
        }
        if (!isset($properties) || in_array('publishedDate', $properties)) {
            $fields['publishedDate'] = $this->validatePublishedDate();
        }
        if (!isset($properties) || in_array('version', $properties)) {
            $fields['version'] = $this->validateVersion();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateTitle()
    {
        return Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
    }

    public function buildTitle()
    {
        return Form::buildField('title' . $this->unique, $this->titleBuilder, $this->getModel()->getTitle());
    }

    public function validateModule()
    {
        return Validate::validate('module', $this->getModel()->getModuleId(), $this->moduleValidator);
    }

    public function buildModule()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Cms\ModuleRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Cms\ModuleRepository::loadAll($options);
        return Form::buildItemList('moduleId' . $this->unique, $models, 'id', 'title', $this->getModel()->getModuleId(), $this->moduleValidator['foreignKey']);
    }

    public function validateAppId()
    {
        return Validate::validate('appId', $this->getModel()->getAppId(), $this->appIdValidator);
    }

    public function buildAppId()
    {
        return Form::buildField('appId' . $this->unique, $this->appIdBuilder, $this->getModel()->getAppId());
    }

    public function validateContentGroup()
    {
        return Validate::validate('contentGroup', $this->getModel()->getContentGroup(), $this->contentGroupValidator);
    }

    public function buildContentGroup()
    {
        return Form::buildField('contentGroup' . $this->unique, $this->contentGroupBuilder, $this->getModel()->getContentGroup());
    }

    public function validateFilter()
    {
        return Validate::validate('filter', $this->getModel()->getFilterId(), $this->filterValidator);
    }

    public function buildFilter()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Cms\FilterRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Cms\FilterRepository::loadAll($options);
        return Form::buildItemList('filterId' . $this->unique, $models, 'id', 'title', $this->getModel()->getFilterId(), $this->filterValidator['foreignKey']);
    }

    public function validateAuthor()
    {
        return Validate::validate('author', $this->getModel()->getAuthorId(), $this->authorValidator);
    }

    public function buildAuthor()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\UserRepository::getList(['id', 'email']);
        $models = \Rebond\Repository\Core\UserRepository::loadAll($options);
        return Form::buildItemList('authorId' . $this->unique, $models, 'id', 'email', $this->getModel()->getAuthorId(), $this->authorValidator['foreignKey']);
    }

    public function validatePublisher()
    {
        return Validate::validate('publisher', $this->getModel()->getPublisherId(), $this->publisherValidator);
    }

    public function buildPublisher()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\UserRepository::getList(['id', 'email']);
        $models = \Rebond\Repository\Core\UserRepository::loadAll($options);
        return Form::buildItemList('publisherId' . $this->unique, $models, 'id', 'email', $this->getModel()->getPublisherId(), $this->publisherValidator['foreignKey']);
    }

    public function validateUrlFriendlyTitle()
    {
        return Validate::validate('urlFriendlyTitle', $this->getModel()->getUrlFriendlyTitle(), $this->urlFriendlyTitleValidator);
    }

    public function buildUrlFriendlyTitle()
    {
        return Form::buildField('urlFriendlyTitle' . $this->unique, $this->urlFriendlyTitleBuilder, $this->getModel()->getUrlFriendlyTitle());
    }

    public function buildUseExpiration()
    {
        return Form::buildBoolean('useExpiration' . $this->unique, 'use_expiration', $this->getModel()->getUseExpiration());
    }

    public function validateGoLiveDate()
    {
        return Validate::validate('goLiveDate', $this->getModel()->getGoLiveDate(), $this->goLiveDateValidator);
    }

    public function buildGoLiveDate()
    {
        return Form::buildField('goLiveDate' . $this->unique, $this->goLiveDateBuilder, $this->getModel()->getGoLiveDate());
    }

    public function validateExpiryDate()
    {
        return Validate::validate('expiryDate', $this->getModel()->getExpiryDate(), $this->expiryDateValidator);
    }

    public function buildExpiryDate()
    {
        return Form::buildField('expiryDate' . $this->unique, $this->expiryDateBuilder, $this->getModel()->getExpiryDate());
    }

    public function validatePublishedDate()
    {
        return Validate::validate('publishedDate', $this->getModel()->getPublishedDate(), $this->publishedDateValidator);
    }

    public function buildPublishedDate()
    {
        return Form::buildField('publishedDate' . $this->unique, $this->publishedDateBuilder, $this->getModel()->getPublishedDate());
    }

    public function validateVersion()
    {
        return Validate::validate('version', $this->getModel()->getVersion(), $this->versionValidator);
    }

    public function buildVersion()
    {
        return Form::buildList('version' . $this->unique, $this->getModel()->getVersionList(), $this->getModel()->getVersion());
    }

}
