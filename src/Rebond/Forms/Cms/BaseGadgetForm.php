<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Gadget;
use Rebond\Repository\Cms\GadgetRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseGadgetForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $pageValidator;
    /* @var array */
    protected $componentValidator;
    /* @var array */
    protected $colValidator;
    /* @var string */
    protected $colBuilder;
    /* @var array */
    protected $filterValidator;
    /* @var array */
    protected $customFilterValidator;
    /* @var string */
    protected $customFilterBuilder;
    /* @var array */
    protected $displayOrderValidator;
    /* @var string */
    protected $displayOrderBuilder;
    /* @var array */
    protected $statusValidator;
    /*
     * @param Gadget $model
     * @param string $unique
     */
    public function __construct(Gadget $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->pageValidator = ['foreignKey' => true];
        $this->componentValidator = ['foreignKey' => true];
        $this->colValidator = ['integer' => true, 'required' => true];
        $this->colBuilder = 'integer';
        $this->filterValidator = ['foreignKey' => false];
        $this->customFilterValidator = ['string' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->customFilterBuilder = 'string';
        $this->displayOrderValidator = ['integer' => true, 'required' => true];
        $this->displayOrderBuilder = 'integer';
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseGadgetForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('pageId', $properties)) {
            $value = Converter::intKey('pageId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setPageId($value);
            }
        }
        if (!isset($properties) || in_array('componentId', $properties)) {
            $value = Converter::intKey('componentId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setComponentId($value);
            }
        }
        if (!isset($properties) || in_array('col', $properties)) {
            $value = Converter::intKey('col' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setCol($value);
            }
        }
        if (!isset($properties) || in_array('filterId', $properties)) {
            $value = Converter::intKey('filterId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setFilterId($value);
            }
        }
        if (!isset($properties) || in_array('customFilter', $properties)) {
            $value = Converter::stringKey('customFilter' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setCustomFilter($value);
            }
        }
        if (!isset($properties) || in_array('displayOrder', $properties)) {
            $value = Converter::intKey('displayOrder' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setDisplayOrder($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('pageId', $properties)) {
            $fields['page'] = $this->validatePage();
        }
        if (!isset($properties) || in_array('componentId', $properties)) {
            $fields['component'] = $this->validateComponent();
        }
        if (!isset($properties) || in_array('col', $properties)) {
            $fields['col'] = $this->validateCol();
        }
        if (!isset($properties) || in_array('filterId', $properties)) {
            $fields['filter'] = $this->validateFilter();
        }
        if (!isset($properties) || in_array('customFilter', $properties)) {
            $fields['customFilter'] = $this->validateCustomFilter();
        }
        if (!isset($properties) || in_array('displayOrder', $properties)) {
            $fields['displayOrder'] = $this->validateDisplayOrder();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validatePage()
    {
        return Validate::validate('page', $this->getModel()->getPageId(), $this->pageValidator);
    }

    public function buildPage()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Cms\PageRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Cms\PageRepository::loadAll($options);
        return Form::buildItemList('pageId' . $this->unique, $models, 'id', 'title', $this->getModel()->getPageId(), $this->pageValidator['foreignKey']);
    }

    public function validateComponent()
    {
        return Validate::validate('component', $this->getModel()->getComponentId(), $this->componentValidator);
    }

    public function buildComponent()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Cms\ComponentRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Cms\ComponentRepository::loadAll($options);
        return Form::buildItemList('componentId' . $this->unique, $models, 'id', 'title', $this->getModel()->getComponentId(), $this->componentValidator['foreignKey']);
    }

    public function validateCol()
    {
        return Validate::validate('col', $this->getModel()->getCol(), $this->colValidator);
    }

    public function buildCol()
    {
        return Form::buildField('col' . $this->unique, $this->colBuilder, $this->getModel()->getCol());
    }

    public function validateFilter()
    {
        return Validate::validate('filter', $this->getModel()->getFilterId(), $this->filterValidator);
    }

    public function buildFilter()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Cms\FilterRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Cms\FilterRepository::loadAll($options);
        return Form::buildItemList('filterId' . $this->unique, $models, 'id', 'title', $this->getModel()->getFilterId(), $this->filterValidator['foreignKey']);
    }

    public function validateCustomFilter()
    {
        return Validate::validate('customFilter', $this->getModel()->getCustomFilter(), $this->customFilterValidator);
    }

    public function buildCustomFilter()
    {
        return Form::buildField('customFilter' . $this->unique, $this->customFilterBuilder, $this->getModel()->getCustomFilter());
    }

    public function validateDisplayOrder()
    {
        return Validate::validate('displayOrder', $this->getModel()->getDisplayOrder(), $this->displayOrderValidator);
    }

    public function buildDisplayOrder()
    {
        return Form::buildField('displayOrder' . $this->unique, $this->displayOrderBuilder, $this->getModel()->getDisplayOrder());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
