<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Gadget;
use Rebond\Repository\Cms\ComponentRepository;
use Rebond\Services\Form;
use Rebond\Services\Lang;

class GadgetForm extends BaseGadgetForm
{
    /**
     * @param Gadget $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }

    public function buildFullComponent()
    {
        $items = ComponentRepository::loadFullComponents();
        $list = [];
        foreach ($items as $item) {
            $list[$item->getId()] = Lang::lang($item->getModule()->getTitle()) . ' - ' . Lang::lang($item->getTitle());
        }
        return Form::buildList('componentId' . $this->unique, $list, $this->getModel()->getComponentId());
    }
}
