<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Forms\Cms;

use Rebond\Enums\Core\Result;
use Rebond\Models\Cms\Page;
use Rebond\Models\Field;
use Rebond\Repository\Cms\LayoutRepository;
use Rebond\Repository\Cms\PageRepository;
use Rebond\Repository\Cms\TemplateRepository;
use Rebond\Services\Cms\PageService;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class PageForm extends BasePageForm
{
    /**
     * @param Page $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }

    public function validateParent()
    {
        if ($this->getModel()->getId() == 1) {
            $this->getModel()->setParentId(0);
            return new Field('parent');
        }

        $v = Validate::validate('parent', $this->getModel()->getParentId(), $this->parentValidator);
        if ($v->getResult() == Result::ERROR) {
            return $v;
        }

        if ($this->getModel()->getParentId() == $this->getModel()->getId()) {
            $v->setResult(Result::ERROR);
            $v->setMessage('the parent page cannot be itself');
        }
        return $v;
    }

    public function buildParent()
    {
        if ($this->getModel()->getId() == 1) {
            return '<span class="input">' . Lang::lang('home') . '</span>';
        }

        $items = PageService::renderList();
        $disabled = [];
        $disabled[] = $this->getModel()->getId();
        return Form::buildList('parentId' . $this->unique, $items, $this->getModel()->getParentId(), true, $disabled);
    }

    public function validateFriendlyUrl()
    {
        $v = Validate::validate('friendlyUrl', $this->getModel()->getFriendlyUrl(), $this->friendlyUrlValidator);
        if ($v->getResult() == Result::ERROR) {
            return $v;
        }

        $options = [];
        $options['where'][] = ['page.friendly_url = ?', $this->getModel()->getFriendlyUrl()];
        $options['where'][] = ['page.id != ?', $this->getModel()->getId()];
        $options['where'][] = 'page.status IN (0,1)';
        $exist = PageRepository::count($options);
        if ($exist >= 1) {
            $v->setResult(Result::ERROR);
            $v->setMessage('a page already exists with this url friendly title');
        }
        return $v;
    }

    public function buildTemplate()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = TemplateRepository::getList(['id', 'title']);
        $models = TemplateRepository::loadAll($options);
        return Form::buildItemList('templateId' . $this->unique, $models, 'id', 'title',
            $this->getModel()->getTemplateId(), $this->templateValidator['foreignKey'], true);
    }

    public function buildLayout()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = LayoutRepository::getList(['id', 'title']);
        $models = LayoutRepository::loadAll($options);
        return Form::buildItemList('layoutId' . $this->unique, $models, 'id', 'title',
            $this->getModel()->getLayoutId(), $this->layoutValidator['foreignKey'], true);
    }
}
