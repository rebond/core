<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Module;

class ModuleForm extends BaseModuleForm
{
    /**
     * @param Module $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }
}
