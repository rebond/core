<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Cms;

use Rebond\Models\Cms\Component;
use Rebond\Repository\Cms\ModuleRepository;
use Rebond\Services\Lang;

class ComponentForm extends BaseComponentForm
{
    /**
     * @param Component $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }

    public function buildModule()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = ModuleRepository::getList(['id', 'has_content', 'title']);
        $options['where'][] = 'module.status = 1';
        $options['order'][] = 'module.title';
        $items = ModuleRepository::loadAll($options);

        $form = '<select class="input" name="moduleId" id="moduleId">';
        foreach ($items as $item) {
            $selected = ($item->getId() == $this->getModel()->getModuleId()) ? ' selected="selected"' : '';
            $form .= '<option value="' . $item->getId() . '" data-app="' . $item->getHasContent() . '"' . $selected . '>' . Lang::lang($item->getTitle()) . '</option>';
        }
        $form .= '</select>';
        return $form;
    }
}
