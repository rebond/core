<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Models\Core\User;
use Rebond\Repository\Core\UserRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseUserForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $emailValidator;
    /* @var string */
    protected $emailBuilder;
    /* @var array */
    protected $passwordValidator;
    /* @var string */
    protected $passwordBuilder;
    /* @var array */
    protected $firstNameValidator;
    /* @var string */
    protected $firstNameBuilder;
    /* @var array */
    protected $lastNameValidator;
    /* @var string */
    protected $lastNameBuilder;
    /* @var array */
    protected $avatarValidator;
    /* @var string */
    protected $avatarBuilder;
    /* @var array */
    protected $isAdminValidator;
    /* @var array */
    protected $isDevValidator;
    /* @var array */
    protected $statusValidator;
    /*
     * @param User $model
     * @param string $unique
     */
    public function __construct(User $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->emailValidator = ['email' => true, 'required' => true, 'minLength' => 5, 'maxLength' => 100];
        $this->emailBuilder = 'email';
        $this->passwordValidator = ['password' => true, 'required' => true, 'minLength' => 2, 'maxLength' => 100];
        $this->passwordBuilder = 'password';
        $this->firstNameValidator = ['string' => true, 'required' => true, 'minLength' => 1, 'maxLength' => 40];
        $this->firstNameBuilder = 'string';
        $this->lastNameValidator = ['string' => true, 'required' => true, 'minLength' => 1, 'maxLength' => 40];
        $this->lastNameBuilder = 'string';
        $this->avatarValidator = ['media' => false, 'image' => true];
        $this->avatarBuilder = 'media';
        $this->isAdminValidator = ['bool' => true];
        $this->isDevValidator = ['bool' => true];
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseUserForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('email', $properties)) {
            $value = Converter::stringKey('email' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setEmail($value);
            }
        }
        if (!isset($properties) || in_array('password', $properties)) {
            $value = Converter::stringKey('password' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setPassword($value);
            }
        }
        if (!isset($properties) || in_array('firstName', $properties)) {
            $value = Converter::stringKey('firstName' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setFirstName($value);
            }
        }
        if (!isset($properties) || in_array('lastName', $properties)) {
            $value = Converter::stringKey('lastName' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setLastName($value);
            }
        }
        if (!isset($properties) || in_array('avatarId', $properties)) {
            $value = Converter::intKey('avatarId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setAvatarId($value);
            }
        }
        if (!isset($properties) || in_array('isAdmin', $properties)) {
            $value = Converter::boolKey('isAdmin' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setIsAdmin($value);
            }
        }
        if (!isset($properties) || in_array('isDev', $properties)) {
            $value = Converter::boolKey('isDev' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setIsDev($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('email', $properties)) {
            $fields['email'] = $this->validateEmail();
        }
        if (!isset($properties) || in_array('password', $properties)) {
            $fields['password'] = $this->validatePassword();
        }
        if (!isset($properties) || in_array('firstName', $properties)) {
            $fields['firstName'] = $this->validateFirstName();
        }
        if (!isset($properties) || in_array('lastName', $properties)) {
            $fields['lastName'] = $this->validateLastName();
        }
        if (!isset($properties) || in_array('avatarId', $properties)) {
            $fields['avatar'] = $this->validateAvatar();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateEmail($checkExisting = true)
    {
        $vrf = Validate::validate('email', $this->getModel()->getEmail(), $this->emailValidator);
        if ($vrf->getResult() == Result::ERROR) {
            return $vrf;
        }
        if ($checkExisting && \Rebond\Repository\Core\UserRepository::emailExists($this->getModel()->getEmail(), $this->getModel()->getId()) > 0) {
            $vrf->setResult(Result::ERROR);
            $vrf->setMessage(Lang::lang('email_exist'));
        }
        return $vrf;
    }

    public function buildEmail()
    {
        return Form::buildField('email' . $this->unique, $this->emailBuilder, $this->getModel()->getEmail());
    }

    public function validatePassword($confirm = null)
    {
        if (isset($confirm)) {
            $this->passwordValidator[] = ['equal' => $confirm];
        }
        return Validate::validate('password', $this->getModel()->getPassword(), $this->passwordValidator);
    }

    public function buildPassword($confirm = '')
    {
        return Form::buildField('password' . $confirm . $this->unique, $this->passwordBuilder, $this->getModel()->getPassword());
    }

    public function validateFirstName()
    {
        return Validate::validate('firstName', $this->getModel()->getFirstName(), $this->firstNameValidator);
    }

    public function buildFirstName()
    {
        return Form::buildField('firstName' . $this->unique, $this->firstNameBuilder, $this->getModel()->getFirstName());
    }

    public function validateLastName()
    {
        return Validate::validate('lastName', $this->getModel()->getLastName(), $this->lastNameValidator);
    }

    public function buildLastName()
    {
        return Form::buildField('lastName' . $this->unique, $this->lastNameBuilder, $this->getModel()->getLastName());
    }

    public function validateAvatar()
    {
        return Validate::validate('avatar', $this->getModel()->getAvatarId(), $this->avatarValidator);
    }

    public function buildAvatar($isMedia = true)
    {
        if ($isMedia) {
            return Form::buildMedia('avatarId' . $this->unique, $this->getModel()->getAvatar());
        }
        return Form::buildField('avatarId' . $this->unique, $this->avatarBuilder, $this->getModel()->getAvatar());
    }

    public function buildIsAdmin()
    {
        return Form::buildBoolean('isAdmin' . $this->unique, 'is_admin', $this->getModel()->getIsAdmin());
    }

    public function buildIsDev()
    {
        return Form::buildBoolean('isDev' . $this->unique, 'is_dev', $this->getModel()->getIsDev());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
