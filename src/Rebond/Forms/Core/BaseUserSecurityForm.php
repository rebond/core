<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Models\Core\UserSecurity;
use Rebond\Repository\Core\UserSecurityRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseUserSecurityForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $userValidator;
    /* @var array */
    protected $secureValidator;
    /* @var string */
    protected $secureBuilder;
    /* @var array */
    protected $typeValidator;
    /* @var array */
    protected $expiryDateValidator;
    /* @var string */
    protected $expiryDateBuilder;
    /*
     * @param UserSecurity $model
     * @param string $unique
     */
    public function __construct(UserSecurity $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->userValidator = ['foreignKey' => true];
        $this->secureValidator = ['string' => true, 'alphaNum' => true, 'maxLength' => 100];
        $this->secureBuilder = 'string';
        $this->typeValidator = ['enum' => true];
        $this->expiryDateValidator = ['date' => true];
        $this->expiryDateBuilder = 'date';
    }

    /**
     * @param array $properties = null
     * @return BaseUserSecurityForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('userId', $properties)) {
            $value = Converter::intKey('userId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setUserId($value);
            }
        }
        if (!isset($properties) || in_array('secure', $properties)) {
            $value = Converter::stringKey('secure' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setSecure($value);
            }
        }
        if (!isset($properties) || in_array('type', $properties)) {
            $value = Converter::intKey('type' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setType($value);
            }
        }
        if (!isset($properties) || in_array('expiryDate', $properties)) {
            $value = Converter::dateKey('expiryDate' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setExpiryDate($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('userId', $properties)) {
            $fields['user'] = $this->validateUser();
        }
        if (!isset($properties) || in_array('secure', $properties)) {
            $fields['secure'] = $this->validateSecure();
        }
        if (!isset($properties) || in_array('type', $properties)) {
            $fields['type'] = $this->validateType();
        }
        if (!isset($properties) || in_array('expiryDate', $properties)) {
            $fields['expiryDate'] = $this->validateExpiryDate();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateUser()
    {
        return Validate::validate('user', $this->getModel()->getUserId(), $this->userValidator);
    }

    public function buildUser()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\UserRepository::getList(['id', 'email']);
        $models = \Rebond\Repository\Core\UserRepository::loadAll($options);
        return Form::buildItemList('userId' . $this->unique, $models, 'id', 'email', $this->getModel()->getUserId(), $this->userValidator['foreignKey']);
    }

    public function validateSecure()
    {
        return Validate::validate('secure', $this->getModel()->getSecure(), $this->secureValidator);
    }

    public function buildSecure()
    {
        return Form::buildField('secure' . $this->unique, $this->secureBuilder, $this->getModel()->getSecure());
    }

    public function validateType()
    {
        return Validate::validate('type', $this->getModel()->getType(), $this->typeValidator);
    }

    public function buildType()
    {
        return Form::buildList('type' . $this->unique, $this->getModel()->getTypeList(), $this->getModel()->getType());
    }

    public function validateExpiryDate()
    {
        return Validate::validate('expiryDate', $this->getModel()->getExpiryDate(), $this->expiryDateValidator);
    }

    public function buildExpiryDate()
    {
        return Form::buildField('expiryDate' . $this->unique, $this->expiryDateBuilder, $this->getModel()->getExpiryDate());
    }

}
