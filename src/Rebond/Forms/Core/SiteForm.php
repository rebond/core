<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\App;
use Rebond\Models\Core\Site;
use Rebond\Services\File;
use Rebond\Services\Form;

class SiteForm extends BaseSiteForm
{
    /**
     * @param Site $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }

    public function buildSkin()
    {
        $skins = File::getFolders(App::instance()->getConfig('site') . '/css/skin');
        $list = [];
        foreach ($skins as $skin) {
            $list[$skin] = $skin;
        }
        return Form::buildList('skin' . $this->unique, $list, $this->getModel()->getSkin());
    }
}
