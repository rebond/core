<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Models\Core\UserRole;
use Rebond\Repository\Core\UserRoleRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseUserRoleForm extends AbstractForm
{
    /* @var array */
    protected $userValidator;
    /* @var string */
    protected $userBuilder;
    /* @var array */
    protected $roleValidator;
    /* @var string */
    protected $roleBuilder;
    /*
     * @param UserRole $model
     * @param string $unique
     */
    public function __construct(UserRole $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->userValidator = ['multipleKey' => true];
        $this->userBuilder = 'multipleKey';
        $this->roleValidator = ['multipleKey' => true];
        $this->roleBuilder = 'multipleKey';
    }

    /**
     * @param array $properties = null
     * @return BaseUserRoleForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('userId', $properties)) {
            $value = Converter::intKey('userId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setUserId($value);
            }
        }
        if (!isset($properties) || in_array('roleId', $properties)) {
            $value = Converter::intKey('roleId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setRoleId($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('userId', $properties)) {
            $fields['user'] = $this->validateUser();
        }
        if (!isset($properties) || in_array('roleId', $properties)) {
            $fields['role'] = $this->validateRole();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function validateUser()
    {
        return Validate::validate('user', $this->getModel()->getUserId(), $this->userValidator);
    }

    public function buildUser()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\UserRepository::getList(['id', 'email']);
        $models = \Rebond\Repository\Core\UserRepository::loadAll($options);
        return Form::buildItemList('userId' . $this->unique, $models, 'id', 'email', $this->getModel()->getUserId(), $this->userValidator['multipleKey']);
    }

    public function buildUserList()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\UserRepository::getList(['id', 'email']);
        $allItems = \Rebond\Repository\Core\UserRepository::loadAll($options);
        $items = UserRoleRepository::loadAllByRoleId($this->getModel()->getRoleId());
        $selectedValues = [];
        foreach ($items as $item) {
            $selectedValues[] = $item->getUserId();
        }
        return Form::buildCheckboxList('user' . $this->unique, $allItems, 'id', 'email', $selectedValues);
    }

    public function validateRole()
    {
        return Validate::validate('role', $this->getModel()->getRoleId(), $this->roleValidator);
    }

    public function buildRole()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\RoleRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Core\RoleRepository::loadAll($options);
        return Form::buildItemList('roleId' . $this->unique, $models, 'id', 'title', $this->getModel()->getRoleId(), $this->roleValidator['multipleKey']);
    }

    public function buildRoleList()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\RoleRepository::getList(['id', 'title']);
        $allItems = \Rebond\Repository\Core\RoleRepository::loadAll($options);
        $items = UserRoleRepository::loadAllByUserId($this->getModel()->getUserId());
        $selectedValues = [];
        foreach ($items as $item) {
            $selectedValues[] = $item->getRoleId();
        }
        return Form::buildCheckboxList('role' . $this->unique, $allItems, 'id', 'title', $selectedValues);
    }

}
