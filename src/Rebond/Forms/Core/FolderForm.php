<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Enums\Core\Result;
use Rebond\Models\Core\Folder;
use Rebond\Repository\Core\FolderRepository;
use Rebond\Services\Core\FolderService;
use Rebond\Services\Form;
use Rebond\Services\Validate;

class FolderForm extends BaseFolderForm
{
    /**
     * @param Folder $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->init();
    }

    public function init()
    {
    }

    public function validateParent()
    {
        $v = Validate::validate('parent', $this->getModel()->getParentId(), $this->parentValidator);
        if ($v->getResult() == Result::ERROR)
            return $v;

        if ($this->getModel()->getParentId() == $this->getModel()->getId()) {
            $v->setResult(Result::ERROR);
            $v->setMessage('a folder cannot be moved to itself');
        }
        return $v;
    }

    public function buildParent()
    {
        if ($this->getModel()->getId() == 1)
            return 'root';

        $items = FolderService::renderList();
        return Form::buildList('parentId' . $this->unique, $items, $this->getModel()->getParentId(), true, [$this->getModel()->getId()]);
    }

    public function validateTitle()
    {
        $v = Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
        if ($v->getResult() == Result::ERROR)
            return $v;

        $options = [];
        $options['where'][] = ['folder.title = ?', $this->getModel()->getTitle()];
        $options['where'][] = ['folder.id != ?', $this->getModel()->getId()];
        $exist = FolderRepository::count($options);
        if ($exist >= 1) {
            $v->setResult(Result::ERROR);
            $v->setMessage('a folder already exists with this name');
        }
        return $v;
    }
}
