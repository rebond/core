<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Models\Core\RolePermission;
use Rebond\Repository\Core\RolePermissionRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseRolePermissionForm extends AbstractForm
{
    /* @var array */
    protected $roleValidator;
    /* @var string */
    protected $roleBuilder;
    /* @var array */
    protected $permissionValidator;
    /* @var string */
    protected $permissionBuilder;
    /*
     * @param RolePermission $model
     * @param string $unique
     */
    public function __construct(RolePermission $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->roleValidator = ['multipleKey' => true];
        $this->roleBuilder = 'multipleKey';
        $this->permissionValidator = ['multipleKey' => true];
        $this->permissionBuilder = 'multipleKey';
    }

    /**
     * @param array $properties = null
     * @return BaseRolePermissionForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('roleId', $properties)) {
            $value = Converter::intKey('roleId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setRoleId($value);
            }
        }
        if (!isset($properties) || in_array('permissionId', $properties)) {
            $value = Converter::intKey('permissionId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setPermissionId($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('roleId', $properties)) {
            $fields['role'] = $this->validateRole();
        }
        if (!isset($properties) || in_array('permissionId', $properties)) {
            $fields['permission'] = $this->validatePermission();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function validateRole()
    {
        return Validate::validate('role', $this->getModel()->getRoleId(), $this->roleValidator);
    }

    public function buildRole()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\RoleRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Core\RoleRepository::loadAll($options);
        return Form::buildItemList('roleId' . $this->unique, $models, 'id', 'title', $this->getModel()->getRoleId(), $this->roleValidator['multipleKey']);
    }

    public function buildRoleList()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\RoleRepository::getList(['id', 'title']);
        $allItems = \Rebond\Repository\Core\RoleRepository::loadAll($options);
        $items = RolePermissionRepository::loadAllByPermissionId($this->getModel()->getPermissionId());
        $selectedValues = [];
        foreach ($items as $item) {
            $selectedValues[] = $item->getRoleId();
        }
        return Form::buildCheckboxList('role' . $this->unique, $allItems, 'id', 'title', $selectedValues);
    }

    public function validatePermission()
    {
        return Validate::validate('permission', $this->getModel()->getPermissionId(), $this->permissionValidator);
    }

    public function buildPermission()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\PermissionRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Core\PermissionRepository::loadAll($options);
        return Form::buildItemList('permissionId' . $this->unique, $models, 'id', 'title', $this->getModel()->getPermissionId(), $this->permissionValidator['multipleKey']);
    }

    public function buildPermissionList()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\PermissionRepository::getList(['id', 'title']);
        $allItems = \Rebond\Repository\Core\PermissionRepository::loadAll($options);
        $items = RolePermissionRepository::loadAllByRoleId($this->getModel()->getRoleId());
        $selectedValues = [];
        foreach ($items as $item) {
            $selectedValues[] = $item->getPermissionId();
        }
        return Form::buildCheckboxList('permission' . $this->unique, $allItems, 'id', 'title', $selectedValues);
    }

}
