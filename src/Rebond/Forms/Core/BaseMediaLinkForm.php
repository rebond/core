<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Models\Core\MediaLink;
use Rebond\Repository\Core\MediaLinkRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseMediaLinkForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $packageValidator;
    /* @var array */
    protected $entityValidator;
    /* @var string */
    protected $entityBuilder;
    /* @var array */
    protected $fieldValidator;
    /* @var string */
    protected $fieldBuilder;
    /* @var array */
    protected $idFieldValidator;
    /* @var string */
    protected $idFieldBuilder;
    /* @var array */
    protected $titleFieldValidator;
    /* @var string */
    protected $titleFieldBuilder;
    /* @var array */
    protected $urlValidator;
    /* @var string */
    protected $urlBuilder;
    /* @var array */
    protected $filterValidator;
    /* @var string */
    protected $filterBuilder;
    /* @var array */
    protected $statusValidator;
    /*
     * @param MediaLink $model
     * @param string $unique
     */
    public function __construct(MediaLink $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->packageValidator = ['enum' => true];
        $this->entityValidator = ['string' => true, 'alphaNumUnderscore' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->entityBuilder = 'string';
        $this->fieldValidator = ['string' => true, 'alphaNumUnderscore' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->fieldBuilder = 'string';
        $this->idFieldValidator = ['string' => true, 'alphaNumUnderscore' => false, 'maxLength' => 20];
        $this->idFieldBuilder = 'string';
        $this->titleFieldValidator = ['string' => true, 'alphaNumUnderscore' => false, 'maxLength' => 20];
        $this->titleFieldBuilder = 'string';
        $this->urlValidator = ['string' => true, 'alphaNumSafe' => false, 'maxLength' => 50];
        $this->urlBuilder = 'string';
        $this->filterValidator = ['string' => true, 'alphaNumSafe' => false, 'maxLength' => 50];
        $this->filterBuilder = 'string';
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseMediaLinkForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('package', $properties)) {
            $value = Converter::intKey('package' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setPackage($value);
            }
        }
        if (!isset($properties) || in_array('entity', $properties)) {
            $value = Converter::stringKey('entity' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setEntity($value);
            }
        }
        if (!isset($properties) || in_array('field', $properties)) {
            $value = Converter::stringKey('field' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setField($value);
            }
        }
        if (!isset($properties) || in_array('idField', $properties)) {
            $value = Converter::stringKey('idField' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setIdField($value);
            }
        }
        if (!isset($properties) || in_array('titleField', $properties)) {
            $value = Converter::stringKey('titleField' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitleField($value);
            }
        }
        if (!isset($properties) || in_array('url', $properties)) {
            $value = Converter::stringKey('url' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setUrl($value);
            }
        }
        if (!isset($properties) || in_array('filter', $properties)) {
            $value = Converter::stringKey('filter' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setFilter($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('package', $properties)) {
            $fields['package'] = $this->validatePackage();
        }
        if (!isset($properties) || in_array('entity', $properties)) {
            $fields['entity'] = $this->validateEntity();
        }
        if (!isset($properties) || in_array('field', $properties)) {
            $fields['field'] = $this->validateField();
        }
        if (!isset($properties) || in_array('idField', $properties)) {
            $fields['idField'] = $this->validateIdField();
        }
        if (!isset($properties) || in_array('titleField', $properties)) {
            $fields['titleField'] = $this->validateTitleField();
        }
        if (!isset($properties) || in_array('url', $properties)) {
            $fields['url'] = $this->validateUrl();
        }
        if (!isset($properties) || in_array('filter', $properties)) {
            $fields['filter'] = $this->validateFilter();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validatePackage()
    {
        return Validate::validate('package', $this->getModel()->getPackage(), $this->packageValidator);
    }

    public function buildPackage()
    {
        return Form::buildList('package' . $this->unique, $this->getModel()->getPackageList(), $this->getModel()->getPackage());
    }

    public function validateEntity()
    {
        return Validate::validate('entity', $this->getModel()->getEntity(), $this->entityValidator);
    }

    public function buildEntity()
    {
        return Form::buildField('entity' . $this->unique, $this->entityBuilder, $this->getModel()->getEntity());
    }

    public function validateField()
    {
        return Validate::validate('field', $this->getModel()->getField(), $this->fieldValidator);
    }

    public function buildField()
    {
        return Form::buildField('field' . $this->unique, $this->fieldBuilder, $this->getModel()->getField());
    }

    public function validateIdField()
    {
        return Validate::validate('idField', $this->getModel()->getIdField(), $this->idFieldValidator);
    }

    public function buildIdField()
    {
        return Form::buildField('idField' . $this->unique, $this->idFieldBuilder, $this->getModel()->getIdField());
    }

    public function validateTitleField()
    {
        return Validate::validate('titleField', $this->getModel()->getTitleField(), $this->titleFieldValidator);
    }

    public function buildTitleField()
    {
        return Form::buildField('titleField' . $this->unique, $this->titleFieldBuilder, $this->getModel()->getTitleField());
    }

    public function validateUrl()
    {
        return Validate::validate('url', $this->getModel()->getUrl(), $this->urlValidator);
    }

    public function buildUrl()
    {
        return Form::buildField('url' . $this->unique, $this->urlBuilder, $this->getModel()->getUrl());
    }

    public function validateFilter()
    {
        return Validate::validate('filter', $this->getModel()->getFilter(), $this->filterValidator);
    }

    public function buildFilter()
    {
        return Form::buildField('filter' . $this->unique, $this->filterBuilder, $this->getModel()->getFilter());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
