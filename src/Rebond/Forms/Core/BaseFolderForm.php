<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Models\Core\Folder;
use Rebond\Repository\Core\FolderRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseFolderForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $parentValidator;
    /* @var array */
    protected $titleValidator;
    /* @var string */
    protected $titleBuilder;
    /* @var array */
    protected $displayOrderValidator;
    /* @var string */
    protected $displayOrderBuilder;
    /* @var array */
    protected $statusValidator;
    /*
     * @param Folder $model
     * @param string $unique
     */
    public function __construct(Folder $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->parentValidator = ['foreignKey' => true];
        $this->titleValidator = ['string' => true, 'required' => true, 'minLength' => 2, 'maxLength' => 20];
        $this->titleBuilder = 'string';
        $this->displayOrderValidator = ['integer' => true];
        $this->displayOrderBuilder = 'integer';
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseFolderForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('parentId', $properties)) {
            $value = Converter::intKey('parentId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setParentId($value);
            }
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $value = Converter::stringKey('title' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitle($value);
            }
        }
        if (!isset($properties) || in_array('displayOrder', $properties)) {
            $value = Converter::intKey('displayOrder' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setDisplayOrder($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('parentId', $properties)) {
            $fields['parent'] = $this->validateParent();
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $fields['title'] = $this->validateTitle();
        }
        if (!isset($properties) || in_array('displayOrder', $properties)) {
            $fields['displayOrder'] = $this->validateDisplayOrder();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateParent()
    {
        return Validate::validate('parent', $this->getModel()->getParentId(), $this->parentValidator);
    }

    public function buildParent()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\FolderRepository::getList(['id', 'title']);
        $models = \Rebond\Repository\Core\FolderRepository::loadAll($options);
        return Form::buildItemList('parentId' . $this->unique, $models, 'id', 'title', $this->getModel()->getParentId(), $this->parentValidator['foreignKey']);
    }

    public function validateTitle()
    {
        return Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
    }

    public function buildTitle()
    {
        return Form::buildField('title' . $this->unique, $this->titleBuilder, $this->getModel()->getTitle());
    }

    public function validateDisplayOrder()
    {
        return Validate::validate('displayOrder', $this->getModel()->getDisplayOrder(), $this->displayOrderValidator);
    }

    public function buildDisplayOrder()
    {
        return Form::buildField('displayOrder' . $this->unique, $this->displayOrderBuilder, $this->getModel()->getDisplayOrder());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
