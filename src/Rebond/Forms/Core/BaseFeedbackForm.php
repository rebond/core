<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Forms\Core;

use Rebond\Models\Core\Feedback;
use Rebond\Repository\Core\FeedbackRepository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class BaseFeedbackForm extends AbstractForm
{
    /* @var string */
    protected $idBuilder;
    /* @var array */
    protected $userValidator;
    /* @var array */
    protected $titleValidator;
    /* @var string */
    protected $titleBuilder;
    /* @var array */
    protected $typeValidator;
    /* @var array */
    protected $descriptionValidator;
    /* @var string */
    protected $descriptionBuilder;
    /* @var array */
    protected $statusValidator;
    /*
     * @param Feedback $model
     * @param string $unique
     */
    public function __construct(Feedback $model, $unique)
    {
        parent::__construct($model, $unique);
        $this->idBuilder = 'primaryKey';
        $this->userValidator = ['foreignKey' => true];
        $this->titleValidator = ['string' => true, 'required' => true, 'minLength' => 2, 'maxLength' => 100];
        $this->titleBuilder = 'string';
        $this->typeValidator = ['enum' => true];
        $this->descriptionValidator = ['text' => true, 'required' => true];
        $this->descriptionBuilder = 'text';
        $this->statusValidator = ['enum' => true];
    }

    /**
     * @param array $properties = null
     * @return BaseFeedbackForm
     */
    public function setFromPost($properties = null)
    {
        if (!isset($properties) || in_array('id', $properties)) {
            $value = Converter::intKey('id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setId($value);
            }
        }
        if (!isset($properties) || in_array('userId', $properties)) {
            $value = Converter::intKey('userId' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setUserId($value);
            }
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $value = Converter::stringKey('title' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setTitle($value);
            }
        }
        if (!isset($properties) || in_array('type', $properties)) {
            $value = Converter::intKey('type' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setType($value);
            }
        }
        if (!isset($properties) || in_array('description', $properties)) {
            $value = Converter::stringKey('description' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setDescription($value);
            }
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $value = Converter::intKey('status' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->setStatus($value);
            }
        }
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
        if (!isset($properties) || in_array('userId', $properties)) {
            $fields['user'] = $this->validateUser();
        }
        if (!isset($properties) || in_array('title', $properties)) {
            $fields['title'] = $this->validateTitle();
        }
        if (!isset($properties) || in_array('type', $properties)) {
            $fields['type'] = $this->validateType();
        }
        if (!isset($properties) || in_array('description', $properties)) {
            $fields['description'] = $this->validateDescription();
        }
        if (!isset($properties) || in_array('status', $properties)) {
            $fields['status'] = $this->validateStatus();
        }
        $this->validation->setFields($fields);
        return $this->validation;
    }

    public function buildId()
    {
        return Form::buildField('id' . $this->unique, $this->idBuilder, $this->getModel()->getId());
    }

    public function validateUser()
    {
        return Validate::validate('user', $this->getModel()->getUserId(), $this->userValidator);
    }

    public function buildUser()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = \Rebond\Repository\Core\UserRepository::getList(['id', 'email']);
        $models = \Rebond\Repository\Core\UserRepository::loadAll($options);
        return Form::buildItemList('userId' . $this->unique, $models, 'id', 'email', $this->getModel()->getUserId(), $this->userValidator['foreignKey']);
    }

    public function validateTitle()
    {
        return Validate::validate('title', $this->getModel()->getTitle(), $this->titleValidator);
    }

    public function buildTitle()
    {
        return Form::buildField('title' . $this->unique, $this->titleBuilder, $this->getModel()->getTitle());
    }

    public function validateType()
    {
        return Validate::validate('type', $this->getModel()->getType(), $this->typeValidator);
    }

    public function buildType()
    {
        return Form::buildList('type' . $this->unique, $this->getModel()->getTypeList(), $this->getModel()->getType());
    }

    public function validateDescription()
    {
        return Validate::validate('description', $this->getModel()->getDescription(), $this->descriptionValidator);
    }

    public function buildDescription()
    {
        return Form::buildField('description' . $this->unique, $this->descriptionBuilder, $this->getModel()->getDescription());
    }

    public function validateStatus()
    {
        return Validate::validate('status', $this->getModel()->getStatus(), $this->statusValidator);
    }

    public function buildStatus()
    {
        return Form::buildList('status' . $this->unique, $this->getModel()->getStatusList(), $this->getModel()->getStatus());
    }

}
