<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Repository;

use Rebond\App;
use Rebond\Enums\Core\Code;
use Rebond\Services\File;
use Rebond\Services\Log;
use Rebond\Services\Security;

class Data
{
    /* @var App */
    private $app;
    /* @var \PDO */
    private $pdo;
    /* @var array */
    private $queryBuilder;
    /* @var string */
    private $statement;
    /* @var array */
    private $params;

    public function __construct(App $app = null)
    {
        $this->app = ($app != null) ? $app : App::instance();
        $this->queryBuilder = [];
        $this->statement = null;
        $this->params = [];
    }

    /** @param string|null $type */
    public function clearQuery($type = null)
    {
        if (isset($type)) {
            $this->queryBuilder[$type] = [];
        } else {
            $this->queryBuilder = [];
        }
    }

    /**
     * Build a query
     * @param string $key
     * @param string|array $value
     */
    public function buildQuery($key, $value)
    {
        $this->queryBuilder[$key][] = $value;
    }

    /**
     * Add options to query
     * @param array $options = []
     */
    public function extendQuery(array $options = [])
    {
        if (empty($options)) {
            return;
        }
        if (!empty($options['clearSelect'])) {
            unset($this->queryBuilder['select']);
        }
        foreach ($options as $type => $list) {
            if ($type == 'clearSelect') {
                continue;
            }
            foreach ($list as $value) {
                $this->queryBuilder[$type][] = $value;
            }
        }
    }

    /**
     * @param string $statement = null
     * @param array $params = []
     * @return int
     */
    public function count($statement = null, array $params = [])
    {
        return (int)$this->selectColumn($statement, $params);
    }

    /**
     * Retrieves the first column of the first row
     * @param string $statement = null
     * @param array $params = []
     * @return int
     * @throws \Exception
     */
    public function selectColumn($statement = null, array $params = [])
    {
        $this->consolidateQuery($statement, $params);
        $stmt = $this->run();

        if ($stmt->rowCount() == 0) {
            return null;
        }

        // fetch row
        $row = $stmt->fetch(\PDO::FETCH_NUM);
        if ($row === false) {
            throw new \Exception('column could not be fetched', Code::SQL_COLUMN);
        }

        // success
        return $row[0];
    }

    /**
     * Retrieve one row
     * @param string $statement = null
     * @param array $params = []
     * @return array
     * @throws \Exception
     */
    public function selectRow($statement = null, array $params = [])
    {
        $this->consolidateQuery($statement, $params);

        $cacheValue = $this->getCache();
        if (isset($cacheValue)) {
            return $cacheValue;
        }

        $stmt = $this->run();

        if ($stmt->rowCount() == 0) {
            return null;
        }

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        if ($row === false) {
            throw new \Exception('selectRow could not be fetched', Code::SQL_SELECT_ONE);
        }

        $this->saveCache($row);

        // success
        return $row;
    }

    /**
     * Retrieve a set of rows
     * @param string $statement = null
     * @param array $params = []
     * @return \PDOStatement
     */
    public function select($statement = null, array $params = [])
    {
        $this->consolidateQuery($statement, $params);
        return $this->run();
    }

    /**
     * Executes an insert, delete or update
     * @param string $statement = null
     * @param array $params = []
     * @return int
     */
    public function execute($statement = null, array $params = [])
    {
        $this->consolidateQuery($statement, $params);
        $stmt = $this->run();

        // success
        if (stripos($this->statement, 'INSERT') !== false) {
            return $this->pdo()->lastInsertId();
        }
        return $stmt->rowCount();
    }

    /** @return \PDO */
    public function pdo()
    {
        if ($this->pdo === null) {
            $this->initConnexion();
        }
        return $this->pdo;
    }


    /**
     * @param string $statement = null
     * @param array $params = []
     * @throws \Exception
     */
    private function consolidateQuery($statement = null, array $params = [])
    {
        if (isset($statement)) {
            $this->statement = $statement;
            $this->params = $params;
            return;
        }

        if (empty($this->queryBuilder)) {
            throw new \Exception('query cannot be empty', Code::SQL_BUILD);
        }

        // SELECT
        if (!isset($this->queryBuilder['select']) && !isset($this->queryBuilder['updateFrom']) && !isset($this->queryBuilder['deleteFrom'])) {
            throw new \Exception('query needs at least 1 SELECT, UPDATE or DELETE clause', Code::SQL_BUILD);
        }

        if (isset($this->queryBuilder['select'])) {

            $this->statement = 'SELECT ' . implode(', ', $this->queryBuilder['select']);
            // FROM
            if (!isset($this->queryBuilder['from']) || count($this->queryBuilder['from']) != 1) {
                throw new \Exception('query needs to have 1 FROM clause', Code::SQL_BUILD);
            }
            $this->statement .= ' FROM ' . $this->queryBuilder['from'][0];

        } else if (isset($this->queryBuilder['updateFrom']) && count($this->queryBuilder['updateFrom']) == 1) {
            $this->statement = 'UPDATE FROM ' . $this->queryBuilder['updateFrom'][0];
        } else if (isset($this->queryBuilder['deleteFrom']) && count($this->queryBuilder['deleteFrom']) == 1) {
            $this->statement = 'DELETE FROM ' . $this->queryBuilder['deleteFrom'][0];
        } else {
            throw new \Exception('query needs at least 1 SELECT, exactly 1 UPDATE or exactly 1 DELETE clause', Code::SQL_BUILD);
        }

        // JOIN
        if (isset($this->queryBuilder['join'])) {
            $this->statement .= ' JOIN ' . implode(' JOIN ', $this->queryBuilder['join']);
        }

        // LEFT JOIN
        if (isset($this->queryBuilder['leftJoin'])) {
            $this->statement .= ' LEFT JOIN ' . implode(' LEFT JOIN ', $this->queryBuilder['leftJoin']);
        }

        // RIGHT JOIN
        if (isset($this->queryBuilder['rightJoin'])) {
            $this->statement .= ' RIGHT JOIN ' . implode(' RIGHT JOIN ', $this->queryBuilder['rightJoin']);
        }

        // WHERE
        $this->statement .= ' WHERE 1';
        if (isset($this->queryBuilder['where'])) {
            foreach ($this->queryBuilder['where'] as $where) {
                if (is_array($where)) {
                    $clause = array_shift($where);
                    foreach ($where as $w) {
                        // IN parameters
                        if (is_array($w)) {
                            $multipleParams = implode(',', array_fill(0, count($w), '?'));
                            $clause = preg_replace('/\?/', $multipleParams, $clause, 1);
                            $this->params = array_merge($this->params, $w);
                        } else {
                            $this->params[] = $w;
                        }
                    }
                } else {
                    $clause = $where;
                }
                $this->statement .= ' AND (' . $clause . ')';
            }
        }

        // GROUP
        if (isset($this->queryBuilder['group'])) {
            if (count($this->queryBuilder['group']) != 1) {
                throw new \Exception('query can only have 1 GROUP BY clause', Code::SQL_BUILD);
            }

            $this->statement .= ' GROUP BY ' . $this->queryBuilder['group'][0];
        }

        // HAVING
        if (isset($this->queryBuilder['having'])) {
            if (count($this->queryBuilder['having']) != 1) {
                throw new \Exception('query can only have 1 HAVING clause', Code::SQL_BUILD);
            }

            $this->statement .= ' HAVING ' . $this->queryBuilder['having'][0];
        }

        // ORDER
        if (isset($this->queryBuilder['order'])) {
            $this->statement .= ' ORDER BY ' . implode(', ', $this->queryBuilder['order']);
        }

        // LIMIT
        if (isset($this->queryBuilder['limit'])) {
            if (count($this->queryBuilder['limit']) != 1) {
                throw new \Exception('query can only have 1 LIMIT clause', Code::SQL_BUILD);
            }

            $this->statement .= ' LIMIT ' . $this->queryBuilder['limit'][0];
        }
    }

    /**
     * @return \PDOStatement
     * @throws \Exception
     */
    private function run()
    {
        if (!isset($this->statement)) {
            throw new \Exception('statement cannot be empty', Code::SQL_BUILD);
        }

        $stmt = $this->pdo()->prepare($this->statement);
        $stmt->execute($this->params);

        $this->log();
        return $stmt;
    }

    /** @return array|null */
    private function getCache()
    {
        $cacheTime = $this->app->getSite()->getCacheTime();
        if ($cacheTime > 0) {
            $file = $this->buildCacheFile();
            if (file_exists($file) && (filemtime($file) + $cacheTime) > time()) {
                return File::readJson($file);
            }
        }
        return null;
    }

    /** @return string */
    private function buildCacheFile()
    {
        $pos = strpos($this->statement, 'FROM');
        $query = substr($this->statement, $pos);
        $pos = strpos($query, ' ', 5);
        $from = substr($query, 5, $pos - 5);
        return $this->app->getPath('own-cache-file')
            . $from . '_'
            . hash('md5', $this->statement . json_encode($this->params)) . '.json';
    }

    /** @param $data */
    private function saveCache($data)
    {
        if ($this->app->getSite()->getCacheTime() == 0) {
            return;
        }
        File::save($this->buildCacheFile(), json_encode($data));
    }

    private function log()
    {
        if (strpos($this->statement, 'core_log') !== false) {
            return;
        }

        $pos = strpos($this->statement, 'FROM');
        $query = substr($this->statement, $pos);
        $this->app->addQuery($query . '<br>' . json_encode($this->params));

        if ($this->app->getSite()->getLogSql()) {
            Log::log(Code::SQL_LOG, count($this->app->getQueries()) . ':' . $query, __FILE__, __LINE__);
        }
    }

    private function initConnexion()
    {
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $this->pdo = new \PDO(
            'mysql:host=' . $this->app->getConfig('db-host') .
            ';port=' . $this->app->getConfig('db-port') .
            ';dbname=' . $this->app->getConfig('db-name') .
            ';charset=utf8',
            $this->app->getConfig('db-username'),
            Security::decode($this->app->getConfig('db-password')),
            $options
        );
    }

    public function __destruct()
    {
        $this->pdo = null;
    }
}
