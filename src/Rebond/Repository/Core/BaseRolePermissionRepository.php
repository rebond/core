<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Models\Core\RolePermission;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseRolePermissionRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = role_permission
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'role_permission')
    {
        if (empty($properties)) {
            $list =
                $alias . '.role_id AS ' . $alias . 'RoleId, ' .
                $alias . '.permission_id AS ' . $alias . 'PermissionId';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(*)');
        $db->buildQuery('from', 'core_role_permission `role_permission`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $roleId
     * @param int $permissionId
     * @return RolePermission
     */
    public static function loadById($roleId, $permissionId)
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_role_permission `role_permission`');
        $db->buildQuery('where', ['role_permission.role_id = ?', $roleId]);
        $db->buildQuery('where', ['role_permission.permission_id = ?', $permissionId]);
        return self::map($db);
    }

    /**
     * @param array $options = []
     * @return RolePermission
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_role_permission `role_permission`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

    /**
     * @param int $id
     * @param array $options = []
     * @return RolePermission[]
     */
    public static function loadAllByRoleId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_role_permission `role_permission`');
        $db->buildQuery('where', ['role_permission.role_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }
    /**
     * @param int $id
     * @param array $options = []
     * @return RolePermission[]
     */
    public static function loadAllByPermissionId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_role_permission `role_permission`');
        $db->buildQuery('where', ['role_permission.permission_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param array $options = []
     * @return RolePermission[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_role_permission `role_permission`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param RolePermission[] $models
     * @return bool
     */
    public static function saveAll(array $models)
    {
        if (count($models) == 0) {
            return true;
        }
        foreach ($models as $model) {
            $model->save();
        }
        return true;
    }

    /**
     * @param RolePermission $model
     * @return int
     */
    public static function save(RolePermission $model)
    {
        $query = 'INSERT INTO core_role_permission (role_id, permission_id) VALUES (?,?)';
        $query .= ' ON DUPLICATE KEY UPDATE ';
        $params = [
                $model->getRoleId(),
                $model->getPermissionId() 
        ];

        $query .= 'role_id = ?, ';
        $params[] = $model->getRoleId();
        $query .= 'permission_id = ?, ';
        $params[] = $model->getPermissionId();
        $query = rtrim(trim($query), ',');
        $db = new Data();
        return $db->execute($query, $params);
    }

    /**
     * @param int $roleId
     * @param int $permissionId
     * @return int
     */
    public static function deleteByIds($roleId, $permissionId)
    {
        $query = 'DELETE FROM core_role_permission WHERE 1';
        $params = [];
        $query .= ' AND role_id = ?';
        $params[] = $roleId;
        $query .= ' AND permission_id = ?';
        $params[] = $permissionId;
        $db = new Data();
        return $db->execute($query, $params);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteByRoleId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM core_role_permission WHERE role_id = ?';
        return $db->execute($query, [$id]);
    }
    /**
     * @param int $id
     * @return int
     */
    public static function deleteByPermissionId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM core_role_permission WHERE permission_id = ?';
        return $db->execute($query, [$id]);
    }

    /**
     * @param array $row
     * @param string $alias = role_permission
     * @return RolePermission
     */
    public static function join(array $row, $alias = 'role_permission')
    {
        if (!isset($row[$alias . '$roleId'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return RolePermission
     */
    protected static function mapper(array $row, $alias = 'role_permission')
    {
        $model = new RolePermission(false);
        if (isset($row[$alias . 'RoleId'])) {
            $model->setRoleId($row[$alias . 'RoleId']);
            $model->setRole(\Rebond\Repository\Core\RoleRepository::join($row, $alias . '_role'));
        }
        if (isset($row[$alias . 'PermissionId'])) {
            $model->setPermissionId($row[$alias . 'PermissionId']);
            $model->setPermission(\Rebond\Repository\Core\PermissionRepository::join($row, $alias . '_permission'));
        }
        return $model;
    }
}
