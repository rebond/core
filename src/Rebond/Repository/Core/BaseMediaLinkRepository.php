<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Models\Core\MediaLink;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseMediaLinkRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = media_link
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'media_link')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.package AS ' . $alias . 'Package, ' .
                $alias . '.entity AS ' . $alias . 'Entity, ' .
                $alias . '.field AS ' . $alias . 'Field, ' .
                $alias . '.id_field AS ' . $alias . 'IdField, ' .
                $alias . '.title_field AS ' . $alias . 'TitleField, ' .
                $alias . '.url AS ' . $alias . 'Url, ' .
                $alias . '.filter AS ' . $alias . 'Filter, ' .
                $alias . '.status AS ' . $alias . 'Status, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(media_link.id)');
        $db->buildQuery('from', 'core_media_link `media_link`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return MediaLink
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new MediaLink() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_media_link `media_link`');
        $db->buildQuery('where', ['media_link.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new MediaLink();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return MediaLink
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_media_link `media_link`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }


    /**
     * @param array $options = []
     * @return MediaLink[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_media_link `media_link`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param MediaLink $model
     * @return int
     */
    public static function save(MediaLink $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO core_media_link (`package`, `entity`, `field`, `id_field`, `title_field`, `url`, `filter`, `status`, `created_date`, `modified_date`) VALUES (?,?,?,?,?,?,?,?,?,?)';
            $params = [
                $model->getPackage(),
                $model->getEntity(),
                $model->getField(),
                $model->getIdField(),
                $model->getTitleField(),
                $model->getUrl(),
                $model->getFilter(),
                $model->getStatus(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE core_media_link SET ';
            $params = [];
            if ($model->getPackage() !== null) {
                $query .= '`package` = ?, ';
                $params[] = $model->getPackage();
            }
            if ($model->getEntity() !== null) {
                $query .= '`entity` = ?, ';
                $params[] = $model->getEntity();
            }
            if ($model->getField() !== null) {
                $query .= '`field` = ?, ';
                $params[] = $model->getField();
            }
            if ($model->getIdField() !== null) {
                $query .= '`id_field` = ?, ';
                $params[] = $model->getIdField();
            }
            if ($model->getTitleField() !== null) {
                $query .= '`title_field` = ?, ';
                $params[] = $model->getTitleField();
            }
            if ($model->getUrl() !== null) {
                $query .= '`url` = ?, ';
                $params[] = $model->getUrl();
            }
            if ($model->getFilter() !== null) {
                $query .= '`filter` = ?, ';
                $params[] = $model->getFilter();
            }
            if ($model->getStatus() !== null) {
                $query .= '`status` = ?, ';
                $params[] = $model->getStatus();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @param int $status
     * @return int
     */
    public static function updateStatus($id, $status)
    {
        $db = new Data();
        $query = 'UPDATE core_media_link SET status = ? WHERE id = ?';
        return $db->execute($query, [$status, $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM core_media_link WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }


    /**
     * @param array $row
     * @param string $alias = media_link
     * @return MediaLink
     */
    public static function join(array $row, $alias = 'media_link')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return MediaLink
     */
    protected static function mapper(array $row, $alias = 'media_link')
    {
        $model = new MediaLink(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'Package'])) {
            $model->setPackage($row[$alias . 'Package']);
        }
        if (isset($row[$alias . 'Entity'])) {
            $model->setEntity($row[$alias . 'Entity']);
        }
        if (isset($row[$alias . 'Field'])) {
            $model->setField($row[$alias . 'Field']);
        }
        if (isset($row[$alias . 'IdField'])) {
            $model->setIdField($row[$alias . 'IdField']);
        }
        if (isset($row[$alias . 'TitleField'])) {
            $model->setTitleField($row[$alias . 'TitleField']);
        }
        if (isset($row[$alias . 'Url'])) {
            $model->setUrl($row[$alias . 'Url']);
        }
        if (isset($row[$alias . 'Filter'])) {
            $model->setFilter($row[$alias . 'Filter']);
        }
        if (isset($row[$alias . 'Status'])) {
            $model->setStatus($row[$alias . 'Status']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
