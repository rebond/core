<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Models\Core\UserRole;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseUserRoleRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = user_role
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'user_role')
    {
        if (empty($properties)) {
            $list =
                $alias . '.user_id AS ' . $alias . 'UserId, ' .
                $alias . '.role_id AS ' . $alias . 'RoleId';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(*)');
        $db->buildQuery('from', 'core_user_role `user_role`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $userId
     * @param int $roleId
     * @return UserRole
     */
    public static function loadById($userId, $roleId)
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user_role `user_role`');
        $db->buildQuery('where', ['user_role.user_id = ?', $userId]);
        $db->buildQuery('where', ['user_role.role_id = ?', $roleId]);
        return self::map($db);
    }

    /**
     * @param array $options = []
     * @return UserRole
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user_role `user_role`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

    /**
     * @param int $id
     * @param array $options = []
     * @return UserRole[]
     */
    public static function loadAllByUserId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user_role `user_role`');
        $db->buildQuery('where', ['user_role.user_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }
    /**
     * @param int $id
     * @param array $options = []
     * @return UserRole[]
     */
    public static function loadAllByRoleId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user_role `user_role`');
        $db->buildQuery('where', ['user_role.role_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param array $options = []
     * @return UserRole[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user_role `user_role`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param UserRole[] $models
     * @return bool
     */
    public static function saveAll(array $models)
    {
        if (count($models) == 0) {
            return true;
        }
        foreach ($models as $model) {
            $model->save();
        }
        return true;
    }

    /**
     * @param UserRole $model
     * @return int
     */
    public static function save(UserRole $model)
    {
        $query = 'INSERT INTO core_user_role (user_id, role_id) VALUES (?,?)';
        $query .= ' ON DUPLICATE KEY UPDATE ';
        $params = [
                $model->getUserId(),
                $model->getRoleId() 
        ];

        $query .= 'user_id = ?, ';
        $params[] = $model->getUserId();
        $query .= 'role_id = ?, ';
        $params[] = $model->getRoleId();
        $query = rtrim(trim($query), ',');
        $db = new Data();
        return $db->execute($query, $params);
    }

    /**
     * @param int $userId
     * @param int $roleId
     * @return int
     */
    public static function deleteByIds($userId, $roleId)
    {
        $query = 'DELETE FROM core_user_role WHERE 1';
        $params = [];
        $query .= ' AND user_id = ?';
        $params[] = $userId;
        $query .= ' AND role_id = ?';
        $params[] = $roleId;
        $db = new Data();
        return $db->execute($query, $params);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteByUserId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM core_user_role WHERE user_id = ?';
        return $db->execute($query, [$id]);
    }
    /**
     * @param int $id
     * @return int
     */
    public static function deleteByRoleId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM core_user_role WHERE role_id = ?';
        return $db->execute($query, [$id]);
    }

    /**
     * @param array $row
     * @param string $alias = user_role
     * @return UserRole
     */
    public static function join(array $row, $alias = 'user_role')
    {
        if (!isset($row[$alias . '$userId'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return UserRole
     */
    protected static function mapper(array $row, $alias = 'user_role')
    {
        $model = new UserRole(false);
        if (isset($row[$alias . 'UserId'])) {
            $model->setUserId($row[$alias . 'UserId']);
            $model->setUser(\Rebond\Repository\Core\UserRepository::join($row, $alias . '_user'));
        }
        if (isset($row[$alias . 'RoleId'])) {
            $model->setRoleId($row[$alias . 'RoleId']);
            $model->setRole(\Rebond\Repository\Core\RoleRepository::join($row, $alias . '_role'));
        }
        return $model;
    }
}
