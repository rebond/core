<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Models\Core\Role;
use Rebond\Repository\Data;

class RoleRepository extends BaseRoleRepository
{
    /**
     * Find all Role
     * @param int $userId
     * @param array $options = []
     * @return Role[]
     */
    public static function loadByUserId($userId, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_role role');
        $db->buildQuery('join', 'core_user_role ur ON ur.role_id = role.id');
        $db->buildQuery('where', 'ur.user_id = ' . $userId);
        $db->extendQuery($options);
        return self::mapList($db);
    }
}
