<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Services\Core\FolderService;

class FolderRepository extends BaseFolderRepository
{
    /**
     * BuildTree
     * @param void
     * @return array
     */
    public static function buildTree()
    {
        $options = [];
        $options['where'][] = 'folder.status IN (0,1)';
        $options['order'][] = 'folder.display_order, folder.title';
        $folders = self::loadAll($options);
        return FolderService::buildNavRecursion($folders, 0, 0);
    }

    /**
     * Get children's folderIds of folder
     * @param int $folderId
     * @return array
     */
    public static function getChildren($folderId)
    {
        return self::getChildrenByPage($folderId);
    }

    private static function getChildrenByPage($folderId, $folderIds = [])
    {
        // Add current folder
        $folderIds[] = $folderId;
        // Find children
        $folders = self::loadAllByParentId($folderId);
        if ($folders) {
            foreach ($folders as $folder) {
                $folderIds = self::getChildrenByPage($folder->getId(), $folderIds);
            }
        }
        return $folderIds;
    }
}
