<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Models\Core\Media;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseMediaRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = media
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'media')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.title AS ' . $alias . 'Title, ' .
                $alias . '.folder_id AS ' . $alias . 'FolderId, ' .
                $alias . '.tags AS ' . $alias . 'Tags, ' .
                $alias . '.upload AS ' . $alias . 'Upload, ' .
                $alias . '.original_filename AS ' . $alias . 'OriginalFilename, ' .
                $alias . '.path AS ' . $alias . 'Path, ' .
                $alias . '.extension AS ' . $alias . 'Extension, ' .
                $alias . '.mime_type AS ' . $alias . 'MimeType, ' .
                $alias . '.file_size AS ' . $alias . 'FileSize, ' .
                $alias . '.width AS ' . $alias . 'Width, ' .
                $alias . '.height AS ' . $alias . 'Height, ' .
                $alias . '.alt AS ' . $alias . 'Alt, ' .
                $alias . '.is_selectable AS ' . $alias . 'IsSelectable, ' .
                $alias . '.status AS ' . $alias . 'Status, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(media.id)');
        $db->buildQuery('from', 'core_media `media`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return Media
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new Media() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_media `media`');
        $db->buildQuery('where', ['media.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new Media();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return Media
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_media `media`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

    /**
     * @param int $id
     * @param array $options = []
     * @return Media[]
     */
    public static function loadAllByFolderId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_media `media`');
        $db->buildQuery('where', ['media.folder_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param array $options = []
     * @return Media[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_media `media`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param Media $model
     * @return int
     */
    public static function save(Media $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO core_media (`title`, folder_id, `tags`, `upload`, `original_filename`, `path`, `extension`, `mime_type`, `file_size`, `width`, `height`, `alt`, `is_selectable`, `status`, `created_date`, `modified_date`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            $params = [
                $model->getTitle(),
                $model->getFolderId(),
                $model->getTags(),
                $model->getUpload(),
                $model->getOriginalFilename(),
                $model->getPath(),
                $model->getExtension(),
                $model->getMimeType(),
                $model->getFileSize(),
                $model->getWidth(),
                $model->getHeight(),
                $model->getAlt(),
                (int)$model->getIsSelectable(),
                $model->getStatus(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE core_media SET ';
            $params = [];
            if ($model->getTitle() !== null) {
                $query .= '`title` = ?, ';
                $params[] = $model->getTitle();
            }
            if ($model->getFolderId() !== null) {
                $query .= 'folder_id = ?, ';
                $params[] = $model->getFolderId();
            }
            if ($model->getTags() !== null) {
                $query .= '`tags` = ?, ';
                $params[] = $model->getTags();
            }
            if ($model->getUpload() !== null) {
                $query .= '`upload` = ?, ';
                $params[] = $model->getUpload();
            }
            if ($model->getOriginalFilename() !== null) {
                $query .= '`original_filename` = ?, ';
                $params[] = $model->getOriginalFilename();
            }
            if ($model->getPath() !== null) {
                $query .= '`path` = ?, ';
                $params[] = $model->getPath();
            }
            if ($model->getExtension() !== null) {
                $query .= '`extension` = ?, ';
                $params[] = $model->getExtension();
            }
            if ($model->getMimeType() !== null) {
                $query .= '`mime_type` = ?, ';
                $params[] = $model->getMimeType();
            }
            if ($model->getFileSize() !== null) {
                $query .= '`file_size` = ?, ';
                $params[] = $model->getFileSize();
            }
            if ($model->getWidth() !== null) {
                $query .= '`width` = ?, ';
                $params[] = $model->getWidth();
            }
            if ($model->getHeight() !== null) {
                $query .= '`height` = ?, ';
                $params[] = $model->getHeight();
            }
            if ($model->getAlt() !== null) {
                $query .= '`alt` = ?, ';
                $params[] = $model->getAlt();
            }
            if ($model->getIsSelectable() !== null) {
                $query .= '`is_selectable` = ?, ';
                $params[] = $model->getIsSelectable();
            }
            if ($model->getStatus() !== null) {
                $query .= '`status` = ?, ';
                $params[] = $model->getStatus();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @param int $status
     * @return int
     */
    public static function updateStatus($id, $status)
    {
        $db = new Data();
        $query = 'UPDATE core_media SET status = ? WHERE id = ?';
        return $db->execute($query, [$status, $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM core_media WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteByFolderId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM core_media WHERE folder_id = ?';
        return $db->execute($query, [$id]);
    }

    /**
     * @param array $row
     * @param string $alias = media
     * @return Media
     */
    public static function join(array $row, $alias = 'media')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return Media
     */
    protected static function mapper(array $row, $alias = 'media')
    {
        $model = new Media(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'Title'])) {
            $model->setTitle($row[$alias . 'Title']);
        }
        if (isset($row[$alias . 'FolderId'])) {
            $model->setFolderId($row[$alias . 'FolderId']);
            $model->setFolder(\Rebond\Repository\Core\FolderRepository::join($row, $alias . '_folder'));
        }
        if (isset($row[$alias . 'Tags'])) {
            $model->setTags($row[$alias . 'Tags']);
        }
        if (isset($row[$alias . 'Upload'])) {
            $model->setUpload($row[$alias . 'Upload']);
        }
        if (isset($row[$alias . 'OriginalFilename'])) {
            $model->setOriginalFilename($row[$alias . 'OriginalFilename']);
        }
        if (isset($row[$alias . 'Path'])) {
            $model->setPath($row[$alias . 'Path']);
        }
        if (isset($row[$alias . 'Extension'])) {
            $model->setExtension($row[$alias . 'Extension']);
        }
        if (isset($row[$alias . 'MimeType'])) {
            $model->setMimeType($row[$alias . 'MimeType']);
        }
        if (isset($row[$alias . 'FileSize'])) {
            $model->setFileSize($row[$alias . 'FileSize']);
        }
        if (isset($row[$alias . 'Width'])) {
            $model->setWidth($row[$alias . 'Width']);
        }
        if (isset($row[$alias . 'Height'])) {
            $model->setHeight($row[$alias . 'Height']);
        }
        if (isset($row[$alias . 'Alt'])) {
            $model->setAlt($row[$alias . 'Alt']);
        }
        if (isset($row[$alias . 'IsSelectable'])) {
            $model->setIsSelectable($row[$alias . 'IsSelectable']);
        }
        if (isset($row[$alias . 'Status'])) {
            $model->setStatus($row[$alias . 'Status']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
