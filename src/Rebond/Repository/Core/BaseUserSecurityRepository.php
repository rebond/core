<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Models\Core\UserSecurity;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseUserSecurityRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = user_security
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'user_security')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.user_id AS ' . $alias . 'UserId, ' .
                $alias . '.secure AS ' . $alias . 'Secure, ' .
                $alias . '.type AS ' . $alias . 'Type, ' .
                $alias . '.expiry_date AS ' . $alias . 'ExpiryDate, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(user_security.id)');
        $db->buildQuery('from', 'core_user_security `user_security`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return UserSecurity
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new UserSecurity() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user_security `user_security`');
        $db->buildQuery('where', ['user_security.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new UserSecurity();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return UserSecurity
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user_security `user_security`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

    /**
     * @param int $id
     * @param array $options = []
     * @return UserSecurity[]
     */
    public static function loadAllByUserId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user_security `user_security`');
        $db->buildQuery('where', ['user_security.user_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param array $options = []
     * @return UserSecurity[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user_security `user_security`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param UserSecurity $model
     * @return int
     */
    public static function save(UserSecurity $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO core_user_security (user_id, `secure`, `type`, `expiry_date`, `created_date`) VALUES (?,?,?,?,?)';
            $params = [
                $model->getUserId(),
                $model->getSecure(),
                $model->getType(),
                $model->getExpiryDate()->format('date'),
                $model->getCreatedDate()->format('createdDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE core_user_security SET ';
            $params = [];
            if ($model->getUserId() !== null) {
                $query .= 'user_id = ?, ';
                $params[] = $model->getUserId();
            }
            if ($model->getSecure() !== null) {
                $query .= '`secure` = ?, ';
                $params[] = $model->getSecure();
            }
            if ($model->getType() !== null) {
                $query .= '`type` = ?, ';
                $params[] = $model->getType();
            }
            if ($model->getExpiryDate() !== null) {
                $query .= '`expiry_date` = ?, ';
                $params[] = $model->getExpiryDate()->format('date');
            }
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM core_user_security WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteByUserId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM core_user_security WHERE user_id = ?';
        return $db->execute($query, [$id]);
    }

    /**
     * @param array $row
     * @param string $alias = user_security
     * @return UserSecurity
     */
    public static function join(array $row, $alias = 'user_security')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return UserSecurity
     */
    protected static function mapper(array $row, $alias = 'user_security')
    {
        $model = new UserSecurity(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'UserId'])) {
            $model->setUserId($row[$alias . 'UserId']);
            $model->setUser(\Rebond\Repository\Core\UserRepository::join($row, $alias . '_user'));
        }
        if (isset($row[$alias . 'Secure'])) {
            $model->setSecure($row[$alias . 'Secure']);
        }
        if (isset($row[$alias . 'Type'])) {
            $model->setType($row[$alias . 'Type']);
        }
        if (isset($row[$alias . 'ExpiryDate'])) {
            $model->setExpiryDate($row[$alias . 'ExpiryDate']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        return $model;
    }
}
