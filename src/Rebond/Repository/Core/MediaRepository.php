<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Repository\Data;

class MediaRepository extends BaseMediaRepository
{
    /**
     * Update medium from a folder to move to root
     * @param int $folderId
     * @return int
     */
    public static function moveToRoot($folderId)
    {
        $query = 'UPDATE core_media SET folder_id = 1 WHERE folder_id = ?';
        $db = new Data();
        return $db->execute($query, [$folderId]);
    }
}
