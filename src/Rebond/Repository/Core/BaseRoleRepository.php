<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Models\Core\Role;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseRoleRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = role
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'role')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.title AS ' . $alias . 'Title, ' .
                $alias . '.summary AS ' . $alias . 'Summary, ' .
                $alias . '.status AS ' . $alias . 'Status, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(role.id)');
        $db->buildQuery('from', 'core_role `role`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return Role
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new Role() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_role `role`');
        $db->buildQuery('where', ['role.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new Role();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return Role
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_role `role`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }


    /**
     * @param array $options = []
     * @return Role[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_role `role`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param Role $model
     * @return int
     */
    public static function save(Role $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO core_role (`title`, `summary`, `status`, `created_date`, `modified_date`) VALUES (?,?,?,?,?)';
            $params = [
                $model->getTitle(),
                $model->getSummary(),
                $model->getStatus(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE core_role SET ';
            $params = [];
            if ($model->getTitle() !== null) {
                $query .= '`title` = ?, ';
                $params[] = $model->getTitle();
            }
            if ($model->getSummary() !== null) {
                $query .= '`summary` = ?, ';
                $params[] = $model->getSummary();
            }
            if ($model->getStatus() !== null) {
                $query .= '`status` = ?, ';
                $params[] = $model->getStatus();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @param int $status
     * @return int
     */
    public static function updateStatus($id, $status)
    {
        $db = new Data();
        $query = 'UPDATE core_role SET status = ? WHERE id = ?';
        return $db->execute($query, [$status, $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM core_role WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }


    /**
     * @param array $row
     * @param string $alias = role
     * @return Role
     */
    public static function join(array $row, $alias = 'role')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return Role
     */
    protected static function mapper(array $row, $alias = 'role')
    {
        $model = new Role(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'Title'])) {
            $model->setTitle($row[$alias . 'Title']);
        }
        if (isset($row[$alias . 'Summary'])) {
            $model->setSummary($row[$alias . 'Summary']);
        }
        if (isset($row[$alias . 'Status'])) {
            $model->setStatus($row[$alias . 'Status']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
