<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Models\Core\User;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseUserRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = user
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'user')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.email AS ' . $alias . 'Email, ' .
                $alias . '.password AS ' . $alias . 'Password, ' .
                $alias . '.first_name AS ' . $alias . 'FirstName, ' .
                $alias . '.last_name AS ' . $alias . 'LastName, ' .
                $alias . '.avatar_id AS ' . $alias . 'AvatarId, ' .
                $alias . '.is_admin AS ' . $alias . 'IsAdmin, ' .
                $alias . '.is_dev AS ' . $alias . 'IsDev, ' .
                $alias . '.status AS ' . $alias . 'Status, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(user.id)');
        $db->buildQuery('from', 'core_user `user`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return User
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new User() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user `user`');
        $db->buildQuery('where', ['user.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new User();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return User
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user `user`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

    /**
     * @param string $email
     * @return User
     */
    public static function loadByEmail($email)
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user `user`');
        $db->buildQuery('where', ['user.email = ?', $email]);
        return self::map($db);
    }

    /**
     * @param string $email
     * @param int $id
     * @return bool
     */
    public static function emailExists($email, $id)
    {
        $db = new Data();
        $db->buildQuery('select', 'count(user.id)');
        $db->buildQuery('from', 'core_user `user`');
        $db->buildQuery('where', ['user.email = ?', $email]);
        $db->buildQuery('where', ['user.id != ?', $id]);
        return $db->count();
    }


    /**
     * @param array $options = []
     * @return User[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'core_user `user`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param User $model
     * @return int
     */
    public static function save(User $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO core_user (`email`, `password`, `first_name`, `last_name`, avatar_id, `is_admin`, `is_dev`, `status`, `created_date`, `modified_date`) VALUES (?,?,?,?,?,?,?,?,?,?)';
            $params = [
                $model->getEmail(),
                $model->getPassword(),
                $model->getFirstName(),
                $model->getLastName(),
                $model->getAvatarId(),
                (int)$model->getIsAdmin(),
                (int)$model->getIsDev(),
                $model->getStatus(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE core_user SET ';
            $params = [];
            if ($model->getEmail() !== null) {
                $query .= '`email` = ?, ';
                $params[] = $model->getEmail();
            }
            if ($model->getPassword() !== null) {
                $query .= '`password` = ?, ';
                $params[] = $model->getPassword();
            }
            if ($model->getFirstName() !== null) {
                $query .= '`first_name` = ?, ';
                $params[] = $model->getFirstName();
            }
            if ($model->getLastName() !== null) {
                $query .= '`last_name` = ?, ';
                $params[] = $model->getLastName();
            }
            if ($model->getAvatarId() !== null) {
                $query .= 'avatar_id = ?, ';
                $params[] = $model->getAvatarId();
            }
            if ($model->getIsAdmin() !== null) {
                $query .= '`is_admin` = ?, ';
                $params[] = $model->getIsAdmin();
            }
            if ($model->getIsDev() !== null) {
                $query .= '`is_dev` = ?, ';
                $params[] = $model->getIsDev();
            }
            if ($model->getStatus() !== null) {
                $query .= '`status` = ?, ';
                $params[] = $model->getStatus();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @param int $status
     * @return int
     */
    public static function updateStatus($id, $status)
    {
        $db = new Data();
        $query = 'UPDATE core_user SET status = ? WHERE id = ?';
        return $db->execute($query, [$status, $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM core_user WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }


    /**
     * @param array $row
     * @param string $alias = user
     * @return User
     */
    public static function join(array $row, $alias = 'user')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return User
     */
    protected static function mapper(array $row, $alias = 'user')
    {
        $model = new User(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'Email'])) {
            $model->setEmail($row[$alias . 'Email']);
        }
        if (isset($row[$alias . 'Password'])) {
            $model->setPassword($row[$alias . 'Password']);
        }
        if (isset($row[$alias . 'FirstName'])) {
            $model->setFirstName($row[$alias . 'FirstName']);
        }
        if (isset($row[$alias . 'LastName'])) {
            $model->setLastName($row[$alias . 'LastName']);
        }
        if (isset($row[$alias . 'AvatarId'])) {
            $model->setAvatarId($row[$alias . 'AvatarId']);
            $model->setAvatar(\Rebond\Repository\Core\MediaRepository::join($row, $alias . '_avatar'));
        }
        if (isset($row[$alias . 'IsAdmin'])) {
            $model->setIsAdmin($row[$alias . 'IsAdmin']);
        }
        if (isset($row[$alias . 'IsDev'])) {
            $model->setIsDev($row[$alias . 'IsDev']);
        }
        if (isset($row[$alias . 'Status'])) {
            $model->setStatus($row[$alias . 'Status']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
