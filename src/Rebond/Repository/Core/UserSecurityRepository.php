<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Core;

use Rebond\Repository\Data;

class UserSecurityRepository extends BaseUserSecurityRepository
{
    /**
     * Delete a UserSecurity by userId and type
     * @param int $userId
     * @param int|array $type
     * @return int $result
     */
    public static function deleteSecure($userId, $type)
    {
        $db = new Data();
        $db->buildQuery('deleteFrom', 'core_user_security');
        $db->buildQuery('where', ['user_id = ?', $userId]);
        if (is_array($type)) {
            $db->buildQuery('where', ['type IN (?)', $type]);
        } else {
            $db->buildQuery('where', ['type = ?', $type]);
        }
        return $db->execute();
    }
}
