<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 24.11.2016
 * Time: 11:30
 */

namespace Rebond\Repository;

use Rebond\Models\AbstractModel;

abstract class AbstractRepository
{
    protected static function mapper(array $row, $alias = null) {
        return null;
    }

    /**
     * @param Data $db
     * @return AbstractModel
     */
    protected static function map(Data $db)
    {
        $row = $db->selectRow();
        if (!isset($row)) {
            return null;
        }
        return static::mapper($row);
    }

    /**
     * @param Data $db
     * @return AbstractModel[]
     */
    protected static function mapList(Data $db)
    {
        $rows = $db->select();
        if ($rows->rowCount() == 0) {
            return [];
        }
        $models = [];
        while ($row = $rows->fetch(\PDO::FETCH_ASSOC)) {
            $models[] = static::mapper($row);
        }
        return $models;
    }

}