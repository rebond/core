<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Cms;

use Rebond\Models\Cms\Module;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseModuleRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = module
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'module')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.name AS ' . $alias . 'Name, ' .
                $alias . '.title AS ' . $alias . 'Title, ' .
                $alias . '.summary AS ' . $alias . 'Summary, ' .
                $alias . '.workflow AS ' . $alias . 'Workflow, ' .
                $alias . '.has_filter AS ' . $alias . 'HasFilter, ' .
                $alias . '.has_content AS ' . $alias . 'HasContent, ' .
                $alias . '.status AS ' . $alias . 'Status, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(module.id)');
        $db->buildQuery('from', 'cms_module `module`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return Module
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new Module() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_module `module`');
        $db->buildQuery('where', ['module.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new Module();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return Module
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_module `module`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }


    /**
     * @param array $options = []
     * @return Module[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_module `module`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param Module $model
     * @return int
     */
    public static function save(Module $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO cms_module (`name`, `title`, `summary`, `workflow`, `has_filter`, `has_content`, `status`, `created_date`, `modified_date`) VALUES (?,?,?,?,?,?,?,?,?)';
            $params = [
                $model->getName(),
                $model->getTitle(),
                $model->getSummary(),
                $model->getWorkflow(),
                (int)$model->getHasFilter(),
                (int)$model->getHasContent(),
                $model->getStatus(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE cms_module SET ';
            $params = [];
            if ($model->getName() !== null) {
                $query .= '`name` = ?, ';
                $params[] = $model->getName();
            }
            if ($model->getTitle() !== null) {
                $query .= '`title` = ?, ';
                $params[] = $model->getTitle();
            }
            if ($model->getSummary() !== null) {
                $query .= '`summary` = ?, ';
                $params[] = $model->getSummary();
            }
            if ($model->getWorkflow() !== null) {
                $query .= '`workflow` = ?, ';
                $params[] = $model->getWorkflow();
            }
            if ($model->getHasFilter() !== null) {
                $query .= '`has_filter` = ?, ';
                $params[] = $model->getHasFilter();
            }
            if ($model->getHasContent() !== null) {
                $query .= '`has_content` = ?, ';
                $params[] = $model->getHasContent();
            }
            if ($model->getStatus() !== null) {
                $query .= '`status` = ?, ';
                $params[] = $model->getStatus();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @param int $status
     * @return int
     */
    public static function updateStatus($id, $status)
    {
        $db = new Data();
        $query = 'UPDATE cms_module SET status = ? WHERE id = ?';
        return $db->execute($query, [$status, $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM cms_module WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }


    /**
     * @param array $row
     * @param string $alias = module
     * @return Module
     */
    public static function join(array $row, $alias = 'module')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return Module
     */
    protected static function mapper(array $row, $alias = 'module')
    {
        $model = new Module(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'Name'])) {
            $model->setName($row[$alias . 'Name']);
        }
        if (isset($row[$alias . 'Title'])) {
            $model->setTitle($row[$alias . 'Title']);
        }
        if (isset($row[$alias . 'Summary'])) {
            $model->setSummary($row[$alias . 'Summary']);
        }
        if (isset($row[$alias . 'Workflow'])) {
            $model->setWorkflow($row[$alias . 'Workflow']);
        }
        if (isset($row[$alias . 'HasFilter'])) {
            $model->setHasFilter($row[$alias . 'HasFilter']);
        }
        if (isset($row[$alias . 'HasContent'])) {
            $model->setHasContent($row[$alias . 'HasContent']);
        }
        if (isset($row[$alias . 'Status'])) {
            $model->setStatus($row[$alias . 'Status']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
