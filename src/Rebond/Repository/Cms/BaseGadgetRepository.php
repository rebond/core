<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Cms;

use Rebond\Models\Cms\Gadget;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseGadgetRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = gadget
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'gadget')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.page_id AS ' . $alias . 'PageId, ' .
                $alias . '.component_id AS ' . $alias . 'ComponentId, ' .
                $alias . '.col AS ' . $alias . 'Col, ' .
                $alias . '.filter_id AS ' . $alias . 'FilterId, ' .
                $alias . '.custom_filter AS ' . $alias . 'CustomFilter, ' .
                $alias . '.display_order AS ' . $alias . 'DisplayOrder, ' .
                $alias . '.status AS ' . $alias . 'Status, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(gadget.id)');
        $db->buildQuery('from', 'cms_gadget `gadget`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return Gadget
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new Gadget() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_gadget `gadget`');
        $db->buildQuery('where', ['gadget.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new Gadget();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return Gadget
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_gadget `gadget`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

    /**
     * @param int $id
     * @param array $options = []
     * @return Gadget[]
     */
    public static function loadAllByPageId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_gadget `gadget`');
        $db->buildQuery('where', ['gadget.page_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }
    /**
     * @param int $id
     * @param array $options = []
     * @return Gadget[]
     */
    public static function loadAllByComponentId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_gadget `gadget`');
        $db->buildQuery('where', ['gadget.component_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }
    /**
     * @param int $id
     * @param array $options = []
     * @return Gadget[]
     */
    public static function loadAllByFilterId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_gadget `gadget`');
        $db->buildQuery('where', ['gadget.filter_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param array $options = []
     * @return Gadget[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_gadget `gadget`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param Gadget $model
     * @return int
     */
    public static function save(Gadget $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO cms_gadget (page_id, component_id, `col`, filter_id, `custom_filter`, `display_order`, `status`, `created_date`, `modified_date`) VALUES (?,?,?,?,?,?,?,?,?)';
            $params = [
                $model->getPageId(),
                $model->getComponentId(),
                $model->getCol(),
                $model->getFilterId(),
                $model->getCustomFilter(),
                $model->getDisplayOrder(),
                $model->getStatus(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE cms_gadget SET ';
            $params = [];
            if ($model->getPageId() !== null) {
                $query .= 'page_id = ?, ';
                $params[] = $model->getPageId();
            }
            if ($model->getComponentId() !== null) {
                $query .= 'component_id = ?, ';
                $params[] = $model->getComponentId();
            }
            if ($model->getCol() !== null) {
                $query .= '`col` = ?, ';
                $params[] = $model->getCol();
            }
            if ($model->getFilterId() !== null) {
                $query .= 'filter_id = ?, ';
                $params[] = $model->getFilterId();
            }
            if ($model->getCustomFilter() !== null) {
                $query .= '`custom_filter` = ?, ';
                $params[] = $model->getCustomFilter();
            }
            if ($model->getDisplayOrder() !== null) {
                $query .= '`display_order` = ?, ';
                $params[] = $model->getDisplayOrder();
            }
            if ($model->getStatus() !== null) {
                $query .= '`status` = ?, ';
                $params[] = $model->getStatus();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @param int $status
     * @return int
     */
    public static function updateStatus($id, $status)
    {
        $db = new Data();
        $query = 'UPDATE cms_gadget SET status = ? WHERE id = ?';
        return $db->execute($query, [$status, $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM cms_gadget WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteByPageId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM cms_gadget WHERE page_id = ?';
        return $db->execute($query, [$id]);
    }
    /**
     * @param int $id
     * @return int
     */
    public static function deleteByComponentId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM cms_gadget WHERE component_id = ?';
        return $db->execute($query, [$id]);
    }
    /**
     * @param int $id
     * @return int
     */
    public static function deleteByFilterId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM cms_gadget WHERE filter_id = ?';
        return $db->execute($query, [$id]);
    }

    /**
     * @param array $row
     * @param string $alias = gadget
     * @return Gadget
     */
    public static function join(array $row, $alias = 'gadget')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return Gadget
     */
    protected static function mapper(array $row, $alias = 'gadget')
    {
        $model = new Gadget(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'PageId'])) {
            $model->setPageId($row[$alias . 'PageId']);
            $model->setPage(\Rebond\Repository\Cms\PageRepository::join($row, $alias . '_page'));
        }
        if (isset($row[$alias . 'ComponentId'])) {
            $model->setComponentId($row[$alias . 'ComponentId']);
            $model->setComponent(\Rebond\Repository\Cms\ComponentRepository::join($row, $alias . '_component'));
        }
        if (isset($row[$alias . 'Col'])) {
            $model->setCol($row[$alias . 'Col']);
        }
        if (isset($row[$alias . 'FilterId'])) {
            $model->setFilterId($row[$alias . 'FilterId']);
            $model->setFilter(\Rebond\Repository\Cms\FilterRepository::join($row, $alias . '_filter'));
        }
        if (isset($row[$alias . 'CustomFilter'])) {
            $model->setCustomFilter($row[$alias . 'CustomFilter']);
        }
        if (isset($row[$alias . 'DisplayOrder'])) {
            $model->setDisplayOrder($row[$alias . 'DisplayOrder']);
        }
        if (isset($row[$alias . 'Status'])) {
            $model->setStatus($row[$alias . 'Status']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
