<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Cms;

use Rebond\Models\Cms\Content;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseContentRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = content
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'content')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.title AS ' . $alias . 'Title, ' .
                $alias . '.module_id AS ' . $alias . 'ModuleId, ' .
                $alias . '.app_id AS ' . $alias . 'AppId, ' .
                $alias . '.content_group AS ' . $alias . 'ContentGroup, ' .
                $alias . '.filter_id AS ' . $alias . 'FilterId, ' .
                $alias . '.author_id AS ' . $alias . 'AuthorId, ' .
                $alias . '.publisher_id AS ' . $alias . 'PublisherId, ' .
                $alias . '.url_friendly_title AS ' . $alias . 'UrlFriendlyTitle, ' .
                $alias . '.use_expiration AS ' . $alias . 'UseExpiration, ' .
                $alias . '.go_live_date AS ' . $alias . 'GoLiveDate, ' .
                $alias . '.expiry_date AS ' . $alias . 'ExpiryDate, ' .
                $alias . '.published_date AS ' . $alias . 'PublishedDate, ' .
                $alias . '.version AS ' . $alias . 'Version, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(content.id)');
        $db->buildQuery('from', 'cms_content `content`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return Content
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new Content() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_content `content`');
        $db->buildQuery('where', ['content.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new Content();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return Content
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_content `content`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

    /**
     * @param int $id
     * @param array $options = []
     * @return Content[]
     */
    public static function loadAllByModuleId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_content `content`');
        $db->buildQuery('where', ['content.module_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }
    /**
     * @param int $id
     * @param array $options = []
     * @return Content[]
     */
    public static function loadAllByFilterId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_content `content`');
        $db->buildQuery('where', ['content.filter_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }
    /**
     * @param int $id
     * @param array $options = []
     * @return Content[]
     */
    public static function loadAllByAuthorId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_content `content`');
        $db->buildQuery('where', ['content.author_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }
    /**
     * @param int $id
     * @param array $options = []
     * @return Content[]
     */
    public static function loadAllByPublisherId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_content `content`');
        $db->buildQuery('where', ['content.publisher_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param array $options = []
     * @return Content[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_content `content`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param Content $model
     * @return int
     */
    public static function save(Content $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO cms_content (`title`, module_id, `app_id`, `content_group`, filter_id, author_id, publisher_id, `url_friendly_title`, `use_expiration`, `go_live_date`, `expiry_date`, `published_date`, `version`, `created_date`, `modified_date`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            $params = [
                $model->getTitle(),
                $model->getModuleId(),
                $model->getAppId(),
                $model->getContentGroup(),
                $model->getFilterId(),
                $model->getAuthorId(),
                $model->getPublisherId(),
                $model->getUrlFriendlyTitle(),
                (int)$model->getUseExpiration(),
                $model->getGoLiveDate()->format('datetime'),
                $model->getExpiryDate()->format('datetime'),
                $model->getPublishedDate()->format('datetime'),
                $model->getVersion(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE cms_content SET ';
            $params = [];
            if ($model->getTitle() !== null) {
                $query .= '`title` = ?, ';
                $params[] = $model->getTitle();
            }
            if ($model->getModuleId() !== null) {
                $query .= 'module_id = ?, ';
                $params[] = $model->getModuleId();
            }
            if ($model->getAppId() !== null) {
                $query .= '`app_id` = ?, ';
                $params[] = $model->getAppId();
            }
            if ($model->getContentGroup() !== null) {
                $query .= '`content_group` = ?, ';
                $params[] = $model->getContentGroup();
            }
            if ($model->getFilterId() !== null) {
                $query .= 'filter_id = ?, ';
                $params[] = $model->getFilterId();
            }
            if ($model->getAuthorId() !== null) {
                $query .= 'author_id = ?, ';
                $params[] = $model->getAuthorId();
            }
            if ($model->getPublisherId() !== null) {
                $query .= 'publisher_id = ?, ';
                $params[] = $model->getPublisherId();
            }
            if ($model->getUrlFriendlyTitle() !== null) {
                $query .= '`url_friendly_title` = ?, ';
                $params[] = $model->getUrlFriendlyTitle();
            }
            if ($model->getUseExpiration() !== null) {
                $query .= '`use_expiration` = ?, ';
                $params[] = $model->getUseExpiration();
            }
            if ($model->getGoLiveDate() !== null) {
                $query .= '`go_live_date` = ?, ';
                $params[] = $model->getGoLiveDate()->format('datetime');
            }
            if ($model->getExpiryDate() !== null) {
                $query .= '`expiry_date` = ?, ';
                $params[] = $model->getExpiryDate()->format('datetime');
            }
            if ($model->getPublishedDate() !== null) {
                $query .= '`published_date` = ?, ';
                $params[] = $model->getPublishedDate()->format('datetime');
            }
            if ($model->getVersion() !== null) {
                $query .= '`version` = ?, ';
                $params[] = $model->getVersion();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM cms_content WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteByModuleId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM cms_content WHERE module_id = ?';
        return $db->execute($query, [$id]);
    }
    /**
     * @param int $id
     * @return int
     */
    public static function deleteByFilterId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM cms_content WHERE filter_id = ?';
        return $db->execute($query, [$id]);
    }
    /**
     * @param int $id
     * @return int
     */
    public static function deleteByAuthorId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM cms_content WHERE author_id = ?';
        return $db->execute($query, [$id]);
    }
    /**
     * @param int $id
     * @return int
     */
    public static function deleteByPublisherId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM cms_content WHERE publisher_id = ?';
        return $db->execute($query, [$id]);
    }

    /**
     * @param array $row
     * @param string $alias = content
     * @return Content
     */
    public static function join(array $row, $alias = 'content')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return Content
     */
    protected static function mapper(array $row, $alias = 'content')
    {
        $model = new Content(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'Title'])) {
            $model->setTitle($row[$alias . 'Title']);
        }
        if (isset($row[$alias . 'ModuleId'])) {
            $model->setModuleId($row[$alias . 'ModuleId']);
            $model->setModule(\Rebond\Repository\Cms\ModuleRepository::join($row, $alias . '_module'));
        }
        if (isset($row[$alias . 'AppId'])) {
            $model->setAppId($row[$alias . 'AppId']);
        }
        if (isset($row[$alias . 'ContentGroup'])) {
            $model->setContentGroup($row[$alias . 'ContentGroup']);
        }
        if (isset($row[$alias . 'FilterId'])) {
            $model->setFilterId($row[$alias . 'FilterId']);
            $model->setFilter(\Rebond\Repository\Cms\FilterRepository::join($row, $alias . '_filter'));
        }
        if (isset($row[$alias . 'AuthorId'])) {
            $model->setAuthorId($row[$alias . 'AuthorId']);
            $model->setAuthor(\Rebond\Repository\Core\UserRepository::join($row, $alias . '_author'));
        }
        if (isset($row[$alias . 'PublisherId'])) {
            $model->setPublisherId($row[$alias . 'PublisherId']);
            $model->setPublisher(\Rebond\Repository\Core\UserRepository::join($row, $alias . '_publisher'));
        }
        if (isset($row[$alias . 'UrlFriendlyTitle'])) {
            $model->setUrlFriendlyTitle($row[$alias . 'UrlFriendlyTitle']);
        }
        if (isset($row[$alias . 'UseExpiration'])) {
            $model->setUseExpiration($row[$alias . 'UseExpiration']);
        }
        if (isset($row[$alias . 'GoLiveDate'])) {
            $model->setGoLiveDate($row[$alias . 'GoLiveDate']);
        }
        if (isset($row[$alias . 'ExpiryDate'])) {
            $model->setExpiryDate($row[$alias . 'ExpiryDate']);
        }
        if (isset($row[$alias . 'PublishedDate'])) {
            $model->setPublishedDate($row[$alias . 'PublishedDate']);
        }
        if (isset($row[$alias . 'Version'])) {
            $model->setVersion($row[$alias . 'Version']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
