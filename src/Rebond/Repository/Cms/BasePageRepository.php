<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Cms;

use Rebond\Models\Cms\Page;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BasePageRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = page
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'page')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.title AS ' . $alias . 'Title, ' .
                $alias . '.parent_id AS ' . $alias . 'ParentId, ' .
                $alias . '.template_id AS ' . $alias . 'TemplateId, ' .
                $alias . '.layout_id AS ' . $alias . 'LayoutId, ' .
                $alias . '.css AS ' . $alias . 'Css, ' .
                $alias . '.js AS ' . $alias . 'Js, ' .
                $alias . '.in_nav_header AS ' . $alias . 'InNavHeader, ' .
                $alias . '.in_nav_side AS ' . $alias . 'InNavSide, ' .
                $alias . '.in_sitemap AS ' . $alias . 'InSitemap, ' .
                $alias . '.in_breadcrumb AS ' . $alias . 'InBreadcrumb, ' .
                $alias . '.in_nav_footer AS ' . $alias . 'InNavFooter, ' .
                $alias . '.friendly_url_path AS ' . $alias . 'FriendlyUrlPath, ' .
                $alias . '.friendly_url AS ' . $alias . 'FriendlyUrl, ' .
                $alias . '.redirect AS ' . $alias . 'Redirect, ' .
                $alias . '.class AS ' . $alias . 'Class, ' .
                $alias . '.permission AS ' . $alias . 'Permission, ' .
                $alias . '.display_order AS ' . $alias . 'DisplayOrder, ' .
                $alias . '.status AS ' . $alias . 'Status, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(page.id)');
        $db->buildQuery('from', 'cms_page `page`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return Page
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new Page() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_page `page`');
        $db->buildQuery('where', ['page.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new Page();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return Page
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_page `page`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

    /**
     * @param int $id
     * @param array $options = []
     * @return Page[]
     */
    public static function loadAllByParentId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_page `page`');
        $db->buildQuery('where', ['page.parent_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }
    /**
     * @param int $id
     * @param array $options = []
     * @return Page[]
     */
    public static function loadAllByTemplateId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_page `page`');
        $db->buildQuery('where', ['page.template_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }
    /**
     * @param int $id
     * @param array $options = []
     * @return Page[]
     */
    public static function loadAllByLayoutId($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_page `page`');
        $db->buildQuery('where', ['page.layout_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param array $options = []
     * @return Page[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_page `page`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param Page $model
     * @return int
     */
    public static function save(Page $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO cms_page (`title`, parent_id, template_id, layout_id, `css`, `js`, `in_nav_header`, `in_nav_side`, `in_sitemap`, `in_breadcrumb`, `in_nav_footer`, `friendly_url_path`, `friendly_url`, `redirect`, `class`, `permission`, `display_order`, `status`, `created_date`, `modified_date`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            $params = [
                $model->getTitle(),
                $model->getParentId(),
                $model->getTemplateId(),
                $model->getLayoutId(),
                $model->getCss(),
                $model->getJs(),
                (int)$model->getInNavHeader(),
                (int)$model->getInNavSide(),
                (int)$model->getInSitemap(),
                (int)$model->getInBreadcrumb(),
                (int)$model->getInNavFooter(),
                $model->getFriendlyUrlPath(),
                $model->getFriendlyUrl(),
                $model->getRedirect(),
                $model->getClass(),
                $model->getPermission(),
                $model->getDisplayOrder(),
                $model->getStatus(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE cms_page SET ';
            $params = [];
            if ($model->getTitle() !== null) {
                $query .= '`title` = ?, ';
                $params[] = $model->getTitle();
            }
            if ($model->getParentId() !== null) {
                $query .= 'parent_id = ?, ';
                $params[] = $model->getParentId();
            }
            if ($model->getTemplateId() !== null) {
                $query .= 'template_id = ?, ';
                $params[] = $model->getTemplateId();
            }
            if ($model->getLayoutId() !== null) {
                $query .= 'layout_id = ?, ';
                $params[] = $model->getLayoutId();
            }
            if ($model->getCss() !== null) {
                $query .= '`css` = ?, ';
                $params[] = $model->getCss();
            }
            if ($model->getJs() !== null) {
                $query .= '`js` = ?, ';
                $params[] = $model->getJs();
            }
            if ($model->getInNavHeader() !== null) {
                $query .= '`in_nav_header` = ?, ';
                $params[] = $model->getInNavHeader();
            }
            if ($model->getInNavSide() !== null) {
                $query .= '`in_nav_side` = ?, ';
                $params[] = $model->getInNavSide();
            }
            if ($model->getInSitemap() !== null) {
                $query .= '`in_sitemap` = ?, ';
                $params[] = $model->getInSitemap();
            }
            if ($model->getInBreadcrumb() !== null) {
                $query .= '`in_breadcrumb` = ?, ';
                $params[] = $model->getInBreadcrumb();
            }
            if ($model->getInNavFooter() !== null) {
                $query .= '`in_nav_footer` = ?, ';
                $params[] = $model->getInNavFooter();
            }
            if ($model->getFriendlyUrlPath() !== null) {
                $query .= '`friendly_url_path` = ?, ';
                $params[] = $model->getFriendlyUrlPath();
            }
            if ($model->getFriendlyUrl() !== null) {
                $query .= '`friendly_url` = ?, ';
                $params[] = $model->getFriendlyUrl();
            }
            if ($model->getRedirect() !== null) {
                $query .= '`redirect` = ?, ';
                $params[] = $model->getRedirect();
            }
            if ($model->getClass() !== null) {
                $query .= '`class` = ?, ';
                $params[] = $model->getClass();
            }
            if ($model->getPermission() !== null) {
                $query .= '`permission` = ?, ';
                $params[] = $model->getPermission();
            }
            if ($model->getDisplayOrder() !== null) {
                $query .= '`display_order` = ?, ';
                $params[] = $model->getDisplayOrder();
            }
            if ($model->getStatus() !== null) {
                $query .= '`status` = ?, ';
                $params[] = $model->getStatus();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @param int $status
     * @return int
     */
    public static function updateStatus($id, $status)
    {
        $db = new Data();
        $query = 'UPDATE cms_page SET status = ? WHERE id = ?';
        return $db->execute($query, [$status, $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM cms_page WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteByParentId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM cms_page WHERE parent_id = ?';
        return $db->execute($query, [$id]);
    }
    /**
     * @param int $id
     * @return int
     */
    public static function deleteByTemplateId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM cms_page WHERE template_id = ?';
        return $db->execute($query, [$id]);
    }
    /**
     * @param int $id
     * @return int
     */
    public static function deleteByLayoutId($id)
    {
        $db = new Data();
        $query = 'DELETE FROM cms_page WHERE layout_id = ?';
        return $db->execute($query, [$id]);
    }

    /**
     * @param array $row
     * @param string $alias = page
     * @return Page
     */
    public static function join(array $row, $alias = 'page')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return Page
     */
    protected static function mapper(array $row, $alias = 'page')
    {
        $model = new Page(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'Title'])) {
            $model->setTitle($row[$alias . 'Title']);
        }
        if (isset($row[$alias . 'ParentId'])) {
            $model->setParentId($row[$alias . 'ParentId']);
            $model->setParent(\Rebond\Repository\Cms\PageRepository::join($row, $alias . '_parent'));
        }
        if (isset($row[$alias . 'TemplateId'])) {
            $model->setTemplateId($row[$alias . 'TemplateId']);
            $model->setTemplate(\Rebond\Repository\Cms\TemplateRepository::join($row, $alias . '_template'));
        }
        if (isset($row[$alias . 'LayoutId'])) {
            $model->setLayoutId($row[$alias . 'LayoutId']);
            $model->setLayout(\Rebond\Repository\Cms\LayoutRepository::join($row, $alias . '_layout'));
        }
        if (isset($row[$alias . 'Css'])) {
            $model->setCss($row[$alias . 'Css']);
        }
        if (isset($row[$alias . 'Js'])) {
            $model->setJs($row[$alias . 'Js']);
        }
        if (isset($row[$alias . 'InNavHeader'])) {
            $model->setInNavHeader($row[$alias . 'InNavHeader']);
        }
        if (isset($row[$alias . 'InNavSide'])) {
            $model->setInNavSide($row[$alias . 'InNavSide']);
        }
        if (isset($row[$alias . 'InSitemap'])) {
            $model->setInSitemap($row[$alias . 'InSitemap']);
        }
        if (isset($row[$alias . 'InBreadcrumb'])) {
            $model->setInBreadcrumb($row[$alias . 'InBreadcrumb']);
        }
        if (isset($row[$alias . 'InNavFooter'])) {
            $model->setInNavFooter($row[$alias . 'InNavFooter']);
        }
        if (isset($row[$alias . 'FriendlyUrlPath'])) {
            $model->setFriendlyUrlPath($row[$alias . 'FriendlyUrlPath']);
        }
        if (isset($row[$alias . 'FriendlyUrl'])) {
            $model->setFriendlyUrl($row[$alias . 'FriendlyUrl']);
        }
        if (isset($row[$alias . 'Redirect'])) {
            $model->setRedirect($row[$alias . 'Redirect']);
        }
        if (isset($row[$alias . 'Class'])) {
            $model->setClass($row[$alias . 'Class']);
        }
        if (isset($row[$alias . 'Permission'])) {
            $model->setPermission($row[$alias . 'Permission']);
        }
        if (isset($row[$alias . 'DisplayOrder'])) {
            $model->setDisplayOrder($row[$alias . 'DisplayOrder']);
        }
        if (isset($row[$alias . 'Status'])) {
            $model->setStatus($row[$alias . 'Status']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
