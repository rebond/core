<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Cms;

use Rebond\Models\Cms\Module;
use Rebond\Repository\Data;

class ModuleRepository extends BaseModuleRepository
{
    /**
     * Load a Module by name
     * @param string $name
     * @return Module
     */
    public static function loadByName($name)
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_module module');
        $db->buildQuery('where', ['module.name = ?', $name]);
        return self::map($db);
    }
}
