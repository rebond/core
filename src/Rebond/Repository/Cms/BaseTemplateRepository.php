<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Cms;

use Rebond\Models\Cms\Template;
use Rebond\Models\DateTime;
use Rebond\Repository\AbstractRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;

class BaseTemplateRepository extends AbstractRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = template
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'template')
    {
        if (empty($properties)) {
            $list =
                $alias . '.id AS ' . $alias . 'Id, ' .
                $alias . '.title AS ' . $alias . 'Title, ' .
                $alias . '.summary AS ' . $alias . 'Summary, ' .
                $alias . '.filename AS ' . $alias . 'Filename, ' .
                $alias . '.menu AS ' . $alias . 'Menu, ' .
                $alias . '.menu_level AS ' . $alias . 'MenuLevel, ' .
                $alias . '.in_breadcrumb AS ' . $alias . 'InBreadcrumb, ' .
                $alias . '.side_nav AS ' . $alias . 'SideNav, ' .
                $alias . '.side_nav_level AS ' . $alias . 'SideNavLevel, ' .
                $alias . '.in_footer AS ' . $alias . 'InFooter, ' .
                $alias . '.footer_level AS ' . $alias . 'FooterLevel, ' .
                $alias . '.status AS ' . $alias . 'Status, ' .
                $alias . '.created_date AS ' . $alias . 'CreatedDate, ' .
                $alias . '.modified_date AS ' . $alias . 'ModifiedDate';
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(template.id)');
        $db->buildQuery('from', 'cms_template `template`');
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExist = false
     * @return Template
     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new Template() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_template `template`');
        $db->buildQuery('where', ['template.id = ?', $id]);
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new Template();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return Template
     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_template `template`');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }


    /**
     * @param array $options = []
     * @return Template[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', 'cms_template `template`');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param Template $model
     * @return int
     */
    public static function save(Template $model)
    {
        $db = new Data();
        if ($model->getId() == 0) {
            $query = 'INSERT INTO cms_template (`title`, `summary`, `filename`, `menu`, `menu_level`, `in_breadcrumb`, `side_nav`, `side_nav_level`, `in_footer`, `footer_level`, `status`, `created_date`, `modified_date`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)';
            $params = [
                $model->getTitle(),
                $model->getSummary(),
                $model->getFilename(),
                $model->getMenu(),
                $model->getMenuLevel(),
                (int)$model->getInBreadcrumb(),
                $model->getSideNav(),
                $model->getSideNavLevel(),
                (int)$model->getInFooter(),
                $model->getFooterLevel(),
                $model->getStatus(),
                $model->getCreatedDate()->format('createdDate'),
                (new DateTime())->format('modifiedDate') 
            ];
            $id = $db->execute($query, $params);
            $model->setId($id);
            return $id;
        } else {
            $query = 'UPDATE cms_template SET ';
            $params = [];
            if ($model->getTitle() !== null) {
                $query .= '`title` = ?, ';
                $params[] = $model->getTitle();
            }
            if ($model->getSummary() !== null) {
                $query .= '`summary` = ?, ';
                $params[] = $model->getSummary();
            }
            if ($model->getFilename() !== null) {
                $query .= '`filename` = ?, ';
                $params[] = $model->getFilename();
            }
            if ($model->getMenu() !== null) {
                $query .= '`menu` = ?, ';
                $params[] = $model->getMenu();
            }
            if ($model->getMenuLevel() !== null) {
                $query .= '`menu_level` = ?, ';
                $params[] = $model->getMenuLevel();
            }
            if ($model->getInBreadcrumb() !== null) {
                $query .= '`in_breadcrumb` = ?, ';
                $params[] = $model->getInBreadcrumb();
            }
            if ($model->getSideNav() !== null) {
                $query .= '`side_nav` = ?, ';
                $params[] = $model->getSideNav();
            }
            if ($model->getSideNavLevel() !== null) {
                $query .= '`side_nav_level` = ?, ';
                $params[] = $model->getSideNavLevel();
            }
            if ($model->getInFooter() !== null) {
                $query .= '`in_footer` = ?, ';
                $params[] = $model->getInFooter();
            }
            if ($model->getFooterLevel() !== null) {
                $query .= '`footer_level` = ?, ';
                $params[] = $model->getFooterLevel();
            }
            if ($model->getStatus() !== null) {
                $query .= '`status` = ?, ';
                $params[] = $model->getStatus();
            }
            $query .= '`modified_date` = ?, ';
            $params[] = (new DateTime())->format('modifiedDate');
            $query = rtrim(trim($query), ',');
            $query .= ' WHERE id = ?';
            $params[] = $model->getId();
            return $db->execute($query, $params);
        }
    }

    /**
     * @param int $id
     * @param int $status
     * @return int
     */
    public static function updateStatus($id, $status)
    {
        $db = new Data();
        $query = 'UPDATE cms_template SET status = ? WHERE id = ?';
        return $db->execute($query, [$status, $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public static function deleteById($id)
    {
        if ($id === 0) {
            return 0;
        }
        $query = 'DELETE FROM cms_template WHERE id = ?';
        $db = new Data();
        return $db->execute($query, [$id]);
    }


    /**
     * @param array $row
     * @param string $alias = template
     * @return Template
     */
    public static function join(array $row, $alias = 'template')
    {
        if (!isset($row[$alias . 'Id'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return Template
     */
    protected static function mapper(array $row, $alias = 'template')
    {
        $model = new Template(false);
        if (isset($row[$alias . 'Id'])) {
            $model->setId($row[$alias . 'Id']);
        }
        if (isset($row[$alias . 'Title'])) {
            $model->setTitle($row[$alias . 'Title']);
        }
        if (isset($row[$alias . 'Summary'])) {
            $model->setSummary($row[$alias . 'Summary']);
        }
        if (isset($row[$alias . 'Filename'])) {
            $model->setFilename($row[$alias . 'Filename']);
        }
        if (isset($row[$alias . 'Menu'])) {
            $model->setMenu($row[$alias . 'Menu']);
        }
        if (isset($row[$alias . 'MenuLevel'])) {
            $model->setMenuLevel($row[$alias . 'MenuLevel']);
        }
        if (isset($row[$alias . 'InBreadcrumb'])) {
            $model->setInBreadcrumb($row[$alias . 'InBreadcrumb']);
        }
        if (isset($row[$alias . 'SideNav'])) {
            $model->setSideNav($row[$alias . 'SideNav']);
        }
        if (isset($row[$alias . 'SideNavLevel'])) {
            $model->setSideNavLevel($row[$alias . 'SideNavLevel']);
        }
        if (isset($row[$alias . 'InFooter'])) {
            $model->setInFooter($row[$alias . 'InFooter']);
        }
        if (isset($row[$alias . 'FooterLevel'])) {
            $model->setFooterLevel($row[$alias . 'FooterLevel']);
        }
        if (isset($row[$alias . 'Status'])) {
            $model->setStatus($row[$alias . 'Status']);
        }
        if (isset($row[$alias . 'CreatedDate'])) {
            $model->setCreatedDate($row[$alias . 'CreatedDate']);
        }
        if (isset($row[$alias . 'ModifiedDate'])) {
            $model->setModifiedDate($row[$alias . 'ModifiedDate']);
        }
        return $model;
    }
}
