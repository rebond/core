<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Repository\Cms;

use Rebond\Models\Cms\Component;
use Rebond\Repository\Data;

class ComponentRepository extends BaseComponentRepository
{
    /**
     * Load all Component
     * @param array $options = []
     * @return Component[]
     */
    public static function loadFullComponents($options = [])
    {
        $db = new Data();
        $list = 'component.id AS componentId,
            component.module_id AS componentModuleId,
            module.title AS moduleTitle,
            component.title AS componentTitle';
        $db->buildQuery('select', $list);
        $db->buildQuery('from', 'cms_component component');
        $db->buildQuery('join', 'cms_module module ON module.id = component.module_id');
        $db->buildQuery('where', 'component.status = 1');
        $db->buildQuery('where', 'module.status = 1');
        $db->buildQuery('order', 'module.title, component.title');
        $db->extendQuery($options);
        return self::mapList($db);
    }
}
