<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Cms;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseUserSettings extends AbstractModel
{
    /* @var int */
    protected $id;
    protected $idItem;
    /* @var int */
    protected $mediaView;
    /* @var array */
    protected $mediaViewList;
    /* @var int */
    protected $mediaPaging;
    /* @var int */
    protected $contentPaging;
    /* @var int */
    protected $paging;

    public function __construct()
    {
        $this->mediaViewList = [0 => 'grid', 1 => 'list'];
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->mediaView = 0;
        $this->mediaPaging = 0;
        $this->contentPaging = 0;
        $this->paging = 0;
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\User
     */
    public function getIdItem($createIfNotExist = false)
    {
        if (!isset($this->idItem)) {
            $ns = $this->ns('\Rebond\Repository\Core\UserRepository');
            $this->idItem = $ns::loadById($this->id, $createIfNotExist);
        }
        return $this->idItem;
    }

    public function setId($value)
    {
        if ($this->id != $value) {
            $this->id = (int) $value;
            $this->idItem = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\User $model = null
     */
    public function setIdItem(\Rebond\Models\Core\User $model = null)
    {
        if (!isset($model)) {
            $this->idItem = null;
            return;
        }
        $this->id = (int) $model->getId();
        $this->idItem = $model;
    }

    /*
     * @return int
     */
    public function getMediaView()
    {
        return $this->mediaView;
    }

    /*
     * @return int
     */
    public function getMediaViewValue()
    {
        if (isset($this->mediaViewList[$this->mediaView])) {
            return $this->mediaViewList[$this->mediaView];
        }

        return Lang::lang('undefined');
    }

    /*
     * @return array
     */
    public function getMediaViewList()
    {
        return $this->mediaViewList;
    }

    /*
     * @param int $value
     */
    public function setMediaView($value)
    {
        $this->mediaView = (int) $value;
    }

    /*
     * @return int
     */
    public function getMediaPaging()
    {
        return $this->mediaPaging;
    }

    /*
     * @return int
     */
    public function getMediaPagingValue()
    {
        return \Rebond\Enums\Cms\Paging::lang($this->mediaPaging);
    }

    /*
     * @return array
     */
    public function getMediaPagingList()
    {
        return \Rebond\Enums\Cms\Paging::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setMediaPaging($value)
    {
        $this->mediaPaging = (int) $value;
    }

    /*
     * @return int
     */
    public function getContentPaging()
    {
        return $this->contentPaging;
    }

    /*
     * @return int
     */
    public function getContentPagingValue()
    {
        return \Rebond\Enums\Cms\Paging::lang($this->contentPaging);
    }

    /*
     * @return array
     */
    public function getContentPagingList()
    {
        return \Rebond\Enums\Cms\Paging::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setContentPaging($value)
    {
        $this->contentPaging = (int) $value;
    }

    /*
     * @return int
     */
    public function getPaging()
    {
        return $this->paging;
    }

    /*
     * @return int
     */
    public function getPagingValue()
    {
        return \Rebond\Enums\Cms\Paging::lang($this->paging);
    }

    /*
     * @return array
     */
    public function getPagingList()
    {
        return \Rebond\Enums\Cms\Paging::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setPaging($value)
    {
        $this->paging = (int) $value;
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'mediaView' => $this->getMediaView(),
            'mediaPaging' => $this->getMediaPaging(),
            'contentPaging' => $this->getContentPaging(),
            'paging' => $this->getPaging(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Save a UserSettings
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\UserSettingsRepository');
        return $ns::save($this);
    }

    /**
     * Delete a UserSettings
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\UserSettingsRepository');
        return $ns::deleteById($this->id);
    }
}
