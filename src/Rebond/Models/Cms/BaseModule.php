<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Cms;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseModule extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var string */
    protected $name;
    /* @var string */
    protected $title;
    /* @var string */
    protected $summary;
    /* @var int */
    protected $workflow;
    /* @var array */
    protected $workflowList;
    /* @var bool */
    protected $hasFilter;
    /* @var bool */
    protected $hasContent;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
        $this->workflowList = [0 => 'Automatic', 1 => '1 Step'];
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->name = '';
        $this->title = '';
        $this->summary = '';
        $this->workflow = 0;
        $this->hasFilter = false;
        $this->hasContent = true;
        $this->status = 1;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /*
     * @param string $value
     */
    public function setName($value)
    {
        $this->name = $value;
    }

    /*
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /*
     * @param string $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /*
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /*
     * @param string $value
     */
    public function setSummary($value)
    {
        $this->summary = $value;
    }

    /*
     * @return int
     */
    public function getWorkflow()
    {
        return $this->workflow;
    }

    /*
     * @return int
     */
    public function getWorkflowValue()
    {
        if (isset($this->workflowList[$this->workflow])) {
            return $this->workflowList[$this->workflow];
        }

        return Lang::lang('undefined');
    }

    /*
     * @return array
     */
    public function getWorkflowList()
    {
        return $this->workflowList;
    }

    /*
     * @param int $value
     */
    public function setWorkflow($value)
    {
        $this->workflow = (int) $value;
    }

    /*
     * @return bool
     */
    public function getHasFilter()
    {
        return $this->hasFilter;
    }

    /*
     * @param int $value
     */
    public function setHasFilter($value)
    {
        $this->hasFilter = (int) $value;
    }

    /*
     * @return bool
     */
    public function getHasContent()
    {
        return $this->hasContent;
    }

    /*
     * @param int $value
     */
    public function setHasContent($value)
    {
        $this->hasContent = (int) $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'title' => $this->getTitle(),
            'summary' => $this->getSummary(),
            'workflow' => $this->getWorkflow(),
            'hasFilter' => $this->getHasFilter(),
            'hasContent' => $this->getHasContent(),
            'status' => $this->getStatus(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Save a Module
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\ModuleRepository');
        return $ns::save($this);
    }

    /**
     * Delete a Module
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\ModuleRepository');
        return $ns::deleteById($this->id);
    }
}
