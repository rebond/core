<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Cms;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BasePage extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var string */
    protected $title;
    /* @var \Rebond\Models\Cms\Page */
    protected $parent;
    /* @var int */
    protected $parentId;
    /* @var \Rebond\Models\Cms\Template */
    protected $template;
    /* @var int */
    protected $templateId;
    /* @var \Rebond\Models\Cms\Layout */
    protected $layout;
    /* @var int */
    protected $layoutId;
    /* @var string */
    protected $css;
    /* @var string */
    protected $js;
    /* @var bool */
    protected $inNavHeader;
    /* @var bool */
    protected $inNavSide;
    /* @var bool */
    protected $inSitemap;
    /* @var bool */
    protected $inBreadcrumb;
    /* @var bool */
    protected $inNavFooter;
    /* @var string */
    protected $friendlyUrlPath;
    /* @var string */
    protected $friendlyUrl;
    /* @var string */
    protected $redirect;
    /* @var string */
    protected $class;
    /* @var string */
    protected $permission;
    /* @var int */
    protected $displayOrder;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->title = '';
        $this->parent = null;
        $this->parentId = 1;
        $this->template = null;
        $this->templateId = 0;
        $this->layout = null;
        $this->layoutId = 0;
        $this->css = '';
        $this->js = '';
        $this->inNavHeader = true;
        $this->inNavSide = true;
        $this->inSitemap = true;
        $this->inBreadcrumb = true;
        $this->inNavFooter = false;
        $this->friendlyUrlPath = '/';
        $this->friendlyUrl = '';
        $this->redirect = '';
        $this->class = '';
        $this->permission = '';
        $this->displayOrder = 0;
        $this->status = 1;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /*
     * @param string $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /*
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Cms\Page
     */
    public function getParent($createIfNotExist = false)
    {
        if (!isset($this->parent)) {
            $ns = $this->ns('\Rebond\Repository\Cms\PageRepository');
            $this->parent = $ns::loadById($this->parentId, $createIfNotExist);
        }
        return $this->parent;
    }

    /*
     * @param int $id
     */
    public function setParentId($id)
    {
        if ($this->parentId !== $id) {
            $this->parentId = (int) $id;
            $this->parent = null;
        }
    }

    /*
     * @param \Rebond\Models\Cms\Page $model = null
     */
    public function setParent(\Rebond\Models\Cms\Page $model = null)
    {
        if (!isset($model)) {
            $this->parent = null;
            return;
        }
        $this->parentId = (int) $model->getId();
        $this->parent = $model;
    }

    /*
     * @return int
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Cms\Template
     */
    public function getTemplate($createIfNotExist = false)
    {
        if (!isset($this->template)) {
            $ns = $this->ns('\Rebond\Repository\Cms\TemplateRepository');
            $this->template = $ns::loadById($this->templateId, $createIfNotExist);
        }
        return $this->template;
    }

    /*
     * @param int $id
     */
    public function setTemplateId($id)
    {
        if ($this->templateId !== $id) {
            $this->templateId = (int) $id;
            $this->template = null;
        }
    }

    /*
     * @param \Rebond\Models\Cms\Template $model = null
     */
    public function setTemplate(\Rebond\Models\Cms\Template $model = null)
    {
        if (!isset($model)) {
            $this->template = null;
            return;
        }
        $this->templateId = (int) $model->getId();
        $this->template = $model;
    }

    /*
     * @return int
     */
    public function getLayoutId()
    {
        return $this->layoutId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Cms\Layout
     */
    public function getLayout($createIfNotExist = false)
    {
        if (!isset($this->layout)) {
            $ns = $this->ns('\Rebond\Repository\Cms\LayoutRepository');
            $this->layout = $ns::loadById($this->layoutId, $createIfNotExist);
        }
        return $this->layout;
    }

    /*
     * @param int $id
     */
    public function setLayoutId($id)
    {
        if ($this->layoutId !== $id) {
            $this->layoutId = (int) $id;
            $this->layout = null;
        }
    }

    /*
     * @param \Rebond\Models\Cms\Layout $model = null
     */
    public function setLayout(\Rebond\Models\Cms\Layout $model = null)
    {
        if (!isset($model)) {
            $this->layout = null;
            return;
        }
        $this->layoutId = (int) $model->getId();
        $this->layout = $model;
    }

    /*
     * @return string
     */
    public function getCss()
    {
        return $this->css;
    }

    /*
     * @param string $value
     */
    public function setCss($value)
    {
        $this->css = $value;
    }

    /*
     * @return string
     */
    public function getJs()
    {
        return $this->js;
    }

    /*
     * @param string $value
     */
    public function setJs($value)
    {
        $this->js = $value;
    }

    /*
     * @return bool
     */
    public function getInNavHeader()
    {
        return $this->inNavHeader;
    }

    /*
     * @param int $value
     */
    public function setInNavHeader($value)
    {
        $this->inNavHeader = (int) $value;
    }

    /*
     * @return bool
     */
    public function getInNavSide()
    {
        return $this->inNavSide;
    }

    /*
     * @param int $value
     */
    public function setInNavSide($value)
    {
        $this->inNavSide = (int) $value;
    }

    /*
     * @return bool
     */
    public function getInSitemap()
    {
        return $this->inSitemap;
    }

    /*
     * @param int $value
     */
    public function setInSitemap($value)
    {
        $this->inSitemap = (int) $value;
    }

    /*
     * @return bool
     */
    public function getInBreadcrumb()
    {
        return $this->inBreadcrumb;
    }

    /*
     * @param int $value
     */
    public function setInBreadcrumb($value)
    {
        $this->inBreadcrumb = (int) $value;
    }

    /*
     * @return bool
     */
    public function getInNavFooter()
    {
        return $this->inNavFooter;
    }

    /*
     * @param int $value
     */
    public function setInNavFooter($value)
    {
        $this->inNavFooter = (int) $value;
    }

    /*
     * @return string
     */
    public function getFriendlyUrlPath()
    {
        return $this->friendlyUrlPath;
    }

    /*
     * @param string $value
     */
    public function setFriendlyUrlPath($value)
    {
        $this->friendlyUrlPath = $value;
    }

    /*
     * @return string
     */
    public function getFriendlyUrl()
    {
        return $this->friendlyUrl;
    }

    /*
     * @param string $value
     */
    public function setFriendlyUrl($value)
    {
        $this->friendlyUrl = $value;
    }

    /*
     * @return string
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    /*
     * @param string $value
     */
    public function setRedirect($value)
    {
        $this->redirect = $value;
    }

    /*
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /*
     * @param string $value
     */
    public function setClass($value)
    {
        $this->class = $value;
    }

    /*
     * @return string
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /*
     * @param string $value
     */
    public function setPermission($value)
    {
        $this->permission = $value;
    }

    /*
     * @return int
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /*
     * @param int $value
     */
    public function setDisplayOrder($value)
    {
        $this->displayOrder = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addDisplayOrder($value)
    {
        $this->displayOrder += (int) $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'parentId' => $this->getParentId(),
            'templateId' => $this->getTemplateId(),
            'layoutId' => $this->getLayoutId(),
            'css' => $this->getCss(),
            'js' => $this->getJs(),
            'inNavHeader' => $this->getInNavHeader(),
            'inNavSide' => $this->getInNavSide(),
            'inSitemap' => $this->getInSitemap(),
            'inBreadcrumb' => $this->getInBreadcrumb(),
            'inNavFooter' => $this->getInNavFooter(),
            'friendlyUrlPath' => $this->getFriendlyUrlPath(),
            'friendlyUrl' => $this->getFriendlyUrl(),
            'redirect' => $this->getRedirect(),
            'class' => $this->getClass(),
            'permission' => $this->getPermission(),
            'displayOrder' => $this->getDisplayOrder(),
            'status' => $this->getStatus(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Save a Page
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\PageRepository');
        return $ns::save($this);
    }

    /**
     * Delete a Page
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\PageRepository');
        return $ns::deleteById($this->id);
    }
}
