<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Cms;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseContent extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var string */
    protected $title;
    /* @var \Rebond\Models\Cms\Module */
    protected $module;
    /* @var int */
    protected $moduleId;
    /* @var int */
    protected $appId;
    /* @var int */
    protected $contentGroup;
    /* @var \Rebond\Models\Cms\Filter */
    protected $filter;
    /* @var int */
    protected $filterId;
    /* @var \Rebond\Models\Core\User */
    protected $author;
    /* @var int */
    protected $authorId;
    /* @var \Rebond\Models\Core\User */
    protected $publisher;
    /* @var int */
    protected $publisherId;
    /* @var string */
    protected $urlFriendlyTitle;
    /* @var bool */
    protected $useExpiration;
    /* @var DateTime */
    protected $goLiveDate;
    /* @var DateTime */
    protected $expiryDate;
    /* @var DateTime */
    protected $publishedDate;
    /* @var int */
    protected $version;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->title = '';
        $this->module = null;
        $this->moduleId = 0;
        $this->appId = 0;
        $this->contentGroup = 0;
        $this->filter = null;
        $this->filterId = 0;
        $this->author = null;
        $this->authorId = 0;
        $this->publisher = null;
        $this->publisherId = 0;
        $this->urlFriendlyTitle = '';
        $this->useExpiration = false;
        $this->goLiveDate = new DateTime();
        $this->expiryDate = new DateTime();
        $this->publishedDate = new DateTime();
        $this->version = 1;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /*
     * @param string $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /*
     * @return int
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Cms\Module
     */
    public function getModule($createIfNotExist = false)
    {
        if (!isset($this->module)) {
            $ns = $this->ns('\Rebond\Repository\Cms\ModuleRepository');
            $this->module = $ns::loadById($this->moduleId, $createIfNotExist);
        }
        return $this->module;
    }

    /*
     * @param int $id
     */
    public function setModuleId($id)
    {
        if ($this->moduleId !== $id) {
            $this->moduleId = (int) $id;
            $this->module = null;
        }
    }

    /*
     * @param \Rebond\Models\Cms\Module $model = null
     */
    public function setModule(\Rebond\Models\Cms\Module $model = null)
    {
        if (!isset($model)) {
            $this->module = null;
            return;
        }
        $this->moduleId = (int) $model->getId();
        $this->module = $model;
    }

    /*
     * @return int
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /*
     * @param int $value
     */
    public function setAppId($value)
    {
        $this->appId = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addAppId($value)
    {
        $this->appId += (int) $value;
    }

    /*
     * @return int
     */
    public function getContentGroup()
    {
        return $this->contentGroup;
    }

    /*
     * @param int $value
     */
    public function setContentGroup($value)
    {
        $this->contentGroup = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addContentGroup($value)
    {
        $this->contentGroup += (int) $value;
    }

    /*
     * @return int
     */
    public function getFilterId()
    {
        return $this->filterId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Cms\Filter
     */
    public function getFilter($createIfNotExist = false)
    {
        if (!isset($this->filter)) {
            $ns = $this->ns('\Rebond\Repository\Cms\FilterRepository');
            $this->filter = $ns::loadById($this->filterId, $createIfNotExist);
        }
        return $this->filter;
    }

    /*
     * @param int $id
     */
    public function setFilterId($id)
    {
        if ($this->filterId !== $id) {
            $this->filterId = (int) $id;
            $this->filter = null;
        }
    }

    /*
     * @param \Rebond\Models\Cms\Filter $model = null
     */
    public function setFilter(\Rebond\Models\Cms\Filter $model = null)
    {
        if (!isset($model)) {
            $this->filter = null;
            return;
        }
        $this->filterId = (int) $model->getId();
        $this->filter = $model;
    }

    /*
     * @return int
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\User
     */
    public function getAuthor($createIfNotExist = false)
    {
        if (!isset($this->author)) {
            $ns = $this->ns('\Rebond\Repository\Core\UserRepository');
            $this->author = $ns::loadById($this->authorId, $createIfNotExist);
        }
        return $this->author;
    }

    /*
     * @param int $id
     */
    public function setAuthorId($id)
    {
        if ($this->authorId !== $id) {
            $this->authorId = (int) $id;
            $this->author = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\User $model = null
     */
    public function setAuthor(\Rebond\Models\Core\User $model = null)
    {
        if (!isset($model)) {
            $this->author = null;
            return;
        }
        $this->authorId = (int) $model->getId();
        $this->author = $model;
    }

    /*
     * @return int
     */
    public function getPublisherId()
    {
        return $this->publisherId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\User
     */
    public function getPublisher($createIfNotExist = false)
    {
        if (!isset($this->publisher)) {
            $ns = $this->ns('\Rebond\Repository\Core\UserRepository');
            $this->publisher = $ns::loadById($this->publisherId, $createIfNotExist);
        }
        return $this->publisher;
    }

    /*
     * @param int $id
     */
    public function setPublisherId($id)
    {
        if ($this->publisherId !== $id) {
            $this->publisherId = (int) $id;
            $this->publisher = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\User $model = null
     */
    public function setPublisher(\Rebond\Models\Core\User $model = null)
    {
        if (!isset($model)) {
            $this->publisher = null;
            return;
        }
        $this->publisherId = (int) $model->getId();
        $this->publisher = $model;
    }

    /*
     * @return string
     */
    public function getUrlFriendlyTitle()
    {
        return $this->urlFriendlyTitle;
    }

    /*
     * @param string $value
     */
    public function setUrlFriendlyTitle($value)
    {
        $this->urlFriendlyTitle = $value;
    }

    /*
     * @return bool
     */
    public function getUseExpiration()
    {
        return $this->useExpiration;
    }

    /*
     * @param int $value
     */
    public function setUseExpiration($value)
    {
        $this->useExpiration = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getGoLiveDate()
    {
        return $this->goLiveDate;
    }

    public function setGoLiveDate($value)
    {
        $this->goLiveDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    public function setExpiryDate($value)
    {
        $this->expiryDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getPublishedDate()
    {
        return $this->publishedDate;
    }

    public function setPublishedDate($value)
    {
        $this->publishedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /*
     * @return int
     */
    public function getVersionValue()
    {
        return \Rebond\Enums\Cms\Version::lang($this->version);
    }

    /*
     * @return array
     */
    public function getVersionList()
    {
        return \Rebond\Enums\Cms\Version::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setVersion($value)
    {
        $this->version = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'moduleId' => $this->getModuleId(),
            'appId' => $this->getAppId(),
            'contentGroup' => $this->getContentGroup(),
            'filterId' => $this->getFilterId(),
            'authorId' => $this->getAuthorId(),
            'publisherId' => $this->getPublisherId(),
            'urlFriendlyTitle' => $this->getUrlFriendlyTitle(),
            'useExpiration' => $this->getUseExpiration(),
            'goLiveDate' => $this->getGoLiveDate()->format(),
            'expiryDate' => $this->getExpiryDate()->format(),
            'publishedDate' => $this->getPublishedDate()->format(),
            'version' => $this->getVersion(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Save a Content
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\ContentRepository');
        return $ns::save($this);
    }

    /**
     * Delete a Content
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\ContentRepository');
        return $ns::deleteById($this->id);
    }
}
