<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Cms;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseComponent extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var string */
    protected $title;
    /* @var \Rebond\Models\Cms\Module */
    protected $module;
    /* @var int */
    protected $moduleId;
    /* @var string */
    protected $summary;
    /* @var string */
    protected $method;
    /* @var bool */
    protected $canBeCached;
    /* @var int */
    protected $type;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->title = '';
        $this->module = null;
        $this->moduleId = 0;
        $this->summary = '';
        $this->method = '';
        $this->canBeCached = false;
        $this->type = 0;
        $this->status = 1;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /*
     * @param string $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /*
     * @return int
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Cms\Module
     */
    public function getModule($createIfNotExist = false)
    {
        if (!isset($this->module)) {
            $ns = $this->ns('\Rebond\Repository\Cms\ModuleRepository');
            $this->module = $ns::loadById($this->moduleId, $createIfNotExist);
        }
        return $this->module;
    }

    /*
     * @param int $id
     */
    public function setModuleId($id)
    {
        if ($this->moduleId !== $id) {
            $this->moduleId = (int) $id;
            $this->module = null;
        }
    }

    /*
     * @param \Rebond\Models\Cms\Module $model = null
     */
    public function setModule(\Rebond\Models\Cms\Module $model = null)
    {
        if (!isset($model)) {
            $this->module = null;
            return;
        }
        $this->moduleId = (int) $model->getId();
        $this->module = $model;
    }

    /*
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /*
     * @param string $value
     */
    public function setSummary($value)
    {
        $this->summary = $value;
    }

    /*
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /*
     * @param string $value
     */
    public function setMethod($value)
    {
        $this->method = $value;
    }

    /*
     * @return bool
     */
    public function getCanBeCached()
    {
        return $this->canBeCached;
    }

    /*
     * @param int $value
     */
    public function setCanBeCached($value)
    {
        $this->canBeCached = (int) $value;
    }

    /*
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /*
     * @return int
     */
    public function getTypeValue()
    {
        return \Rebond\Enums\Cms\Component::lang($this->type);
    }

    /*
     * @return array
     */
    public function getTypeList()
    {
        return \Rebond\Enums\Cms\Component::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setType($value)
    {
        $this->type = (int) $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'moduleId' => $this->getModuleId(),
            'summary' => $this->getSummary(),
            'method' => $this->getMethod(),
            'canBeCached' => $this->getCanBeCached(),
            'type' => $this->getType(),
            'status' => $this->getStatus(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Save a Component
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\ComponentRepository');
        return $ns::save($this);
    }

    /**
     * Delete a Component
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Cms\ComponentRepository');
        return $ns::deleteById($this->id);
    }
}
