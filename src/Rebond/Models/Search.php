<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Models;

class Search
{
    private $type;
    private $title;
    private $status;
    private $statusValue;
    private $link;

    public function __construct()
    {
        $this->type = '';
        $this->title = '';
        $this->status = 0;
        $this->statusValue = '';
        $this->link = '/';
    }

    /** 
     * Set Type
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /** 
     * Get Type
     */
    public function getType()
    {
        return $this->type;
    }

    /** 
     * Set Title
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /** 
     * Get Title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /** 
     * Set Link
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /** 
     * Get Link
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /** 
     * Set Status
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /** 
     * Get Status
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

     /** 
     * Set StatusValue
     * @param string $value
     */
    public function setStatusValue($value)
    {
        $this->statusValue = $value;
    }

    /** 
     * Get StatusValue
     * @return string
     */
    public function getStatusValue()
    {
        return $this->statusValue;
    }
}
