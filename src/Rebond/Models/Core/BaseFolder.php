<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Core;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseFolder extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var \Rebond\Models\Core\Folder */
    protected $parent;
    /* @var int */
    protected $parentId;
    /* @var string */
    protected $title;
    /* @var int */
    protected $displayOrder;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->parent = null;
        $this->parentId = 1;
        $this->title = '';
        $this->displayOrder = 0;
        $this->status = 1;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\Folder
     */
    public function getParent($createIfNotExist = false)
    {
        if (!isset($this->parent)) {
            $ns = $this->ns('\Rebond\Repository\Core\FolderRepository');
            $this->parent = $ns::loadById($this->parentId, $createIfNotExist);
        }
        return $this->parent;
    }

    /*
     * @param int $id
     */
    public function setParentId($id)
    {
        if ($this->parentId !== $id) {
            $this->parentId = (int) $id;
            $this->parent = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\Folder $model = null
     */
    public function setParent(\Rebond\Models\Core\Folder $model = null)
    {
        if (!isset($model)) {
            $this->parent = null;
            return;
        }
        $this->parentId = (int) $model->getId();
        $this->parent = $model;
    }

    /*
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /*
     * @param string $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /*
     * @return int
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /*
     * @param int $value
     */
    public function setDisplayOrder($value)
    {
        $this->displayOrder = (int) $value;
    }

    /*
     * @param int $value
     */
    public function addDisplayOrder($value)
    {
        $this->displayOrder += (int) $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'parentId' => $this->getParentId(),
            'title' => $this->getTitle(),
            'displayOrder' => $this->getDisplayOrder(),
            'status' => $this->getStatus(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Save a Folder
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Core\FolderRepository');
        return $ns::save($this);
    }

    /**
     * Delete a Folder
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Core\FolderRepository');
        return $ns::deleteById($this->id);
    }
}
