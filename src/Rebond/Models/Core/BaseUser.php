<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Core;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseUser extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var \Rebond\Models\Cms\UserSettings */
    protected $userSettings;
    /* @var string */
    protected $fullname;
    /* @var string */
    protected $email;
    /* @var string */
    protected $password;
    /* @var string */
    protected $firstName;
    /* @var string */
    protected $lastName;
    /* @var \Rebond\Models\Core\Media */
    protected $avatar;
    /* @var int */
    protected $avatarId;
    /* @var bool */
    protected $isAdmin;
    /* @var bool */
    protected $isDev;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->userSettings = null;
        $this->fullname = '';
        $this->email = '';
        $this->password = '';
        $this->firstName = '';
        $this->lastName = '';
        $this->avatar = null;
        $this->avatarId = 0;
        $this->isAdmin = false;
        $this->isDev = false;
        $this->status = 0;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return \Rebond\Models\Cms\UserSettings
     */
    public function getUserSettings()
    {
        if (!isset($this->userSettings)) {
            $ns = $this->ns('\Rebond\Repository\Cms\UserSettingsRepository');
            $this->userSettings = $ns::loadById($this->getId());
        }
        return $this->userSettings;
    }

    /*
     * @param \Rebond\Models\Cms\UserSettings $value
     */
    public function setUserSettings($value)
    {
        $this->userSettings = $value;
    }

    /*
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /*
     * @param string $value
     */
    public function setFullname($value)
    {
        $this->fullname = $value;
    }

    /*
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /*
     * @param string $value
     */
    public function setEmail($value)
    {
        $this->email = $value;
    }

    /*
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /*
     * @param string $value
     */
    public function setPassword($value)
    {
        $this->password = $value;
    }

    /*
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /*
     * @param string $value
     */
    public function setFirstName($value)
    {
        $this->firstName = $value;
    }

    /*
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /*
     * @param string $value
     */
    public function setLastName($value)
    {
        $this->lastName = $value;
    }

    /*
     * @return int
     */
    public function getAvatarId()
    {
        return $this->avatarId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\Media
     */
    public function getAvatar($createIfNotExist = true)
    {
        if (!isset($this->avatar)) {
            $ns = $this->ns('\Rebond\Repository\Core\MediaRepository');
            $this->avatar = $ns::loadById($this->avatarId, $createIfNotExist);
        }
        return $this->avatar;
    }

    /*
     * @param int $id
     */
    public function setAvatarId($id)
    {
        if ($this->avatarId !== $id) {
            $this->avatarId = (int) $id;
            $this->avatar = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\Media $model = null
     */
    public function setAvatar(\Rebond\Models\Core\Media $model = null)
    {
        if (!isset($model)) {
            $this->avatar = null;
            return;
        }
        $this->avatarId = (int) $model->getId();
        $this->avatar = $model;
    }

    /*
     * @return bool
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /*
     * @param int $value
     */
    public function setIsAdmin($value)
    {
        $this->isAdmin = (int) $value;
    }

    /*
     * @return bool
     */
    public function getIsDev()
    {
        return $this->isDev;
    }

    /*
     * @param int $value
     */
    public function setIsDev($value)
    {
        $this->isDev = (int) $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'fullname' => $this->getFullname(),
            'email' => $this->getEmail(),
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'avatarId' => $this->getAvatarId(),
            'isAdmin' => $this->getIsAdmin(),
            'isDev' => $this->getIsDev(),
            'status' => $this->getStatus(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getEmail();
    }

    /**
     * Save a User
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Core\UserRepository');
        return $ns::save($this);
    }

    /**
     * Delete a User
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Core\UserRepository');
        return $ns::deleteById($this->id);
    }
}
