<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Models\Core;

class Role extends BaseRole
{
    const ADMIN = 1;
    const USER_MANAGEMENT = 2;
    const ARCHITECT = 3;
    const EDITOR = 4;
    const PUBLISHER = 5;
    const VIEWER = 6;
    const MEMBER = 7;

    public function __construct($setDefault = true)
    {
        parent::__construct();
        if ($setDefault) {
            $this->setDefault();
        }
    }

    public function setDefault()
    {
        $this->setDefaultBase();
        // set your defaults values here
    }
}
