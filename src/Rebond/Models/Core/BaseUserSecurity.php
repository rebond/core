<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Core;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseUserSecurity extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var \Rebond\Models\Core\User */
    protected $user;
    /* @var int */
    protected $userId;
    /* @var string */
    protected $secure;
    /* @var int */
    protected $type;
    /* @var DateTime */
    protected $expiryDate;
    /* @var DateTime */
    protected $createdDate;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->user = null;
        $this->userId = 0;
        $this->secure = '';
        $this->type = 0;
        $this->expiryDate = new DateTime();
        $this->createdDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\User
     */
    public function getUser($createIfNotExist = false)
    {
        if (!isset($this->user)) {
            $ns = $this->ns('\Rebond\Repository\Core\UserRepository');
            $this->user = $ns::loadById($this->userId, $createIfNotExist);
        }
        return $this->user;
    }

    /*
     * @param int $id
     */
    public function setUserId($id)
    {
        if ($this->userId !== $id) {
            $this->userId = (int) $id;
            $this->user = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\User $model = null
     */
    public function setUser(\Rebond\Models\Core\User $model = null)
    {
        if (!isset($model)) {
            $this->user = null;
            return;
        }
        $this->userId = (int) $model->getId();
        $this->user = $model;
    }

    /*
     * @return string
     */
    public function getSecure()
    {
        return $this->secure;
    }

    /*
     * @param string $value
     */
    public function setSecure($value)
    {
        $this->secure = $value;
    }

    /*
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /*
     * @return int
     */
    public function getTypeValue()
    {
        return \Rebond\Enums\Core\Security::lang($this->type);
    }

    /*
     * @return array
     */
    public function getTypeList()
    {
        return \Rebond\Enums\Core\Security::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setType($value)
    {
        $this->type = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    public function setExpiryDate($value)
    {
        $this->expiryDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'userId' => $this->getUserId(),
            'secure' => $this->getSecure(),
            'type' => $this->getType(),
            'expiryDate' => $this->getExpiryDate()->format(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getUser();
    }

    /**
     * Save a UserSecurity
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Core\UserSecurityRepository');
        return $ns::save($this);
    }

    /**
     * Delete a UserSecurity
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Core\UserSecurityRepository');
        return $ns::deleteById($this->id);
    }
}
