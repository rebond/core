<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Core;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BasePermission extends AbstractModel
{
    /* @var int */
    protected $id;
    /* @var string */
    protected $title;
    /* @var string */
    protected $summary;
    /* @var int */
    protected $status;
    /* @var DateTime */
    protected $createdDate;
    /* @var DateTime */
    protected $modifiedDate;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->id = 0;
        $this->title = '';
        $this->summary = 'Allow the user to ';
        $this->status = 1;
        $this->createdDate = new DateTime();
        $this->modifiedDate = new DateTime();
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = (int) $value;
    }

    /*
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /*
     * @param string $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }

    /*
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /*
     * @param string $value
     */
    public function setSummary($value)
    {
        $this->summary = $value;
    }

    /*
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /*
     * @return int
     */
    public function getStatusValue()
    {
        return \Rebond\Enums\Core\Status::lang($this->status);
    }

    /*
     * @return array
     */
    public function getStatusList()
    {
        return \Rebond\Enums\Core\Status::toArrayLang();
    }

    /*
     * @param int $value
     */
    public function setStatus($value)
    {
        $this->status = (int) $value;
    }

    /*
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($value)
    {
        $this->createdDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

    /*
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    public function setModifiedDate($value)
    {
        $this->modifiedDate = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'summary' => $this->getSummary(),
            'status' => $this->getStatus(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * Save a Permission
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Core\PermissionRepository');
        return $ns::save($this);
    }

    /**
     * Delete a Permission
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Core\PermissionRepository');
        return $ns::deleteById($this->id);
    }
}
