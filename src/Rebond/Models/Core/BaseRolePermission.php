<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Rebond\Models\Core;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class BaseRolePermission extends AbstractModel
{
    /* @var \Rebond\Models\Core\Role */
    protected $role;
    /* @var int */
    protected $roleId;
    /* @var \Rebond\Models\Core\Permission */
    protected $permission;
    /* @var int */
    protected $permissionId;

    public function __construct()
    {
    }

    protected function setDefaultBase()
    {
        $this->role = null;
        $this->roleId = 0;
        $this->permission = null;
        $this->permissionId = 0;
    }

    public function getId()
    {
        return $this->roleId . '_' . $this->permissionId;
    }
    /*
     * @return int
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\Role
     */
    public function getRole($createIfNotExist = false)
    {
        if (!isset($this->role)) {
            $ns = $this->ns('\Rebond\Repository\Core\RoleRepository');
            $this->role = $ns::loadById($this->roleId, $createIfNotExist);
        }
        return $this->role;
    }

    /*
     * @return array
     */
    public function getRoleList()
    {
        if (!isset($this->roleList)) {
            $ns = $this->ns('\Rebond\Repository\Core\RolePermissionRepository');
            $this->roleList = $ns::loadAllByPermissionId($this->permissionId);
        }
        return $this->roleList;
    }

    /*
     * @param int $id
     */
    public function setRoleId($id)
    {
        if ($this->roleId !== $id) {
            $this->roleId = (int) $id;
            $this->role = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\Role $model = null
     */
    public function setRole(\Rebond\Models\Core\Role $model = null)
    {
        if (!isset($model)) {
            $this->role = null;
            return;
        }
        $this->roleId = (int) $model->getId();
        $this->role = $model;
    }

    /*
     * @return int
     */
    public function getPermissionId()
    {
        return $this->permissionId;
    }

    /*
     * @param bool $createIfNoExist = true
     * @return \Rebond\Models\Core\Permission
     */
    public function getPermission($createIfNotExist = false)
    {
        if (!isset($this->permission)) {
            $ns = $this->ns('\Rebond\Repository\Core\PermissionRepository');
            $this->permission = $ns::loadById($this->permissionId, $createIfNotExist);
        }
        return $this->permission;
    }

    /*
     * @return array
     */
    public function getPermissionList()
    {
        if (!isset($this->permissionList)) {
            $ns = $this->ns('\Rebond\Repository\Core\RolePermissionRepository');
            $this->permissionList = $ns::loadAllByRoleId($this->roleId);
        }
        return $this->permissionList;
    }

    /*
     * @param int $id
     */
    public function setPermissionId($id)
    {
        if ($this->permissionId !== $id) {
            $this->permissionId = (int) $id;
            $this->permission = null;
        }
    }

    /*
     * @param \Rebond\Models\Core\Permission $model = null
     */
    public function setPermission(\Rebond\Models\Core\Permission $model = null)
    {
        if (!isset($model)) {
            $this->permission = null;
            return;
        }
        $this->permissionId = (int) $model->getId();
        $this->permission = $model;
    }


    /*
     * @return array
     */
    public function toArray()
    {
        return [
            'roleId' => $this->getRoleId(),
            'permissionId' => $this->getPermissionId(),
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getRole();
    }

    /**
     * Save a RolePermission
     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\Rebond\Repository\Core\RolePermissionRepository');
        return $ns::save($this);
    }

    /**
     * Delete a RolePermission
     * @return int
     */
    public function delete()
    {
        $ns = $this->ns('\Rebond\Repository\Core\RolePermissionRepository');
        return $ns::deleteByIds($this->roleId, $this->permissionId);
    }
}
