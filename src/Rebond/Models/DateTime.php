<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Models;

use Rebond\Services\Lang;

class DateTime extends \DateTime
{
    /**
     * Create a date from a timestamp
     * @param int $timestamp
     * @return DateTime
     */
    public static function createFromTimestamp($timestamp)
    {
        $date = new self();
        $date->setTimestamp($timestamp);
        return $date;
    }

    /**
     * Return a formatted date
     * @param string $format = null
     * @return string
     */
    public function format($format = null)
    {
        if (!isset($format) || $format == 'smart')
            return $this->smartDate();

        if ($format == 'short')
            return $this->shortDate();

        $list = [
            'datetime'      => 'Y-m-d H:i:s',
            'createdDate'   => 'Y-m-d H:i:s',
            'modifiedDate'  => 'Y-m-d H:i:s',
            'date'          => 'Y-m-d',
            'time'          => 'Y-m-d H:i:s',
            'showDatetime'  => 'D d M Y H:i:s',
            'showDate'      => 'D, d M Y',
            'showTime'      => 'H:i',
            'shortDatetime' => 'd M H:i',
            'shortDate'     => 'd/m/Y',
            'string'        => 'Ymd-His',
        ];

        if (array_key_exists($format, $list))
            return parent::format($list[$format]);

        return parent::format($format);
    }

    /**
     * Return a smart date
     * @return string
     */
    private function smartDate()
    {
        $interval = $this->diff(new \DateTime());

        if ($interval->y > 0) {
            return $this->format('date');
        }
        if ($interval->m > 0) {
            return $this->format('j M');
        }

        $smartDate = '';

        if ($interval->d > 2) {
            $smartDate .= $interval->d . ' ' . Lang::locale('days');
        } else {
            if ($interval->d > 0) {
                $smartDate .= $interval->d . ' ' . Lang::locale('days') . ' ';
            }
            if ($interval->h > 2) {
                $smartDate .= $interval->h . 'h';
            } else {
                if ($interval->h > 0) {
                    $smartDate .= $interval->h . 'h ';
                }
                if ($interval->i > 2) {
                    $smartDate .= $interval->i . ' min';
                } else {
                    if ($interval->i > 0) {
                        $smartDate .= $interval->i . ' min ';
                    }
                    if ($interval->s > 0) {
                        $smartDate .= $interval->s . 's';
                    } else {
                        return Lang::locale('now');
                    }
                }
            }
        }

         // future
        if ($interval->invert == 1)
            return Lang::locale('in') . ' ' . $smartDate;

        // past
        return Lang::locale('ago', [$smartDate]);
    }

    /**
     * Return a short date
     * @return string
     */
    private function shortDate()
    {
        $now = new \DateTime();

        if ((int)$this->format('Y') != (int)$now->format('Y')) {
            return $this->format('date');
        }
        if ((int)$this->format('m') != (int)$now->format('m')) {
            return $this->format('j M');
        }
        if ((int)$this->format('d') + 1 == (int)$now->format('d')) {
            return Lang::lang('yesterday_at') . ' ' . $this->format('H:i');
        }
        if ((int)$this->format('d') - 1 == (int)$now->format('d')) {
            return Lang::lang('tomorrow_at') . ' ' . $this->format('H:i');
        }
        if ((int)$this->format('d') != (int)$now->format('d')) {
            return $this->format('D H:m:i');
        }

        return Lang::lang('today_at') . ' ' . $this->format('H:i');
    }
}
