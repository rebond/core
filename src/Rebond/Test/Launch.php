<?php
namespace Rebond\Test;

use Rebond\Services\Template;

class Launch
{
    public function __construct()
    {
    }

    public function start()
    {
        $html = $this->generic('Cms', 'Component');
        //$html .= $this->generic('Cms', 'Content');
        $html .= $this->generic('Cms', 'Filter');
        $html .= $this->generic('Cms', 'Gadget');
        $html .= $this->generic('Cms', 'Layout');
        $html .= $this->generic('Cms', 'Module');
        $html .= $this->generic('Cms', 'Page');
        $html .= $this->generic('Cms', 'Template');
        $html .= $this->generic('Cms', 'UserSettings');
        $html .= $this->generic('Core', 'Feedback');
        $html .= $this->generic('Core', 'Folder');
        $html .= $this->generic('Core', 'Log');
        $html .= $this->generic('Core', 'Media');
        $html .= $this->generic('Core', 'MediaLink');
        $html .= $this->generic('Core', 'Permission');
        $html .= $this->generic('Core', 'Role');
        $html .= $this->generic('Core', 'RolePermission');
        $html .= $this->generic('Core', 'Site');
        $html .= $this->generic('Core', 'User');
        $html .= $this->generic('Core', 'UserRole');
        $html .= $this->generic('Core', 'UserSecurity');
        return $html;
    }

    public function generic($package, $module)
    {
        $tpl = new Template(Template::ADMIN, ['admin', 'dev']);

        $entityModel = '\Rebond\Models\\' . $package . '\\' . $module;
        $entityForm = '\Rebond\Forms\\' . $package . '\\' . $module . 'Form';
        $entityData = '\Rebond\Repository\\' . $package . '\\' . $module . 'Repository';

        $model = new $entityModel();
        $form = new $entityForm($model);
        $validate = $form->validate();

        $items = $entityData::count();

        $tpl->set('title', $module);
        $tpl->set('count', count($items));
        $tpl->set('valid', $validate->isValid());
        $tpl->set('fields', $validate->getFields());
        return $tpl->render('test-entity');
    }
}
