<?php
// this controller can be used on the site as well, when a configuration error occurs

namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\AppLevel;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Result;

class Renderer
{
    /* @var Template */
    protected $tplMain;
    /* @var Template */
    protected $tplLayout;
    /* @var Template */
    protected $tplMaster;
    /* @var App */
    protected $app;
    /* @var */
    protected $signedUser;
    /* @var bool */
    protected $isSite;

    /** @param App $app */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->isSite = $this->app->getAppLevel() == AppLevel::WWW;
        $this->tplMain = new Template(Template::ADMIN, ['admin', 'error']);
        $this->tplLayout = new Template(Template::ADMIN, ['admin']);
        $this->tplMaster = new Template(Template::ADMIN, ['admin']);
    }

    /** @param bool $isRunning */
    private function setBaseTpl($isRunning = true)
    {
        if ($isRunning) {
            $this->signedUser = $this->app->getUser();
            $this->tplMaster->set('site', $this->app->getSite()->getTitle());
        }

        $this->tplMain->set('title', Lang::lang('error'));
        $this->tplMaster->set('title', Lang::lang('error'));

        $this->tplMaster->addCss('/node_modules/normalize-css/normalize.css');
        $this->tplMaster->addCss('https://fonts.googleapis.com/css?family=Work+Sans');
        $this->tplMaster->addCss('/node_modules/rebond-assets/css/rebond.css');

        // from site
        if ($this->isSite) {
            $this->tplMain->setPath(Template::MODULE, ['error']);
            $this->tplMain->set('adminUrl', $isRunning ? $this->app->getConfig('admin-url') : '');
            $this->tplLayout->setPath(Template::SITE, ['www']);
            $this->tplMaster->setPath(Template::SITE, ['www']);
            $this->tplMaster->addCss('/node_modules/rebond-assets-site/css/rebond-site.css');
            $this->tplMaster->addCss('/css/skin/' . $this->app->getSkin() .'/own.css');
        } else {
            $this->tplMaster->addCss('/node_modules/rebond-assets-admin/css/rebond-admin.css');
            $this->tplMaster->addCss('/css/own.css');
        }
    }

    /**
     * @param int $code
     * @param string $error
     * @return string
     */
    public function configError($code, $error)
    {
        $this->setBaseTpl(false);

        header('HTTP/1.1 500 Internal Server Error');

        $this->tplMain->set('title', '#' . $code . ' ' . Code::lang($code));
        $this->tplMain->set('error', $error);

        // layout
        $this->tplLayout->set('column1', $this->tplMain->render('config'));

        // master
        $this->tplMaster->set('title', 'Configuration Error');
        $this->tplMaster->set('site', 'Rebond');
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-error');
    }

    /**
     * @param array $log
     * @return string
     */
    public function detailError(array $log)
    {
        if (!$this->app->getUser()->getIsDev()) {
            return $this->genericError($log);
        }

        $this->setBaseTpl();
        header('HTTP/1.1 500 Internal Server Error');

        // main
        $this->tplMain->set('log', $log);
        $this->tplMain->set('currentUrl', $this->app->url());

        // layout
        $this->tplLayout->set('column1', $this->tplMain->render('detail'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-error');
    }

    public function inactive()
    {
        if ($this->app->isAjax()) {
            return [
                'result' => Result::ERROR,
                'message' => Lang::lang('error_maintenance'),
            ];
        }

        // force isSite
        $this->isSite = true;
        $this->setBaseTpl();

        // layout
        $this->tplLayout->set('column1', $this->tplMain->render('inactive'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-error');
    }

    /**
     * @param array $log
     * @return string
     */
    private function genericError(array $log)
    {
        $this->setBaseTpl();

        // main
        if (in_array($log['code'], [Code::PAGE_NOT_FOUND, Code::ADMIN_PAGE_NOT_FOUND])) {
            header('HTTP/1.1 404 Not Found');
            $title = Lang::lang('page_not_found');
            $error = Lang::lang('page_not_found_message');
        } else {
            header('HTTP/1.1 500 Internal Server Error');
            $title = Lang::lang('error');
            $error = Lang::lang('error_generic', [$log['code']]);
        }
        $this->tplMain->set('title', $title);
        $this->tplMain->set('error', $error);
        $this->tplMain->set('currentUrl', $this->app->url());

        // layout
        $this->tplLayout->set('column1', $this->tplMain->render('generic'));

        // master
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-error');
    }
}
