<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\Code;
use Rebond\Repository\Cms\PageRepository;

class Nav
{
     const HOME_PAGE = 1;
    /**
     * Read cms request uri
     * @param string $uri = ''
     * @return string
     * @throws \Exception
     */
    public static function readCmsRequest($uri = '')
    {
        if (stripos($uri, '?') !== false) {
            $uri = strstr($uri, '?', true);
        }
        $request = explode('/', $uri, 2);
        $action = (count($request) >= 2 && $request[1] != '') ? $request[1] : null;

        if (!isset($action)) {
            $page = PageRepository::loadById(self::HOME_PAGE);
            if (!isset($page)) {
                throw new \Exception(self::HOME_PAGE, Code::PAGE_NOT_FOUND);
            }

            $action = $page->getFriendlyUrl();
        }

        // return cleaned friendly url
        return '/' . rtrim(strtolower($action), '/');
    }

    /**
     * Read request uri
     * @param string $uri = ''
     * @return array = [$module, $action]
     */
    public static function readRequest($uri = '')
    {
        if (stripos($uri, '?') !== false) {
            $uri = strstr($uri, '?', true);
        }
        $request = explode('/', $uri, 3);
        $module = (count($request) >= 2 && $request[1] != '') ? ucfirst(strtolower($request[1])) : 'Home';
        $action = (count($request) >= 3 && $request[2] != '') ? $request[2] : 'index';
        $action = rtrim(strtolower($action), '/');
        $action = str_replace(['-', '/'], '_', $action);
        $action = Converter::toCamelCase($action);
        return [$module, $action];
    }

    /**
     * Remove port from a url
     * @param string $url
     * @return string
     */
    public static function removePort($url)
    {
        $i = strpos($url, ':');
        if ($i === false) {
            return $url;
        }
        return substr($url, 0, $i);
    }

    /**
     * Get domain
     * @param string $url
     * @param string $name = 'host'
     * @return string
     */
    public static function readUrl($url, $name = 'host')
    {
        $pu = parse_url($url);
        switch ($name) {
            case 'host':
                return (isset($pu['scheme']) && isset($pu['host']))
                    ? $pu['scheme'] . '://' . $pu['host']
                    : '';
                break;
            case 'domain':
                return (isset($pu['host']))
                    ? $pu['host']
                    : '';
                break;
            case 'path':
                return (isset($pu['path']))
                    ? $pu['path']
                    : '';
                break;
            case 'query':
                return (isset($pu['query']))
                    ? $pu['query']
                    : '';
                break;
            case 'fragment':
                return (isset($pu['fragment']))
                    ? $pu['fragment']
                    : '';
                break;
            case 'relative':
                $newUrl = (isset($pu['path'])) ? $pu['path'] : '';
                $newUrl .= (isset($pu['query'])) ? '?' . $pu['query'] : '';
                $newUrl .= (isset($pu['fragment'])) ? '#' . $pu['fragment'] : '';
                return $newUrl;
                break;
        }
        return '';
    }

    /**
     * Get url segment
     * @param int $num
     * @return string
     */
    public static function getUrlSegment($num)
    {
        $request = explode('/', $_SERVER['REQUEST_URI'], 5);
        return (count($request) > $num)
            ? strtolower($request[$num])
            : ($num == 2 ? 'index' : '');
    }

    /**
     * Check if menu is selected
     * @param string $menu
     * @param int $num
     * @return string
     */
    public static function renderActive($menu, $num)
    {
        $uri = self::getUrlSegment($num);
        return ($uri == $menu || ($num == 2 && $menu == 'index' && $uri == '')) ? ' class="active" ' : '';
    }

    /**
     * Render the pagination
     * @param int $count
     * @param string $url
     * @param int $maxByPage
     * @param int $current (between 1 and $count)
     * @param string $urlExtension = ''
     * @return string
     */
    public static function renderPaging($count, $url, $maxByPage, $current, $urlExtension = '')
    {
        $pages = (int) ceil($count / $maxByPage);

        $first = (($current - 2) < 1)
            ? 1
            : (($current + 3 > $pages)
                ? max(1, $pages - 4)
                : $current - 2);

        $last = (($current + 2) > $pages)
            ? $pages 
            : (($current < 4)
                ? min($pages, 5)
                : $current + 2);

        $tplPaging = new Template(Template::ADMIN, ['admin', 'filter']);

        $tplPaging->set('maxByPage', $maxByPage);
        $tplPaging->set('count', $count);
        $tplPaging->set('current', $current);
        $tplPaging->set('url', $url);
        $tplPaging->set('urlExtension', $urlExtension);
        $tplPaging->set('pages', $pages);
        $tplPaging->set('first', $first);
        $tplPaging->set('last', $last);

        return $tplPaging->render('paging');
    }

    public static function renderItemsPerPage($maxByPage)
    {
        $tplItemPerPage = new Template(Template::ADMIN, ['admin', 'filter']);
        $tplItemPerPage->set('maxByPage', $maxByPage);

        return $tplItemPerPage->render('itemsPerPage');
    }

    public static function getCurrentPage($page, $count, $maxByPage)
    {
        if ($page < 1) {
            $page = 1;
        }
        if ($page > ceil($count / $maxByPage) && $count > 0) {
            $page = ceil($count / $maxByPage);
        }
        return $page;
    }

    /**
     * Check if menu is selected
     * @param string $current
     * @param int $url
     * @return bool
     */
    public static function isActive($current, $url)
    {
        $segments = explode('/', $current);
        foreach ($segments as $segment) {
            if ($segment == $url) {
                return true;
            }
        }
        return false;
    }

    public static function isPreview()
    {
        if (!isset($_GET['preview']) || $_GET['preview'] != '1') {
            return false;
        }

        $user = App::instance()->getUser();
        if (!Auth::isAuthorized($user, 'admin.content.preview')) {
            return false;
        }

        return true;
    }
}
