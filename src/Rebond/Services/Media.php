<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\Ratio;
use Rebond\Enums\Core\Result;
use Rebond\Repository\Core\MediaRepository;

class Media
{
    /**
     * Upload a media
     * @param string $name
     * @param bool $isForm = false
     * @param int $folderId = 1
     * @param int $replaceId = null
     * @param string $path = ''
     * @return array = ['result' => int, 'mediaId' => int, 'filename' => string, message' => string]
     */
    public static function upload($name, $isForm = false, $folderId = 1, $replaceId = null, $path = '')
    {
        $mediaToReplace = null;

        if (!$isForm) {
            $result = self::generate($name);
            $originalFilename = $result['filename'];
        } else {
            $originalFilename = '';
            if (isset($_FILES[$name])) {
                $originalFilename = $_FILES[$name]['name'];
            }
            if ($originalFilename == '') {
                $remove = Converter::boolKey('remove-' . $name, 'post');
                if ($remove) {
                    return [
                        'result' => Result::SUCCESS,
                        'mediaId' => 0,
                        'filename' => ''
                    ];
                }
                return ['result' => Result::NO_CHANGE];
            }

            if (!isset($replaceId)) {
                $result = self::uploadNew($_FILES[$name], $path);
            } else {
                $mediaToReplace = MediaRepository::loadById($replaceId);
                if (!isset($mediaToReplace)) {
                    return [
                        'result' => Result::ERROR,
                        'message' => Lang::lang('error_media_replace')
                    ];
                }
                $result = self::replace($_FILES[$name], $mediaToReplace, $path);
            }
        }

        if ($result['result'] != Result::SUCCESS) {
            return $result;
        }

        $filename = $result['filename'];
        $mediaPath = App::instance()->getConfig('media') . $path;

        list($width, $height) = getimagesize($mediaPath . $filename);
        $ext = File::getExtension($mediaPath . $filename);
        $noExt = File::getNoExtension($originalFilename);

        $media = new \Rebond\Models\Core\Media();
        if (isset($mediaToReplace)) {
            $media = $mediaToReplace;
        } else {
            $media->setFolderId($folderId);
        }

        $media->setPath('');
        $media->setTitle($noExt);
        $media->setAlt($noExt);
        $media->setOriginalFilename($originalFilename);
        $media->setExtension($ext);
        $media->setWidth($width);
        $media->setHeight($height);
        $media->setFileSize(filesize($mediaPath . $filename));
        $media->setUpload($filename);

        if ($name != 'file') {
            $media->setIsSelectable(false);
        }

        $mimeType = new \finfo(FILEINFO_MIME_TYPE);
        $media->setMimeType($mimeType->file($mediaPath . $filename));

        $media->save();

        return [
            'result' => Result::SUCCESS,
            'mediaId' => $media->getId(),
            'filename' => $media->getOriginalFilename()
        ];
    }

    /**
     * @param string $name
     * @return array = ['result' => int, 'filename' => string]
     */
    public static function generate($name)
    {
        $width = 105;
        $height = 105;
        $pixel = 15;
        $filename = str_replace(['-', '@', '.'], '_', $name) . '.png';

        $image = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($image, 255, 255, 255);
        if (!isset($color)) {
            $color = imagecolorallocate($image, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
        }

        for ($i = 0; $i <= $width / 2; $i = $i + $pixel) {
            for ($j = 0; $j < $height; $j = $j + $pixel) {
                if (mt_rand(0, 1) == 0) {
                    imagefilledrectangle($image, $i, $j, $i + $pixel, $j + $pixel, $white);
                    imagefilledrectangle($image, $width - $i - $pixel, $j, $width - $i, $j + $pixel, $white);
                } else {
                    imagefilledrectangle($image, $i, $j, $i + $pixel, $j + $pixel, $color);
                    imagefilledrectangle($image, $width  - $i - $pixel, $j, $width  - $i, $j + $pixel, $color);
                }
            }
        }
        imagepng($image, App::instance()->getConfig('media') . $filename);

        return [
            'result' => Result::SUCCESS,
            'filename' => $filename
        ];
    }

    /**
     * Upload a new media
     * @param array $file
     * @param string $path = ''
     * @return array = ['result' => int, 'filename' => string, 'message' => string]
     */
    public static function uploadNew(array $file, $path = '')
    {
        $mediaPath = App::instance()->getConfig('media');
        $newName = str_replace(['-', ' '], '_', $file['name']);
        if (file_exists($mediaPath . $path . $newName)) {
            $newName = File::getNoExtension($newName) . '_' . uniqid() . '.' . File::getExtension($newName);
        }

        if (!move_uploaded_file($file['tmp_name'], $mediaPath . $path . $newName)) {
            return [
                'result' => Result::ERROR,
                'message' => Lang::lang('error_file_upload', [$file['name']])
            ];
        }

        return [
            'result' => Result::SUCCESS,
            'filename' => $newName
        ];
    }

    /**
     * Replace a media
     * @param array $file
     * @param \Rebond\Models\Core\Media $media
     * @param string $path = ''
     * @return array = ['result' => int, 'filename' => string, 'message' => string]
     */
    public static function replace(array $file, \Rebond\Models\Core\Media $media, $path = '')
    {
        $mediaPath = App::instance()->getConfig('media');
        $newName = $media->getUpload();

        if (file_exists($mediaPath . $path . $newName)
            && $media->getExtension() != File::getExtension($file['name'])) {
            return [
                'result' => Result::ERROR,
                'message' => Lang::lang('error_file_upload_format', [$media->getExtension(), File::getExtension($file['name'])])
            ];
        }

        if (!move_uploaded_file($file['tmp_name'], $mediaPath . $path . $newName)) {
            return [
                'result' => Result::ERROR,
                'message' => Lang::lang('error_file_upload', [$file['tmp_name']])
            ];
        }

        File::deleteAllMedia($path, $newName, false);

        return [
            'result' => Result::SUCCESS,
            'filename' => $newName
        ];
    }

    /**
     * Show image from media using options
     * @param \Rebond\Models\Core\Media $media
     * @param int $destWidth = 64
     * @param int $destHeight = 64
     * @param int $ratio = Ratio::SMART
     * @param int $compression = 10
     * @param bool $canZoom = false
     * @return string
     */
    public static function showFromModel($media, $destWidth = 64, $destHeight = 64, $ratio = Ratio::SMART, $compression = 10, $canZoom = false)
    {
        if (!isset($media)) {
            $media = new \Rebond\Models\Core\Media();
        }
        return self::show(App::instance()->getConfig('media'), App::instance()->getConfig('media-url'), $media->getPath(), $media->getUpload(), $destWidth, $destHeight, $ratio, $compression, $canZoom);
    }

    /**
     * Show image using options
     * @param string $serverPath
     * @param string $fullUrl
     * @param string $path
     * @param string $filename
     * @param int $destWidth = 64
     * @param int $destHeight = 64
     * @param int $ratio = Ratio::SMART
     * @param int $compression = 10
     * @param bool $canZoom = false
     * @return string
     */
    public static function show($serverPath, $fullUrl, $path, $filename, $destWidth = 64, $destHeight = 64, $ratio = Ratio::SMART, $compression = 10, $canZoom = false)
    {
        if (substr($path, 0, 1) == '/') {
            $path = substr($path, 1);
        }

        list($path, $filename) = self::validateFilename($serverPath, $path, $filename);
        $fullPath = $serverPath . $path;

        list($srcWidth, $srcHeight) = getimagesize($fullPath . $filename);
        $extension = File::getExtension($filename);

        list($calcWidth, $calcHeight) = self::getDimensions($srcWidth, $srcHeight, $destWidth, $destHeight, $ratio, $canZoom);

        // Crop
        $x = 0;
        $y = 0;
        if ($ratio == Ratio::SMART) {
            if ($calcWidth == $destWidth) {
                $srcHeight *= ($destHeight / $calcHeight);
                $y = ($calcHeight - $destHeight) * ($srcHeight / $destHeight) / 2;
            } else if ($calcHeight == $destHeight) {
                $srcWidth *= ($destWidth / $calcWidth);
                $x = ($calcWidth - $destWidth) * ($srcWidth / $destWidth) / 2;
            }
        } else {
            $destWidth = $calcWidth;
            $destHeight = $calcHeight;
        }

        $compression = self::getCompression($extension, $compression);

        // set new filename
        $newFilename = File::getNewFilename($filename, $destWidth, $destHeight, $ratio, $compression);

        self::create($serverPath, $path, $filename, $x, $y, $srcWidth, $srcHeight, $newFilename, $destWidth, $destHeight, $compression);
        return $fullUrl . $path . $newFilename;
    }

    /**
     * Crop image from media using options
     * @param \Rebond\Models\Core\Media $media
     * @param int $x
     * @param int $y
     * @param int $destWidth
     * @param int $destHeight
     * @return string
     */
    public static function cropFromModel(\Rebond\Models\Core\Media $media, $x, $y, $destWidth, $destHeight)
    {
        return self::crop(App::instance()->getConfig('media'), $media->getPath(), $media->getUpload(), $x, $y, $destWidth, $destHeight);
    }

    /**
     * Crop image from options
     * @param string $serverPath
     * @param string $path
     * @param string $filename
     * @param int $x
     * @param int $y
     * @param int $destWidth
     * @param int $destHeight
     * @return string
     */
    public static function crop($serverPath, $path, $filename, $x, $y, $destWidth, $destHeight)
    {
        list($path, $filename) = self::validateFilename($serverPath, $path, $filename);

        $extension = File::getExtension($filename);
        $temp = explode('-', File::getNoExtension($filename));
        $noExtension = $temp[0];
        $compression = self::getCompression($extension);

        $newName = $noExtension . '_' . $destWidth . 'x' . $destHeight . '.' . $extension;
        if (file_exists($serverPath . $path . $newName)) {
            $newName = $noExtension . '_' . $destWidth . 'x' . $destHeight . '_' . uniqid() . '.' . $extension;
        }

        self::create($serverPath, $path, $filename, $x, $y, $destWidth, $destHeight, $newName, $destWidth, $destHeight, $compression);
        return $newName;
    }


    // HELPER METHODS

    /**
     * Create new image from options
     * @param string $serverPath
     * @param string $path
     * @param string $filename
     * @param int $x
     * @param int $y
     * @param int $srcWidth
     * @param int $srcHeight
     * @param string $destFilename
     * @param int $destWidth
     * @param int $destHeight
     * @param int $compression
     * @return string
     */
    private static function create($serverPath, $path, $filename, $x, $y, $srcWidth, $srcHeight, $destFilename, $destWidth, $destHeight, $compression)
    {
        $fullPath = $serverPath . strtolower($path);

        if (file_exists($fullPath . $destFilename))
            return true;

        $extension = File::getExtension($destFilename);

        // generate image
        $newImage = imagecreatetruecolor($destWidth, $destHeight);
        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                $image = imagecreatefromjpeg($fullPath . $filename);
                break;
            case 'gif':
                $image = imagecreatefromgif ($fullPath . $filename);
                break;
            case 'png':
                imagealphablending($newImage, false);
                imagesavealpha($newImage, true);
                $image = imagecreatefrompng($fullPath . $filename);
                break;
            default:
                $image = imagecreatefromjpeg($fullPath . $filename);
        }

        imagecopyresampled($newImage, $image, 0, 0, $x, $y, $destWidth, $destHeight, $srcWidth, $srcHeight);

        // save image
        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                imagejpeg($newImage, $fullPath . $destFilename, $compression);
                break;
            case 'gif':
                imagegif ($newImage, $fullPath . $destFilename);
                break;
            case 'png':
                imagepng($newImage, $fullPath . $destFilename, $compression);
                break;
            default:
                imagejpeg($newImage, $fullPath . $destFilename, $compression);
        }
        imagedestroy($newImage);
        return true;
    }

    /**
     * Validate a filename
     * @param string $serverPath
     * @param string $path
     * @param string $filename
     * @return array = [string, string]
     */
    private static function validateFilename($serverPath, $path, $filename)
    {
        if ($filename == '' || !file_exists($serverPath . $path . $filename))
            return ['rebond/', 'default.png'];
        $ext = File::getExtension($filename);
        switch ($ext) {
            case 'png':
            case 'gif':
            case 'jpg':
            case 'jpeg':
                return [$path, $filename];
                break;
            case 'doc':
            case 'docx':
            case 'odt':
                return ['rebond/', 'doc.png'];
                break;
            case 'pdf':
                return ['rebond/', 'pdf.png'];
                break;
            case 'txt':
            case 'xml':
                return ['rebond/', 'txt.png'];
                break;
            case 'mp3':
                return ['rebond/', 'music.png'];
                break;
            case 'mp4':
            case 'avi':
                return ['rebond/', 'video.png'];
                break;
            case 'xls':
            case 'xlsx':
            case 'ods':
                return ['rebond/', 'xls.png'];
                break;
        }
        return ['rebond/', 'default.png'];
    }

    /**
     * Set compression of media
     * @param string $extension
     * @param int $compression
     * @return int
     */
    private static function getCompression($extension, $compression = 10)
    {
        if ($compression < 1) $compression = 1;
        if ($compression > 10) $compression = 10;
        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                $compression = $compression * 10;
                break;
            case 'png':
                $compression = 10 - $compression;
                break;
            default:
                $compression = $compression * 10;
        }
        return $compression;
    }

    /**
     * Get dimensions of new media
     * @param int $srcWidth
     * @param int $srcHeight
     * @param int $destWidth
     * @param int $destHeight
     * @param int $ratio
     * @param bool $canZoom
     * @return array = [$int, $int]
     */
    private static function getDimensions($srcWidth, $srcHeight, $destWidth, $destHeight, $ratio, $canZoom)
    {
        $destWidth = ($destWidth != 0) ? $destWidth : $srcWidth;
        $destHeight = ($destHeight != 0) ? $destHeight : $srcHeight;

        // set dimensions
        if ($destWidth  < 5) $destWidth  = 5;
        if ($destWidth  > 2000) $destWidth  = 2000;
        if ($destHeight < 5) $destHeight = 5;
        if ($destHeight > 2000) $destHeight = 2000;

        // set ratio
        if ($ratio < Ratio::NO_RATIO || $ratio > Ratio::SMART)
            $ratio = Ratio::SMART;

        if ($ratio !== Ratio::NO_RATIO) {

            $imageRatio = $srcWidth / $srcHeight;
            $newImageRatio = $destWidth / $destHeight;
            $diff = ($destWidth / $srcWidth) > ($destHeight / $srcHeight);

            if ($imageRatio != $newImageRatio) {
                switch ($ratio) {
                    // Keep width
                    case Ratio::KEEP_WIDTH:
                        $destHeight = round($destWidth / $imageRatio);
                        break;
                    // Keep height
                    case Ratio::KEEP_HEIGHT:
                        $destWidth = round($imageRatio * $destHeight);
                        break;
                    // Keep maximum
                    case Ratio::KEEP_MAX:
                        if ($diff)
                            $destHeight = round($destWidth / $imageRatio);
                        else
                            $destWidth = round($imageRatio * $destHeight);
                        break;
                    // Keep minimum
                    case Ratio::KEEP_MIN:
                        if ($diff)
                            $destWidth = round($imageRatio * $destHeight);
                        else
                            $destHeight = round($destWidth / $imageRatio);
                        break;
                    case Ratio::SMART:
                        if (1 - ($destWidth / $srcWidth) > 1 - ($destHeight / $srcHeight)) {
                            $destWidth = round($imageRatio * $destHeight);
                        } else {
                            $destHeight = round($destWidth / $imageRatio);
                        }
                        break;
                }
            }
            if (!$canZoom && $ratio != Ratio::SMART) {
                if ($destWidth > $srcWidth)
                    $destWidth = $srcWidth;
                if ($destHeight > $srcHeight)
                    $destHeight = $srcHeight;
            }
        }
        return [$destWidth, $destHeight];
    }
}
