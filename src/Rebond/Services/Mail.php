<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\Code;

class Mail
{
    /**
     * @param array $to
     * @param string $subject
     * @param string $message
     * @return int
     * @throws \Exception
     */
    public static function send(array $to, $subject, $message)
    {
        $app = App::instance();
        $messageObject = (new \Swift_Message($subject))
            ->setContentType('text/html')
            ->setFrom($app->getConfig('mail-email'))
            ->setTo($to)
            ->setBody($message);

        $transport = null;
        if ($app->getConfig('mail-type') == 'smtp') {
            $transport = (new \Swift_SmtpTransport(
                $app->getConfig('mail-host'),
                $app->getConfig('mail-port'),
                $app->getConfig('mail-option')
            ))
                ->setUsername($app->getConfig('mail-email'))
                ->setPassword(Security::decode($app->getConfig('mail-password')));
        } else if ($app->getConfig('mail-type') == 'mail') {
            $transport = new \Swift_SendmailTransport();
        }

        if (!isset($transport)) {
            throw new \Exception('', Code::MAIL_TRANSPORT_NOT_FOUND);
        }

        $mailer = new \Swift_Mailer($transport);

        try {
            return $mailer->send($messageObject);
        } catch (\Exception $e) {
            Log::log($e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine());
        }
        return 0;
    }
}
