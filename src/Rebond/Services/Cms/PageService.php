<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services\Cms;

use Rebond\Models\Cms\Page;
use Rebond\Repository\Cms\GadgetRepository;
use Rebond\Repository\Cms\PageRepository;
use Rebond\Services\Converter;
use Rebond\Services\Lang;
use Rebond\Services\Nav;

class PageService
{
    /**
     * buildNavRecursion
     * @param Page[] $pages
     * @param int $parentId
     * @param int $maxDepth
     * @param bool $checkHome = false
     * @param int $depth = 1
     * @return array
     */
    public static function buildNavRecursion(array $pages, $parentId, $maxDepth, $checkHome = false, $depth = 1)
    {
        $nav = [];
        $count = count($pages);
        $j = 0;
        for ($i = 0; $i < $count; $i++) {
            $addHome = $checkHome && $pages[$i]->getId() == 1 && $depth == 1;
            if ($addHome || $pages[$i]->getParentId() == $parentId) {
                $nav[$j]['id'] = $pages[$i]->getId();
                $nav[$j]['title'] = $addHome
                    ? Lang::lang('home')
                    : $pages[$i]->getTitle();

                $nav[$j]['friendlyurl'] = $pages[$i]->getFriendlyUrl();
                $nav[$j]['url'] = $pages[$i]->getFullUrl();
                $nav[$j]['displayOrder'] = $pages[$i]->getDisplayOrder();
                $nav[$j]['status'] = $pages[$i]->getStatus();
                unset($pages[$i]);
                $pages = array_values($pages);

                if (!$addHome && ($depth < $maxDepth || $maxDepth == 0)) {
                    $nav[$j]['children'] = self::buildNavRecursion($pages, $nav[$j]['id'], $maxDepth, $checkHome, $depth + 1);
                }
                $j++;
                $count--;
                $i--;
            }
        }
        return $nav;
    }

    /**
     * renderNav
     * @param array $nav
     * @param string $currentUrl = null
     * @param int $depth = 1
     * @return string
     */
    public static function renderNav($nav, $currentUrl = null, $depth = 1)
    {
        $html = '';
        foreach ($nav as $loop) {
            $selected = (isset($currentUrl) && Nav::isActive($currentUrl, $loop['friendlyurl']))
                ? 'class="active"'
                : '';

            if (isset($loop['children']) && count($loop['children']) > 0) {
                $html .= '<div class="link has-sub-menu"><a href="' . $loop['url'] . '" ' . $selected . '>' . $loop['title'] . '</a>';
                $html .= '<div class="sub-nav sub-nav-hide">';
                $html .= self::renderNav($loop['children'], $currentUrl, $depth + 1);
                $html .= '</div>';
                $html .= '</div>';
            } else {
                $html .= '<div class="link"><a href="' . $loop['url'] . '" ' . $selected . '>' . $loop['title'] . '</a></div>';
            }
        }
        return $html;
    }

    /**
     * Render page tree
     * @param array $tree
     * @return string
     */
    public static function renderTree(array $tree)
    {
        $html = '';
        $count = count($tree);
        for ($i = 0; $i < $count; $i++) {

            $active = ($tree[$i]['status'] == 1) ? '' : ' inactive';
            $home = ($tree[$i]['id'] == 1) ? ' home' : '';

            $html .= '<li id="p_' . $tree[$i]['id'] .  '" class="' . $active . $home . '">
                <a href="#" data-id="' . $tree[$i]['id'] . '">
                    <i class="icon">&nbsp;</i>
                    <span class="title">';
            if ($tree[$i]['id'] == 1) {
                $html .= Lang::lang('home');
            } else {
                $html .= $tree[$i]['title'];
            }
            if ($tree[$i]['displayOrder'] != 0) {
                $html .= '<span class="do">' . Lang::lang('order') . ' ' . $tree[$i]['displayOrder'] . '</span>';
            }
            $html .= '</span>
                </a>';
            if (isset($tree[$i]['children'])) {
                $html .= '<ul>';
                $html .= self::renderTree($tree[$i]['children']);
                $html .= '</ul>';
            }
            $html .= '</li>';
        }
        return $html;
    }

    /**
     * renderBreadcrumb
     * @param int $pageId
     * @return string
     */
    public static function renderBreadcrumb($pageId)
    {
        $render = false;
        $html = '';
        $title = Converter::stringKey('title');
        if (isset($title)) {
            $html = '<span class="active">' . $title . '</span>';
        }
        while ($pageId != 0) {
            $page = PageRepository::loadById($pageId);
            $pageId = $page->getParentId();
            if (!$page->getInBreadcrumb()) {
                continue;
            }
            if ($html == '') {
                $html = '<span class="active">' . $page->getTitle() . '</span>';
            } else {
                if ($page->getRedirect() != '') {
                    $isLink = 1;
                } else {
                    $options = [];
                    $options['where'][] = ['gadget.page_id = ?', $page->getId()];
                    $options['where'][] = 'gadget.status = 1';
                    $isLink = GadgetRepository::count($options);
                }

                $render = true;
                $html = ($isLink > 0)
                    ? '<a href="' . $page->getFullUrl() . '">' . $page->getTitle() . '</a>' . $html
                    : '<span>' . $page->getTitle() . '</span>' . $html;
            }
        }
        if ($render) {
            return '<div id="rb-breadcrumb">' . $html . '</div>';
        }
        return '';
    }

    /**
     * Render page list as a dropDownList
     */
    public static function renderList()
    {
        $options = [];
        $options['where'][] = 'page.status IN (0,1)';
        $options['order'][] = 'page.display_order, page.title';
        $pages = PageRepository::loadAll($options);

        $list = [];
        self::buildList($pages, 0, 0, $list);
        return $list;
    }

    private static function buildList(array $pages, $parentId, $depth, &$list)
    {
        $count = count($pages);
        for ($i = 0; $i < $count; $i++) {
            if ($pages[$i]->getParentId() == $parentId) {
                if ($pages[$i]->getId() == 1) {
                    $list[$pages[$i]->getId()] = Lang::lang('home');
                } else {
                    $list[$pages[$i]->getId()] = trim(str_pad('', $depth * 2, '-') . ' ' . $pages[$i]->getTitle());
                }
                $newParentId = $pages[$i]->getId();
                unset($pages[$i]);
                $pages = array_values($pages);
                self::buildList($pages, $newParentId, ++$depth, $list);
                $count--;
                $i--;
                $depth--;
            }
        }
    }
}
