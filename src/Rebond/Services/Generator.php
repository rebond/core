<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\AppLevel;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Status;
use Rebond\Generator\FormGenerator;
use Rebond\Generator\GadgetGenerator;
use Rebond\Generator\ModelGenerator;
use Rebond\Generator\MysqlGenerator;
use Rebond\Generator\RepositoryGenerator;
use Rebond\Generator\ServiceGenerator;
use Rebond\Generator\ViewGenerator;
use Rebond\Models\Cms\Module;
use Rebond\Repository\Cms\ModuleRepository;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;

class Generator
{
    private $app;
    private $model;
    private $info;
    private $errors;
    private $package;
    private $generateTpl;

    /**
     * @param App $app
     * @param string $package
     * @param bool $generateTpl
     * @throws \Exception
     */
    public function __construct(App $app, $package, $generateTpl)
    {
        if (!isset($package) || !in_array($package, ['app', 'bus', 'cms', 'core'])) {
            throw new \Exception(isset($package) ? $package : 'null', Code::INVALID_PACKAGE);
        }

        $this->app = $app;
        $this->info = [];
        $this->errors = 0;
        $this->package = ucfirst(strtolower($package));
        $this->generateTpl = $generateTpl;

        $path = $this->app->getPath('own-model-file');
        if (in_array($package, ['cms', 'core'])) {
            $path = $this->app->getPath('rebond-model-file');
        }

        $modelPath = $path . $package . '.yaml';
        if (file_exists($modelPath)) {
            $yaml = new Parser();
            $this->model = $yaml->parse(file_get_contents($modelPath));
        }
        $this->model = ($this->model != null) ? $this->model : [];
    }

    /** Run generation of the package */
    public function run()
    {
        $this->createNecessaryFolders();

        foreach ($this->model as $sqlEntity => $options) {
            $entity = Converter::toCamelCase($sqlEntity, true);
            $mainProperty = ($this->package != 'App') ? Converter::stringKey('mainProperty', $options, '') : '';
            $isEnabled = Converter::boolKey('isEnabled', $options, true);
            $hasContent = Converter::boolKey('hasContent', $options, true);
            $isPersistent = Converter::boolKey('isPersistent', $options, true);

            $this->info[] = '<h2>' . $entity . '</h2>';

            if (!$isEnabled) {
                $this->info[] = '<div class="text-warning">' . $entity . ' skipped.</div>';
                continue;
            }

            $valid = $this->isValidEntity($sqlEntity);
            if (!$valid['result']) {
                $this->info[] = '<div class="text-error">' . $valid['message'] . '.</div>';
                $this->errors++;
                continue;
            }

            $properties = Converter::arrayKey('properties', $options, []);
            $valid = $this->isValidProperties($properties, $mainProperty);
            if (!$valid['result']) {
                $this->info[] = '<div class="text-error">' . $valid['message'] . '</div>';
                $this->errors++;
                continue;
            }

            $views = Converter::arrayKey('views', $options, []);
            $valid = $this->isValidViews($views);
            if (!$valid['result']) {
                $this->info[] = '<div class="text-error">' . $valid['message'] . '</div>';
                $this->errors++;
                continue;
            }

            $this->createNecessaryViewFolders($entity);

            $service = new ServiceGenerator($this->app, $this->package, $entity);
            $service->produce();
            $this->info = array_merge($this->info, $service->getInfo());
            $this->errors += $service->getErrors();

            // For App with content, Cms, Core, Bus
            if ($hasContent) {
                $model = new ModelGenerator($this->app, $this->package, $entity, $mainProperty, $isPersistent, $properties);
                $model->produce();
                $this->info = array_merge($this->info, $model->getInfo());
                $this->errors += $model->getErrors();

                $form = new FormGenerator($this->app, $this->package, $entity, $properties);
                $form->produce();
                $this->info = array_merge($this->info, $form->getInfo());
                $this->errors += $form->getErrors();

                if ($isPersistent) {
                    if ($this->app->getAppLevel() != AppLevel::STANDALONE) {
                        $mysql = new MysqlGenerator($this->app, $this->package, $entity, $sqlEntity, $properties);
                        $mysql->produce();
                        $this->info = array_merge($this->info, $mysql->getInfo());
                        $this->errors += $mysql->getErrors();
                    }

                    $view = new ViewGenerator($this->app, $this->package, $entity, $sqlEntity, $mainProperty, $properties, $views, $this->generateTpl);
                    $view->produce();
                    $this->info = array_merge($this->info, $view->getInfo());
                    $this->errors += $view->getErrors();

                    $repository = new RepositoryGenerator($this->app, $this->package, $entity, $sqlEntity, $properties);
                    $repository->produce();
                    $this->info = array_merge($this->info, $repository->getInfo());
                    $this->errors += $repository->getErrors();
                }
            }

            if ($this->package == 'App') {
                $module = ModuleRepository::loadByName($entity);
                if (!isset($module)) {
                    $module = new Module();
                    $module->setTitle($entity);
                    $module->setSummary($entity . ' summary');
                    $module->setStatus(Status::DELETED);
                    $module->setHasContent($hasContent);
                    $module->save();
                }

                $gadget = new GadgetGenerator($this->app, $this->package, $entity, $views, $hasContent, $module->getId());
                $gadget->produce();
                $this->info = array_merge($this->info, $gadget->getInfo());
                $this->errors += $gadget->getErrors();
            }
        }
        $errorMsg = $this->errors > 0 ? ' (' . $this->errors . ' errors)' : '';
        Log::log(Code::PACKAGE_GENERATED, 'Generation of the [' . $this->package  . '] package' . $errorMsg, __FILE__, __LINE__);
    }

    public function getInfo()
    {
        return implode('', $this->info);
    }

    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param string $entity
     * @return bool
     */
    private function createNecessaryFolders()
    {
        if (in_array($this->package, ['Core', 'Cms'])) {
            return false;
        }

        $generated = $this->app->getPath('generated');
        $own = $this->app->getPath('own');
        $ownViews = $this->app->getPath('own-view');

        File::createFolder($generated . 'Forms/' . $this->package);
        File::createFolder($generated . 'Repository/' . $this->package);
        File::createFolder($generated . 'Models/' . $this->package);

        File::createFolder($own . 'Forms/' . $this->package);
        File::createFolder($own . 'Repository/' . $this->package);
        File::createFolder($own . 'Models/' . $this->package);
        File::createFolder($own . 'Services/' . $this->package);
        File::createFolder($own . 'Gadgets/App');
        File::createFolder($own . 'scripts/' . strtolower($this->package));
        return true;
    }

    private function createNecessaryViewFolders($entity)
    {
        if (in_array($this->package, ['Core', 'Cms'])) {
            return false;
        }

        $generatedViews = $this->app->getPath('generated-view');
        $ownViews = $this->app->getPath('own-view');

        File::createFolder($generatedViews . strtolower($this->package) . '/' . lcfirst($entity));
        File::createFolder($ownViews . strtolower($this->package) . '/' . lcfirst($entity));
        return true;
    }

    private function isValidEntity($entity)
    {
        $v = Validate::validate('entity', $entity, ['alphaNumUnderscore' => true]);
        if (!$v->isValid()) {
            return [
                'result' => false,
                'message' => 'Invalid entity: ' . $entity . '. It must start with a letter and contains alphanumeric characters.'
            ];
        }
        return ['result' => true];
    }

    private function isValidProperties($properties, $mainProperty)
    {
        $foundMainProperty = false;
        $v = Validate::validate('mainProperty', $mainProperty, ['alphaNumUnderscore' => false]);
        if (!$v->isValid()) {
            return [
                'result'  => false,
                'message' => 'Invalid mainProperty: ' . $mainProperty . '. It must start with a letter and contains alphanumeric characters.'
            ];
        }

        foreach ($properties as $property => $options) {
            $v = Validate::validate('property', $property, ['alphaNumUnderscore' => true]);
            if (!$v->isValid()) {
                return [
                    'result'  => false,
                    'message' => 'Invalid mainProperty: ' . $property . '. It must start with a letter and contains alphanumeric characters.'
                ];
            }
            $type = Converter::stringKey('type', $options, '');
            if (!in_array($type, ['primaryKey', 'string', 'hidden', 'email', 'password', 'confirm', 'integer', 'numeric',
                'datetime', 'date', 'time', 'media', 'bool', 'text', 'richText', 'enum', 'createdDate', 'modifiedDate',
                'foreignKey', 'staticKey', 'singleKey', 'multipleKey', 'foreignKeyLink', 'singleKeyLink'])
            ) {
                return [
                    'result'  => false,
                    'message' => 'Invalid type property: ' . $property . ' (' . $type . ').'
                ];
            }

            if (in_array($type, ['singleKey', 'foreignKey', 'multipleKey', 'singleKeyLink', 'foreignKeyLink'])) {
                $package = Converter::stringKey('package', $options, '');
                $model = Converter::stringKey('model', $options, '');
                $link = Converter::stringKey('link', $options, '');
                $validPackage = Validate::validate('package', $package, ['alphaNum' => true]);
                $validModel = Validate::validate('model', $model, ['alphaNumUnderscore' => true]);
                $validLink = Validate::validate('link', $link, ['alphaNumUnderscore' => true]);

                if (!$validPackage->isValid()) {
                    return [
                        'result'  => false,
                        'message' => 'Invalid package property: ' . $property . ' (' . $validPackage . ').'
                    ];
                }
                if (!$validModel->isValid()) {
                    return [
                        'result'  => false,
                        'message' => 'Invalid model property: ' . $property . ' (' . $validModel . ').'
                    ];
                }
                 if (!$validLink->isValid()) {
                    return [
                        'result'  => false,
                        'message' => 'Invalid link property: ' . $property . ' (' . $link . ').'
                    ];
                }
            }
            if ($property == $mainProperty && !in_array($type, ['richText', 'text', 'createdDate', 'modifiedDate', 'foreignKeyLink', 'singleKeyLink'])) {
                $foundMainProperty = true;
            }
        }

        if ($this->package != 'App' && !$foundMainProperty) {
            return [
                'result' => false,
                'message' => 'The main property could not be found or is not a valid type: ' . $mainProperty
            ];
        }

        return ['result' => true];
    }

    private function isValidViews($views)
    {
        if ($this->package == 'App') {
            $unknownViews = array_diff($views, ['cards', 'filtered-cards', 'editor', 'single']);
        } else if ($this->package == 'Bus') {
            $unknownViews = array_diff($views, ['filter', 'cards', 'table', 'editor', 'view', 'single']);
        } else {
            $unknownViews = array_diff($views, ['filter', 'table', 'editor', 'view', 'single']);
        }

        if (!empty($unknownViews)) {
            return [
                'result' => false,
                'message' => 'Some views are not valid: ' . implode(',', $unknownViews)
            ];
        }
        return ['result' => true];
    }
}
