<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services\Core;

use Rebond\App;
use Rebond\Enums\Cms\Version;
use Rebond\Enums\Core\Package;
use Rebond\Models\Core\MediaLink;
use Rebond\Repository\Core\MediaLinkRepository;

class MediaLinkService
{
    public static function getLinkedMedia($mediaId, $countOnly = false)
    {
        $apps = [];
        $groupedItems = [];
        $count = 0;

        $options = [];
        $options['where'][] = 'status = 1';
        $mediaLinks = MediaLinkRepository::loadAll($options);
        if (count($mediaLinks) == 0) {
            return $countOnly ? 0 : [];
        }

        /* @var MediaLink $mediaLink */
        foreach ($mediaLinks as $mediaLink) {
            if (in_array($mediaLink->getPackage(), [Package::CORE, Package::CMS])) {
                $namespace = App::instance()->getPath('rebond');
                $class = '\Rebond\Repository\\' . ucfirst($mediaLink->getPackageValue()) . '\\' . ucfirst($mediaLink->getEntity()) . 'Repository';
            } else {
                $namespace = App::instance()->getPath('own');
                $class = '\Own\Repository\\' . ucfirst($mediaLink->getPackageValue()) . '\\' . ucfirst($mediaLink->getEntity()) . 'Repository';
            }
            $file = 'Repository/' . ucfirst($mediaLink->getPackageValue()) . '/' . ucfirst($mediaLink->getEntity()) . 'Repository.php';

            if (file_exists($namespace . $file)) {
                if ($mediaLink->getPackage() == Package::APP) {
                    $options = [];
                    $options['where'][] = [$mediaLink->getField() . ' = ?', $mediaId];
                    $options['where'][] = ['content.version NOT IN (?)', [Version::DELETED, Version::OLD]];
                    if ($countOnly) {
                        $count += $class::count($options);
                    } else {
                        $apps = array_merge($apps, $class::loadAll($options));
                    }
                } else {
                    $options = [];
                    if (!$countOnly) {
                        $options['clearSelect'] = true;
                        $options['select'][] = $class::getList([$mediaLink->getIdField(), $mediaLink->getTitleField()]);
                    }
                    $options['where'][] = [$mediaLink->getField() . ' = ?', $mediaId];
                    if ($mediaLink->getFilter() != '') {
                        $options['where'][] = $mediaLink->getFilter();
                    }
                    if ($countOnly) {
                        $count += $class::count($options);
                    } else {
                        $groupedItems[$mediaLink->getId()] = [
                            'mediaLink' => $mediaLink,
                            'items' => $class::loadAll($options)
                        ];
                    }
                }
            }
        }

        if ($countOnly) {
            return $count;
        }

        return [
            'apps' => $apps,
            'groupedItems' => $groupedItems,
        ];
    }
}
