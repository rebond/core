<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services\Core;

use Rebond\Models\Core\Folder;
use Rebond\Repository\Core\FolderRepository;
use Rebond\Services\Lang;

class FolderService
{
    public function __construct()
    {
    }

    /**
     * @param Folder[] $folders
     * @param $parentId
     * @param $depth
     * @return array
     */
    public static function buildNavRecursion($folders, $parentId, $depth)
    {
        $nav = [];
        $count = count($folders);
        $j = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($folders[$i]->getParentId() == $parentId) {
                $nav[$j]['folder'] = $folders[$i];
                $newParentId = $folders[$i]->getId();
                unset($folders[$i]);
                $folders = array_values($folders);
                $nav[$j]['children'] = self::buildNavRecursion($folders, $newParentId, $depth + 1);
                $j++;
                $count--;
                $i--;
            }
        }
        return $nav;
    }

    /**
     * Render folder list as a dropDownList
     * @return array
     */
    public static function renderList()
    {
        $options = [];
        $options['where'][] = 'folder.status IN (0,1)';
        $options['order'][] = 'folder.display_order, folder.modified_date, folder.title';
        $folders = FolderRepository::loadAll($options);

        $list = [];
        self::buildList($folders, 0, 0, $list);
        return $list;
    }

    /**
     * @param Folder[] $folders
     * @param int $parentId
     * @param int $depth
     * @param array &$list
     */
    private static function buildList($folders, $parentId, $depth, &$list)
    {
        $count = count($folders);
        for ($i = 0; $i < $count; $i++) {
            if ($folders[$i]->getParentId() == $parentId) {
                if ($folders[$i]->getId() == 1) {
                    $list[$folders[$i]->getId()] = Lang::lang('root');
                } else {
                    $list[$folders[$i]->getId()] = trim(str_pad('', $depth * 2, '-') . ' ' . $folders[$i]->getTitle());
                }
                $newParentId = $folders[$i]->getId();
                unset($folders[$i]);
                $folders = array_values($folders);
                self::buildList($folders, $newParentId, ++$depth, $list);
                $count--;
                $i--;
                $depth--;
            }
        }
    }

     /**
     * Render folder tree
     * @param array $tree
     * @param int $depth = 0
     * @return string
     */
    public static function renderTree($tree, $depth = 0)
    {
        $html = '';
        $count = count($tree);
        for ($i = 0; $i < $count; $i++) {
            /* @var $folder Folder */
            $folder = $tree[$i]['folder'];

            $root = '';
            if ($folder->getId() == 1) {
                $root = ' home';
                $folder->setTitle(Lang::lang('root'));
            }
            $html .= '<li id="f_' . $folder->getId() . '" class="' . $root . '">
                <a href="#" data-id="' . $folder->getId() . '">
                    <i class="icon">&nbsp;</i>
                    <span class="title">' . $folder->getTitle();
            if ($folder->getDisplayOrder() != 0) {
                $html .= '<span class="do">' . Lang::lang('order') . ' ' . $folder->getDisplayOrder() . '</span>';
            }
            $html .= '</span>
                </a>';

            if (isset($tree[$i]['children'])) {
                $html .= '<ul>';
                $html .= self::renderTree($tree[$i]['children'], $depth + 1);
                $html .= '</ul>';
            }
            $html .= '</li>';
        }
        return $html;
    }
}
