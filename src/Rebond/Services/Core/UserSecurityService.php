<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services\Core;

use Rebond\App;
use Rebond\Enums\Core\Security As SecurityEnum;
use Rebond\Models\Core\User;
use Rebond\Models\Core\UserSecurity;
use Rebond\Repository\Core\UserSecurityRepository;
use Rebond\Models\DateTime;
use Rebond\Services\Security;

class UserSecurityService
{
    /**
     * Get all secure items by user
     * @param int $userId
     * @return UserSecurity[]
     */
    public static function getAllSecureByUserId($userId)
    {
        $options = [];
        $options['where'][] = ['user_id = ?', $userId];
        $options['where'][] = ['type IN (?)', [SecurityEnum::REMEMBER, SecurityEnum::API]];
        $items = UserSecurityRepository::loadAll($options);
        return $items;
    }

    /**
     * @param int $userId
     * @param UserSecurity[] $items
     * @param bool $newKey
     * @param bool $extendKey
     * @return bool
     */
    public static function renewApiKey($userId, $items, $newKey, $extendKey)
    {
        if (!$newKey && !$extendKey) {
            return false;
        }

        if (!isset($items)) {
            $items = self::getAllSecureByUserId($userId);
        }

        $apiModel = null;
        foreach ($items as $item) {
            if ($item->getType() == SecurityEnum::API) {
                $apiModel = $item;
                break;
            }
        }
        if (!isset($apiModel)) {
            $items[] = self::saveSecure($userId, SecurityEnum::API);
            return true;
        }

        if ($newKey) {
            $apiModel->setSecure(Security::generateToken());
        }
        $expiryDate = new DateTime();
        $expiryDate->add(new \DateInterval('P12M'));
        $apiModel->setExpiryDate($expiryDate);
        $apiModel->save();
        return true;
    }

    /**
     * @param string $secure
     * @param int $type
     * @return null|User
     */
    public static function getUserBySecure($secure, $type)
    {
        $options = [];
        $options['where'][] = ['secure = ?', $secure];
        if ($type != SecurityEnum::REMEMBER) {
            $options['where'][] = 'expiry_date >= CURDATE()';
        }
        $userSecurity = UserSecurityRepository::load($options);

        if (isset($userSecurity) && $userSecurity->getUser() != null) {
            switch ($type) {
                case SecurityEnum::REMEMBER:
                    $expire = time() + 60 * 60 * 24 * 7; // 7 days
                    setcookie('signedUser', $secure, $expire, '/', App::instance()->getConfig('cookie-domain'));
                    break;
                default:
            }
            return $userSecurity->getUser();
        }
        return null;
    }

    /**
     * @param int $userId
     * @param int $type
     * @return UserSecurity
     */
    public static function saveSecure($userId, $type)
    {
        $secure = Security::generateToken();
        $expiryDate = new DateTime();
        $month = 1;

        switch ($type) {
            case SecurityEnum::REMEMBER:
                $expire = time() + 60 * 60 * 24 * 7; // 7 days
                setcookie('signedUser', $secure, $expire, '/', App::instance()->getConfig('cookie-domain'));
                $month = 0;
                break;
            case SecurityEnum::API:
                $month = 12;
                break;
            default:
        }
        $expiryDate->add(new \DateInterval('P' . $month . 'M'));

        $userSecure = new UserSecurity();
        $userSecure->setUserId($userId);
        $userSecure->setSecure($secure);
        $userSecure->setType($type);
        $userSecure->setExpiryDate($expiryDate->format('Y-m-d'));
        $userSecure->save();
        return $userSecure;
    }
}
