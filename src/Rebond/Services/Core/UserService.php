<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Services\Core;

use Rebond\App;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Security as SecurityEnum;
use Rebond\Enums\Core\Result;
use Rebond\Enums\Core\Status;
use Rebond\Forms\Core\UserForm;
use Rebond\Models\Core\Role;
use Rebond\Models\Core\User;
use Rebond\Models\Core\UserRole;
use Rebond\Repository\Core\UserRepository;
use Rebond\Repository\Core\UserRoleRepository;
use Rebond\Repository\Core\UserSecurityRepository;
use Rebond\Services\Auth;
use Rebond\Services\Lang;
use Rebond\Services\Log;
use Rebond\Services\Mail;
use Rebond\Services\Media;
use Rebond\Services\Security;
use Rebond\Services\Session;
use Rebond\Services\Template;

class UserService
{
    const DEV_USER_ID = 1;

    /**
     * @param User $signedUser
     * @param int $id
     * @return bool
     */
    public static function isEditable(User $signedUser, $id)
    {
        return $signedUser->getIsDev() || $id != self::DEV_USER_ID;
    }

    /**
     * @param UserForm $form
     * @param bool $withPassword
     * @param bool $sendMail
     * @return bool
     */
    public static function createOrEdit(UserForm $userForm, $withPassword, $sendMail)
    {
        /* @var User $user */
        $user = $userForm->getModel();

        $properties = ['email', 'firstName', 'lastName'];
        if ($withPassword) {
            $properties[] = 'password';
        }

        if ($userForm->setFromPost()->validate($properties)->isValid()) {
            $resultUpload = Media::upload('avatarId', true);
            if ($resultUpload['result'] == Result::ERROR) {
                Session::set('allError', $resultUpload['message']);
                return false;
            }
            if ($resultUpload['result'] == Result::SUCCESS) {
                $user->setAvatarId($resultUpload['mediaId']);
            }
            if ($withPassword) {
                $user->setPassword(Security::encryptPassword($user->getPassword()));
            }
            $newUser = ($user->getId() == 0);
            $user->save();
            Log::log(Code::TRACK,
                'user ' . ($newUser ? 'created' : 'saved') . ' [' . $user->getId() . ':' . $user->getFullname() . ']',
                __FILE__,
                __LINE__);
            if ($newUser) {
                if (!$withPassword || $sendMail) {
                    self::registrationMail(App::instance(), $user);
                } else {
                    self::activate($user);
                }
            }
            return true;
        }
        Session::set('allError', $userForm->getValidation()->getMessage());
        return false;
    }

    /**
     * @param UserForm $userForm
     * @param string $confirm
     * @param string $btnPasswordReset
     * @return string
     */
    public static function confirmUser(UserForm $userForm, $confirm, $btnPasswordAction)
    {
        $user = UserSecurityService::getUserBySecure($confirm, SecurityEnum::CONFIRM);
        $tpl = new Template(Template::MODULE, ['app', 'user']);
        $success = false;

        if (!isset($user) || $user->getId() == 0) {
            return null;
        }

        $userForm->setModel($user);

        if ($user->getPassword() != '') {
            $success = true;
        } else if (isset($btnPasswordAction)) {
            $success = $userForm->changePassword(true, false);
        }

        if ($success) {
            UserService::activate($user);
            UserSecurityRepository::deleteSecure($user->getId(), $confirm);
            Session::set('allSuccess', Lang::lang('hi', [$user->getFirstName()]));
            Session::set('signedUser', $user->getId());
            App::instance()->setUser($user);
            return $tpl->render('register-confirm');
        }

        $tpl->set('action', 'password_create');
        $tpl->set('item', $userForm);
        $tpl->set('checkCurrentPassword', false);
        return $tpl->render('password-change');
    }

    /** @param User $user */
    public static function activate(User $user)
    {
        $user->setStatus(Status::ACTIVE);
        $user->save();

        $options = [];
        $options['where'][] = ['user_role.user_id = ?', $user->getId()];
        $options['where'][] = ['user_role.role_id = ?', $user->getId()];
        $role = UserRoleRepository::load($options);
        if (isset($role) && $role->getRoleId() == Role::MEMBER) {
            return;
        }
        $userRole = new UserRole();
        $userRole->setUserId($user->getId());
        $userRole->setRoleId(Role::MEMBER);
        $userRole->save();
    }

    /**
     * @param App $app
     * @param bool $isFormSubmitted
     * @param string $email
     * @param string $resetKey
     * @param bool $isPasswordReset
     * @return string
     */
    public static function forgotPassword(App $app, $isFormSubmitted, $email, $resetKey, $isPasswordReset)
    {
        $signedUser = $app->getUser();

        // auth
        if (Auth::isAuth($signedUser)) {
            Session::redirect('/profile');
        }

        $userForm = new UserForm($signedUser);
        $tpl = new Template(Template::MODULE, ['app', 'user']);

        if ($isFormSubmitted) {
            if ($email == '') {
                Session::set('siteError', Lang::lang('email_empty'));
            } else {
                $user = UserRepository::loadByEmail($email);
                if (isset($user)) {
                    self::resetPasswordMail($app, $user);
                    return $tpl->render('forgot-password-send');
                } else {
                    Session::set('siteError', Lang::lang('email_not_found'));
                }
            }
        }

        // reset password form
        if (isset($resetKey)) {
            $user = UserSecurityService::getUserBySecure($resetKey, SecurityEnum::RESET);
            if (isset($user)) {
                $userForm = new UserForm($user);
                // reset password
                if ($isPasswordReset) {
                    if ($userForm->changePassword(true, false)) {
                        Session::allSuccess('password_reset', '/profile');
                    }
                }

                $tpl->set('action', 'password_reset');
                $tpl->set('item', $userForm);
                $tpl->set('checkCurrentPassword', false);
                return $tpl->render('password-change');
            }
        }

        $tpl->set('item', $userForm);
        return $tpl->render('forgot-password');
    }

    /** @param User $signedUser */
    public static function signOut(User $signedUser)
    {
        // auth
        if (!Auth::isAuth($signedUser) || !isset($_POST['sign-out'])) {
            Session::redirect('/');
        }

        UserSecurityRepository::deleteSecure($signedUser->getId(), [
            SecurityEnum::REMEMBER,
            SecurityEnum::CONFIRM,
            SecurityEnum::RESET
        ]);
        setcookie('signedUser', '', time() - 3600, '/', App::instance()->getConfig('cookie-domain'));
        session_destroy();
        Session::redirect('/');
    }

    /**
     * @param (App $app
     * @param User $user
     * @return int
     */
    public static function registrationMail(App $app, User $user)
    {
        $siteTitle = $app->getSite()->getTitle();
        $userSecure = UserSecurityService::saveSecure($user->getId(), SecurityEnum::CONFIRM);

        $tpl = new Template(Template::MODULE, ['app', 'user']);
        $tpl->set('url', $app->getConfig('site-url'));
        $tpl->set('site', $siteTitle);
        $tpl->set('item', $user);
        $tpl->set('confirm', $userSecure->getSecure());

        $tplMail = new Template(Template::SITE, ['www']);
        $tplMail->set('title', Lang::lang('register'));
        $tplMail->set('site', $siteTitle);
        $tplMail->set('url', $app->getConfig('site-url'));
        $tplMail->set('layout', $tpl->render('mail-register'));

        return Mail::send([$user->getEmail()],
            $siteTitle . ' - ' . Lang::lang('register'),
            $tplMail->render('tpl-mail')
        );
    }

    /**
     * @param App $app
     * @param User $user
     * @return int
     */
    public static function resetPasswordMail(App $app, User $user)
    {
        $siteTitle = $app->getSite()->getTitle();
        $userSecure = UserSecurityService::saveSecure($user->getId(), SecurityEnum::RESET);

        $tpl = new Template(Template::MODULE, ['app', 'user']);
        $tpl->set('url', $app->getConfig('site-url'));
        $tpl->set('site', $siteTitle);
        $tpl->set('item', $user);
        $tpl->set('reset', $userSecure->getSecure());

        $tplMail = new Template(Template::SITE, ['www']);
        $tplMail->set('title', Lang::lang('password_reset'));
        $tplMail->set('site', $siteTitle);
        $tplMail->set('url', $app->getConfig('site-url'));
        $tplMail->set('layout', $tpl->render('mail-reset-password'));

        return Mail::send([$user->getEmail()],
            $siteTitle . ' - ' . Lang::lang('password_reset'),
            $tplMail->render('tpl-mail')
        );
    }
}
