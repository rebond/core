<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\AppLevel;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Result;

class Error
{
    /** @param \Exception|\Throwable $ex */
    public static function exception($ex)
    {
        $code = ($ex->getCode() != 0) ? $ex->getCode() : Code::UNHANDLED_EXCEPTION;
        try {
            self::kill($code, $ex->getMessage(), $ex->getFile(), $ex->getLine(), $ex->getTrace());
        } catch (\Throwable $ex) {
            echo 'Exception while killing application. ' . $ex->getCode() . ': ' . $ex->getMessage();
            exit();
        }
    }

    /**
     * @param int $code
     * @param string $error
     * @param string $file
     * @param int $line
     */
    public static function error($code, $error, $file, $line)
    {
        $code = ($code != 0) ? $code : Code::UNHANDLED_ERROR;
        try {
            self::kill($code, $error, $file, $line);
        } catch (\Throwable $ex) {
            echo 'Error while killing application. ' . $ex->getCode() . ': ' . $ex->getMessage();
            exit();
        }
    }

    /**
     * @param int $code
     * @param string $error
     * @param string $file
     * @param int $line
     * @param array $trace = []
     */
    private static function kill($code, $error, $file, $line, array $trace = [])
    {
        $app = App::instance();
        $app->checkRedirect($code, $error);

        $log = Log::log($code, $error, $file, $line, $trace);
        self::sendMail($log);

        if ($app->getAppLevel() == AppLevel::STANDALONE) {
            echo Code::value($code) . ': ' . $error;
            Session::close();
        }

        $errorRendering = new Renderer($app);

        if ($app->getAppLevel() == AppLevel::INSTALL) {
            echo $errorRendering->configError($code, $error);
            Session::close();
        }

        if (!$app->isRunning()) {
            if ($app->isAjax()) {
                $json = [
                    'result' => Result::ERROR,
                    'message' => Lang::locale('error_configuration')
                ];
                echo json_encode($json);
                Session::close();
            }

            echo $errorRendering->configError(Code::CONFIGURATION, Lang::locale('error_configuration'));
            Session::close();
        }

        if ($app->isAjax()) {
            if ($app->getUser()->getIsDev()) {
                $json = [
                    'result' => Result::ERROR,
                    'message' => Lang::lang('error_service') . ': ' . $error
                        . ' [<a href="' . $app->getConfig('admin-url') . 'tools/logs" target="_blank">'
                        . Lang::lang('logs_view')
                        . '</a>]'
                ];
                echo json_encode($json, JSON_UNESCAPED_SLASHES);
            } else {
                $json = [
                    'result' => Result::ERROR,
                    'message' => Lang::lang('error_unknown')
                ];
                echo json_encode($json);
            }
            exit();
        }

        echo $errorRendering->detailError($log);
        Session::close();
    }

    /**
     * @param array $log
     */
    public static function sendMail(array $log)
    {
        if (in_array($log['code'], [Code::PAGE_NOT_FOUND, Code::ADMIN_PAGE_NOT_FOUND])) {
            return;
        }

        $app = App::instance();

        if (!$app->getSite()->getSendMailOnError()) {
            return;
        }

        $emails = $app->getSite()->getMailListOnError();
        if ($emails == '') {
            return;
        }

        $tpl = new Template(Template::SITE, ['error']);
        $tpl->set('code', Code::value($log['code']));
        $tpl->set('error', $log['message']);
        $tpl->set('file', $log['file']);
        $tpl->set('line', $log['line']);

        $tplMail = new Template(Template::SITE, ['www']);
        $tplMail->set('title', Lang::lang('log'));
        $tplMail->set('site', $app->getSite()->getTitle());
        $tplMail->set('url', $app->getConfig('site-url'));
        $tplMail->set('layout', $tpl->render('mail'));

        Mail::send(
            explode(',', $emails),
            $app->getSite()->getTitle(),
            $tplMail->render('tpl-mail')
        );
    }
}
