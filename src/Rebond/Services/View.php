<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;

class View
{
    /**
     * View list of linked items
     * @param array $items
     * @param array $allItems = []
     * @return string
     */
    public static function viewForeignKeyLink(array $items, array $allItems = [])
    {
        if (empty($items)) {
            return '';
        }

        $html = '<div class="rb-form-item rb-check-list">';
        if (empty($allItems)) {
            foreach ($items as $item) {
                $html .= '<label class="check">' . $item . '</label>';
            }
        } else {
            foreach ($allItems as $item) {
                $selected = (in_array($item, $items)) ? ' highlight' : '';
                $html .= '<label class="check' . $selected . '">' . $item . '</label>';
            }
        }
        $html .= '</div>';
        return $html;
    }

    /**
     * View linked item
     * @param string $item
     * @return string
     */
    public static function viewSingleKeyLink($item)
    {
        $html = '<div class="rb-form-item">';
        $html .= '<div class="check">' . $item . '</div>';
        $html .= '</div>';
        return $html;
    }
}
