<?php
/**
 * Session
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Services;

use Rebond\Enums\Core\Code;

class Session
{
    public static function int($key, $default = 0)
    {
        return (int) self::get($key, $default);
    }

    public static function decimal($key, $default = 0.0)
    {
        return (float) self::get($key, $default);
    }

    public static function bool($key, $default = false)
    {
        return (bool) self::get($key, $default);
    }

    public static function get($key, $default = '', $clear = false)
    {
        if (!isset($_SESSION[$key])) {
            return $default;
        }
        $value = $_SESSION[$key];
        if ($clear) {
            unset($_SESSION[$key]);
        }
        return $value;
    }

    public static function getAndClear($key)
    {
        return self::get($key, '', true);
    }

    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function setAndRedirect($key, $value, $location)
    {
        self::set($key, $value);
        self::redirect($location);
    }

    public static function add($key, $value)
    {
        if (isset($_SESSION[$key])) {
            $_SESSION[$key] .= ' ' . $value;
        } else {
            $_SESSION[$key] = $value;
        }
    }

    public static function redirect($location)
    {
        if (!isset($location)) {
            throw new \Exception('', Code::INVALID_REDIRECT);
        }
        header('Location: ' . $location);
        self::close();
    }

    public static function kill($key)
    {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }

    public static function close()
    {
        Session::kill('redirect');
        session_write_close();
        exit();
    }

    public static function adminSuccess($text, $location, $params = [])
    {
        self::setAndRedirect('adminSuccess', Lang::lang($text, $params), $location);
    }

    public static function adminError($text, $location, $params = [])
    {
        self::setAndRedirect('adminError', Lang::lang($text, $params), $location);
    }

    public static function siteSuccess($text, $location, $params = [])
    {
        self::setAndRedirect('siteSuccess', Lang::lang($text, $params), $location);
    }

    public static function siteError($text, $location, $params = [])
    {
        self::setAndRedirect('siteError', Lang::lang($text, $params), $location);
    }

    public static function allSuccess($text, $location)
    {
        self::setAndRedirect('allSuccess', Lang::lang($text), $location);
    }

    public static function allError($text, $param, $location)
    {
        self::setAndRedirect('allError', Lang::lang($text, $param), $location);
    }

    public static function renderAdminResp()
    {
        $html = '<div id="rb-noti">';
        if (isset($_SESSION['adminSuccess'])) {
            $html .= '<div class="rb-success">' . $_SESSION['adminSuccess'] . '</div>';
        }
        if (isset($_SESSION['adminError'])) {
            $html .= '<div class="rb-error">' . $_SESSION['adminError'] . '</div>';
        }
        if (isset($_SESSION['allSuccess'])) {
            $html .= '<div class="rb-success">' . $_SESSION['allSuccess'] . '</div>';
        }
        if (isset($_SESSION['allError'])) {
            $html .= '<div class="rb-error">' . $_SESSION['allError'] . '</div>';
        }
        $html .= '</div>';
        self::kill('adminSuccess');
        self::kill('adminError');
        self::kill('allSuccess');
        self::kill('allError');
        return $html;
    }

    public static function renderSiteResp()
    {
        $html = '<div id="rb-noti">';
        if (isset($_SESSION['siteSuccess'])) {
            $html .= '<div class="rb-success">' . $_SESSION['siteSuccess'] . '</div>';
        }
        if (isset($_SESSION['siteError'])) {
            $html .= '<div class="rb-error">' . $_SESSION['siteError'] . '</div>';
        }
        if (isset($_SESSION['allSuccess'])) {
            $html .= '<div class="rb-success">' . $_SESSION['allSuccess'] . '</div>';
        }
        if (isset($_SESSION['allError'])) {
            $html .= '<div class="rb-error">' . $_SESSION['allError'] . '</div>';
        }
        $html .= '</div>';
        self::kill('siteSuccess');
        self::kill('siteError');
        self::kill('allSuccess');
        self::kill('allError');
        return $html;
    }

    public static function renderAll()
    {
        $html = '<table class="dual">';
        foreach ($_SESSION as $key => $value) {
            $html .= '<tr><th>' . $key . '</th><td>' . $value . '</td></tr>';
        }
        $html .= '</table>';
        return $html;
    }
}