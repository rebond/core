<?php

namespace Rebond\Services;

use Rebond\App;
use Rebond\Models\Core\User;
use Symfony\Component\Yaml\Parser;

class Menu
{
    /**
     * @param User $user
     * @return array
     */
    public static function buildMenu(User $user)
    {
        $menu = [];
        $menu[] = self::addMenu($user, 'admin.page', 'page', 'pages');
        $menu[] = self::addMenu($user, 'admin.content', 'content', 'content');
        $menu[] = self::addMenu($user, 'admin.media', 'media', 'documents');
        $menu[] = self::addMenu($user, 'admin.user', 'user', 'users');
        $menu[] = self::addMenu($user, 'admin.config', 'configuration', 'configuration');
        $menu[] = self::addMenu($user, 'admin.cms', 'cms', 'cms');
        $menu[] = self::addMenu($user, 'admin.tools', 'tools', 'tools');
        $menu[] = self::addMenu($user, 'admin.quickview', 'quickview', 'quick_view');
        $menu[] = self::addMenu($user, 'admin.dev', 'dev', 'developer');
        $menu[] = self::addMenu($user, 'admin.designer', 'designer', 'designer');
        $menu[] = self::addMenu($user, 'admin.own', 'own', 'own');
        return array_filter($menu);
    }

    /**
     * @param User $user
     * @param string $permission
     * @param string $link
     * @param string $text
     * @return array|null
     */
    private static function addMenu(User $user, $permission, $link, $text)
    {
        if ($user->hasAccess($permission, true)) {
            return [
                'link' => $link,
                'text' => Lang::lang($text),
            ];
        }
        return null;
    }

    /**
     * @param App $app
     * @param User $user
     * @param string $mainActive
     * @return array
     */
    public static function buildSubMenu(App $app, User $user, $mainActive)
    {
        $subMenu = [];
        switch ($mainActive) {
            case 'page':
                $subMenu[] = self::addSubMenu($user, 'admin.page.edit', '/page/edit', 'new-page', '', '', 'new');
                $subMenu[] = self::addSubMenu($user, 'admin.page', '/', 'edit-page', 'disabled', 'edit', 'edit');
                $subMenu[] = self::addSubMenu($user, 'admin.page', '/', 'edit-gadget', 'disabled', 'gadget', 'content_add');
                $subMenu[] = self::addSubMenu($user, 'admin.page', '#', 'delete-page', 'disabled', 'delete', 'delete');
                break;
            case 'content':
                $subMenu[] = self::addSubMenu($user, 'admin.content', '/content', '', '', 'index', 'content');
                $subMenu[] = self::addSubMenu($user, 'admin.content', '/content/filter', '', '', 'filter', 'filters');
                break;
            case 'media':
                $subMenu[] = self::addSubMenu($user, 'admin.media', '/media', '', '', 'index', 'view');
                $subMenu[] = self::addSubMenu($user, 'admin.media.upload', '/media/upload', '', '', 'upload', 'upload');
                $subMenu[] = self::addSubMenu($user, 'admin.media', '/media/folder-edit/', 'folder-add', '', 'folder-edit', 'folder_new');
                $subMenu[] = self::addSubMenu($user, 'admin.media', '/', 'folder-edit', 'disabled', 'folder-edit', 'folder_edit');
                $subMenu[] = self::addSubMenu($user, 'admin.media', '#', 'folder-delete', 'disabled', 'folder-delete', 'folder_delete');
                $subMenu[] = self::addSubMenu($user, 'admin.media', '/media/text-editor', '', '', 'text-editor', 'text_editor');
                break;
            case 'configuration':
                $subMenu[] = self::addSubMenu($user, 'admin.config', '/configuration/site', '', '', 'site', 'site');
                $subMenu[] = self::addSubMenu($user, 'admin.config', '/configuration/media-link', '', '', 'media-link', 'media_link');
                $subMenu[] = self::addSubMenu($user, 'admin.config', '/configuration/lang', '', '', 'lang', 'lang');
                $subMenu[] = self::addSubMenu($user, 'admin.config', '/configuration/cache', '', '', 'cache', 'cache');
                $subMenu[] = self::addSubMenu($user, 'admin.config', '/configuration/generated-photos', '', '', 'generated-photos', 'generated_photos');
                $subMenu[] = self::addSubMenu($user, 'admin.config', '/configuration/roles', '', '', 'role', 'roles');
                $subMenu[] = self::addSubMenu($user, 'admin.config', '/configuration/permissions', '', '', 'permission', 'permissions');
                break;
            case 'cms':
                $subMenu[] = self::addSubMenu($user, 'admin.cms', '/cms/template', '', '', 'template', 'templates');
                $subMenu[] = self::addSubMenu($user, 'admin.cms', '/cms/layout', '', '', 'layout', 'layouts');
                $subMenu[] = self::addSubMenu($user, 'admin.cms', '/cms/module', '', '', 'module', 'modules');
                $subMenu[] = self::addSubMenu($user, 'admin.cms', '/cms/component', '', '', 'component', 'components');
                break;
            case 'tools':
                $subMenu[] = self::addSubMenu($user, 'admin.tools', '/tools/logs', '', '', 'logs', 'logs');
                $subMenu[] = self::addSubMenu($user, 'admin.tools', '/tools/site-info', '', '', 'site-info', 'site_info');
                $subMenu[] = self::addSubMenu($user, 'admin.tools', '/tools/php-info', '', '', 'php-info', 'php_info');
                break;
            case 'dev':
                $subMenu[] = self::addSubMenu($user, 'admin.dev', '/dev/generator', '', '', 'generator', 'generator');
                $subMenu[] = self::addSubMenu($user, 'admin.dev', '/dev/database', '', '', 'database', 'database');
                $subMenu[] = self::addSubMenu($user, 'admin.dev', '/dev/feedback', '', '', 'feedback', 'feedback');
                $subMenu[] = self::addSubMenu($user, 'admin.dev', '/dev/test', '', '', 'test', 'tests');
                $subMenu[] = self::addSubMenu($user, 'admin.dev', '/dev/bin', '', '', 'bin', 'bin');
                $subMenu[] = self::addSubMenu($user, 'admin.dev', '/dev/reinitialize', '', '', 'reinitialize', 'reinitialize');
                break;
            case 'designer':
                $subMenu[] = self::addSubMenu($user, 'admin.designer', '/designer/css', '', '', 'css', 'stylesheets');
                $subMenu[] = self::addSubMenu($user, 'admin.designer', '/designer/tpl', '', '', 'tpl', 'templates');
                break;
            case 'profile':
                $subMenu[] = self::addSubMenu($user, '', '/profile/log', '', '', 'index', 'profile');
                $subMenu[] = self::addSubMenu($user, '', '/profile/change-password', '', '', 'password', 'password_change');
                $subMenu[] = self::addSubMenu($user, '', '/profile/security', '', '', 'security', 'security');
                break;
            case 'own':
                $adminMenuFile = $app->getPath('own-model-file') . 'admin-menu.yaml';
                if (file_exists($adminMenuFile)) {
                    $yml = new Parser();
                    $ownMenu = $yml->parse(file_get_contents($adminMenuFile));
                    foreach ($ownMenu as $menu => $item) {
                        $subMenu[] = self::addSubMenu($user, 'admin.own', $item['link'], '', '', $menu, $item['name']);
                    }
                }
            default:
        }

        return array_filter($subMenu);
    }

    /**
     * @param User $user
     * @param string $permission
     * @param string $link
     * @param string $text
     * @return array|null
     */
    private static function addSubMenu(User $user, $permission, $link, $id, $class, $menu, $text)
    {
        if ($user->hasAccess($permission, true)) {
            return [
                'link' => $link,
                'id' => $id,
                'class' => $class,
                'menu' => $menu,
                'text' => Lang::lang($text),
            ];
        }
        return null;
    }

    /**
     * @param User $user
     * @return array
     */
    public static function buildHomeMenu(User $user)
    {
        $homeMenu = [];
        $homeMenu[] = self::addHomeMenu($user, 'admin.page', 'fa-book', '/page', 'page', [
            ['link' => '/page/edit', 'text' => Lang::lang('new')]
        ]);
        $homeMenu[] = self::addHomeMenu($user, 'admin.content', 'fa-newspaper-o', '/content', 'content', [
            ['link' => '/content/edit/?module=Article', 'text' => Lang::lang('article_new')],
            ['link' => '/content/filter', 'text' => Lang::lang('filters')],
        ]);
        $homeMenu[] = self::addHomeMenu($user, 'admin.media', 'fa-picture-o', '/media', 'documents', [
            ['link' => '/media/upload', 'text' => Lang::lang('upload')],
        ]);
        $homeMenu[] = self::addHomeMenu($user, 'admin.user', 'fa-users', '/user', 'users', [
            ['link' => '/user/edit', 'text' => Lang::lang('new')],
        ]);
        $homeMenu[] = self::addHomeMenu($user, 'admin.config', 'fa-wrench', '/configuration', 'configuration', [
            ['link' => '/configuration/site', 'text' => Lang::lang('site')],
        ]);
        $homeMenu[] = self::addHomeMenu($user, 'admin.cms', 'fa-cogs', '/cms', 'cms', []);
        $homeMenu[] = self::addHomeMenu($user, 'admin.tools', 'fa-tachometer', '/tools', 'tools', [
            ['link' => '/tools/logs', 'text' => Lang::lang('logs')],
            ['link' => '/tools/site-info', 'text' => Lang::lang('site_info')],
        ]);
        $homeMenu[] = self::addHomeMenu($user, 'admin.dev', 'fa-code', '/dev', 'developer', [
            ['link' => '/dev/generator', 'text' => Lang::lang('generator')],
        ]);
        $homeMenu[] = self::addHomeMenu($user, 'admin.designer', 'fa-puzzle-piece', '/designer', 'designer', [
            ['link' => '/designer/css', 'text' => Lang::lang('stylesheets')],
            ['link' => '/designer/tpl', 'text' => Lang::lang('templates')],
        ]);

        return array_filter($homeMenu);
    }

    private static function addHomeMenu(User $user, $permission, $icon, $link, $text, array $shortcuts)
    {
        if ($user->hasAccess($permission, true)) {
            return [
                'icon' => $icon,
                'link' => $link,
                'text' => Lang::lang($text),
                'shortcuts' => $shortcuts,
            ];

        }
        return null;
    }
}