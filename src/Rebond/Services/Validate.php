<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Services;

use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Result;
use Rebond\Models\DateTime;
use Rebond\Models\Field;

class Validate
{
    const SAFE_CHARS = [' ', '/', '-', '_', ',', '.', ':', '=', '+', '?', '#', '&', '@'];

    /**
     * Validate a Field
     * @param string $field
     * @param string $value
     * @param array $validation
     * @return Field
     */
    public static function validate($field, $value, array $validation)
    {
        foreach ($validation as $key => $condition) {
            switch ($key) {
                // required
                case 'required':
                    $resultField = self::required($field, $value, $condition);
                    break;
                // file
                case 'image':
                    $resultField = self::validateImage($field, $condition);
                    break;
                case 'document':
                    $resultField = self::validateDocument($field, $condition);
                    break;
                // type
                case 'date':
                case 'datetime':
                    $resultField = self::date($field, $value, $condition);
                    break;
                case 'integer':
                case 'enum':
                case 'status':
                case 'version':
                    $resultField = self::integer($field, $value, $condition);
                    break;
                case 'primaryKey':
                case 'multipleKey':
                case 'foreignKey':
                case 'singleKey':
                case 'media':
                    $resultField = self::key($field, $value, $condition);
                    break;
                case 'decimal':
                    $resultField = self::numeric($field, $value, $condition);
                    break;
                case 'password':
                case 'string':
                case 'richText':
                case 'text':
                    $resultField = self::string($field, $value, $condition);
                    break;
                case 'email':
                    $resultField = self::email($field, $value, $condition);
                    break;
                case 'phone':
                    $resultField = self::phone($field, $value, $condition);
                    break;
                case 'url':
                    $resultField = self::url($field, $value, $condition);
                    break;
                // string restrictions
                case 'alphaNum':
                    $resultField = self::alphaNum($field, $value, $condition, []);
                    break;
                case 'alphaNumUnderscore':
                    $resultField = self::alphaNum($field, $value, $condition, ['_'],
                        'error_alpha_num_underscore_invalid');
                    break;
                case 'alphaNumDash':
                    $resultField = self::alphaNum($field, $value, $condition, ['-'], 'error_alpha_num_dash_invalid');
                    break;
                case 'alphaNumDot':
                    $resultField = self::alphaNum($field, $value, $condition, ['.'], 'error_alpha_num_dot_invalid');
                    break;
                case 'alphaNumSafe':
                    $resultField = self::alphaNum($field, $value, $condition,
                        self::SAFE_CHARS, 'error_alpha_num_safe_invalid');
                    break;
                case 'alphaNumFile':
                    $resultField = self::alphaNum($field, $value, $condition, ['-', '_', '.'],
                        'error_filename_invalid');
                    break;
                // comparison
                case 'exactLength':
                    $resultField = self::exactLength($field, $value, $condition);
                    break;
                case 'maxLength':
                    $resultField = self::maxLength($field, $value, $condition);
                    break;
                case 'minLength':
                    $resultField = self::minLength($field, $value, $condition);
                    break;
                case 'equal':
                    $resultField = self::equal($field, $value, $condition);
                    break;
                case 'different':
                    $resultField = self::different($field, $value, $condition);
                    break;
                case 'maxValue':
                    $resultField = self::maxValue($field, $value, $condition);
                    break;
                case 'minValue':
                    $resultField = self::minValue($field, $value, $condition);
                    break;
                default:
                    Log::log(Code::VALIDATION_NOT_FOUND, $key, __FILE__, __LINE__);
                    $resultField = new Field($field);
            }
            if ($resultField->getResult() == Result::ERROR) {
                return $resultField;
            }
        }
        return new Field($field);
    }

    /**
     * Validate a Required field
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function required($field, $value, $condition)
    {
        $vrf = new Field($field);
        if (!$condition || $value !== '') {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_empty'));
        return $vrf;
    }

    /**
     * @param string $field
     * @param $condition
     * @return Field
     */
    private static function validateImage($field, $condition)
    {
        $vrf = new Field($field);
        if (!$condition) {
            return $vrf;
        }

        if (!isset($_FILES[$field . 'Id']) || $_FILES[$field . 'Id']['name'] == '') {
            return $vrf;
        }

        if (File::isImage($_FILES[$field . 'Id']['type'])) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_image_invalid', [$field]));
        return $vrf;
    }

    /**
     * @param string $field
     * @param $condition
     * @return Field
     */
    private static function validateDocument($field, $condition)
    {
        $vrf = new Field($field);
        if (!$condition) {
            return $vrf;
        }

        if (!isset($_FILES[$field . 'Id']) || $_FILES[$field . 'Id']['name'] == '') {
            return $vrf;
        }

        if (File::isDocument($_FILES[$field . 'Id']['type'])) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_document_invalid', [$field]));
        return $vrf;
    }

    /**
     * Validate a Date
     * @param string $field
     * @param \DateTime|string $value
     * @param mixed $condition
     * @return Field
     */
    private static function date($field, $value, $condition)
    {
        $vrf = new Field($field);
        if (!$condition || !isset($value) || $value instanceOf DateTime) {
            return $vrf;
        }

        try {
            new DateTime($value);
        } catch (\Exception $ex) {
            $vrf->setResult(Result::ERROR);
            $vrf->setMessage(Lang::lang('error_date_invalid', [$value]));
            return $vrf;
        }
        return $vrf;
    }

    /**
     * Validate a String
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function string($field, $value, $condition)
    {
        $vrf = new Field($field);
        if (!$condition || is_string($value)) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('errorInvalidString', [$field]));
        return $vrf;
    }

    /**
     * Validate a Integer
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function integer($field, $value, $condition)
    {
        $vrf = new Field($field);
        if (!$condition || preg_match('/^-?[0-9]*$/', $value)) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_integer_invalid', [$field]));
        return $vrf;
    }

    /**
     * Validate a key
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function key($field, $value, $condition)
    {
        $vrf = new Field($field);
        if (!$condition || (int)$value > 0) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_key_invalid', [$field]));
        return $vrf;
    }

    /**
     * Validate a numeric
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function numeric($field, $value, $condition)
    {
        $vrf = new Field($field);
        if (!$condition || is_numeric($value)) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_number_invalid', [$field]));
        return $vrf;
    }

    /**
     * Validate an exact size
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function exactLength($field, $value, $condition)
    {
        $vrf = new Field($field);
        if ($value == '' || strlen($value) === (int)$condition) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_length_exact', [$field, strlen($value), $condition]));
        return $vrf;
    }

    /**
     * Validate a Maximum size
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function maxLength($field, $value, $condition)
    {
        $vrf = new Field($field);
        if (strlen($value) <= (int)$condition) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_length_max', [$field, strlen($value), $condition]));
        return $vrf;
    }

    /**
     * Validate a Minimum size
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function minLength($field, $value, $condition)
    {
        $vrf = new Field($field);
        if ($value == '' || strlen($value) >= $condition) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_length_min', [$field, strlen($value), $condition]));
        return $vrf;
    }

    /**
     * Validate url
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function url($field, $value, $condition)
    {
        $vrf = new Field($field);
        if (!$condition || $value == '' || preg_match('/^http/', $value)) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_url_invalid', [$field]));
        return $vrf;
    }

    /**
     * Validate phone
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function phone($field, $value, $condition)
    {
        $vrf = new Field($field);
        $value = preg_replace(['/\s+/', '/-/', '/\./'], '', $value);
        if ($value == '' || (preg_match('/^[0-9]*$/', $value))) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_phone_invalid'));
        return $vrf;
    }

    /**
     * Validate email
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function email($field, $value, $condition)
    {
        $vrf = new Field($field);
        if (!$condition || $value == '' || filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_email_invalid', [$field]));
        return $vrf;
    }

    /**
     * Validate letters and numbers
     * @param string $field
     * @param string $value
     * @param boolean $required
     * @param array $extraCharacters
     * @param string $error = 'error_alpha_num_invalid'
     * @return Field
     */
    private static function alphaNum(
        $field,
        $value,
        $isRequired,
        array $extraCharacters,
        $error = 'error_alpha_num_invalid'
    ) {
        $vrf = new Field($field);

        if (!$isRequired && $value == '') {
            return $vrf;
        }

        $value = str_replace($extraCharacters, '', $value);

        if (ctype_alnum($value)) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang($error, [$field]));
        return $vrf;
    }

    /**
     * Validate with other field
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function equal($field, $value, $condition)
    {
        $vrf = new Field($field);
        if ($value === $condition) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_equal', [$field]));
        return $vrf;
    }

    /**
     * Validate difference with other field
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function different($field, $value, $condition)
    {
        $vrf = new Field($field);
        if ($value !== $condition) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_different', [$field]));
        return $vrf;
    }

    /**
     * Validate password confirm
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function passwordConfirm($field, $value, $condition)
    {
        $vrf = new Field($field);
        if ($value === $condition) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_password_confirm'));
        return $vrf;
    }

    /**
     * Validate with other field
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function maxValue($field, $value, $condition)
    {
        $vrf = new Field($field);
        if ((int)$value <= (int)$condition) {
            return $vrf;
        }

        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_value_max', [$condition]));
        return $vrf;
    }

    /**
     * Validate with other field
     * @param string $field
     * @param string $value
     * @param mixed $condition
     * @return Field
     */
    private static function minValue($field, $value, $condition)
    {
        $vrf = new Field($field);
        if ((int)$value >= (int)$condition) {
            return $vrf;
        }
        $vrf->setResult(Result::ERROR);
        $vrf->setMessage(Lang::lang('error_value_min', [$condition]));
        return $vrf;
    }
}
