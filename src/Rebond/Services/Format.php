<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Services;

class Format
{
    /**
     * Return a formatted text
     * @param string $value
     * @param int $length = 0
     * @return string
     */
    public static function toText($value, $length = 0)
    {
        if ($length <= 0) {
            return $value;
        }

        return (strlen($value) < $length)
            ? $value
            : strip_tags(substr($value, 0, $length - 3)) . '...';
    }

    /**
     * Remove special characters
     * @param string $text
     * @return string
     */
    public static function friendlyTitle($text)
    {
        $newText = strtr($text, self::getCharacterMap());
        $newText = preg_replace('/[^a-zA-Z0-9-_]/', '', $newText);
        return urlencode(strtolower($newText));
    }

    /** @return array */
    private static function getCharacterMap()
    {
        return [
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'aa', 'æ' => 'ae',
            'Þ' => 'B', 'þ' => 'b',
            'Ç' => 'C',
            'ç' => 'c',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
            'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'Ñ' => 'N',
            'ñ' => 'n',
            'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O',
            'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ð' => 'o',
            'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U',
            'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u',
            'Š' => 'S', 'š' => 's', 'ß' => 'ss',
            'Ý' => 'Y',
            'ÿ' => 'y',
            'ý' => 'y',
            'Ž' => 'Z',
            'ž' => 'z',
            ' ' => '-',
        ];
    }
}
