<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\Code;
use Rebond\Enums\Core\Ratio;

class Template
{
    /* @var string */
    private $path;
    /* @var array */
    private $css;
    /* @var array */
    private $js;
    /* @var array */
    private $meta;
    /* @var array */
    private $vars;
    /* @var App */
    private $app;

    const MODULE_ADMIN = 0;
    const MODULE = 1;
    const ADMIN = 2;
    const SITE = 3;

    public function __construct($path, array $folders)
    {
        $this->app = App::instance();
        $this->css = [0 => [], 1 => []];
        $this->js = [0 => [], 1 => []];
        $this->meta = [];
        $this->vars = [];
        $this->pathSetter($path, $folders);
    }

    /**
     * Set path
     * @param string $path
     * @param array $folders = []
     */
    public function setPath($path, $folders = [])
    {
        $this->pathSetter($path, $folders);
    }

    /**
     * Set Variable
     * @param string $var
     * @return bool
     */
    public function varIsSet($var)
    {
        return isset($this->vars[$var]);
    }

    /**
     * Set Variable
     * @param string $var
     * @param mixed $value
     */
    public function set($var, $value)
    {
        $this->vars[$var] = $value;
    }

    /**
     * Add to Variable
     * @param string $var
     * @param string $value
     */
    public function add($var, $value)
    {
        if (!isset($this->vars[$var])) {
            $this->vars[$var] = '';
        }
        $this->vars[$var] .= $value;
    }

    /**
     * Set CSS
     * @param string $css
     * @param bool $first = true
     */
    public function addCss($css, $first = true)
    {
        $order = $first ? 0 : 1;
        $this->css[$order][] = '<link rel="stylesheet" href="' . $css . '" type="text/css" />';
    }

    public function renderCss()
    {
        foreach ($this->css[0] as $css) {
            echo $css;
        }
        foreach ($this->css[1] as $css) {
            echo $css;
        }
    }

    /**
     * Set JS
     * @param string $js
     * @param bool $first = true
     */
    public function addJs($js, $first = true)
    {
        $order = $first ? 0 : 1;
        $this->js[$order][] = '<script type="text/javascript" src="' . $js . '"></script>';
    }

    public function renderJs()
    {
        foreach ($this->js[0] as $js) {
            echo $js;
        }
        foreach ($this->js[1] as $js) {
            echo $js;
        }
    }

    /**
     * Set meta
     * @param string $name
     * @param string $content
     */
    public function addMeta($name, $content)
    {
        $this->meta[] = '<meta name="' . $name . '" content="' . $content . '">';
    }

    public function renderMeta()
    {
        foreach ($this->meta as $meta) {
            echo $meta;
        }
    }

    /**
     * Set multiple variables
     * @param string $vars
     * @param bool $clear
     */
    public function setVars($vars, $clear = false)
    {
        if ($clear) {
            $this->vars = $vars;
        } else {
            if (is_array($vars)) {
                $this->vars = array_merge($this->vars, $vars);
            }
        }
    }

    public function findTpl($tpl)
    {
        $fullPath = $this->path . $tpl . '.tpl';
        $localizedPath = $this->path . $tpl . '-' . $this->app->getCurrentLang();

        if (file_exists($localizedPath . '.tpl')) {
            $fullPath = $localizedPath . '.tpl';
        }

        if ($this->app->getSite()->getUseDeviceTemplate()) {
            $detect = new \Mobile_Detect();
            if ($detect->isMobile() && !$detect->isTablet()) {
                if (file_exists($localizedPath . '.mbl')) {
                    $fullPath = $localizedPath . '.mbl';
                } else if (file_exists($this->path . $tpl . '.mbl')) {
                    $fullPath = $this->path . $tpl . '.mbl';
                }
            } else if ($detect->isTablet()) {
                if (file_exists($localizedPath . '.tbl')) {
                    $fullPath = $localizedPath . '.tbl';
                } else if (file_exists($this->path . $tpl . '.tbl')) {
                    $fullPath = $this->path . $tpl . '.tbl';
                }
            }
        }

        if (!file_exists($fullPath)) {
            return false;
        }

        return $fullPath;
    }

    /**
     * Render template
     * @param string $tpl
     * @return string
     */
    public function render($tpl)
    {
        $fullPath = $this->findTpl($tpl);

        if ($fullPath === false) {
            Log::log(Code::TEMPLATE_NOT_FOUND, $this->path . $tpl . '.tpl', __FILE__, __LINE__);
            return $this->app->getUser()->getIsDev()
                ? '<div class="status-error">' . Lang::lang('error_file_not_found', [$this->path . $tpl . '.tpl']) . '</div>'
                : '';
        }

        ob_start();
        try {
            extract($this->vars);
            require($fullPath);
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
        return ob_get_clean();
    }

    /**
     * Render a loading page
     * @return string $html
     */
    public static function loading()
    {
        return '<p><i class="fa fa-spinner fa-pulse fa-lg fa-fw"></i></p>';
    }

    /**
     * Set the template path
     * @param string $path
     * @param array $folders
     */
    private function pathSetter($path, array $folders)
    {
        switch ($path) {
            case self::MODULE_ADMIN:
                $namespace = (in_array($folders[0], ['app', 'bus']))
                    ? $this->app->getPath('generated-view')
                    : $this->app->getPath('rebond-view');
                $this->path = $namespace . $this->getFolderPath($folders);
                break;
            case self::MODULE:
                $this->path = $this->app->getPath('own-view') . $this->getFolderPath($folders);
                break;
            case self::ADMIN:
                $this->path = $this->app->getPath('rebond-view') . implode('/', $folders) . '/';
                break;
            case self::SITE:
                $this->path = $this->app->getPath('own-view') . implode('/', $folders) . '/';
                break;
        }
    }

    /**
     * @param array $folders
     * @return string
     */
    private function getFolderPath(array $folders)
    {
        $path = '';
        foreach ($folders as $folder) {
            $path .= $folder . '/';
        }
        return $path;
    }

    // Shortcut methods to use in templates

    private function lang($text, $params = [], $options = null)
    {
        switch ($options) {
            case 'lower':
                return Lang::lower($text, $params);
            case 'capital':
                return Lang::capital($text, $params);
            case 'locale':
                return Lang::locale($text, $params);
        }
        return Lang::lang($text, $params);
    }

    private function img($path, $text)
    {
        return '<img src="' . $path . '" alt="' . Lang::lang($text) . '" title="' . Lang::lang($text) . '" />';
    }

    private function showFromModel($model, $destWidth = 64, $destHeight = 64, $ratio = Ratio::SMART, $compression = 10, $canZoom = false)
    {
        return Media::showFromModel($model, $destWidth, $destHeight, $ratio, $compression, $canZoom);
    }

    private function renderTitle($name, $id = 0, $title = null)
    {
        return Form::renderTitle($name, $id, $title);
    }

    private function buildSubmit($id = 0, $type = null)
    {
        return Form::buildSubmit($id, $type);
    }

    private function e(&$var, $default = null)
    {
        if (!isset($var)) {
            if (!isset($default)) {
                return;
            }
            echo $default;
            return;
        }
        echo $var;
    }

    private function debug()
    {
        if (!$this->app->getUser()->getIsDev()) {
            return;
        }

        $tplDebug = new Template(Template::ADMIN, ['admin']);
        $tplDebug->set('queries', $this->app->getQueries());
        $tplDebug->set('time', $this->app->getTimer());
        echo $tplDebug->render('debug');
    }
}
