<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond\Services;

use Rebond\App;
use Rebond\Enums\Core\Result;
use Rebond\Models\Field;

class File
{
    const PATH_OWN = 0;
    const PATH_REBOND = 1;
    const PATH_GENERATED = 2;

    /**
     * Create folder
     * @param string $dir
     * @param int $mode = 0755
     */
    public static function createFolder($dir, $mode = 0755)
    {
        if (!is_dir($dir)) {
            mkdir($dir, $mode, true);
        }
    }

    /**
     * Retrieve list of folders
     * @param string $path
     * @param int $from = self::PATH_OWN
     * @return array
     */
    public static function getFolders($path, $from = self::PATH_OWN)
    {
        $path = self::fixPath($path, $from);

        $folders = [];
        $exempt = ['.', '..'];
        if (is_dir($path)) {
            if ($dh = opendir($path)) {
                while (($folder = readdir($dh)) !== false) {
                    if (!in_array($folder, $exempt) && filetype($path . $folder) == 'dir') {
                        $folders[] = $folder;
                    }
                }
                closedir($dh);
            }
        }
        sort($folders);
        return $folders;
    }

    /**
     * @param bool $isApp = false
     * @param bool $isTpl = true
     */
    public static function getViews($isApp = false, $isTpl = true)
    {
        $templateList = [];

        $path = $isApp ? 'views/App/' : 'views/';

        $folders = self::getFolders($path);
        foreach ($folders as $folder) {
            if (!$isApp && $folder == 'App') {
                continue;
            }
            $templateList[$folder] = [];
            $templates = self::getFiles($path . $folder);
            foreach ($templates as $template) {
                if ((self::getExtension($template) == 'tpl') === $isTpl) {
                    $templateList[$folder][] = $template;
                }
            }
        }
        return $templateList;
    }

    /**
     * Retrieve list of medias
     * @param string $path
     * @param int $from = self::PATH_OWN
     * @return array
     */
    public static function getFiles($path, $from = self::PATH_OWN)
    {
        $path = self::fixPath($path, $from);

        $files = [];
        $exempt = ['.', '..', '.gitkeep'];
        if (is_dir($path)) {
            if ($dh = opendir($path)) {
                while (($file = readdir($dh)) !== false) {
                    if (!in_array(strtolower($file), $exempt) && filetype($path . $file) == 'file') {
                        $files[] = $file;
                    }
                }
                closedir($dh);
            }
        }
        return $files;
    }

    /**
     * @param string $file
     * @param string $data
     */
    public static function save($file, $data)
    {
        file_put_contents($file, $data);
    }

    /**
     * @param string $file
     * @param array $data
     */
    public static function saveJson($file, array $data)
    {
        self::save($file, json_encode($data));
    }

    /**
     * @param string $file
     * @return bool|string
     */
    public static function read($file)
    {
        if (!file_exists($file)) {
            return false;
        }
        return file_get_contents($file);
    }

    public static function readJson($file)
    {
        if (!file_exists($file)) {
            return [];
        }
        $data = file_get_contents($file);
        return ($data !== false) ? json_decode($data, true) : [];
    }

    /**
     * @param string $path
     * @param string $filename
     * @return bool
     */
    public static function delete($path, $filename)
    {
        $file = $path . $filename;
        if (file_exists($file)) {
            return unlink($file);
        }
        return false;
    }


    /**
     * @param string $path
     * @param string $filename
     * @param bool $deleteMainMedia = false
     */
    public static function deleteAllMedia($path, $filename, $deleteMainMedia = false)
    {
        $mediaPath = App::instance()->getConfig('media');
        $upload = File::getNoExtension($filename);
        if ($deleteMainMedia && file_exists($mediaPath . $path . $filename)) {
            unlink($mediaPath . $path . $filename);
        }
        $files = glob($mediaPath . $path . $upload . '-*.*');
        foreach ($files as $file) {
            unlink($file);
        }
    }

    public static function rename($path, $oldFile, $filename)
    {
        $mediaPath = App::instance()->getConfig('media');
        if ($oldFile == '' || !file_exists($mediaPath . $path . $oldFile) || $oldFile == $filename) {
            return false;
        }

        rename($mediaPath . $path . $oldFile, $mediaPath . $path . $filename);
        self::deleteAllMedia($path, $oldFile, true);
        return true;
    }

    /**
     * Get filename without the extension
     * @param string $file
     * @return string
     */
    public static function getNoExtension($file)
    {
        if (strrpos($file, '.') === false) {
            return $file;
        }

        return substr($file, 0, strrpos($file, '.'));
    }

    /**
     * Get extension of file
     * @param string $file
     * @return string
     */
    public static function getExtension($file)
    {
        $file = strtolower($file);
        return substr($file, strrpos($file, '.') + 1);
    }

    /**
     * @param string $file
     * @return string
     */
    public static function getMainMedia($file)
    {
        return explode('-', $file)[0] . '.' . self::getExtension($file);
    }

    /**
     * @param $mimeType
     * @return bool
     */
    public static function isImage($mimeType)
    {
        return in_array(strtolower($mimeType), ['image/png', 'image/gif', 'image/jpg', 'image/jpeg']);
    }

    /**
     * @param $mimeType
     * @return bool
     */
    public static function isDocument($mimeType)
    {
        return in_array(strtolower($mimeType),
            ['text/plain', 'text/html', 'application/pdf', 'application/msword', 'application/excel']);
    }

    /**
     * Generate new filename from options
     * @param string $filename
     * @param int $destWidth
     * @param int $destHeight
     * @param int $ratio
     * @param int $compression
     * @return string
     */
    public static function getNewFilename($filename, $destWidth, $destHeight, $ratio, $compression)
    {
        return self::getNoExtension($filename) . '-'
            . str_pad($destWidth, 4, 'w')
            . str_pad($destHeight, 4, 'h')
            . $ratio
            . str_pad($compression, 3, 'c')
            . '.' . self::getExtension($filename);
    }

    /**
     * Get filename from path
     * @param string $filePath
     * @return string
     */
    public static function getFilename($filePath)
    {
        if (strrpos($filePath, '/') === false) {
            return $filePath;
        }
        return substr($filePath, strrpos($filePath, '/') + 1, strlen($filePath) - strrpos($filePath, '/'));
    }

    public static function fixPath($path, $from)
    {
        if (substr($path, -1) != '/') {
            $path .= '/';
        }

        $app = App::instance();

        if (strpos($path, $app->getFullPath()) === false) {
            if ($from == self::PATH_OWN) {
                $path = $app->getFullPath() . $path;
            } else if ($from == self::PATH_REBOND) {
                $path = $app->getRebondPath() . $path;
            } else if ($from == self::PATH_GENERATED) {
                $path = $app->getGeneratedPath() . $path;
            }
        }
        return $path;
    }
}
