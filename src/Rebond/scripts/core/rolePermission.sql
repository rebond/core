CREATE TABLE IF NOT EXISTS `core_role_permission` (
    role_id INT UNSIGNED NOT NULL
,    permission_id INT UNSIGNED NOT NULL
    , PRIMARY KEY (role_id, permission_id)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
