CREATE TABLE IF NOT EXISTS `core_site` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL
,    `google_analytics` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `keywords` VARCHAR(500) COLLATE utf8_unicode_ci NOT NULL
,    `description` VARCHAR(1000) COLLATE utf8_unicode_ci NOT NULL
,    `css` VARCHAR(500) COLLATE utf8_unicode_ci NOT NULL
,    `js` VARCHAR(500) COLLATE utf8_unicode_ci NOT NULL
,    `sign_in_url` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL
,    `is_debug` TINYINT UNSIGNED NOT NULL
,    `log_sql` TINYINT UNSIGNED NOT NULL
,    `timezone` VARCHAR(40) COLLATE utf8_unicode_ci NOT NULL
,    `is_cms` TINYINT UNSIGNED NOT NULL
,    `cache_time` SMALLINT UNSIGNED NOT NULL
,    `use_device_template` TINYINT UNSIGNED NOT NULL
,    `skin` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `send_mail_on_error` TINYINT UNSIGNED NOT NULL
,    `mail_list_on_error` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
