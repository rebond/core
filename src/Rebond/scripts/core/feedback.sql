CREATE TABLE IF NOT EXISTS `core_feedback` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    user_id INT UNSIGNED NOT NULL
,    `title` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL
,    `type` TINYINT UNSIGNED NOT NULL
,    `description` TEXT COLLATE utf8_unicode_ci NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
