CREATE TABLE IF NOT EXISTS `core_user` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    `email` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL
,    `password` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL
,    `first_name` VARCHAR(40) COLLATE utf8_unicode_ci NOT NULL
,    `last_name` VARCHAR(40) COLLATE utf8_unicode_ci NOT NULL
,    avatar_id INT UNSIGNED NOT NULL
,    `is_admin` TINYINT UNSIGNED NOT NULL
,    `is_dev` TINYINT UNSIGNED NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
