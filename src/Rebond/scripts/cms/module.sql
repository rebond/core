CREATE TABLE IF NOT EXISTS `cms_module` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    `name` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `title` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `summary` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL
,    `workflow` TINYINT UNSIGNED NOT NULL
,    `has_filter` TINYINT UNSIGNED NOT NULL
,    `has_content` TINYINT UNSIGNED NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
