CREATE TABLE IF NOT EXISTS `cms_filter` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    `title` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    module_id INT UNSIGNED NOT NULL
,    `display_order` TINYINT UNSIGNED NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
