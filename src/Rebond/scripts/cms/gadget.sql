CREATE TABLE IF NOT EXISTS `cms_gadget` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    page_id INT UNSIGNED NOT NULL
,    component_id INT UNSIGNED NOT NULL
,    `col` TINYINT UNSIGNED NOT NULL
,    filter_id INT UNSIGNED NOT NULL
,    `custom_filter` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `display_order` TINYINT UNSIGNED NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
