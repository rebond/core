CREATE TABLE IF NOT EXISTS `cms_page` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    `title` VARCHAR(40) COLLATE utf8_unicode_ci NOT NULL
,    parent_id INT UNSIGNED NOT NULL
,    template_id INT UNSIGNED NOT NULL
,    layout_id INT UNSIGNED NOT NULL
,    `css` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL
,    `js` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL
,    `in_nav_header` TINYINT UNSIGNED NOT NULL
,    `in_nav_side` TINYINT UNSIGNED NOT NULL
,    `in_sitemap` TINYINT UNSIGNED NOT NULL
,    `in_breadcrumb` TINYINT UNSIGNED NOT NULL
,    `in_nav_footer` TINYINT UNSIGNED NOT NULL
,    `friendly_url_path` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL
,    `friendly_url` VARCHAR(60) COLLATE utf8_unicode_ci NOT NULL
,    `redirect` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL
,    `class` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `permission` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `display_order` TINYINT UNSIGNED NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
