CREATE TABLE IF NOT EXISTS `cms_component` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    `title` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    module_id INT UNSIGNED NOT NULL
,    `summary` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL
,    `method` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL
,    `can_be_cached` TINYINT UNSIGNED NOT NULL
,    `type` TINYINT UNSIGNED NOT NULL
,    `status` TINYINT UNSIGNED NOT NULL
,    `created_date` DATETIME NOT NULL
,    `modified_date` DATETIME NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
