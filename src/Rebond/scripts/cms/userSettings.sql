CREATE TABLE IF NOT EXISTS `cms_user_settings` (
    id INT UNSIGNED NOT NULL  
,    `media_view` TINYINT UNSIGNED NOT NULL
,    `media_paging` TINYINT UNSIGNED NOT NULL
,    `content_paging` TINYINT UNSIGNED NOT NULL
,    `paging` TINYINT UNSIGNED NOT NULL
    , PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
