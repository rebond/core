<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Cms;

use Rebond\Enums\AbstractEnum;

class Paging extends AbstractEnum
{
    const NUM_10 = 0;
    const NUM_20 = 1;
    const NUM_50 = 2;
    const NUM_100 = 3;

    public static function toArray()
    {
        return [
            0 => 10,
            1 => 20,
            2 => 50,
            3 => 100,
        ];
    }
}
