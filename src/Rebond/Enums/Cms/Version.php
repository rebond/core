<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Cms;

use Rebond\Enums\AbstractEnum;

class Version extends AbstractEnum
{
    const PENDING = 0;
    const PUBLISHED = 1; // visible
    const UPDATING = 2; // visible
    const PUBLISHING = 3;
    const DELETED = 4;
    const OLD = 5;
}
