<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Cms;

use Rebond\Enums\AbstractEnum;

class SideNav extends AbstractEnum
{
    const NONE = 0;
    const HOME = 1;
    const CHILDREN = 2;
    const PARENT_1 = 3;
    const PARENT_2 = 4;
    const PARENT_3 = 5;
}
