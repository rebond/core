<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums;

use Rebond\Services\Converter;
use Rebond\Services\Lang;

class AbstractEnum
{
    protected static $cachedConstants = [];

    /**
     * @param $key
     * @param string|null $convert
     * @return string
     */
    public static function value($key, $convert = '_')
    {
        $values = static::toArray();

        if (isset($values[$key])) {
            return isset($convert)
                ? Converter::replace($values[$key], $convert)
                : $values[$key];
        }

        return isset($converter)
            ? Converter::replace('e_undefined', $convert)
            : 'e_undefined';
    }

    public static function lang($key)
    {
        return Lang::lang(self::value($key));
    }

    public static function toArray()
    {
        $calledClass = get_called_class();
        if (isset(static::$cachedConstants[$calledClass])) {
            return static::$cachedConstants[$calledClass];
        }

        $rc = new \ReflectionClass($calledClass);
        foreach ($rc->getConstants() as $value => $key) {
            static::$cachedConstants[$calledClass][$key] = Converter::replace($value, '_');
        }
        return static::$cachedConstants[$calledClass];
    }

    public static function toArrayLang()
    {
        $values = static::toArray();
        foreach ($values as $key => &$value) {
            $values[$key] = Lang::lang($value);
        }
        return $values;
    }
}
