<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Core;

use Rebond\Enums\AbstractEnum;

class Status extends AbstractEnum
{
    const INACTIVE = 0;
    const ACTIVE = 1;
    const DELETED = 2;
    const TO_DELETE = 3;
}
