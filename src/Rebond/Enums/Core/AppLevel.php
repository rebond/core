<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Core;

use Rebond\Enums\AbstractEnum;

class AppLevel extends AbstractEnum
{
    const STANDALONE = 0; // standalone, command line, generate core and cms code
    const INSTALL = 1; // install, cache config file
    const ADMIN = 2; // admin, generate css, js, lang, generate app and bus code
    const WWW = 3; // www, generate css, js, lang, generate app and bus code
}