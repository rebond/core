<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Core;

use Rebond\Enums\AbstractEnum;

class InstallStatus extends AbstractEnum
{
    const AUTH = 0;
    const CONFIG = 1;
    const DB = 2;
    const USER = 3;
    const LAUNCH = 4;
}