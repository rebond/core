<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */
namespace Rebond\Enums\Core;

use Rebond\Enums\AbstractEnum;

class Package extends AbstractEnum
{
    const APP = 0;
    const BUS = 1;
    const CORE = 2;
    const CMS = 3;
}
