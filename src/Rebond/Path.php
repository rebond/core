<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Rebond;

use Generated\Cache\Config;
use Rebond\Enums\Core\AppLevel;
use Rebond\Enums\Core\Code;
use Rebond\Services\Nav;
use Rebond\Services\Security;

class Path
{
    /* @var string */
    private $fullPath;
    /* @var string */
    private $generatedPath;
    /* @var string */
    private $rebondPath;
    /* @var array */
    private $pathList;
    /* @var array */
    private $configList;

    public function __construct($appLevel)
    {
        // set application paths
        $path = str_replace('\\', '/', (dirname(__FILE__)));
        $this->rebondPath = substr($path, 0, -strlen('src/Rebond'));

        if ($appLevel == AppLevel::STANDALONE) {
            $this->fullPath = $this->rebondPath . 'site/';
            $this->generatedPath = $this->rebondPath . '../../rebond/generated/';
        } else {
            $this->fullPath = substr($path, 0, -strlen('vendor/rebond/core/src/Rebond'));
            $this->generatedPath = $this->fullPath . 'vendor/rebond/generated/';
        }
    }

    public function getFullPath()
    {
        return $this->fullPath;
    }

    public function getRebondPath()
    {
        return $this->rebondPath;
    }

    public function getGeneratedPath()
    {
        return $this->generatedPath;
    }

    public function getPath($key)
    {
        if (!array_key_exists($key, $this->getPathList())) {
            throw new \Exception('From path list: ' . $key, Code::CONFIG_KEY_NOT_FOUND);
        }
        return $this->getPathList()[$key];
    }

    private function getPathList()
    {
        if (isset($this->pathList)) {
            return $this->pathList;
        }

        $this->pathList = [
            'own' => $this->fullPath . 'Own/',
            'own-file' => $this->fullPath . 'files/',
            'own-backup-file' => $this->fullPath . 'files/backup/',
            'own-cache-file' => $this->fullPath . 'files/cache/',
            'own-export-file' => $this->fullPath . 'files/export/',
            'own-lang-file' => $this->fullPath . 'files/lang/',
            'own-log-file' => $this->fullPath . 'files/log/',
            'own-model-file' => $this->fullPath . 'files/model/',
            'own-temp-file' => $this->fullPath . 'files/temp/',
            'own-view' => $this->fullPath . 'views/',

            'rebond' => $this->rebondPath . 'src/Rebond/',
            'rebond-view' => $this->rebondPath . 'views/',
            'rebond-index' => $this->rebondPath . 'files/index/',
            'rebond-install-file' => $this->rebondPath . 'files/install/',
            'rebond-lang-file' => $this->rebondPath . 'files/lang/',
            'rebond-model-file' => $this->rebondPath . 'files/model/',

            'generated' => $this->generatedPath,
            'generated-view' => $this->generatedPath . 'views/',
            'generated-cache' => $this->generatedPath . 'Cache/',
        ];
        return $this->pathList;
    }

    public function getLangList()
    {
        return \Generated\Cache\Config::langList();
    }

    public function getConfig($key)
    {
        if (!array_key_exists($key, $this->getConfigList())) {
            throw new \Exception('From config list: ' . $key, Code::CONFIG_KEY_NOT_FOUND);
        }
        return $this->getConfigList()[$key];
    }

    private function getConfigList()
    {
        if (isset($this->configList)) {
            return $this->configList;
        }

        self::checkCacheConfig();

        $configList = Config::configList();
        $langList = Config::langList();

        $siteUrl = $configList['site-domain'];
        if ($siteUrl != '') {
            $siteUrl .= '.';
        }

        $customList = [
            'site-url' => 'http://' . $siteUrl . $configList['site-host'] . '/',
            'media-url' => 'http://' . $siteUrl . $configList['site-host'] . '/media/',
            'admin-url' => 'http://' . $configList['admin-domain'] . '.' . $configList['site-host'] . '/',
            'cookie-domain' => '.' . Nav::removePort(trim($configList['site-host'])),
            'site' => $this->fullPath . $configList['site-folder'] . '/',
            'media' => $this->fullPath . $configList['site-folder'] . '/media/',
            'admin' => $this->fullPath . $configList['admin-folder'] . '/',
        ];

        $this->configList = array_merge($configList, $langList, $customList);
        return $this->configList;
    }

    private function checkCacheConfig()
    {
        if (!class_exists(Config::class, false)) {
            throw new \Exception('Config.php', Code::CONFIG_NOT_FOUND);
        }
    }
}
