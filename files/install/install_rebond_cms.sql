DROP TABLE IF EXISTS cms_component;

CREATE TABLE `cms_component` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_id` INT UNSIGNED NOT NULL,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `summary` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
  `method` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` TINYINT UNSIGNED NOT NULL,
  `can_be_cached` TINYINT UNSIGNED NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `modified_date` DATETIME NOT NULL,
  `created_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO cms_component VALUES (NULL, 1, 'single_element', 'display a single element', 'single', 0, 1, 1, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 1, 'filtered_cards', 'filtered cards', 'filteredCards', 2, 1, 1, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 1, 'cards', 'cards', 'cards', 1, 1, 1, NOW(), NOW());

INSERT INTO cms_component VALUES (NULL, 2, 'sitemap', 'display the sitemap of the site', 'sitemap', 4, 0, 1, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 2, 'locale', 'locale gadget to change language', 'locale', 4, 0, 1, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 2, 'landing_page', 'show the children pages', 'landingPage', 4, 0, 1, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 2, 'google_map', 'show a google map based on the gmap setting', 'googleMap', 4, 0, 1, NOW(), NOW());

INSERT INTO cms_component VALUES (NULL, 3, 'twitter', 'renders a user twitter box using settings in the config file', 'twitter', 4, 0, 0, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 3, 'facebook_page', 'renders a facebook page using settings in the config file', 'facebookPage', 4, 0, 0, NOW(), NOW());

INSERT INTO cms_component VALUES (NULL, 4, 'status', 'user status', 'status', 4, 0, 1, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 4, 'sign_in', 'sign in form', 'signIn', 4, 0, 1, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 4, 'sign_out', 'sign out', 'signOut', 4, 0, 1, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 4, 'profile', 'profile', 'profile', 4, 0, 1, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 4, 'register', 'register form', 'register', 4, 0, 1, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 4, 'password_forgot', 'forgot password form', 'forgotPassword', 4, 0, 1, NOW(), NOW());
INSERT INTO cms_component VALUES (NULL, 4, 'password_change', 'change password', 'changePassword', 4, 0, 1, NOW(), NOW());


DROP TABLE IF EXISTS cms_content;

CREATE TABLE `cms_content` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `app_id` INT UNSIGNED NOT NULL,
  `module_id` INT UNSIGNED NOT NULL,
  `content_group` MEDIUMINT UNSIGNED NOT NULL,
  `title` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `url_friendly_title` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `filter_id` INT UNSIGNED NOT NULL,
  `author_id` INT UNSIGNED NOT NULL,
  `publisher_id` INT UNSIGNED NOT NULL,
  `use_expiration` TINYINT UNSIGNED NOT NULL,
  `go_live_date` DATETIME NOT NULL,
  `expiry_date` DATETIME NOT NULL,
  `version` TINYINT UNSIGNED NOT NULL,
  `published_date` DATETIME NOT NULL,
  `created_date` DATETIME NOT NULL,
  `modified_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO cms_content VALUES (NULL, 1, 1, 1, 'Welcome', 'welcome', 1, 1, 1, 0, NOW(), NOW(), 1, NOW(), NOW(), NOW());


DROP TABLE IF EXISTS cms_filter;

CREATE TABLE `cms_filter` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_id` INT UNSIGNED NOT NULL,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `display_order` TINYINT UNSIGNED NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `created_date` DATETIME NOT NULL,
  `modified_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO cms_filter VALUES (NULL, 1, 'General', 0, 1, NOW(), NOW());


DROP TABLE IF EXISTS cms_gadget;

CREATE TABLE `cms_gadget` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `page_id` INT UNSIGNED NOT NULL,
  `component_id` INT UNSIGNED NOT NULL,
  `col` TINYINT UNSIGNED NOT NULL,
  `filter_id` INT UNSIGNED NOT NULL,
  `custom_filter` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL,
  `display_order` TINYINT UNSIGNED NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `created_date` DATETIME NOT NULL,
  `modified_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO cms_gadget VALUES (NULL, 1, 1, 1, 1, '', 0, 1, NOW(), NOW());
INSERT INTO cms_gadget VALUES (NULL, 2, 10, 1, 0, '', 0, 1, NOW(), NOW());
INSERT INTO cms_gadget VALUES (NULL, 3, 11, 1, 0, '', 0, 1, NOW(), NOW());
INSERT INTO cms_gadget VALUES (NULL, 4, 12, 1, 0, '', 0, 1, NOW(), NOW());
INSERT INTO cms_gadget VALUES (NULL, 5, 13, 1, 0, '', 0, 1, NOW(), NOW());
INSERT INTO cms_gadget VALUES (NULL, 6, 14, 1, 0, '', 0, 1, NOW(), NOW());
INSERT INTO cms_gadget VALUES (NULL, 7, 15, 1, 0, '', 0, 1, NOW(), NOW());
INSERT INTO cms_gadget VALUES (NULL, 8, 16, 1, 0, '', 0, 1, NOW(), NOW());
INSERT INTO cms_gadget VALUES (NULL, 9, 4, 1, 0, '', 0, 1, NOW(), NOW());


DROP TABLE IF EXISTS cms_layout;

CREATE TABLE `cms_layout` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `summary` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
  `filename` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `columns` TINYINT UNSIGNED NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `created_date` DATETIME NOT NULL,
  `modified_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO cms_layout VALUES (NULL, 'columns_1', 'layout with 1 column', 'layout-1-col', 1, 1, NOW(), NOW());
INSERT INTO cms_layout VALUES (NULL, 'columns_2', 'layout with 2 columns', 'layout-2-col', 2, 1, NOW(), NOW());
INSERT INTO cms_layout VALUES (NULL, 'columns_center', 'layout with 1 centered columns', 'layout-center', 2, 1, NOW(), NOW());
INSERT INTO cms_layout VALUES (NULL, 'rows_2', 'layout with 2 rows', 'layout-2-row', 2, 1, NOW(), NOW());


DROP TABLE IF EXISTS cms_module;

CREATE TABLE `cms_module` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `summary` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `workflow` TINYINT UNSIGNED NOT NULL,
  `has_filter` TINYINT UNSIGNED NOT NULL,
  `has_content` TINYINT UNSIGNED NOT NULL,
  `modified_date` DATETIME NOT NULL,
  `created_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO cms_module VALUES (NULL, 'Article', 'article', 'article module', 1, 0, 1, 1, NOW(), NOW());
INSERT INTO cms_module VALUES (NULL, 'Plugin', 'plugin', 'plugin module', 1, 0, 0, 0, NOW(), NOW());
INSERT INTO cms_module VALUES (NULL, 'Social', 'social', 'social module', 1, 0, 0, 0, NOW(), NOW());
INSERT INTO cms_module VALUES (NULL, 'User', 'user', 'user module', 1, 0, 0, 0, NOW(), NOW());


DROP TABLE IF EXISTS cms_page;

CREATE TABLE `cms_page` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` INT UNSIGNED NOT NULL,
  `template_id` INT UNSIGNED NOT NULL,
  `layout_id` INT UNSIGNED NOT NULL,
  `css` VARCHAR(500) COLLATE utf8_unicode_ci NOT NULL,
  `js` VARCHAR(500) COLLATE utf8_unicode_ci NOT NULL,
  `in_nav_header` TINYINT UNSIGNED NOT NULL,
  `in_nav_side` TINYINT UNSIGNED NOT NULL,
  `in_sitemap` TINYINT UNSIGNED NOT NULL,
  `in_breadcrumb` TINYINT UNSIGNED NOT NULL,
  `in_nav_footer` TINYINT UNSIGNED NOT NULL,
  `friendly_url` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url_path` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `class` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL,
  `permission` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `display_order` TINYINT UNSIGNED NOT NULL,
  `created_date` DATETIME NOT NULL,
  `modified_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO cms_page VALUES (NULL, 'Home', 0, 1, 1, '', '', 1, 1, 1, 1, 1, 'home', '/', '', '', '', 1, 0, NOW(), NOW());
INSERT INTO cms_page VALUES (NULL, 'Profile', 1, 1, 1, '', '', 1, 1, 1, 1, 1, 'profile', '/', '', '', '', 1, 100, NOW(), NOW());
INSERT INTO cms_page VALUES (NULL, 'Sign in', 2, 1, 1, '', '', 0, 0, 0, 0, 0, 'sign-in', '/profile/', '', '', '', 1, 0, NOW(), NOW());
INSERT INTO cms_page VALUES (NULL, 'Sign out', 2, 1, 1, '', '', 0, 0, 0, 0, 0, 'sign-out', '/profile/', '', '', '', 1, 1, NOW(), NOW());
INSERT INTO cms_page VALUES (NULL, 'Edit', 2, 1, 1, '', '', 1, 1, 1, 1, 0, 'edit', '/profile/', '', '', '', 1, 2, NOW(), NOW());
INSERT INTO cms_page VALUES (NULL, 'Register', 2, 1, 1, '', '', 0, 0, 0, 0, 0, 'register', '/profile/', '', '', '', 1, 3, NOW(), NOW());
INSERT INTO cms_page VALUES (NULL, 'Forgot password', 2, 1, 1, '', '', 0, 0, 0, 0, 0, 'forgot-password', '/profile/', '', '', '', 1, 4, NOW(), NOW());
INSERT INTO cms_page VALUES (NULL, 'Change password', 2, 1, 1, '', '', 1, 1, 1, 1, 0, 'change-password', '/profile/', '', '', '', 1, 5, NOW(), NOW());
INSERT INTO cms_page VALUES (NULL, 'Sitemap', 1, 1, 1, '', '', 0, 0, 0, 0, 1, 'sitemap', '/', '', '', '', 1, 101, NOW(), NOW());


DROP TABLE IF EXISTS cms_template;

CREATE TABLE `cms_template` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `summary` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
  `filename` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `menu` TINYINT UNSIGNED NOT NULL,
  `menu_level` TINYINT UNSIGNED NOT NULL,
  `in_breadcrumb` TINYINT UNSIGNED NOT NULL,
  `side_nav` TINYINT UNSIGNED NOT NULL,
  `side_nav_level` TINYINT UNSIGNED NOT NULL,
  `in_footer` TINYINT UNSIGNED NOT NULL,
  `footer_level` TINYINT UNSIGNED NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `created_date` DATETIME NOT NULL,
  `modified_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO cms_template VALUES (NULL, 'default', 'default template', 'tpl-default', 1, 1, 1, 0, 1, 0, 1, 1, NOW(), NOW());


DROP TABLE IF EXISTS cms_user_settings;

CREATE TABLE `cms_user_settings` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `media_view` TINYINT UNSIGNED NOT NULL,
  `media_paging` TINYINT UNSIGNED NOT NULL,
  `content_paging` TINYINT UNSIGNED NOT NULL,
  `paging` TINYINT NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO cms_user_settings VALUES (NULL, 1, 0, 0, 0);
