DROP TABLE IF EXISTS core_feedback;

CREATE TABLE `core_feedback` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` TINYINT UNSIGNED NOT NULL,
  `description` TEXT COLLATE utf8_unicode_ci NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `created_date` DATETIME NOT NULL,
  `modified_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS core_folder;

CREATE TABLE `core_folder` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `parent_id` INT NOT NULL,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `display_order` TINYINT UNSIGNED NOT NULL,
  `modified_date` DATETIME NOT NULL,
  `created_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO core_folder VALUES (NULL, 0, 'Root', 1, 0, now(), now());


DROP TABLE IF EXISTS core_log;

CREATE TABLE `core_log` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` SMALLINT UNSIGNED NOT NULL,
  `user_id` INT UNSIGNED NOT NULL,
  `ip` VARCHAR(39) COLLATE utf8_unicode_ci NOT NULL,
  `request_uri` VARCHAR(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referrer` VARCHAR(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` TEXT COLLATE utf8_unicode_ci NOT NULL,
  `trace` TEXT COLLATE utf8_unicode_ci NOT NULL,
  `file` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` SMALLINT UNSIGNED NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `created_date` DATETIME NOT NULL,
  `modified_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS core_media;

CREATE TABLE `core_media` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `folder_id` INT UNSIGNED NOT NULL,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `tags` VARCHAR(150) COLLATE utf8_unicode_ci NOT NULL,
  `upload` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `original_filename` VARCHAR(150) COLLATE utf8_unicode_ci NOT NULL,
  `path` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `extension` VARCHAR(5) COLLATE utf8_unicode_ci NOT NULL,
  `mime_type` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_size` MEDIUMINT UNSIGNED NOT NULL,
  `width` SMALLINT UNSIGNED NOT NULL,
  `height` SMALLINT UNSIGNED NOT NULL,
  `alt` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_selectable` TINYINT UNSIGNED NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `modified_date` DATETIME NOT NULL,
  `created_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS core_media_link;

CREATE TABLE IF NOT EXISTS `core_media_link` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `package` VARCHAR(10) COLLATE utf8_unicode_ci NOT NULL,
    `entity` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `field` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `id_field` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `title_field` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `url` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `filter` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    `status` TINYINT UNSIGNED NOT NULL,
    `created_date` DATETIME NOT NULL,
    `modified_date` DATETIME NOT NULL,
     PRIMARY KEY (id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO core_media_link VALUES (NULL, 'app', 'article', 'media_id', '', '', '', '', 1, now(), now());
INSERT INTO core_media_link VALUES (NULL, 'core', 'user', 'avatar_id', 'id', 'email', '/user/edit?id=', 'status = 1', 1, now(), now());


DROP TABLE IF EXISTS core_permission;

CREATE TABLE `core_permission` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `summary` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `modified_date` DATETIME NOT NULL,
  `created_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO core_permission VALUES (NULL, 'admin.page.edit', 'Allow the user to edit pages', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.page.view', 'Allow the user to view pages', 1, now(), now());

INSERT INTO core_permission VALUES (NULL, 'admin.content.edit', 'Allow the user to edit contents', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.content.view', 'Allow the user to view contents', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.content.publish', 'Allow the user to publish contents', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.content.preview', 'Allow the user to view unpublished content on the site', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.content.filter', 'Allow the user to edit filters', 1, now(), now());

INSERT INTO core_permission VALUES (NULL, 'admin.media.edit', 'Allow the user to edit media', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.media.view', 'Allow the user to view media', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.media.upload', 'Allow the user to upload media', 1, now(), now());

INSERT INTO core_permission VALUES (NULL, 'admin.user.edit', 'Allow the user to edit users', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.user.view', 'Allow the user to view users', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.user.password', 'Allow the user to change users password', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.user.role', 'Allow the user to change user roles', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.user.security', 'Allow the user to change the user security keys', 1, now(), now());

INSERT INTO core_permission VALUES (NULL, 'admin.config.edit', 'Allow the user to edit the configuration', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.config.view', 'Allow the user to view the configuration', 1, now(), now());

INSERT INTO core_permission VALUES (NULL, 'admin.cms.edit', 'Allow the user to edit the cms section', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.cms.view', 'Allow the user to view the cms section', 1, now(), now());

INSERT INTO core_permission VALUES (NULL, 'admin.tools.edit', 'Allow the user to edit the tools section', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.tools.view', 'Allow the user to view the tools section', 1, now(), now());

INSERT INTO core_permission VALUES (NULL, 'admin.quickview', 'Allow the user to view and edit quickviews', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.dev', 'Allow the user to access the developer section', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.designer', 'Allow the user to access the designs section', 1, now(), now());
INSERT INTO core_permission VALUES (NULL, 'admin.own', 'Allow the user to access the own section', 1, now(), now());

INSERT INTO core_permission VALUES (NULL, 'member', 'access to specific sections of the site', 1, now(), now());


DROP TABLE IF EXISTS core_role;

CREATE TABLE `core_role` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `summary` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `modified_date` DATETIME NOT NULL,
  `created_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO core_role VALUES (NULL, 'admin', 'access to everything in the admin', 1, now(), now());
INSERT INTO core_role VALUES (NULL, 'user_manager', 'access to the user management section', 1, now(), now());
INSERT INTO core_role VALUES (NULL, 'architect', 'access to the page section', 1, now(), now());
INSERT INTO core_role VALUES (NULL, 'editor', 'access to the content and media sections', 1, now(), now());
INSERT INTO core_role VALUES (NULL, 'publisher', 'access to the publishing functionality of content', 1, now(), now());
INSERT INTO core_role VALUES (NULL, 'viewer', 'access to view everything in the admin, but no modification is allowed', 1, now(), now());
INSERT INTO core_role VALUES (NULL, 'member', 'access to specific sections of the site', 1, now(), now());


DROP TABLE IF EXISTS core_role_permission;

CREATE TABLE `core_role_permission` (
  `role_id` INT UNSIGNED NOT NULL,
  `permission_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `permission_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO core_role_permission VALUES (1, 1);
INSERT INTO core_role_permission VALUES (1, 3);
INSERT INTO core_role_permission VALUES (1, 5);
INSERT INTO core_role_permission VALUES (1, 6);
INSERT INTO core_role_permission VALUES (1, 7);
INSERT INTO core_role_permission VALUES (1, 8);
INSERT INTO core_role_permission VALUES (1, 10);
INSERT INTO core_role_permission VALUES (1, 11);
INSERT INTO core_role_permission VALUES (1, 13);
INSERT INTO core_role_permission VALUES (1, 14);
INSERT INTO core_role_permission VALUES (1, 15);
INSERT INTO core_role_permission VALUES (1, 16);
INSERT INTO core_role_permission VALUES (1, 18);
INSERT INTO core_role_permission VALUES (1, 20);
INSERT INTO core_role_permission VALUES (1, 22);
INSERT INTO core_role_permission VALUES (1, 23);
INSERT INTO core_role_permission VALUES (1, 24);
INSERT INTO core_role_permission VALUES (1, 25);
INSERT INTO core_role_permission VALUES (1, 26);

INSERT INTO core_role_permission VALUES (2, 11);
INSERT INTO core_role_permission VALUES (2, 12);
INSERT INTO core_role_permission VALUES (2, 13);
INSERT INTO core_role_permission VALUES (2, 14);
INSERT INTO core_role_permission VALUES (2, 15);

INSERT INTO core_role_permission VALUES (3, 1);
INSERT INTO core_role_permission VALUES (3, 4);
INSERT INTO core_role_permission VALUES (3, 9);

INSERT INTO core_role_permission VALUES (4, 2);
INSERT INTO core_role_permission VALUES (4, 3);
INSERT INTO core_role_permission VALUES (4, 6);
INSERT INTO core_role_permission VALUES (4, 7);
INSERT INTO core_role_permission VALUES (4, 8);
INSERT INTO core_role_permission VALUES (4, 10);

INSERT INTO core_role_permission VALUES (5, 2);
INSERT INTO core_role_permission VALUES (5, 4);
INSERT INTO core_role_permission VALUES (5, 5);
INSERT INTO core_role_permission VALUES (5, 6);
INSERT INTO core_role_permission VALUES (5, 9);

INSERT INTO core_role_permission VALUES (6, 2);
INSERT INTO core_role_permission VALUES (6, 4);
INSERT INTO core_role_permission VALUES (6, 9);

INSERT INTO core_role_permission VALUES (7, 26);


DROP TABLE IF EXISTS core_site;

CREATE TABLE `core_site` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `google_analytics` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` VARCHAR(500) COLLATE utf8_unicode_ci NOT NULL,
  `description` VARCHAR(1000) COLLATE utf8_unicode_ci NOT NULL,
  `css` VARCHAR(500) COLLATE utf8_unicode_ci NOT NULL,
  `js` VARCHAR(500) COLLATE utf8_unicode_ci NOT NULL,
  `sign_in_url` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_debug` TINYINT UNSIGNED NOT NULL,
  `log_sql` TINYINT UNSIGNED NOT NULL,
  `timezone` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_cms` TINYINT NOT NULL,
  `cache_time` SMALLINT unsigned NOT NULL,
  `skin` VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL,
  `use_device_template` TINYINT unsigned NOT NULL,
  `send_mail_on_error` TINYINT unsigned NOT NULL,
  `mail_list_on_error` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `modified_date` DATETIME NOT NULL,
  `created_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO core_site VALUES (NULL, 'Rebond', '', '', '', '', '', '/profile/sign-in', 1, 0, 'Europe/Oslo', 1, 0, 'default', 0, 0, '', 1, now(), now());


DROP TABLE IF EXISTS core_user;

CREATE TABLE `core_user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
  `avatar_id` INT UNSIGNED NOT NULL,
  `is_admin` TINYINT UNSIGNED NOT NULL,
  `is_dev` TINYINT UNSIGNED NOT NULL,
  `status` TINYINT UNSIGNED NOT NULL,
  `created_date` DATETIME NOT NULL,
  `modified_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS core_user_role;

CREATE TABLE `core_user_role` (
  `user_id` INT UNSIGNED NOT NULL,
  `role_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO core_user_role VALUES (1, 1);
INSERT INTO core_user_role VALUES (1, 7);


DROP TABLE IF EXISTS core_user_security;

CREATE TABLE `core_user_security` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `secure` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` TINYINT UNSIGNED NOT NULL,
  `expiry_date` DATETIME NOT NULL,
  `created_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
