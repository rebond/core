<?php
require '../vendor/autoload.php';

use Rebond\Controller\CmsController;
use Rebond\App;
use Rebond\Enums\Core\AppLevel;
use Rebond\Services\Nav;

$app = App::create(AppLevel::WWW);
$app->launch();

$controller = new CmsController();
$friendlyUrl = Nav::readCmsRequest($_SERVER['REQUEST_URI']);
echo $controller->run($app, $friendlyUrl);

