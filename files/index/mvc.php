<?php
require '../vendor/autoload.php';

use Rebond\App;
use Rebond\Enums\Core\AppLevel;
use Rebond\Enums\Core\Code;
use Rebond\Services\Nav;

$app = App::create(AppLevel::WWW);
$app->launch();

// read request uri
list($module, $action) = Nav::readRequest($_SERVER['REQUEST_URI']);

$class = '\Own\Controller\Site\\' . $module . 'Controller';

// check controller
if (!class_exists($class)) {
    throw new \Exception($module . '/' . $action, Code::PAGE_NOT_FOUND);
}

// call controller
$controller = new $class($app);

// check method
if (!method_exists($controller, $action)) {
    throw new \Exception($module . '/' . $action, Code::PAGE_NOT_FOUND);
}

// call method
echo $controller->$action();
