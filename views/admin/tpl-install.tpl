<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Installation - Rebond</title>
    <link rel="apple-touch-icon" href="/images/brand-touch.png" />
    <?php $this->renderCss() ?>
</head>
<body class="body-small bg-grey">
    <?php echo $notifications ?>
    <img class="big-logo" src="/images/brand-logo.png" alt="Rebond" />
    <div class="rb-container bg-white">
        <div id="rb-layout">
            <?php echo $layout ?>
            <div class="rb-row">
                <div class="rb-col right">
                <a class="rb-btn rb-btn-blank rb-btn-small" href="https://github.com/rebond/core/blob/master/README.md" target="_blank">Check out the doc</a>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="js-launcher" value="<?php $this->e($jsLauncher) ?>" />
    <?php $this->renderJs() ?>
</body>
</html>