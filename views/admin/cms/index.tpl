<h2><?php echo $this->lang('cms') ?></h2>
<p><?php echo $this->lang('intro_cms') ?></p>
<ul class="list-menu">
    <li>
        <i class="fa fa-2x fa-fw fa-files-o"></i>
        <a href="/cms/template"><?php echo $this->lang('templates') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-columns"></i>
        <a href="/cms/layout"><?php echo $this->lang('layouts') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-object-group"></i>
        <a href="/cms/module"><?php echo $this->lang('modules') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-square-o"></i>
        <a href="/cms/component"><?php echo $this->lang('components') ?></a>
    </li>
</ul>
