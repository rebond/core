<div class="help">
    <h2>Help</h2>
    <p><?php echo $this->lang('intro') ?></p>
    <p>This help section is constantly being updated</p>
    <ol>
        <li><a href="#">How to add a page?</a>
            <div>This is how you add a page</div>
        </li>
        <li><a href="#">How to add a content?</a>
            <div>This is how you add a content</div>
        </li>
        <li><a href="#">How to add a media?</a>
            <div>This is how you add a media</div>
        </li>
    </ol>
</div>