<h2><?php echo $this->lang('tools') ?></h2>
<p><?php echo $this->lang('intro_tools') ?></p>
<ul class="list-menu">
    <li>
        <i class="fa fa-2x fa-fw fa-archive"></i>
        <a href="/tools/logs"><?php echo $this->lang('logs') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-info"></i>
        <a href="/tools/site-info"><?php echo $this->lang('site_info') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-th-list"></i>
        <a href="/tools/phpinfo"><?php echo $this->lang('php_info') ?></a>
    </li>
</ul>
