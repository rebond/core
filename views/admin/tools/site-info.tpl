<h2><?php echo $this->lang('site_info') ?></h2>
<h3>Version</h3>
<table class="dual">
    <tr><th>PHP version</th><td><?php echo phpversion() ?></td></tr>
    <tr><th>MYSQL version</th><td><?php echo $dbVersion ?></td></tr>
    <tr><th>Display errors</th><td><?php echo ini_get('display_errors') ?></td></tr>
    <tr><th>Locale</th><td><?php echo $currentLang ?></td></tr>
    <tr><th>Environment</th><td><?php echo $env ?></td></tr>
    <tr><th>Debug Mode</th><td><?php echo (int) $isDebug ?></td></tr>
    <tr><th>Log SQL</th><td><?php echo (int) $logSql ?></td></tr>
    <tr><th>Cookie/Session domain</th><td><?php echo $cookieDomain ?></td></tr>
</table>

<h3>Bower packages</h3>
<table class="dual">
    <?php foreach ($bowers as $name => $version) { ?>
    <tr><th><?php echo $name ?></th><td><?php echo $version ?></td></tr>
    <?php } ?>
</table>

<h3>PHP Libraries</h3>
<table class="dual">
    <?php foreach ($composer as $package) { ?>
    <tr><th><?php echo $package['name'] ?></th><td><?php echo $package['version'] ?></td></tr>
    <?php } ?>
</table>

<h3>Sessions</h3>
<?php echo \Rebond\Services\Session::renderAll() ?>

<h3>Cookies</h3>
<table class="dual">
    <?php
    foreach ($cookies as $key => $value) {
        echo '<tr><th>' . $key . '</th><td>' . $value . '</td></tr>';
    }
    ?>
</table>

<h3>Server variables</h3>
<table class="dual">
    <tr><th>__FILE__</th><td><?php echo __FILE__ ?></td></tr>
    <tr><th>dirname(__FILE__)</th><td><?php echo dirname(__FILE__) ?></td></tr>
<?php
foreach ($servers as $key => $value)
    echo '<tr><th>' . $key . '</th><td>' . (is_array($value) ? '#' . count($value) : $value) . '</td></tr>';
?>
</table>