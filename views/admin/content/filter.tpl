<div class="rb-filter">
    <div>
        <a href="/content/edit/?module=<?php echo $module ?>" class="rb-btn">
            <?php echo $this->lang('new') ?> <?php echo $module ?>
        </a>
    </div>

    <div>
        <div><?php echo $this->lang('order') ?></div>
        <select class="input rb-select" data-hash="3" id="order">
            <option value="#<?php echo $module . '/' . $version ?>/1/title-asc"><?php echo $this->lang('title') ?> (a->z)</option>
            <option value="#<?php echo $module . '/' . $version ?>/1/title-desc"><?php echo $this->lang('title') ?> (z->a)</option>
            <option value="#<?php echo $module . '/' . $version ?>/1/published_date-asc"><?php echo $this->lang('published_date') ?> (old->new)</option>
            <option value="#<?php echo $module . '/' . $version ?>/1/published_date-desc"><?php echo $this->lang('published_date') ?> (new->old)</option>
            <option value="#<?php echo $module . '/' . $version ?>/1/created_date-asc"><?php echo $this->lang('created_date') ?> (old->new)</option>
            <option value="#<?php echo $module . '/' . $version ?>/1/created_date-desc"><?php echo $this->lang('created_date') ?> (new->old)</option>
        </select>
    </div>

    <?php echo $itemsPerPage ?>
    <?php echo $paging ?>
</div>