<a href="#" id="rb-burger">
    <span></span>
    <span></span>
    <span></span>
</a>

<div id="rb-nav" class="rb-nav-mobile rb-activator" data-active="<?php echo $mainActive ?>">
    <?php foreach ($mainMenu as $menu) { ?>
        <a href="/<?php echo $menu['link'] ?>" data-menu="<?php echo $menu['link'] ?>"><?php echo $menu['text'] ?></a>
    <?php } ?>
</div>

<div class="rb-sub-menu rb-activator" data-active="<?php echo $subActive ?>">
    <input type="hidden" id="del-page" value="0" />
    <input type="hidden" id="folder-del" value="0" />

    <?php foreach ($subMenu as $menu) { ?>
        <a href="<?php echo $menu['link'] ?>" id="<?php echo $menu['id'] ?>" class="<?php echo $menu['class'] ?>" data-menu="<?php echo $menu['menu'] ?>"><?php echo $menu['text'] ?></a>
    <?php } ?>
</div>