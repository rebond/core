<h2><?php echo $this->lang('lang_change') ?></h2>
<div class="lang-selector">
<?php if (isset($langList)) {
    foreach ($langList as $key => $locale) {
        $class = ($current == $key) ? ' class="active"' : '';
        echo '<a href="?lang=' . $key . '"' . $class . '>' . $key . '</a>';
    }
} ?>
</div>

<h2><?php echo $this->lang('files_yaml') ?></h2>
<div class="rb-list">
<?php foreach ($xmlLangList as $file) {
    echo '<a href="/configuration/lang-edit/?f=' . $file . '">' . $file . '</a>';
} ?>
</div>
