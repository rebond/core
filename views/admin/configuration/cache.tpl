<h2><?php echo $this->lang('cache') ?></h2>

<p>
    <?php echo $this->lang('items_cached_for') ?>
    <?php echo ($cache == 0) ? '0' : floor($cache / 60) . ':' . str_pad(($cache % 60), 2, '0', STR_PAD_LEFT); ?>
    <?php echo $this->lang('minutes') ?> (<a href="#" id="delete-all-cache"><?php echo $this->lang('delete_all') ?></a>)
</p>

<?php if (count($fileData) > 0) { ?>
<table class="list">
    <tr>
        <th><?php echo $this->lang('file') ?></th>
        <th><?php echo $this->lang('expires_in') ?></th>
        <th></th>
    </tr>
    <?php foreach ($fileData as $file) { ?>
    <tr>
        <td><?php echo $file['name'] ?></td>
        <td><?php echo $file['expiry'] ?></td>
        <td><a href="#" class="delete-cache" data-file="<?php echo $file['name'] ?>"><?php echo $this->lang('delete') ?></a></td>
    </tr>
    <?php } ?>
</table>
<?php } else { ?>
<p><?php echo $this->lang('cache_none') ?></p>
<?php } ?>