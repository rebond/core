<div>
    <div><?php echo $this->lang('items_per_page') ?></div>
    <select class="input" id="count">
        <option value="0"<?php if ($maxByPage == 10) echo ' selected="selected"';?>>10</option>
        <option value="1"<?php if ($maxByPage == 20) echo ' selected="selected"';?>>20</option>
        <option value="2"<?php if ($maxByPage == 50) echo ' selected="selected"';?>>50</option>
        <option value="3"<?php if ($maxByPage == 100) echo ' selected="selected"'; ?>>100</option>
    </select>
</div>
