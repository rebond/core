<h2>Journal des modifications</h2>
<h3>v2.1.4</h3>
<div class="changelog">
    <ul>
        <li>Afficher du journal des modifications sur la page d'accueil</li>
        <li>Localiser les modules, composants, modèles, dispositions et rôles</li>
        <li>DEV: Corrige un bug dans l'affichage du ficher lorsque du HTML se trouve dans la trace de pile</li>
        <li>DEV: Améliorer et afficher les requêtes SQL</li>
    </ul>

    <div class="rb-form-item">
        <a href="#"
           class="rb-btn rb-btn-blank modal-link"
           data-modal="changelog"
           data-title="Journal des modifications">
            Voir plus
        </a>
    </div>
</div>

<div class="modal" id="modal-changelog">
    <h3>v2.1.4</h3>
    <ul>
        <li>Afficher du journal des modifications sur la page d'accueil</li>
        <li>Localiser les modules, composants, modèles, dispositions et rôles</li>
        <li>DEV: Améliorer et afficher les requêtes SQL</li>
    </ul>

    <h3>v2.1.3</h3>
    <ul>
        <li>Renommer le module Standard en Article</li>
        <li>Ajouter le rôle management des utilisateurs</li>
        <li>Afficher seulement le menu s'il est accessible par l'utilisateur connecté</li>
    </ul>

    <h3>v2.1.2</h3>
    <ul>
        <li>Améliorer les rôles et permissions</li>
        <li>Améliorer le mode débuggage</li>
        <li>Protéger les utilisateurs développeurs d'être modifié</li>
        <li>Ajouter un lien direct vers l'administration sur le site web (profile utilisateur)</li>
    </ul>

    <h3>v2.1.1</h3>
    <ul>
        <li>Remplacer le module de téléchargement en Flash par du HTML5 (dropzone.js)</li>
    </ul>

    <h3>Avant v2.1.1</h3>
    <ul>
        <li><a href="https://github.com/rebond/core" target="_blank">Voir sur github</a></li>
    </ul>
</div>
