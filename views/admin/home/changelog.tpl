<h2>Changelog</h2>

<h3>v2.1.4</h3>
<div class="changelog">
    <ul>
        <li>Display the changelog on the home page</li>
        <li>Localise modules, components, templates, layouts and roles</li>
        <li>DEV: Fix the log file when some HTML appears in the stack trace</li>
        <li>DEV: Improve and display the list of SQL queries in the debug section</li>
    </ul>

    <div class="rb-form-item">
        <a href="#"
           class="rb-btn rb-btn-blank modal-link"
           data-modal="changelog"
           data-title="Changelog">
            See more
        </a>
    </div>
</div>

<div class="modal" id="modal-changelog">
    <h3>v2.1.4</h3>
    <ul>
        <li>Display the changelog on the home page</li>
        <li>Localise modules, components, templates, layouts and roles</li>
        <li>DEV: Fix the log file when some HTML appears in the stack trace</li>
        <li>DEV: Improve and display the list of SQL queries in the debug section</li>
    </ul>

    <h3>v2.1.4</h3>
    <ul>
        <li><a href="https://github.com/rebond/core" target="_blank">Check on github</a></li>
    </ul>
</div>
