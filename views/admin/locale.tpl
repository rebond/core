<?php foreach ($langList as $lang => $locale) {
    $active = ($current == $lang) ? ' id="js-lang"' : ''; ?>
    <a href="?lang=<?php echo $lang ?>" class="lang" data-locale="<?php echo $locale ?>" <?php echo $active ?>><?php echo $lang ?></a>
<?php } ?>
