<div class="debug">
    <ul>
        <li>
            <a href="#" class="modal-link" data-modal="queries" data-title="List of queries">
                <?php echo count($queries) ?> <?php echo $this->lang('queries') ?>
            </a>
        </li>
        <li><?php echo $time ?> <?php echo $this->lang('seconds') ?></li>
        <li><?php echo round(memory_get_peak_usage() / 1024) ?> kb</li>
    </ul>
</div>

<div class="modal" id="modal-queries">
    <table class="dual">
        <?php foreach ($queries as $query) { ?>
        <tr><td><?php echo $query ?></td></tr>
        <?php } ?>
    </table>
</div>