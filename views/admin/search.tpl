<?php if (count($results) > 0) { ?>
<table class="list">
    <tr>
        <th><?php echo $this->lang('type') ?></th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('status') ?></th>
    </tr>
<?php foreach ($results as $result) { ?>
    <tr>
        <td><?php echo $result->getType() ?></td>
        <td><a href="<?php echo $result->getLink() ?>"><?php echo $result->getTitle() ?></a></td>
        <td><span class="enum version-<?php echo $result->getStatus() ?>"><?php echo $result->getStatusValue() ?></span></td>
    </tr>
<?php } ?>
</table>
<?php } else { ?>
    <?php echo $this->lang('result_none') ?>
<?php } ?>