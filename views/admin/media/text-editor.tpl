<h2><?php echo $this->lang('photo_tinymce') ?></h2>

<?php if (isset($link)) { ?>
    <div class="rb-list">
        <a href="<?php echo $link ?>" target="_blank"><?php echo $this->lang('photo_list') ?></a>
    </div>
<?php } else { ?>
    <b class="text-error"><?php echo $this->lang('image_none') ?></b>
<?php } ?>

<h3><?php echo $this->lang('generate') ?></h3>
<div><?php echo $this->lang('lang_updated') ?>: <b><?php echo isset($date) ? $date->format() : 'n\a' ?></b></div><br>
<div><a class="rb-btn" href="/media/text-editor?generate=true"><?php echo $this->lang('generate_photos_file') ?></a></div>
<?php $this->e($info) ?>
