<h2><?php echo $this->lang('upload') ?></h2>

<div class="rb-form-item">
    <button class="rb-btn" id="media-upload">Submit all files</button>
</div>
<div class="rb-form-item">
    <form class="dropzone" id="media-uploader"></form>
</div>

<p class="text-info"><?php echo $this->lang('size_limit', [$uploadMax]) ?></p>
<input type="hidden" id="folder-id" value="<?php echo $folderId ?>" />
<input type="hidden" id="upload-max" value="<?php echo $uploadMax ?>" />
