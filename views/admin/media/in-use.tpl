<h2><?php echo $this->lang('media_in_use') ?></h2>
<div class="rb-row">
    <div class="rb-col">
        <h3><?php echo $media->getTitle() ?></h3>
        <img src="<?php echo $this->showFromModel($media, 128, 128) ?>" class="in-use" />
        <p>
            <a href="/media/edit/?id=<?php echo $media->getId() ?>" class="rb-btn"><?php echo $this->lang('edit') ?></a>
        </p>
        <a href="#" class="rb-btn rb-btn-blank go-back"><?php echo $this->lang('back_to_list') ?></a>
    </div>

    <div class="rb-col rb-col-3">
        <h3><?php echo $this->lang('app') ?></h3>
        <table class="list">
            <tr>
                <th><?php echo $this->lang('title') ?></th>
                <th><?php echo $this->lang('created_date') ?></th>
                <th><?php echo $this->lang('modified_date') ?></th>
                <th><?php echo $this->lang('published_date') ?></th>
            </tr>
            <?php foreach ($apps as $item) { ?>
            <tr>
                <td>
                    <?php if ($item->getVersion() == \Rebond\Enums\Cms\Version::UPDATING) { ?>
                    <span class="enum"><?php echo $this->lang('version_newer') ?></span>
                    <?php } ?>
                    <a href="/content/edit/?module=<?php echo $item->getModule()->getName() ?>&id=<?php echo $item->getAppId() ?>"><?php echo $item->getTitle() ?></a>
                </td>
                <td><?php echo $item->getCreatedDate()->format() ?></td>
                <td><?php echo $item->getModifiedDate()->format() ?></td>
                <td><?php echo $item->getPublishedDate()->format() ?></td>
            </tr>
            <?php } ?>
        </table>

        <h3><?php echo $this->lang('items') ?></h3>
        <table class="list">
            <tr>
                <th><?php echo $this->lang('title') ?></th>
                <th><?php echo $this->lang('package') ?></th>
                <th><?php echo $this->lang('entity') ?></th>
            </tr>
            <?php foreach ($groupedItems as $items) {
        $id = 'get'. ucfirst($items['mediaLink']->getIdField());
            $title = 'get'. ucfirst($items['mediaLink']->getTitleField());

            foreach ($items['items'] as $item) { ?>
            <tr>
                <td><a href="<?php echo $items['mediaLink']->getUrl() . $item->$id() ?>" target="_blank"><?php echo $item->$title() ?></a></td>
                <td><?php echo $items['mediaLink']->getPackage() ?></td>
                <td><?php echo $items['mediaLink']->getEntity() ?></td>
            </tr>
            <?php }} ?>
        </table>
    </div>
</div>
