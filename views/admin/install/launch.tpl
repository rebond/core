<p>Before launching your site, you need to choose between a CMS site or a MVC site. This can changed later as well.</p>

<form method="POST" class="editor">
    <div class="rb-form-item">
        <?php echo \Rebond\Services\Form::buildRadio('site', ['CMS', 'MVC'], 0) ?>
    </div>
    <div class="rb-form-item">
        <button type="submit" class="rb-btn" name="btnLaunch">Launch</button>
    </div>
</form>