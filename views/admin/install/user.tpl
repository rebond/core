<p>Now that the database has been installed successfully, it is time to create an admin user.</p>

<form method="POST" class="editor">
    <?php echo $form->buildId() ?>
    <?php echo $form->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo \Rebond\Services\Lang::lang('email') ?>
            <?php echo $form->req('email') ?>
            <?php echo $form->buildEmail() ?>
        </label>
        <?php echo $form->getFieldError('email') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo \Rebond\Services\Lang::lang('password') ?>
            <?php echo $form->req('password') ?>
            <?php echo $form->buildPassword() ?>
        </label>
        <?php echo $form->getFieldError('password') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo \Rebond\Services\Lang::lang('first_name') ?>
            <?php echo $form->req('firstName') ?>
            <?php echo $form->buildFirstName() ?>
        </label>
        <?php echo $form->getFieldError('firstName') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo \Rebond\Services\Lang::lang('last_name') ?>
            <?php echo $form->req('lastName') ?>
            <?php echo $form->buildLastName() ?>
        </label>
        <?php echo $form->getFieldError('lastName') ?>
    </div>
    <div class="rb-form-item">
        <button type="submit" class="rb-btn" name="btnCreateUser">Create user</button>
    </div>
</form>