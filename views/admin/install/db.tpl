<p>You are ready to install the database.</p>
<p>The config file you just edited will be used to create the database.</p>

<form method="POST">
    <div class="rb-form-item">
        <button type="submit" class="rb-btn" name="btnInstallDb">Install the database</button>
        <button type="submit" class="rb-btn rb-btn-blank" name="btnSkipDb">Skip the database install</button>
    </div>
</form>
