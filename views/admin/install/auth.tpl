<p>Welcome to the installation guide of <b >Rebond</b>!</p>
<p>If you can see this page, that means you successfully followed the steps to setup your environment (git, composer and npm).</p>
<p>
    The first thing to do is to authenticate yourself, to make sure you are the owner of this site.
    To do so, open the authentication file [<b class="text-important">\files\authentication.txt</b>], then copy and paste your authentication number.
</p>

<form method="POST" class="editor">
    <div class="rb-form-item">
        <label>Authentication number
            <input type="text" class="input" id="auth" name="auth" maxlength="40" />
        </label>
    </div>
    <div class="rb-form-item">
        <button type="submit" class="rb-btn" name="btnAuth">Authenticate</button>
    </div>
</form>
