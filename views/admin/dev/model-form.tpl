<h2><?php echo $model ?></h2>

<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <textarea id="fileContent" name="fileContent"><?php echo file_get_contents($filePath) ?></textarea>
    <input type="hidden" id="file" name="file" value="<?php echo $model ?>" />
    <div class="rb-form-item">
        <button type="submit" class="rb-btn" name="btnSave"><?php echo $this->lang('save') ?></button>
        <a href="/dev/generator" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
    </div>
</form>
