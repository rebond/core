<div class="rb-filter">
    <div>
        <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST">
            <?php echo $this->buildSubmit(0, 'backup') ?>
        </form>
    </div>
    <div class="paging">
        <div class="items">
            <span id="item-count"><?php echo $count ?></span> <?php echo $this->lang('items') ?>
        </div>
    </div>
</div>