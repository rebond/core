<div class="dev-test">
    <h2><?php echo $title ?></h2>
    <p>Count: <?php echo $count ?></p>
    <?php if ($valid) { ?>
        <p>Valid model</p>
    <?php } else { ?>
        <p>Invalid model</p>
        <ul>
        <?php
            foreach ($fields as $field) {
                if (!$field->isValid()) {
                    echo '<li>' . $field->getFieldName() . ': ' . $field->getMessage() . '</li>';
                }
            }
        ?>
        </ul>
    <?php } ?>
</div>
<hr />