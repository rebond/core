<div class="rb-filter">
    <div>
        <a href="/dev/bin/?empty=true" class="rb-btn"><?php echo $this->lang('bin_empty') ?></a>
    </div>
</div>

<?php echo $result ?>

<h2><?php echo $this->lang('modules') ?></h2>

<?php foreach ($contentItems as $module => $items) { ?>
<h3><?php echo $module ?></h3>
<table class="list">
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('version') ?></th>
        <th><?php echo $this->lang('created_date') ?></th>
        <th><?php echo $this->lang('modified_date') ?></th>
        <th><?php echo $this->lang('published_date') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td><a href="/content/trail/?module=<?php echo $module ?>&id=<?php echo $item->getContentGroup() ?>" target="_blank"><?php echo $item->getTitle() ?></a></td>
        <td><span class="enum version_<?php echo $item->getVersion() ?>"><?php echo $item->getVersionValue() ?></span></td>
        <td><?php echo $item->getCreatedDate()->format() ?></td>
        <td><?php echo $item->getModifiedDate()->format() ?></td>
        <td><?php echo $item->getPublishedDate()->format() ?></td>
        <td><a href="#" class="bin-delete" data-module="<?php echo $item->getModule()->getName() ?>" data-id="<?php echo $item->getAppId() ?>"><?php echo $this->lang('delete') ?></a></td>
    </tr>
    <?php } ?>
</table>
<?php } ?>


<h2><?php echo $this->lang('stylesheets') ?></h2>

<?php foreach ($cssFiles as $skin => $files) { ?>
    <?php if (count($files) > 0) { ?>
        <h3><?php echo ucfirst($skin) ?></h3>
        <table class="list">
        <tr>
            <th><?php echo $this->lang('title') ?></th>
            <th><?php echo $this->lang('modified_date') ?></th>
            <th></th>
        </tr>
        <?php foreach ($files as $file) { ?>
            <tr>
                <td><a href="/designer/css-edit/?s=<?php echo $skin ?>&f=<?php echo $file['name'] ?>" target="_blank"><?php echo $file['name'] ?></a></td>
                <td><?php echo $file['date'] ?></td>
                <td><a href="#" class="bin-delete" data-skin="<?php echo $skin ?>" data-file="<?php echo $file['name'] ?>"><?php echo $this->lang('delete') ?></a></td>
            </tr>
        <?php } ?>
        </table>
    <?php } ?>
<?php } ?>

<h2><?php echo $this->lang('template_main') ?></h2>

<?php if (count($mainTpl) > 0) { ?>
    <table class="list">
        <tr>
            <th><?php echo $this->lang('title') ?></th>
            <th><?php echo $this->lang('modified_date') ?></th>
            <th></th>
        </tr>
        <?php foreach ($mainTpl as $tpl) { ?>
            <tr>
                <td><a href="/designer/tpl-edit/?folder=<?php echo $tpl['name'] ?>" target="_blank"><?php echo $tpl['name'] ?></a></td>
                <td><?php echo $tpl['date'] ?></td>
                <td>
                    <a href="#" class="bin-delete" data-folder="<?php echo $tpl['folder'] ?>" data-file="<?php echo $tpl['name'] ?>">
                        <?php echo $this->lang('delete') ?>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </table>
<?php } ?>

<h2><?php echo $this->lang('template_modules') ?></h2>

<?php foreach ($appTpl as $module => $templates) { ?>
    <?php if (count($templates) > 0) { ?>
        <h3><?php echo $module ?></h3>
        <table class="list">
            <tr>
                <th><?php echo $this->lang('title') ?></th>
                <th><?php echo $this->lang('modified_date') ?></th>
                <th></th>
            </tr>
            <?php foreach ($templates as $tpl) { ?>
                <tr>
                    <td>
                        <a href="/designer/tpl-edit/?folder=app&module=<?php echo $module ?>&template=<?php echo $tpl['name'] ?>" target="_blank">
                            <?php echo $tpl['name'] ?>
                        </a>
                    </td>
                    <td><?php echo $tpl['date'] ?></td>
                    <td>
                        <a href="#" class="bin-delete" data-module="<?php echo $module ?>" data-file="<?php echo $tpl['name'] ?>">
                            <?php echo $this->lang('delete') ?>
                        </a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    <?php } ?>
<?php } ?>
