<?php if (count($files) > 0) { ?>
    <table class="list">
        <tr>
            <th><?php echo $this->lang('file') ?></th>
            <th><?php echo $this->lang('file_size') ?></th>
            <th><?php echo $this->lang('created_date') ?></th>
            <th></th>
        </tr>
        <?php foreach ($files as $file) { ?>
            <tr>
                <td><?php echo $file ?></td>
                <td><?php echo round(filesize($path . $file) / 1000) ?> kb</td>
                <td><?php echo (new \Rebond\Models\DateTime())->setTimestamp(filemtime($path . $file))->format() ?></td>
                <td>
                    <a href="/dev/download/?backup=<?php echo $file ?>"><?php echo $this->lang('download') ?></a>
                    |
                    <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" class="inline" method="POST" name="form" id="form">
                        <input type="hidden" name="file" value="<?php echo $file ?>" />
                        <button name="btnRestore" class="rb-link restore"><?php echo $this->lang('restore') ?></button>
                    </form>
                    |
                    <a href="#" class="delete-backup text-error" data-file="<?php echo $file ?>"><?php echo $this->lang('delete') ?></a>
                </td>
            </tr>
        <?php } ?>
    </table>
<?php } else { ?>
    <p><?php echo $this->lang('backup_none') ?></p>
<?php } ?>