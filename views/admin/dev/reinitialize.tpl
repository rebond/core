<h2><?php echo $this->lang('reinitialize') ?></h2>

<form method="POST" class="editor">
    <div class="rb-form-item">
        <button type="submit" name="btnFiles" class="rb-btn">Index files</button>
    </div>
    <div class="rb-form-item">
        <?php echo \Rebond\Services\Form::buildCheckbox('files', ['Confirm']) ?>
    </div>

    <p class="text-info">Index files will be reverted to install mode. The database will not be touched.</p>

    <div class="rb-form-item">
        <button type="submit" name="btnRestore" class="rb-btn">Index files + restore DB</button>
    </div>
    <div class="rb-form-item">
        <?php echo \Rebond\Services\Form::buildCheckbox('restore', ['Confirm']) ?>
    </div>

    <p class="text-info">Index files will be reverted to install mode. The database will be backed up and then restored using the launch script.</p>

    <div class="rb-form-item">
        <button type="submit" name="btnFull" class="rb-btn">Index files + Delete DB</button>
    </div>
    <div class="rb-form-item">
        <?php echo \Rebond\Services\Form::buildCheckbox('full', ['Confirm']) ?>
    </div>

    <p class="text-info">Index files will be reverted to install mode. The database will be backed up and then deleted.</p>
</form>
