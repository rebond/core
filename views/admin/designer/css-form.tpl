<h2><?php echo $skin . ' / ' . $file ?></h2>

<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <textarea id="fileContent" name="fileContent"><?php echo file_get_contents($filePath) ?></textarea>
    <input type="hidden" id="skin" name="skin" value="<?php echo $skin ?>" />
    <input type="hidden" id="file" name="file" value="<?php echo $file ?>" />
    <div class="rb-form-item">
        <?php if ($editable) { ?>
            <button type="submit" class="rb-btn" name="btnSave" id="btnSave"><?php echo $this->lang('save') ?></button>
        <?php } ?>
        <a class="rb-btn rb-btn-blank" href="/designer/css"><?php echo $this->lang('cancel') ?></a>
    </div>
</form>
