<h2><?php echo $this->lang('template_main') ?></h2>

<?php foreach ($mainTemplates as $folder => $templates) { ?>
    <h3><?php echo $folder ?></h3>
    <div class="rb-list">
        <?php foreach ($templates as $template) { ?>
            <a href="/designer/tpl-edit/?folder=<?php echo $folder ?>&template=<?php echo $template ?>"><?php echo $template ?></a>
        <?php } ?>
    </div>
<?php } ?>

<h2><?php echo $this->lang('template_modules') ?></h2>

<?php foreach ($appTemplates as $module => $templates) { ?>
    <h3><?php echo $module ?></h3>
    <div class="rb-list">
        <?php foreach ($templates as $template) { ?>
            <a href="/designer/tpl-edit/?folder=App&module=<?php echo $module ?>&template=<?php echo $template ?>"><?php echo $template ?></a>
        <?php } ?>
    </div>
<?php } ?>
