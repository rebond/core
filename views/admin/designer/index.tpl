<h2><?php echo $this->lang('designer') ?></h2>
<p><?php echo $this->lang('intro_designer') ?></p>
<ul class="list-menu">
    <li>
        <i class="fa fa-2x fa-fw fa-css3"></i>
        <a href="/designer/css"><?php echo $this->lang('stylesheets') ?></a>
    </li>
    <li>
        <i class="fa fa-2x fa-fw fa-html5"></i>
        <a href="/designer/tpl"><?php echo $this->lang('templates') ?></a>
    </li>
</ul>
