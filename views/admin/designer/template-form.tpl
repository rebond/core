<h2><?php echo $folder . ' / ' . (isset($module) ? $module . ' / ' : '') . $template ?></h2>

<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <textarea id="fileContent" name="fileContent"><?php echo file_get_contents($templatePath) ?></textarea>
    <input type="hidden" name="template" value="<?php echo $template ?>" />
    <input type="hidden" name="folder" value="<?php echo $folder ?>" />
    <input type="hidden" name="module" value="<?php echo $module ?>" />
    <div class="rb-form-item">
        <?php if ($editable) { ?>
            <button type="submit" class="rb-btn" name="btnSave"><?php echo $this->lang('save') ?></button>
        <?php } ?>
        <a class="rb-btn rb-btn-blank" href="/designer/tpl"><?php echo $this->lang('cancel') ?></a>
    </div>
</form>
