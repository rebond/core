<h2><?php echo $this->lang('stylesheets') ?></h2>

<?php foreach ($files as $skin => $cssFiles) { ?>
    <h3><?php echo $skin ?></h3>
    <div class="rb-list">
        <?php foreach ($cssFiles as $file) { ?>
            <a href="/designer/css-edit/?skin=<?php echo $skin ?>&file=<?php echo $file ?>"><?php echo $file ?></a>
        <?php } ?>
    </div>
<?php } ?>
