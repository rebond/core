<h2><?php echo $title ?></h2>
<ul class="list-menu">
    <?php foreach ($menu as $item) { ?>
    <li><i class="fa fa-2x fa-fw <?php echo $item['icon'] ?>"></i>
        <a href="<?php echo $item['link'] ?>"><?php echo $item['text'] ?></a>
        <span class="list">
            <?php foreach ($item['shortcuts'] as $shortcut) { ?><a href="<?php echo $shortcut['link'] ?>"><?php echo $shortcut['text'] ?></a><?php } ?>
        </span>
    </li>
    <? } ?>
</ul>
