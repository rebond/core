<h2><?php echo $selectedLog ?></h2>
<table class="list">
    <tr>
        <th><?php echo $this->lang('date') ?></th>
        <th><?php echo $this->lang('code') ?></th>
        <th><?php echo $this->lang('user') ?></th>
        <th><?php echo $this->lang('message') ?></th>
        <th><?php echo $this->lang('file') ?></th>
        <th><?php echo $this->lang('line') ?></th>
        <th></th>
    </tr>
<?php $i = 0;
foreach ($logs as $log) { ?>
    <tr>
        <td><?php echo $log['date'] ?></td>
        <td><span class="enum"><?php echo \Rebond\Enums\Core\Code::lang($log['code']) ?></span></td>
        <td><a href="/user/edit?view=true&id=<?php echo $log['userId'] ?>" target="_blank"><?php echo $log['user']->getFullName() ?></a></td>
        <td><?php echo $log['message'] ?></td>
        <td class="large" title="<?php echo $log['file'] ?>"><?php echo $log['file'] ?></td>
        <td><?php echo $log['line'] ?></td>
        <td>
            <a href="#" class="log-stack" data-modal="log-stack-<?php echo $i ?>" data-title="<?php echo $log['message'] ?>">
                <?php echo $this->lang('stack_view') ?>
            </a>
            <div class="modal" id="modal-log-stack-<?php echo $i++ ?>">
                <pre><?php echo json_encode($log['stack'], JSON_PRETTY_PRINT) ?></pre>
            </div>
        </td>
    </tr>
<?php } ?>
</table>

