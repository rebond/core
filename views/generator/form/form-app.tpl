<#php
<?php echo $license ?>

namespace Own\Forms\App;

use Own\Models\<?php echo $package ?>\<?php echo $entity ?>;

class <?php echo $entity ?>Form extends \Generated\Forms\App\Base<?php echo $entity ?>Form
{
    /**
     * @param <?php echo $entity ?> $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->initApp();
    }

    public function initApp()
    {
    }
}
