<#php
<?php echo $license ?>

namespace <?php echo $namespace ?>\Forms\<?php echo $package ?>;

use <?php echo $mainNamespace ?>\Models\<?php echo $package ?>\<?php echo $entity ?>;
use <?php echo $mainNamespace ?>\Repository\<?php echo $package ?>\<?php echo $entity ?>Repository;
use Rebond\Enums\Core\Result;
use Rebond\Forms\AbstractForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;

class Base<?php echo $entity ?>Form extends AbstractForm
{
<?php echo $propertyList ?>
    /*
     * @param <?php echo $entity ?> $model
     * @param string $unique
     */
    public function __construct(<?php echo $entity ?> $model, $unique)
    {
        parent::__construct($model, $unique);
<?php echo $propertyInitBase ?>
    }

    /**
     * @param array $properties = null
     * @return Base<?php echo $entity ?>Form
     */
    public function setFromPost($properties = null)
    {
<?php echo $propertySetPost ?>
        $this->token = Converter::stringKey('token' . $this->unique, 'post', $this->token);
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        if ($this->getValidation()->isClean()) {
            return $this->validation;
        }
        $this->validation->clear();
        $fields = [];
        $fields['token'] = $this->validateToken();
<?php echo $propertyValidate ?>
        $this->validation->setFields($fields);
        return $this->validation;
    }

<?php echo $propertyMethod ?>
}
