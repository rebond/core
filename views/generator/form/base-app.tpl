<#php
<?php echo $license ?>

namespace Generated\Forms\App;

use Rebond\Enums\Core\Result;
use Rebond\Forms\Cms\ContentForm;
use Rebond\Models\FormValidator;
use Rebond\Services\Converter;
use Rebond\Services\Form;
use Rebond\Services\Lang;
use Rebond\Services\Validate;
use Own\Models\App\<?php echo $entity ?>;

class Base<?php echo $entity ?>Form extends ContentForm
{
<?php echo $propertyList ?>
    /**
     * @param <?php echo $entity ?> $model
     * @param string $unique
     */
    public function __construct(<?php echo $entity ?> $model, $unique)
    {
        parent::__construct($model, $unique);
<?php echo $propertyInitBase ?>
    }

    /**
     * @param array $properties = null
     * @return Base<?php echo $entity ?>Form
     */
    public function setFromPost($properties = null)
    {
        parent::setFromPost($properties);
<?php echo $propertySetPost ?>
        return $this;
    }

    /**
     * @param array $properties = null
     * @return FormValidator
     */
    public function validate($properties = null)
    {
        parent::validate($properties);
        $fields = [];
<?php echo $propertyValidate ?>
        $this->validation->addFields($fields);
        return $this->validation;
    }

<?php echo $propertyMethod ?>
}
