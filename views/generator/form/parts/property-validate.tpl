<?php if ($type == 'primaryKey' && !$isAutoIncrement) { ?>
        if (!isset($properties) || in_array('<?php echo $propertyName ?>', $properties)) {
            $fields['<?php echo $propertyName ?>'] = $this->validate<?php echo ucfirst($propertyName) ?>();
        }
<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
        if (!isset($properties) || in_array('<?php echo $propertyName ?>Id', $properties)) {
            $fields['<?php echo $propertyName ?>'] = $this->validate<?php echo ucfirst($propertyName) ?>();
        }
<?php } else if (!in_array($type, ['primaryKey', 'bool', 'time', 'hidden'])) { ?>
        if (!isset($properties) || in_array('<?php echo $propertyName ?>', $properties)) {
            $fields['<?php echo $propertyName ?>'] = $this->validate<?php echo ucfirst($propertyName) ?>();
        }
<?php } ?>
