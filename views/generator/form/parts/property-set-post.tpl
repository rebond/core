<?php if ($type == 'primaryKey') { ?>
        if (!isset($properties) || in_array('<?php echo $propertyName ?>', $properties)) {
            $value = Converter::intKey('<?php echo $propertyName ?>' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->set<?php echo ucfirst($propertyName) ?>($value);
            }
        }
<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
        if (!isset($properties) || in_array('<?php echo $propertyName ?>Id', $properties)) {
            $value = Converter::intKey('<?php echo $propertyName ?>Id' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->set<?php echo ucfirst($propertyName) ?>Id($value);
            }
        }
<?php } else if (in_array($type, ['datetime', 'date', 'time'])) { ?>
        if (!isset($properties) || in_array('<?php echo $propertyName ?>', $properties)) {
            $value = Converter::dateKey('<?php echo $propertyName ?>' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->set<?php echo ucfirst($propertyName) ?>($value);
            }
        }
<?php } else if (in_array($type, ['bool'])) { ?>
        if (!isset($properties) || in_array('<?php echo $propertyName ?>', $properties)) {
            $value = Converter::boolKey('<?php echo $propertyName ?>' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->set<?php echo ucfirst($propertyName) ?>($value);
            }
        }
<?php } else if (in_array($type, ['integer', 'enum', 'status', 'version'])) { ?>
        if (!isset($properties) || in_array('<?php echo $propertyName ?>', $properties)) {
            $value = Converter::intKey('<?php echo $propertyName ?>' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->set<?php echo ucfirst($propertyName) ?>($value);
            }
        }
<?php } else if (in_array($type, ['numeric'])) { ?>
        if (!isset($properties) || in_array('<?php echo $propertyName ?>', $properties)) {
            $value = Converter::floatKey('<?php echo $propertyName ?>' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->set<?php echo ucfirst($propertyName) ?>($value);
            }
        }
<?php } else { ?>
        if (!isset($properties) || in_array('<?php echo $propertyName ?>', $properties)) {
            $value = Converter::stringKey('<?php echo $propertyName ?>' . $this->unique, 'post');
            if (isset($value)) {
                $this->getModel()->set<?php echo ucfirst($propertyName) ?>($value);
            }
        }
<?php } ?>
