<?php if (!in_array($type, ['primaryKey', 'foreignKeyLink', 'singleKeyLink']) || ($type == 'primaryKey' && !$isAutoIncrement)) { ?>
    /* @var array */
    protected $<?php echo $propertyName ?>Validator;
<?php } ?>
<?php if (!in_array($type, ['foreignKey', 'singleKey', 'bool', 'enum', 'status', 'version'])) { ?>
    /* @var string */
    protected $<?php echo $propertyName ?>Builder;
<?php } ?>