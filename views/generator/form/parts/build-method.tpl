<?php if ($type == 'primaryKey' && isset($relation)) { ?>
    public function build<?php echo ucfirst($propertyName) ?>()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = <?php echo $relation ?>::getList(['id', '<?php echo $link ?>']);
        $models = <?php echo $relation ?>::loadAll($options);
        return Form::buildItemList('<?php echo $propertyName ?>' . $this->unique, $models, 'id', '<?php echo $link ?>', $this->getModel()->get<?php echo ucfirst($propertyName) ?>(), $this-><?php echo $propertyName ?>Validator['<?php echo $type ?>']);
    }
<?php } else if ($type == 'primaryKey') { ?>
    public function build<?php echo ucfirst($propertyName) ?>()
    {
        return Form::buildField('<?php echo $propertyName ?>' . $this->unique, $this-><?php echo $propertyName ?>Builder, $this->getModel()->get<?php echo ucfirst($propertyName) ?>());
    }
<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey'])) { ?>
    public function build<?php echo ucfirst($propertyName) ?>()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = <?php echo $relation ?>::getList(['id', '<?php echo $link ?>']);
        $models = <?php echo $relation ?>::loadAll($options);
        return Form::buildItemList('<?php echo $propertyName ?>Id' . $this->unique, $models, 'id', '<?php echo $link ?>', $this->getModel()->get<?php echo ucfirst($propertyName) ?>Id(), $this-><?php echo $propertyName ?>Validator['<?php echo $type ?>']);
    }
<?php if ($type == 'multipleKey') { ?>

    public function build<?php echo ucfirst($propertyName) ?>List()
    {
        $options = [];
        $options['clearSelect'] = true;
        $options['select'][] = <?php echo $relation ?>::getList(['id', '<?php echo $link ?>']);
        $allItems = <?php echo $relation ?>::loadAll($options);
        $items = <?php echo $entity ?>Repository::loadAllBy<?php echo ucfirst($otherMultipleKey) ?>Id($this->getModel()->get<?php echo ucfirst($otherMultipleKey) ?>Id());
        $selectedValues = [];
        foreach ($items as $item) {
            $selectedValues[] = $item->get<?php echo ucfirst($propertyName) ?>Id();
        }
        return Form::buildCheckboxList('<?php echo $propertyName ?>' . $this->unique, $allItems, 'id', '<?php echo $link ?>', $selectedValues);
    }
<?php } ?>
<?php } else if ($type == 'media') { ?>
    public function build<?php echo ucfirst($propertyName) ?>($isMedia = true)
    {
        if ($isMedia) {
            return Form::buildMedia('<?php echo $propertyName ?>Id' . $this->unique, $this->getModel()->get<?php echo ucfirst($propertyName) ?>());
        }
        return Form::buildField('<?php echo $propertyName ?>Id' . $this->unique, $this-><?php echo $propertyName ?>Builder, $this->getModel()->get<?php echo ucfirst($propertyName) ?>());
    }
<?php } else if (in_array($type, ['enum', 'status', 'version'])) { ?>
    public function build<?php echo ucfirst($propertyName) ?>()
    {
        return Form::buildList('<?php echo $propertyName ?>' . $this->unique, $this->getModel()->get<?php echo ucfirst($propertyName) ?>List(), $this->getModel()->get<?php echo ucfirst($propertyName) ?>());
    }
<?php } else if ($type == 'bool') { ?>
    public function build<?php echo ucfirst($propertyName) ?>()
    {
        return Form::buildBoolean('<?php echo $propertyName ?>' . $this->unique, '<?php echo $property ?>', $this->getModel()->get<?php echo ucfirst($propertyName) ?>());
    }
<?php } else if ($type == 'password') { ?>
    public function build<?php echo ucfirst($propertyName) ?>($confirm = '')
    {
        return Form::buildField('<?php echo $propertyName ?>' . $confirm . $this->unique, $this-><?php echo $propertyName ?>Builder, $this->getModel()->get<?php echo ucfirst($propertyName) ?>());
    }
<?php } else { ?>
    public function build<?php echo ucfirst($propertyName) ?>()
    {
        return Form::buildField('<?php echo $propertyName ?>' . $this->unique, $this-><?php echo $propertyName ?>Builder, $this->getModel()->get<?php echo ucfirst($propertyName) ?>());
    }
<?php } ?>

