<#php
<?php echo $license ?>

namespace Own\Gadgets\App;

use Rebond\App;
<?php if ($hasContent) { ?>
use Own\Forms\App\<?php echo $entity ?>Form;
use Own\Models\App\<?php echo $entity ?>;
use Own\Repository\App\<?php echo $entity ?>Repository;
<?php } ?>
use Rebond\Enums\Core\Result;
use Rebond\Gadgets\AbstractGadget;
<?php if (!$hasContent) { ?>
use Rebond\Services\Template;
<?php } ?>

class <?php echo $entity ?>Gadget extends AbstractGadget
{
    public function __construct(App $app)
    {
        parent::__construct($app, '<?php echo $entity ?>');
    }

<?php if (in_array('cards', $views)) { ?>
    public function cards()
    {
        $items = <?php echo $entity ?>Repository::loadByVersion('published');
        return $this->renderCards($items);
    }

<?php } ?>
<?php if (in_array('filtered-cards', $views)) { ?>
    public function filteredCards($filterId)
    {
        $options = [];
        if ($filterId != 0) {
            $options['where'][] = ['content.filter_id = ?', $filterId];
        }
        $items = <?php echo $entity ?>Repository::loadByVersion('published', true, $options);
        return $this->renderCards($items);
    }

<?php } ?>
<?php if (in_array('single', $views)) { ?>
    public function single($contentGroup)
    {
        $item = <?php echo $entity ?>Repository::loadCurrent($contentGroup);
        return $this->renderSingle($contentGroup, $item);
    }

<?php } ?>
<?php if (in_array('editor', $views)) { ?>
    public function editor()
    {
        $model = new <?php echo $entity ?>();
        $form = new <?php echo $entity ?>Form($model);
        return $this->renderEditor($form);
    }

<?php } ?>
<?php if (!$hasContent) { ?>
    public function custom()
    {
        $var = 'custom template';
        $tpl = new Template(Template::MODULE, ['app', '<?php echo $entity ?>']);
        $tpl->set('var', $var);
        return $tpl->render('custom');
    }

<?php } ?>
}