<?php echo $isGenerated ? $autoGenerated : '' ?>
<div class="bg-white">
    <#php foreach ($items as $item) { ?>
<?php if ($package == 'App') { ?>
    <h2>
        <a href="#" class="modal-link"
           data-modal="<?php echo $entity ?>-<#php echo $item->getId() ?>"
           data-title="<#php echo $item->getTitle() ?>">
            <#php echo $item->getTitle() ?>
        </a>
        <#php if ($filter) { ?>
        <small><#php echo $item->getFilter() ?></small>
        <#php } ?>
    </h2>
<?php } ?>

    <?php echo $propertyList ?>

    <div class="right">
        <a href="#" class="modal-link"
           data-modal="<?php echo $entity ?>-<#php echo $item->getId() ?>"
           data-title="<#php echo $item->getTitle() ?>"><#php echo $this->lang('view_more') ?></a>
    </div>
    <div class="modal" id="modal-<?php echo $entity ?>-<#php echo $item->getId() ?>">
        <?php echo $propertyList ?>
    </div>
    <#php } ?>
</div>