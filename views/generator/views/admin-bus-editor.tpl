<?php echo $autoGenerated ?>

<#php echo $this->renderTitle('<?php echo $sqlEntity ?>', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<#php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <#php echo $item->build<?php echo ucfirst($primaryKey) ?>() ?>
    <#php echo $item->buildToken() ?>
<?php echo $propertyList ?>
    <div class="rb-form-item">
        <#php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="/quickview/#<?php echo $entity ?>" class="rb-btn rb-btn-blank"><#php echo $this->lang('cancel') ?></a>
        <#php echo $item->getFieldError('token') ?>
    </div>
</form>
