<?php if ($type == 'primaryKey' && isset($pkRelation)) { ?>
        <#php echo $item->get<?php echo ucfirst($property) ?>Item() ?>
<?php } else if (in_array($type, ['datetime', 'date', 'time', 'modifiedDate', 'createdDate'])) { ?>
        <#php echo $item->get<?php echo ucfirst($property) ?>()->format('<?php echo $type ?>') ?>
<?php } else if (in_array($type, ['media'])) { ?>
        <img src="<#php echo $this->showFromModel($item->get<?php echo ucfirst($property) ?>()) ?>" alt="<#php echo $item->get<?php echo ucfirst($property) ?>()->getTitle() ?>" />
<?php } else if (in_array($type, ['bool'])) { ?>
        <span class="bool-<#php echo $item->get<?php echo ucfirst($property) ?>() ?>"><#php echo $item->get<?php echo ucfirst($property) ?>() ?></span>
<?php } else if (in_array($type, ['enum']) && $property == 'status') { ?>
        <#php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
            <a href="#" class="status-<#php echo $item->getStatus() ?>" data-package="<?php echo $package ?>" data-entity="<?php echo $entity ?>" data-id="<#php echo $item->getId() ?>" data-status="<#php echo $item->getStatus() ?>"><#php echo $item->getStatusValue() ?></a>
        <#php } else { ?>
            <span class="enum status-<#php echo $item->getStatus() ?>"><#php echo $item->getStatusValue() ?></span>
        <#php } ?>
<?php } else if (in_array($type, ['enum'])) { ?>
        <span class="enum <?php echo $property ?>-<#php echo $item->get<?php echo ucfirst($property) ?>() ?>"><#php echo $item->get<?php echo ucfirst($property) ?>Value() ?></span>
<?php } else { ?>
        <#php echo $item->get<?php echo ucfirst($property) ?>() ?>
<?php } ?>