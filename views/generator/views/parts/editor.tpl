    <div class="rb-form-item">
<?php if (in_array($type, ['bool'])) { ?>
        <#php echo $item->build<?php echo ucfirst($propertyName) ?>() ?>
<?php } else if (!$isSiteEditor && in_array($type, ['media'])) { ?>
        <label>
            <#php echo $this->lang('<?php echo $property ?>') ?>
            <#php echo $item->req('<?php echo $propertyName ?>') ?>
        </label>
        <#php echo $item->build<?php echo ucfirst($propertyName) ?>() ?>
        <#php echo $item->getFieldError('<?php echo $propertyName ?>') ?>
<?php } else if (in_array($type, ['datetime', 'date', 'time'])) { ?>
        <label>
            <#php echo $this->lang('<?php echo $property ?>') ?>
            <#php echo $item->req('<?php echo $propertyName ?>') ?>
        </label>
        <#php echo $item->build<?php echo ucfirst($propertyName) ?>(false) ?>
        <#php echo $item->getFieldError('<?php echo $propertyName ?>') ?>
<?php } else if (in_array($type, ['richText'])) { ?>
        <label>
            <#php echo $this->lang('<?php echo $property ?>') ?>
            <#php echo $item->req('<?php echo $propertyName ?>') ?>
        </label>
        <#php echo $item->build<?php echo ucfirst($propertyName) ?>(false) ?>
<?php } else { ?>
        <label>
            <#php echo $this->lang('<?php echo $property ?>') ?>
            <#php echo $item->req('<?php echo $propertyName ?>') ?>
            <#php echo $item->build<?php echo ucfirst($propertyName) ?>() ?>
        </label>
        <#php echo $item->getFieldError('<?php echo $propertyName ?>') ?>
<?php } ?>
    </div>