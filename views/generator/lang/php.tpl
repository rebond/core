<#php
<?php echo $license ?>

namespace Generated\Cache;

class <?php echo $site . ucfirst($lang) ?>

{
    public static function getLang()
    {
        return '<?php echo $lang ?>';
    }

    public static function get($node)
    {
        if (!array_key_exists($node, self::$lang)) {
            return null;
        }

        return self::$lang[$node];
    }

    private static $lang = [
<?php echo $list ?>
    ];
}
