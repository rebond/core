<#php
<?php echo $license ?>

namespace Generated\Cache;

class Config
{
    public static function configList()
    {
        return [
            'site-host' => '<?php echo $host ?>',
            'site-folder' => '<?php echo $siteFolder ?>',
            'admin-folder' => '<?php echo $adminFolder ?>',
            'site-domain' => '<?php echo $siteDomain ?>',
            'admin-domain' => '<?php echo $adminDomain ?>',
            'salt' => '<?php echo $salt ?>',

            'db-host' => '<?php echo $dbHost ?>',
            'db-port' => '<?php echo $dbPort ?>',
            'db-username' => '<?php echo $dbUsername ?>',
            'db-password' => '<?php echo $dbPassword ?>',
            'db-name' => '<?php echo $dbName ?>',

            'mail-type' => '<?php echo $mailType ?>',
            'mail-host' => '<?php echo $mailHost ?>',
            'mail-port' => '<?php echo $mailPort ?>',
            'mail-option' => '<?php echo $mailOption ?>',
            'mail-email' => '<?php echo $mailEmail ?>',
            'mail-password' => '<?php echo $mailPassword ?>',

<?php echo $setting ?>
        ];
    }

    public static function langList()
    {
        return [
<?php echo $lang ?>
        ];
    }
}
