<#php
<?php echo $license ?>

namespace Generated\Repository\App;

use Rebond\Enums\Cms\Version;
use Own\Models\App\<?php echo $entity ?>;
use Rebond\Models\Cms\Content;
use Rebond\Models\DateTime;
use Rebond\Repository\Cms\ContentRepository;
use Rebond\Repository\Data;
use Rebond\Services\Converter;
use Rebond\Services\Nav;

class Base<?php echo $entity ?>Repository extends ContentRepository
{
    /**
     * Get field list of properties
     * @param array $properties = []
     * @param string $alias = x
     * @return string
     */
    public static function getList(array $properties = [], $alias = 'x')
    {
        if (empty($properties)) {
            $list =
<?php echo $selectList ?>
            return $list;
        }

        $list = '';
        foreach ($properties as $property) {
            $list .= $alias . '.' . $property . ' AS ' . $alias . Converter::toCamelCase($property, true) . ', ';
        }
        return rtrim(trim($list), ',');
    }

    /**
     * @param array $options = []
     * @return int
     */
    public static function count(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(x.<?php echo $primaryKey ?>)');
        $db->buildQuery('from', '<?php echo $table ?> x');
        ContentRepository::autoJoin($db, '<?php echo $entity ?>', false);
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param string $version = 'published'
     * @param bool $onSite = true
     * @param array $options = []
     * @return int
     */
    public static function countByVersion($version = 'published', $onSite = true, array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', 'count(x.<?php echo $primaryKey ?>)');
        $db->buildQuery('from', '<?php echo $table ?> x');

        if (Nav::isPreview()) {
            $version = 'preview';
        }
        if ($onSite && $version != 'preview') {
            self::condition($db, 'expiration');
        }
        self::condition($db, $version);
        ContentRepository::autoJoin($db, '<?php echo $entity ?>', false);
        $db->extendQuery($options);
        return $db->count();
    }

    /**
     * @param int $id
     * @param bool $createIfNotExists = false
     * @return <?php echo $entity ?>

     */
    public static function loadById($id, $createIfNotExist = false)
    {
        if ($id === 0) {
            return $createIfNotExist ? new <?php echo $entity ?>() : null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> x');
        $db->buildQuery('where', ['x.<?php echo $primaryKey ?> = ?', $id]);
        ContentRepository::autoJoin($db, '<?php echo $entity ?>');
        $model = self::map($db);
        if (!isset($model) && $createIfNotExist) {
            $model = new <?php echo $entity ?>();
        }
        return $model;
    }

    /**
     * @param array $options = []
     * @return <?php echo $entity ?>

     */
    public static function load(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> x');
        ContentRepository::autoJoin($db, '<?php echo $entity ?>');
        $db->buildQuery('limit', 1);
        $db->extendQuery($options);
        return self::map($db);
    }

    /**
     * @param int $contentGroup
     * @param bool $onSite = true
     * @return <?php echo $entity ?>

     */
    public static function loadCurrent($contentGroup, $onSite = true)
    {
        if ($contentGroup === 0) {
            return null;
        }
        $db = new Data();

        $versions = [Version::PENDING, Version::PUBLISHED, Version::PUBLISHING];
        if ($onSite && !Nav::isPreview()) {
            $versions = [Version::PUBLISHED, Version::UPDATING];
            self::condition($db, 'expiration');
        }
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> x');
        $db->buildQuery('where', ['content.content_group = ?', $contentGroup]);
        $db->buildQuery('where', ['content.version IN (?)', $versions]);
        ContentRepository::autoJoin($db, '<?php echo $entity ?>');
        return self::map($db);
    }

<?php echo $loadBy ?>
<?php echo $loadUserMethods ?>

    /**
     * @param array $options = []
     * @return <?php echo $entity ?>[]
     */
    public static function loadAll(array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> x');
        ContentRepository::autoJoin($db, '<?php echo $entity ?>');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param int $contentGroup
     * @return <?php echo $entity ?>[]
     */
    public static function loadTrail($contentGroup)
    {
        if ($contentGroup === 0) {
            return null;
        }
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> x');
        $db->buildQuery('where', ['content.content_group = ?', $contentGroup]);
        $db->buildQuery('where', ['content.version IN (?)', [Version::UPDATING, Version::DELETED, Version::OLD]]);
        $db->buildQuery('order', 'content.modified_date DESC');
        ContentRepository::autoJoin($db, '<?php echo $entity ?>');
        return self::mapList($db);
    }

    /**
     * @param string $version = 'published'
     * @param bool $onSite = true
     * @param array $options = []
     * @return <?php echo $entity ?>[]
     */
    public static function loadByVersion($version = 'published', $onSite = true, array $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> x');
        if (Nav::isPreview()) {
            $version = 'preview';
        }
        if ($onSite && $version != 'preview') {
            self::condition($db, 'expiration');
        }
        self::condition($db, $version);
        ContentRepository::autoJoin($db, '<?php echo $entity ?>');
        $db->extendQuery($options);
        return self::mapList($db);
    }

    /**
     * @param <?php echo $entity ?> $model
     * @return int
     */
    public static function save(Content $model)
    {
        $query = 'INSERT INTO <?php echo $table ?> (<?php echo $insertList ?>) VALUES (<?php echo $insertListParams ?>)';
        $params = [
<?php echo $insertValueList ?> 
        ];
        $db = new Data();
        $appId = $db->execute($query, $params);
        $model->setAppId($appId);
    }

<?php echo $updateStatus ?>

    /**
     * @param int $contentId
     * @return int
     */
    public static function deleteById($contentId)
    {
        if ($contentId == 0) {
            return 0;
        }
        $query = 'DELETE FROM <?php echo $table ?> WHERE <?php echo $primaryKey ?> = ?';
        $db = new Data();
        return $db->execute($query, [$contentId]);
    }

<?php echo $deleteBy ?>

    /**
     * @param array $row
     * @param string $alias = <?php echo strtolower($entity) ?>

     * @return <?php echo $entity ?>

     */
    public static function join(array $row, $alias = '<?php echo strtolower($entity) ?>')
    {
        if (!isset($row[$alias . '<?php echo \Rebond\Services\Converter::toCamelCase($primaryKey) ?>'])) {
            return null;
        }
        return self::mapper($row, $alias);
    }

    /**
     * @param array $row
     * @param string $alias = x
     * @return <?php echo $entity ?>

     */
    protected static function mapper(array $row, $alias = 'x')
    {
        $model = new \Own\Models\<?php echo $package ?>\<?php echo $entity ?>();
<?php echo $mapList ?>
        return parent::mapEntity($model, $row);
    }
}
