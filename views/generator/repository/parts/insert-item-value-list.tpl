<?php if (in_array($type, ['datetime', 'date', 'time', 'createdDate'])) { ?>
                ' . $db->pdo()->quote($model->get<?php echo ucfirst($propertyName) ?>()->format('<?php echo $type ?>')) . ',
<?php } else if ($type == 'modifiedDate') { ?>
                \'' . (new Datetime())->format('<?php echo $type ?>') . '\',
<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
                ' . $db->pdo()->quote($model->get<?php echo ucfirst($propertyName) ?>Id()) . ',
<?php } else if (!in_array($type, ['primaryKey', 'foreignKeyLink', 'singleKeyLink'])) { ?>
                ' . $db->pdo()->quote($model->get<?php echo ucfirst($propertyName) ?>()) . ',
<?php } ?>