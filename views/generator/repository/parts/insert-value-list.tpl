<?php if (in_array($type, ['datetime', 'date', 'time', 'createdDate'])) { ?>
                $model->get<?php echo ucfirst($propertyName) ?>()->format('<?php echo $type ?>'),
<?php } else if ($type == 'modifiedDate') { ?>
                (new DateTime())->format('<?php echo $type ?>'),
<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
                $model->get<?php echo ucfirst($propertyName) ?>Id(),
<?php } else if (in_array($type, ['bool'])) { ?>
                (int)$model->get<?php echo ucfirst($propertyName) ?>(),
<?php } else if (!in_array($type, ['primaryKey', 'foreignKeyLink', 'singleKeyLink'])) { ?>
                $model->get<?php echo ucfirst($propertyName) ?>(),
<?php } ?>