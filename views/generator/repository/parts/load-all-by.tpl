    /**
     * @param int $id
     * @param array $options = []
     * @return <?php echo $entity ?>[]
     */
    public static function loadAllBy<?php echo ucfirst($propertyName) ?>Id($id, $options = [])
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> `<?php echo $sqlEntity ?>`');
        $db->buildQuery('where', ['<?php echo $sqlEntity ?>.<?php echo $property ?>_id = ?', $id]);
        $db->extendQuery($options);
        return self::mapList($db);
    }
