<?php if ($type == 'primaryKey' && isset($relation)) { ?>
        if (isset($row[$alias . '<?php echo ucfirst($propertyName) ?>'])) {
            $model->set<?php echo ucfirst($propertyName) ?>($row[$alias . '<?php echo ucfirst($propertyName) ?>']);
            $model->set<?php echo ucfirst($propertyName) ?>Item(<?php echo $relation ?>::join($row, $alias . '_<?php echo $property ?>'));
        }
<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
        if (isset($row[$alias . '<?php echo ucfirst($propertyName) ?>Id'])) {
            $model->set<?php echo ucfirst($propertyName) ?>Id($row[$alias . '<?php echo ucfirst($propertyName) ?>Id']);
            $model->set<?php echo ucfirst($propertyName) ?>(<?php echo $relation ?>::join($row, $alias . '_<?php echo $property ?>'));
        }
<?php } else if (!in_array($type, ['foreignKeyLink', 'singleKeyLink'])) { ?>
        if (isset($row[$alias . '<?php echo ucfirst($propertyName) ?>'])) {
            $model->set<?php echo ucfirst($propertyName) ?>($row[$alias . '<?php echo ucfirst($propertyName) ?>']);
        }
<?php } ?>