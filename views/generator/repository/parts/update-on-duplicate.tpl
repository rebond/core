<?php if (in_array($type, ['datetime', 'date', 'time'])) { ?>
        $query .= '`<?php echo $property ?>` = ?, ';
        $params[] = $model->get<?php echo ucfirst($propertyName) ?>()->format('<?php echo $type ?>');
<?php } else if (in_array($type, ['foreignKey', 'multipleKey', 'singleKey', 'media'])) { ?>
        $query .= '<?php echo $property ?>_id = ?, ';
        $params[] = $model->get<?php echo ucfirst($propertyName) ?>Id();
<?php } else if (in_array($type, ['modifiedDate'])) { ?>
        $query .= '`<?php echo $property ?>` = ?, ';
        $params[] = (new DateTime())->format('<?php echo $type ?>');
<?php } else if (!in_array($type, ['primaryKey', 'multipleKey', 'createdDate', 'foreignKeyLink', 'singleKeyLink'])) { ?>
        $query .= '`<?php echo $property ?>` = ?, ';
        $params[] = $model->get<?php echo ucfirst($propertyName) ?>();
<?php } ?>
