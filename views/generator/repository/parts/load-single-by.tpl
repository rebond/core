    /**
     * @param int $id
     * @return <?php echo $entity ?>

     */
    public static function loadBy<?php echo ucfirst($propertyName) ?>Id($id)
    {
        $db = new Data();
        $db->buildQuery('select', self::getList());
        $db->buildQuery('from', '<?php echo $table ?> `<?php echo $sqlEntity ?>`');
        $db->buildQuery('where', ['<?php echo $sqlEntity ?>.<?php echo $property ?>_id = ?', $id]);
        return self::map($db);
    }
