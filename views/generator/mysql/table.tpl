CREATE TABLE IF NOT EXISTS `<?php echo $table ?>` (
<?php echo $fieldList ?>
<?php if (isset($primaryKey)) { ?>
    , PRIMARY KEY (<?php echo $primaryKey ?>)
 <?php } else if (!empty($multipleKey)) { ?>
    , PRIMARY KEY (<?php echo implode(', ', $multipleKey) ?>)
<?php } ?>
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
