ALTER TABLE `<?php echo $table ?>` CHANGE COLUMN `<?php echo $oldProperty ?>` `<?php echo $property ?>`
<?php if (in_array($type, ['primaryKey'])) { ?>
    <?php echo $sqlType ?> NOT NULL <?php echo $autoIncrement ?>;
<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
    <?php echo $sqlType ?> NOT NULL;
<?php } else if (in_array($type, ['integer'])) { ?>
    <?php echo $sqlType ?> NOT NULL;
<?php } else if (in_array($type, ['numeric'])) { ?>
    FLOAT NOT NULL;
<?php } else if (in_array($type, ['enum', 'status', 'bool'])) { ?>
    TINYINT UNSIGNED NOT NULL;
<?php } else if (in_array($type, ['datetime', 'createdDate', 'modifiedDate'])) { ?>
    DATETIME NOT NULL;
<?php } else if (in_array($type, ['date'])) { ?>
    DATE NOT NULL;
<?php } else if (in_array($type, ['time'])) { ?>
    TIME NOT NULL;
<?php } else if (in_array($type, ['richText'])) { ?>
    TEXT COLLATE utf8_unicode_ci NOT NULL;
<?php } else { ?>
    VARCHAR(<?php echo $maxLength ?>) COLLATE utf8_unicode_ci NOT NULL;
<?php } ?>