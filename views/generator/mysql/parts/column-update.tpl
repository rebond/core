<?php if (in_array($type, ['primaryKey'])) { ?>
    ALTER TABLE `<?php echo $table ?>` MODIFY COLUMN `<?php echo $property ?>` <?php echo $sqlType ?> NOT NULL <?php echo $autoIncrement ?>;
<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
    ALTER TABLE `<?php echo $table ?>` MODIFY COLUMN `<?php echo $property ?>` <?php echo $sqlType ?> NOT NULL;
<?php } else if (in_array($type, ['integer'])) { ?>
    ALTER TABLE `<?php echo $table ?>` MODIFY COLUMN `<?php echo $property ?>` <?php echo $sqlType ?> NOT NULL<?php echo $default ?>;
<?php } else if (in_array($type, ['numeric'])) { ?>
    ALTER TABLE `<?php echo $table ?>` MODIFY COLUMN `<?php echo $property ?>` FLOAT NOT NULL<?php echo $default ?>;
<?php } else if (in_array($type, ['enum', 'status', 'bool'])) { ?>
    ALTER TABLE `<?php echo $table ?>` MODIFY COLUMN `<?php echo $property ?>` TINYINT UNSIGNED NOT NULL<?php echo $default ?>;
<?php } else if (in_array($type, ['datetime', 'createdDate', 'modifiedDate'])) { ?>
    ALTER TABLE `<?php echo $table ?>` MODIFY COLUMN `<?php echo $property ?>` DATETIME NOT NULL;
<?php } else if (in_array($type, ['date'])) { ?>
    ALTER TABLE `<?php echo $table ?>` MODIFY COLUMN `<?php echo $property ?>` DATE NOT NULL;
<?php } else if (in_array($type, ['time'])) { ?>
    ALTER TABLE `<?php echo $table ?>` MODIFY COLUMN `<?php echo $property ?>` TIME NOT NULL;
<?php } else if (in_array($type, ['richText'])) { ?>
    ALTER TABLE `<?php echo $table ?>` MODIFY COLUMN `<?php echo $property ?>` TEXT COLLATE utf8_unicode_ci NOT NULL<?php echo $default ?>;
<?php } else { ?>
    ALTER TABLE `<?php echo $table ?>` MODIFY COLUMN `<?php echo $property ?>` VARCHAR(<?php echo $maxLength ?>) COLLATE utf8_unicode_ci NOT NULL<?php echo $default ?>;
<?php } ?>