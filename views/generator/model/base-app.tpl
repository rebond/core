<#php
<?php echo $license ?>

namespace Generated\Models\App;

use Own\Repository\App\<?php echo $entity ?>Repository;
use Rebond\Models\Cms\Content;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class Base<?php echo $entity ?> extends Content
{
<?php echo $propertyList ?>

    public function __construct()
    {
        parent::__construct();
<?php echo $propertySetList ?>
    }

    protected function setAppDefaultBase()
    {
<?php echo $propertySetDefault ?>
    }

    public function loadModule()
    {
        if (!isset($module)) {
            $this->module = \Rebond\Repository\Cms\ModuleRepository::loadByName('<?php echo $entity ?>');
            $this->moduleId = $this->module->getId();
        }
    }

<?php echo $propertyMethod ?>
    /*
     * return @array
     */
    public function toArray()
    {
        $list = [
<?php echo $toArray ?>
        ];
        return array_merge($list, parent::toArray());
    }

    /** @return int */
    public function save()
    {
        <?php echo $entity ?>Repository::save($this);
        return $this->saveEntity();
    }

    /** @return int */
    public function delete()
    {
        parent::delete();
        return <?php echo $entity ?>Repository::deleteById($this->getAppId());
    }
}
