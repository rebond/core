<#php
<?php echo $license ?>

namespace <?php echo $namespace ?>\Models\<?php echo $package ?>;

class <?php echo $entity ?> extends <?php echo $extends ?>Base<?php echo $entity ?>

{
    /*
     * @param bool $setDefault = true
     */
    public function __construct($setDefault = true)
    {
        parent::__construct();
        if ($setDefault) {
            $this->setDefault();
        }
    }

    public function setDefault()
    {
        $this->setDefaultBase();
        // set your defaults values here
    }
}
