<#php
<?php echo $license ?>

namespace <?php echo $namespace ?>\Models\<?php echo $package ?>;

use Rebond\Models\AbstractModel;
use Rebond\Models\DateTime;
use Rebond\Services\Format;
use Rebond\Services\Lang;

class Base<?php echo $entity ?> extends AbstractModel
{
<?php echo $propertyList ?>

    public function __construct()
    {
<?php echo $propertySetList ?>
    }

    protected function setDefaultBase()
    {
<?php echo $propertySetDefault ?>
    }

<?php if (count($multipleKeyVar) > 0) { ?>
    public function getId()
    {
        return <?php echo implode(" . '_' . ", $multipleKeyVar) ?>;
    }
<?php } ?>
<?php echo $propertyMethod ?>

    /*
     * @return array
     */
    public function toArray()
    {
        return [
<?php echo $toArray ?>
        ];
    }

    /*
     * @return string
     */
    public function __toString()
    {
        return (string) <?php echo $toString ?>;
    }

<?php if ($isPersistent) { ?>
    /**
     * Save a <?php echo $entity ?>

     * @return int
     */
    public function save()
    {
        $ns = $this->ns('\<?php echo $mainNamespace ?>\Repository\<?php echo $package ?>\<?php echo $entity ?>Repository');
        return $ns::save($this);
    }

<?php if (isset($primaryKey) || count($multipleKeyVar) > 0) { ?>
    /**
     * Delete a <?php echo $entity ?>

     * @return int
     */
    public function delete()
    {
<?php if (isset($primaryKey)) { ?>
        $ns = $this->ns('\<?php echo $mainNamespace ?>\Repository\<?php echo $package ?>\<?php echo $entity ?>Repository');
        return $ns::deleteById($this-><?php echo $primaryKey ?>);
<?php } else if (count($multipleKeyVar) > 0) { ?>
        $ns = $this->ns('\<?php echo $mainNamespace ?>\Repository\<?php echo $package ?>\<?php echo $entity ?>Repository');
        return $ns::deleteByIds(<?php echo implode(', ', $multipleKeyVar) ?>);
<?php } ?>
    }
<?php } ?>
<?php } else { ?>

    public function save()
    {
        return true;
    }

    public function delete()
    {
        return true;
    }
<?php } ?>
}
