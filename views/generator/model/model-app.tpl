<#php
<?php echo $license ?>

namespace Own\Models\App;

class <?php echo $entity ?> extends \Generated\Models\App\Base<?php echo $entity ?>

{
    public function __construct()
    {
        parent::__construct();
        $this->setAppDefault();
    }

    public function setAppDefault()
    {
        $this->setAppDefaultBase();
        // set your defaults values here
    }
}
