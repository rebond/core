    /* @var <?php echo $phpType ?> */
    protected $<?php echo $propertyName ?>;
<?php if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
    /* @var int */
    protected $<?php echo $propertyName ?>Id;
<?php } else if ($type == 'primaryKey' && isset($relationModel)) { ?>
    protected $<?php echo $propertyName ?>Item;
<?php } else if (in_array($type, ['enum', 'status', 'version']) && !isset($enumClass)) { ?>
    /* @var array */
    protected $<?php echo $propertyName ?>List;
<?php } ?>
