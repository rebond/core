<?php if (in_array($type, ['text', 'richText'])) { ?>
    /*
     * @param int $length = 0
     * @return string
     */
    public function get<?php echo ucfirst($propertyName) ?>($length = 0)
    {
        return Format::toText($this-><?php echo $propertyName ?>, $length);
    }

<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
    /*
     * @return int
     */
    public function get<?php echo ucfirst($propertyName) ?>Id()
    {
        return $this-><?php echo $propertyName ?>Id;
    }

<?php } else if (!in_array($type, ['foreignKeyLink', 'singleKeyLink'])) { ?>
    /*
     * @return <?php echo $phpType ?>

     */
    public function get<?php echo ucfirst($propertyName) ?>()
    {
        return $this-><?php echo $propertyName ?>;
    }

<?php } ?>
<?php if ($type == 'primaryKey' && isset($relationRepository)) { ?>
    /*
     * @param bool $createIfNoExist = true
     * @return <?php echo $relationModel ?>

     */
    public function get<?php echo ucfirst($propertyName) ?>Item($createIfNotExist = false)
    {
        if (!isset($this-><?php echo $propertyName ?>Item)) {
            $ns = $this->ns('<?php echo $relationRepository ?>');
            $this-><?php echo $propertyName ?>Item = $ns::loadById($this-><?php echo $propertyName ?>, $createIfNotExist);
        }
        return $this-><?php echo $propertyName ?>Item;
    }

<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey'])) { ?>
    /*
     * @param bool $createIfNoExist = true
     * @return <?php echo $relationModel ?>

     */
    public function get<?php echo ucfirst($propertyName) ?>($createIfNotExist = false)
    {
        if (!isset($this-><?php echo $propertyName ?>)) {
            $ns = $this->ns('<?php echo $relationRepository ?>');
            $this-><?php echo $propertyName ?> = $ns::loadById($this-><?php echo $propertyName ?>Id, $createIfNotExist);
        }
        return $this-><?php echo $propertyName ?>;
    }

<?php if ($type == 'multipleKey') { ?>
    /*
     * @return array
     */
    public function get<?php echo ucfirst($propertyName) ?>List()
    {
        if (!isset($this-><?php echo $propertyName ?>List)) {
            $ns = $this->ns('\Rebond\Repository\<?php echo $package ?>\<?php echo $entity ?>Repository');
            $this-><?php echo $propertyName ?>List = $ns::loadAllBy<?php echo ucfirst($otherMultipleKey) ?>Id($this-><?php echo $otherMultipleKey ?>Id);
        }
        return $this-><?php echo $propertyName ?>List;
    }

<?php } ?>
<?php } else if (in_array($type, ['media'])) { ?>
    /*
     * @param bool $createIfNoExist = true
     * @return <?php echo $relationModel ?>

     */
    public function get<?php echo ucfirst($propertyName) ?>($createIfNotExist = true)
    {
        if (!isset($this-><?php echo $propertyName ?>)) {
            $ns = $this->ns('<?php echo $relationRepository ?>');
            $this-><?php echo $propertyName ?> = $ns::loadById($this-><?php echo $propertyName ?>Id, $createIfNotExist);
        }
        return $this-><?php echo $propertyName ?>;
    }

<?php } else if (in_array($type, ['foreignKeyLink'])) { ?>
    /*
     * @return <?php echo $relationModel ?>

     */
    public function get<?php echo ucfirst($propertyName) ?>()
    {
        if (!isset($this-><?php echo $propertyName ?>)) {
            $ns = $this->ns('<?php echo $relationRepository ?>');
            $this-><?php echo $propertyName ?> = $ns::loadAllBy<?php echo ucfirst($link) ?>($this->getId());
        }
        return $this-><?php echo $propertyName ?>;
    }

<?php } else if (in_array($type, ['singleKeyLink'])) { ?>
    /*
     * @return <?php echo $relationModel ?>

     */
    public function get<?php echo ucfirst($propertyName) ?>()
    {
        if (!isset($this-><?php echo $propertyName ?>)) {
            $ns = $this->ns('<?php echo $relationRepository ?>');
            $this-><?php echo $propertyName ?> = $ns::loadBy<?php echo ucfirst($link) ?>($this->getId());
        }
        return $this-><?php echo $propertyName ?>;
    }

<?php } else if (in_array($type, ['enum', 'status', 'version'])) { ?>
<?php if (isset($enumClass)) { ?>
    /*
     * @return int
     */
    public function get<?php echo ucfirst($propertyName) ?>Value()
    {
        return <?php echo $enumClass ?>::lang($this-><?php echo $propertyName ?>);
    }

    /*
     * @return array
     */
    public function get<?php echo ucfirst($propertyName) ?>List()
    {
        return <?php echo $enumClass ?>::toArrayLang();
    }

<?php } else { ?>
    /*
     * @return int
     */
    public function get<?php echo ucfirst($propertyName) ?>Value()
    {
        if (isset($this-><?php echo $propertyName ?>List[$this-><?php echo $propertyName ?>])) {
            return $this-><?php echo $propertyName ?>List[$this-><?php echo $propertyName ?>];
        }

        return Lang::lang('undefined');
    }

    /*
     * @return array
     */
    public function get<?php echo ucfirst($propertyName) ?>List()
    {
        return $this-><?php echo $propertyName ?>List;
    }

<?php } ?>
<?php } ?>
