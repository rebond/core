<?php if (in_array($type, ['datetime', 'date', 'time', 'createdDate', 'modifiedDate'])) { ?>
    public function set<?php echo ucfirst($propertyName) ?>($value)
    {
        $this-><?php echo $propertyName ?> = ($value instanceof DateTime)
            ? $value
            : new DateTime($value);
    }

<?php } else if ($type == 'primaryKey' && isset($relationModel)) { ?>
    public function set<?php echo ucfirst($propertyName) ?>($value)
    {
        if ($this-><?php echo $propertyName ?> != $value) {
            $this-><?php echo $propertyName ?> = (int) $value;
            $this-><?php echo $propertyName ?>Item = null;
        }
    }

    /*
     * @param <?php echo $relationModel ?> $model = null
     */
    public function set<?php echo ucfirst($propertyName) ?>Item(<?php echo $relationModel ?> $model = null)
    {
        if (!isset($model)) {
            $this-><?php echo $propertyName ?>Item = null;
            return;
        }
        $this-><?php echo $propertyName ?> = (int) $model->getId();
        $this-><?php echo $propertyName ?>Item = $model;
    }

<?php } else if ($type == 'primaryKey') { ?>
    /*
     * @param int $value
     */
    public function set<?php echo ucfirst($propertyName) ?>($value)
    {
        $this-><?php echo $propertyName ?> = (int) $value;
    }

<?php } else if (in_array($type, ['integer'])) { ?>
    /*
     * @param int $value
     */
    public function set<?php echo ucfirst($propertyName) ?>($value)
    {
        $this-><?php echo $propertyName ?> = (int) $value;
    }

    /*
     * @param int $value
     */
    public function add<?php echo ucfirst($propertyName) ?>($value)
    {
        $this-><?php echo $propertyName ?> += (int) $value;
    }

<?php } else if (in_array($type, ['enum', 'status', 'version', 'bool'])) { ?>
    /*
     * @param int $value
     */
    public function set<?php echo ucfirst($propertyName) ?>($value)
    {
        $this-><?php echo $propertyName ?> = (int) $value;
    }

<?php } else if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
    /*
     * @param int $id
     */
    public function set<?php echo ucfirst($propertyName) ?>Id($id)
    {
        if ($this-><?php echo $propertyName ?>Id !== $id) {
            $this-><?php echo $propertyName ?>Id = (int) $id;
            $this-><?php echo $propertyName ?> = null;
        }
    }

    /*
     * @param <?php echo $relationModel ?> $model = null
     */
    public function set<?php echo ucfirst($propertyName) ?>(<?php echo $relationModel ?> $model = null)
    {
        if (!isset($model)) {
            $this-><?php echo $propertyName ?> = null;
            return;
        }
        $this-><?php echo $propertyName ?>Id = (int) $model->getId();
        $this-><?php echo $propertyName ?> = $model;
    }

<?php } else { ?>
    /*
     * @param <?php echo $phpType ?> $value
     */
    public function set<?php echo ucfirst($propertyName) ?>($value)
    {
        $this-><?php echo $propertyName ?> = $value;
    }

<?php } ?>
