<?php if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'singleKeyLink', 'foreignKeyLink', 'media'])) { ?>
        $this-><?php echo $propertyName ?> = null;
<?php } ?>
<?php if (in_array($type, ['foreignKey', 'singleKey', 'multipleKey', 'media'])) { ?>
        $this-><?php echo $propertyName ?>Id = <?php echo $default ?>;
<?php } else if (in_array($type, ['primaryKey', 'integer', 'numeric', 'version', 'status', 'bool', 'enum', 'datetime', 'date', 'time', 'createdDate', 'modifiedDate'])) { ?>
        $this-><?php echo $propertyName ?> = <?php echo $default ?>;
<?php } else if (!in_array($type, ['singleKeyLink', 'foreignKeyLink'])) { ?>
        $this-><?php echo $propertyName ?> = '<?php echo $default ?>';
<?php } ?>
