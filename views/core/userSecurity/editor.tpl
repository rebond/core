<?php echo $this->renderTitle('user_security', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
        <div class="rb-form-item">
        <label>
            <?php echo $this->lang('user') ?>
            <?php echo $item->req('user') ?>
            <?php echo $item->buildUser() ?>
        </label>
        <?php echo $item->getFieldError('user') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('secure') ?>
            <?php echo $item->req('secure') ?>
            <?php echo $item->buildSecure() ?>
        </label>
        <?php echo $item->getFieldError('secure') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('type') ?>
            <?php echo $item->req('type') ?>
            <?php echo $item->buildType() ?>
        </label>
        <?php echo $item->getFieldError('type') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('expiry_date') ?>
            <?php echo $item->req('expiryDate') ?>
        </label>
        <?php echo $item->buildExpiryDate(false) ?>
        <?php echo $item->getFieldError('expiryDate') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="#" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
