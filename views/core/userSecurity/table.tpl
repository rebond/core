<table class="list">
    <tr>
        <th><?php echo $this->lang('user') ?></th>
        <th><?php echo $this->lang('secure') ?></th>
        <th><?php echo $this->lang('type') ?></th>
        <th><?php echo $this->lang('expiry_date') ?></th>
        <th><?php echo $this->lang('created_date') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td><?php echo $item->getUser() ?></td>
        <td><?php echo $item->getSecure() ?></td>
        <td><span class="enum type-<?php echo $item->getType() ?>"><?php echo $item->getTypeValue() ?></span></td>
        <td><?php echo $item->getExpiryDate()->format('date') ?></td>
        <td><?php echo $item->getCreatedDate()->format('createdDate') ?></td>
        <td>
            <a href="#" class="status-3" data-package="Core" data-entity="UserSecurity"
               data-id="<?php echo $item->getId() ?>" data-status="3"><?php echo $this->lang('delete') ?></a>
        </td>
    </tr>
    <?php } ?>
</table>

<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <button class="rb-btn" name="btnNewKey"><?php echo $this->lang('api_key_generate') ?></button>
    <button class="rb-btn" name="btnExtendKey"><?php echo $this->lang('api_key_extend') ?></button>
    <p>
    <a href="/user" class="rb-btn rb-btn-blank"><?php echo $this->lang('back_to_list') ?></a>
    </p>
</form>