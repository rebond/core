<div class="rb-filter">
    <div class="type">
        <div><?php echo $this->lang('type') ?></div>
        <select class="input rb-select" id="type" data-hash="2">
            <option value="#<?php echo $folderId ?>/1/all/<?php echo $order . '/' . $selectable ?>"><?php echo $this->lang('all') ?></option>
            <option value="#<?php echo $folderId ?>/1/images/<?php echo $order . '/' . $selectable ?>"><?php echo $this->lang('images') ?></option>
            <option value="#<?php echo $folderId ?>/1/videos/<?php echo $order . '/' . $selectable ?>"><?php echo $this->lang('videos') ?></option>
            <option value="#<?php echo $folderId ?>/1/documents/<?php echo $order . '/' . $selectable ?>"><?php echo $this->lang('documents') ?></option>
        </select>
    </div>

    <div class="order">
        <div><?php echo $this->lang('order') ?></div>
        <select class="input rb-select" id="order" data-hash="3">
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/title-asc/<?php echo $selectable ?>"><?php echo $this->lang('title') ?> (a->z)</option>
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/title-desc/<?php echo $selectable ?>"><?php echo $this->lang('title') ?> (z->a)</option>
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/modified-asc/<?php echo $selectable ?>"><?php echo $this->lang('modified_date') ?> (old->new)</option>
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/modified-desc/<?php echo $selectable ?>"><?php echo $this->lang('modified_date') ?> (new->old)</option>
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/extension-asc/<?php echo $selectable ?>"><?php echo $this->lang('extension') ?> (a->z)</option>
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/extension-asc/<?php echo $selectable ?>"><?php echo $this->lang('extension') ?> (z->a)</option>
        </select>
    </div>

    <div class="selectable">
        <div><?php echo $this->lang('is_selectable') ?></div>
        <select class="input rb-select" id="selectable" data-hash="4">
            <option value="#<?php echo $folderId ?>/1/<?php echo $type . '/' . $order ?>/1"><?php echo $this->lang('yes') ?></option>
            <option value="#<?php echo $folderId ?>/1/<?php echo $type . '/' . $order ?>/0"><?php echo $this->lang('no') ?></option>
        </select>
    </div>

    <div class="view">
        <div><?php echo $this->lang('view') ?></div>
        <select class="input rb-activator" id="view" data-active="<?php echo $mediaView ?>">
            <option value="0" data-menu="grid"><?php echo $this->lang('grid') ?></option>
            <option value="1" data-menu="list"><?php echo $this->lang('list') ?></option>
        </select>
    </div>

    <?php echo $itemsPerPage ?>
    <?php echo $paging ?>
</div>