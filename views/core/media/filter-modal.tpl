<div class="rb-filter">
    <div>
        <a href="/media/upload" target="_blank" class="rb-btn"><?php echo $this->lang('new') . ' ' . $this->lang('media') ?></a>
    </div>
    <div class="search">
        <div><?php echo $this->lang('search') ?></div>
        <input type="text" class="input" name="search" id="search-media" value="<?php echo $search ?>" />
    </div>

    <div class="folder">
        <div><?php echo $this->lang('folder') ?></div>
        <select class="input rb-select" id="folderId" data-hash="0">
            <option value="#0/1/<?php echo $type . '/' . $order ?>"><?php echo $this->lang('all') ?></option>
            <?php foreach ($folders as $key => $value) { ?>
                <option value="#<?php echo $key ?>/1/<?php echo $type . '/' . $order ?>"><?php echo $value ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="type">
        <div><?php echo $this->lang('type') ?></div>
        <select class="input rb-select" id="type" data-hash="2">
            <option value="#<?php echo $folderId ?>/1/all/<?php echo $order ?>"><?php echo $this->lang('all') ?></option>
            <option value="#<?php echo $folderId ?>/1/images/<?php echo $order ?>"><?php echo $this->lang('images') ?></option>
            <option value="#<?php echo $folderId ?>/1/videos/<?php echo$order ?>"><?php echo $this->lang('videos') ?></option>
            <option value="#<?php echo $folderId ?>/1/documents/<?php echo$order ?>"><?php echo $this->lang('documents') ?></option>
        </select>
    </div>

    <div class="order">
        <div><?php echo $this->lang('order') ?></div>
        <select class="input rb-select" id="order" data-hash="3">
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/title-asc"><?php echo $this->lang('title') ?> (a->z)</option>
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/title-desc"><?php echo $this->lang('title') ?> (z->a)</option>
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/modified_date-asc"><?php echo $this->lang('modified_date') ?> (old->new)</option>
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/modified_date-desc"><?php echo $this->lang('modified_date') ?> (new->old)</option>
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/extension-asc"><?php echo $this->lang('extension') ?> (a->z)</option>
            <option value="#<?php echo $folderId ?>/1/<?php echo $type ?>/extension-desc"><?php echo $this->lang('extension') ?> (z->a)</option>
        </select>
    </div>

    <div class="view">
        <div><?php echo $this->lang('view') ?></div>
        <select class="input rb-activator" id="view" data-active="<?php echo $mediaView ?>">
            <option value="0" data-menu="grid"><?php echo $this->lang('grid') ?></option>
            <option value="1" data-menu="list"><?php echo $this->lang('list') ?></option>
        </select>
    </div>

    <?php echo $itemsPerPage ?>
    <?php echo $paging ?>

    <input type="hidden" id="hash" value="<?php echo $hash ?>" />
</div>