<h2>Media</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <td><?php echo $item->getTitle() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('folder') ?></th>
        <td><?php echo $item->getFolder() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('tags') ?></th>
        <td><?php echo $item->getTags() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('upload') ?></th>
        <td><?php echo $item->getUpload() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('original_filename') ?></th>
        <td><?php echo $item->getOriginalFilename() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('path') ?></th>
        <td><?php echo $item->getPath() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('extension') ?></th>
        <td><?php echo $item->getExtension() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('mime_type') ?></th>
        <td><?php echo $item->getMimeType() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('file_size') ?></th>
        <td><?php echo $item->getFileSize() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('width') ?></th>
        <td><?php echo $item->getWidth() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('height') ?></th>
        <td><?php echo $item->getHeight() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('alt') ?></th>
        <td><?php echo $item->getAlt() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('is_selectable') ?></th>
        <td><span class="bool-<?php echo $item->getIsSelectable() ?>"><?php echo $item->getIsSelectable() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<a href="#"><?php echo $this->lang('back_to_list') ?></a>