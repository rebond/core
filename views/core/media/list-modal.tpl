<input type="hidden" class="selected" id="media-selected" value="<?php echo $field ?>" />
<table class="list">
    <tr>
        <th>&nbsp;</th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('extension') ?></th>
        <th><?php echo $this->lang('file_size') ?></th>
        <th><?php echo $this->lang('uploaded_date') ?></th>
    </tr>
    <?php foreach ($items as $item) { ?>
        <tr class="media" data-id="<?php echo $item->getId() ?>" data-title="<?php echo $item->getTitle() ?>">
            <td class="no-padding">
                <img src="<?php echo $this->showFromModel($item) ?>" alt="<?php echo $item->getAlt() ?>" />
            </td>
            <td>
                <span class="title"><?php echo $item->getTitle() ?></span>
            </td>
            <td><?php echo $item->getExtension() ?></td>
            <td><?php echo round($item->getFilesize() / 1000) ?> kb</td>
            <td><?php echo $item->getCreatedDate()->format() ?></td>
        </tr>
    <?php } ?>
</table>