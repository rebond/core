<table class="list">
    <tr>
        <th><?php echo $this->lang('folder') ?></th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('tags') ?></th>
        <th><?php echo $this->lang('upload') ?></th>
        <th><?php echo $this->lang('original_filename') ?></th>
        <th><?php echo $this->lang('path') ?></th>
        <th><?php echo $this->lang('extension') ?></th>
        <th><?php echo $this->lang('mime_type') ?></th>
        <th><?php echo $this->lang('file_size') ?></th>
        <th><?php echo $this->lang('width') ?></th>
        <th><?php echo $this->lang('height') ?></th>
        <th><?php echo $this->lang('alt') ?></th>
        <th><?php echo $this->lang('is_selectable') ?></th>
        <th><?php echo $this->lang('status') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td><?php echo $item->getFolder() ?></td>
        <td><a href="#&id=<?php echo $item->getId() ?>"><?php echo $item->getTitle() ?></a></td>
        <td><?php echo $item->getTags() ?></td>
        <td><?php echo $item->getUpload() ?></td>
        <td><?php echo $item->getOriginalFilename() ?></td>
        <td><?php echo $item->getPath() ?></td>
        <td><?php echo $item->getExtension() ?></td>
        <td><?php echo $item->getMimeType() ?></td>
        <td><?php echo $item->getFileSize() ?></td>
        <td><?php echo $item->getWidth() ?></td>
        <td><?php echo $item->getHeight() ?></td>
        <td><?php echo $item->getAlt() ?></td>
        <td><span class="bool-<?php echo $item->getIsSelectable() ?>"><?php echo $item->getIsSelectable() ?></span></td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-<?php echo $item->getStatus() ?>" data-package="Core" data-entity="Media" data-id="<?php echo $item->getId() ?>" data-status="<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></a>
            <?php } else { ?>
                <span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span>
            <?php } ?>
        </td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-3" data-package="Core" data-entity="Media" data-id="<?php echo $item->getId() ?>" data-status="3"><?php echo $this->lang('delete') ?></a>
            <?php } else { ?>
                <a href="#" class="status-2" data-package="Core" data-entity="Media" data-id="<?php echo $item->getId() ?>" data-status="2"><?php echo $this->lang('undelete') ?></a>
            <?php } ?>
        </td>
    </tr>
    <?php } ?>
</table>