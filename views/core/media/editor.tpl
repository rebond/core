<div id="editor">
    <?php echo $this->renderTitle('media', $item->getModel()->getId(), $item->getModel()) ?>
    <form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
        <?php echo $item->buildId() ?>
        <?php echo $item->buildToken() ?>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('folder') ?>
                <?php echo $item->req('folder') ?>
                <?php echo $item->buildFolder() ?>
            </label>
            <?php echo $item->getFieldError('folder') ?>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('title') ?>
                <?php echo $item->req('title') ?>
                <?php echo $item->buildTitle() ?>
            </label>
            <?php echo $item->getFieldError('title') ?>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('preview') ?>
            </label>
            <img src="<?php echo $this->showFromModel($item->getModel(), 128, 128) ?>" alt="<?php echo $item->getModel()->getAlt() ?>" />
            <span class="input"><a href="<?php echo $url ?>" target="_blank"><?php echo $url ?></a></span>
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('tags') ?>
                <?php echo $item->req('tags') ?>
                <?php echo $item->buildTags() ?>
            </label>
            <?php echo $item->getFieldError('tags') ?>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('upload') ?>
                <?php echo $item->req('upload') ?>
                <?php echo \Rebond\Services\Form::buildField('upload', 'string', \Rebond\Services\File::getNoExtension($item->getModel()->getUpload())) ?>
            </label>
            <?php echo $item->getFieldError('upload') ?>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('alt') ?>
                <?php echo $item->req('alt') ?>
                <?php echo $item->buildAlt() ?>
            </label>
            <?php echo $item->getFieldError('alt') ?>
        </div>
        <div class="rb-form-item">
            <?php echo $item->buildIsSelectable() ?>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('original_filename') ?>
                <span class="input"><?php echo $item->getModel()->getOriginalFilename() ?></span>
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('path') ?>
                <span class="input"><?php echo $item->getModel()->getPath() ?></span>
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('extension') ?>
                <span class="input"><?php echo $item->getModel()->getExtension() ?></span>
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('mime_type') ?>
                <span class="input"><?php echo $item->getModel()->getMimetype() ?></span>
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('file_size') ?>
                <span class="input"><?php echo round($item->getModel()->getFilesize() / 1000.0) ?> kb</span>
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('width') ?>
                <span class="input"><?php echo $item->getModel()->getWidth() ?> px</span>
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('height') ?>
                <span class="input"><?php echo $item->getModel()->getHeight() ?> px</span>
            </label>
        </div>
        <div class="rb-form-item">
            <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
            <a href="#" class="rb-btn rb-btn-blank go-back"><?php echo $this->lang('cancel') ?></a>
            <a href="#" id="button-replace" class="rb-btn rb-btn-warning"><?php echo $this->lang('replace') ?></a>
            <?php echo $item->getFieldError('token') ?>
        </div>
    </form>
</div>

<div id="replace">
    <h2><?php echo $this->lang('medium_replace') ?></h2>

    <div class="editor">
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('title') ?>
                <span class="input"><?php echo $item->getModel()->getTitle() ?></span>
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('preview') ?>
            </label>
            <img src="<?php echo $this->showFromModel($item->getModel(), 128, 128) ?>" alt="<?php echo $item->getModel()->getAlt() ?>" />
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('original_filename') ?>
                <span class="input"><?php echo $item->getModel()->getOriginalFilename() ?></span>
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('file_size') ?>
                <span class="input"><?php echo round($item->getModel()->getFilesize() / 1000.0) ?> kb</span>
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('width') ?>
                <span class="input"><?php echo $item->getModel()->getWidth() ?> px</span>
            </label>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('height') ?>
                <span class="input"><?php echo $item->getModel()->getHeight() ?> px</span>
            </label>
        </div>
    </div>

    <form class="dropzone" id="media-uploader"></form>

    <div class="rb-form-item">
        <a href="#" class="rb-btn" id="media-upload"><?php echo $this->lang('medium_replace') ?></a>
        <a href="#" class="rb-btn rb-btn-blank" id="cancel-replace"><?php echo $this->lang('cancel') ?></a>
    </div>

    <p class="text-info"><?php echo $this->lang('size_limit', [$uploadMax]) ?></p>
    <input type="hidden" id="replace-id" value="<?php echo $item->getModel()->getId() ?>" />
    <input type="hidden" id="upload-max" value="<?php echo $uploadMax ?>" />
</div>
