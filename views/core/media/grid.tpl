<div id="media-grid">
<?php foreach ($items as $item) { ?>
    <div class="media" id="media-<?php echo $item->getId() ?>">
        <div class="action">
            <a class="title" href="/media/edit/?id=<?php echo $item->getId() ?>" title="<?php echo $item->getTitle() ?>"><?php echo $item->getTitle() ?></a>
            <a class="option" href="/media/in-use?id=<?php echo $item->getId() ?>" title="<?php echo $this->lang('media_in_use') ?>"><i class="fa fa-2-lg fa-link"></i></a>
            <?php if (\Rebond\Services\File::isImage($item->getMimeType())) { ?>
                <a class="option" href="/media/crop/?id=<?php echo $item->getId() ?>" title="<?php echo $this->lang('crop') ?>"><i class="fa fa-lg fa-crop"></i></a>
            <?php } ?>
            <a class="option media-delete" href="#" data-id="<?php echo $item->getId() ?>" title="<?php echo $this->lang('delete') ?>"><i class="fa fa-lg fa-times"></i></a>
        </div>
        <div class="view">
            <?php if (\Rebond\Services\File::isImage($item->getMimeType())) { ?>
                <img class="thumb" data-id="<?php echo $item->getId() ?>" src="<?php echo $this->showFromModel($item, 164, 164) ?>" alt="<?php echo $item->getAlt() ?>" />
            <?php } else { ?>
                <img src="<?php echo $this->showFromModel($item, 164, 164) ?>" alt="<?php echo $item->getAlt() ?>" />
            <?php } ?>
        </div>
    </div>
<?php } ?>
</div>