<table class="list">
    <tr>
        <th>&nbsp;</th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('extension') ?></th>
        <th><?php echo $this->lang('file_size') ?></th>
        <th><?php echo $this->lang('date_uploaded') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
        <tr class="media" id="media-<?php echo $item->getId() ?>">
            <td class="no-padding">
                <?php if (\Rebond\Services\File::isImage($item->getMimeType())) { ?>
                <img class="thumb" data-id="<?php echo $item->getId() ?>" src="<?php echo $this->showFromModel($item) ?>" alt="<?php echo $item->getAlt() ?>" />
            <?php } else { ?>
                <img src="<?php echo $this->showFromModel($item) ?>" alt="<?php echo $item->getAlt() ?>" />
            <?php } ?>
            </td>
            <td>
                <a class="title" href="/media/edit/?id=<?php echo $item->getId() ?>"><?php echo $item->getTitle() ?></a>
            </td>
            <td><?php echo $item->getExtension() ?></td>
            <td><?php echo round($item->getFilesize() / 1000) ?> kb</td>
            <td><?php echo $item->getCreatedDate()->format() ?></td>
            <td>
                <a class="option" href="/media/in-use?id=<?php echo $item->getId() ?>" title="<?php echo $this->lang('media_in_use') ?>"><i class="fa fa-2x fa-link"></i></a>
                <?php if (\Rebond\Services\File::isImage($item->getMimeType())) { ?>
                <a class="option" href="/media/crop/?id=<?php echo $item->getId() ?>" title="<?php echo $this->lang('crop') ?>"><i class="fa fa-2x fa-crop"></i></a>
                <?php } ?>
                <a class="option media-delete" href="#" data-id="<?php echo $item->getId() ?>" title="<?php echo $this->lang('delete') ?>"><i class="fa fa-2x fa-times"></i></a>
            </td>
        </tr>
    <?php } ?>
</table>