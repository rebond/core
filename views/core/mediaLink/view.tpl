<h2>MediaLink</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('package') ?></th>
        <td><?php echo $item->getPackage() ?></td>
    </tr>

    <tr>
        <th><?php echo $this->lang('entity') ?></th>
        <td><?php echo $item->getEntity() ?></td>
    </tr>

    <tr>
        <th><?php echo $this->lang('field') ?></th>
        <td><?php echo $item->getField() ?></td>
    </tr>

    <tr>
        <th><?php echo $this->lang('¨filter') ?></th>
        <td><?php echo $item->getFilter() ?></td>
    </tr>

    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>

</table>
<a href="#"><?php echo $this->lang('back_to_list') ?></a>