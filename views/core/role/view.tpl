<h2>Role</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <td><?php echo $this->lang($item->getTitle()) ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('summary') ?></th>
        <td><?php echo $item->getSummary() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<a href="#"><?php echo $this->lang('back_to_list') ?></a>