<h2>Role Permissions</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('permissions') ?></th>
        <td>
            <?php echo Rebond\Services\View::viewForeignKeyLink($permissions, $allPermissions) ?>
        </td>
    </tr>

</table>
<a href="/user"><?php echo $this->lang('back_to_list') ?></a>
