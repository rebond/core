<table class="list">
    <tr>
        <th></th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('summary') ?></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-<?php echo $item->getStatus() ?>" data-package="Core" data-entity="Role" data-id="<?php echo $item->getId() ?>" data-status="<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></a>
            <?php } else { ?>
                <span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span>
            <?php } ?>
        </td>
        <td><?php echo $item->getTitle() ?></td>
        <td><?php echo $item->getSummary() ?></td>
    </tr>
    <?php } ?>
</table>