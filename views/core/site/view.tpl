<h2>Site</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <td><?php echo $item->getTitle() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('google_analytics') ?></th>
        <td><?php echo $item->getGoogleAnalytics() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('keywords') ?></th>
        <td><?php echo $item->getKeywords() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('description') ?></th>
        <td><?php echo $item->getDescription() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('css') ?></th>
        <td><?php echo $item->getCss() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('js') ?></th>
        <td><?php echo $item->getJs() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('sign_in_url') ?></th>
        <td><?php echo $item->getSignInUrl() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('is_debug') ?></th>
        <td><span class="bool-<?php echo $item->getIsDebug() ?>"><?php echo $item->getIsDebug() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('timezone') ?></th>
        <td><?php echo $item->getTimezone() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('is_cms') ?></th>
        <td><span class="bool-<?php echo $item->getIsCms() ?>"><?php echo $item->getIsCms() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('cache_time') ?></th>
        <td><?php echo $item->getCacheTime() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('use_device_template') ?></th>
        <td><span class="bool-<?php echo $item->getUseDeviceTemplate() ?>"><?php echo $item->getUseDeviceTemplate() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('skin') ?></th>
        <td><?php echo $item->getSkin() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('send_mail_on_error') ?></th>
        <td><span class="bool-<?php echo $item->getSendMailOnError() ?>"><?php echo $item->getSendMailOnError() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('mail_list_on_error') ?></th>
        <td><?php echo $item->getMailListOnError() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<a href="/configuration"><?php echo $this->lang('back_to_list') ?></a>