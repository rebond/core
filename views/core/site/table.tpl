<table class="list">
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('google_analytics') ?></th>
        <th><?php echo $this->lang('keywords') ?></th>
        <th><?php echo $this->lang('sign_in_url') ?></th>
        <th><?php echo $this->lang('environment') ?></th>
        <th><?php echo $this->lang('sqlLog') ?></th>
        <th><?php echo $this->lang('timezone') ?></th>
        <th><?php echo $this->lang('is_cms') ?></th>
        <th><?php echo $this->lang('cache_time') ?></th>
        <th><?php echo $this->lang('use_device_template') ?></th>
        <th><?php echo $this->lang('skin') ?></th>
        <th><?php echo $this->lang('send_mail_on_error') ?></th>
        <th><?php echo $this->lang('mail_list_on_error') ?></th>
        <th><?php echo $this->lang('status') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td><a href="#&id=<?php echo $item->getId() ?>"><?php echo $item->getTitle() ?></a></td>

        <td><?php echo $item->getGoogleAnalytics() ?></td>

        <td><?php echo $item->getKeywords() ?></td>

        <td><?php echo $item->getSignInUrl() ?></td>
        <td><span class="enum environment-<?php echo $item->getEnvironment() ?>"><?php echo $item->getEnvironmentValue() ?></span></td>
        <td><span class="bool-<?php echo $item->getLogSql() ?>"><?php echo $item->getLogSql() ?></span></td>

        <td><?php echo $item->getTimezone() ?></td>
        <td><span class="bool-<?php echo $item->getIsCms() ?>"><?php echo $item->getIsCms() ?></span></td>

        <td><?php echo $item->getCacheTime() ?></td>
        <td><span class="bool-<?php echo $item->getUseDeviceTemplate() ?>"><?php echo $item->getUseDeviceTemplate() ?></span></td>

        <td><?php echo $item->getSkin() ?></td>
        <td><span class="bool-<?php echo $item->getSendMailOnError() ?>"><?php echo $item->getSendMailOnError() ?></span></td>
        <td><?php echo $item->getMailListOnError() ?></td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-<?php echo $item->getStatus() ?>" data-package="Core" data-entity="Site" data-id="<?php echo $item->getId() ?>" data-status="<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></a>
            <?php } else { ?>
                <span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span>
            <?php } ?>
        </td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-3" data-package="Core" data-entity="Site" data-id="<?php echo $item->getId() ?>" data-status="3"><?php echo $this->lang('delete') ?></a>
            <?php } else { ?>
                <a href="#" class="status-2" data-package="Core" data-entity="Site" data-id="<?php echo $item->getId() ?>" data-status="2"><?php echo $this->lang('undelete') ?></a>
            <?php } ?>
        </td>
    </tr>
    <?php } ?>
</table>