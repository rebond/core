<?php echo $this->renderTitle('site', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('title') ?>
            <?php echo $item->req('title') ?>
            <?php echo $item->buildTitle() ?>
        </label>
        <?php echo $item->getFieldError('title') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('timezone') ?>
            <?php echo $item->req('timezone') ?>
            <?php echo $item->buildTimezone() ?>
        </label>
        <?php echo $item->getFieldError('timezone') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('cache_time') ?>
            <?php echo $item->req('cacheTime') ?>
            <?php echo $item->buildCacheTime() ?>
        </label>
        <?php echo $item->getFieldError('cacheTime') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('google_analytics') ?>
            <?php echo $item->req('googleAnalytics') ?>
            <?php echo $item->buildGoogleAnalytics() ?>
        </label>
        <?php echo $item->getFieldError('googleAnalytics') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('keywords') ?>
            <?php echo $item->req('keywords') ?>
            <?php echo $item->buildKeywords() ?>
        </label>
        <?php echo $item->getFieldError('keywords') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('description') ?>
            <?php echo $item->req('description') ?>
            <?php echo $item->buildDescription() ?>
        </label>
        <?php echo $item->getFieldError('description') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('css') ?>
            <?php echo $item->req('css') ?>
            <?php echo $item->buildCss() ?>
        </label>
        <?php echo $item->getFieldError('css') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('js') ?>
            <?php echo $item->req('js') ?>
            <?php echo $item->buildJs() ?>
        </label>
        <?php echo $item->getFieldError('js') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('sign_in_url') ?>
            <?php echo $item->req('signInUrl') ?>
            <?php echo $item->buildSignInUrl() ?>
        </label>
        <?php echo $item->getFieldError('signInUrl') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildIsDebug() ?>
    </div>
     <div class="rb-form-item">
        <?php echo $item->buildLogSql() ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildUseDeviceTemplate() ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildIsCms() ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('skin') ?>
            <?php echo $item->req('skin') ?>
            <?php echo $item->buildSkin() ?>
        </label>
        <?php echo $item->getFieldError('skin') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildSendMailOnError() ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('mail_list_on_error') ?>
            <?php echo $item->req('mailListOnError') ?>
            <?php echo $item->buildMailListOnError() ?>
        </label>
        <?php echo $item->getFieldError('mailListOnError') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('status') ?>
            <?php echo $item->req('status') ?>
            <?php echo $item->buildStatus() ?>
        </label>
        <?php echo $item->getFieldError('status') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="/configuration" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
