<?php echo $this->renderTitle('feedback', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('type') ?>
            <?php echo $item->req('type') ?>
            <?php echo $item->buildType() ?>
        </label>
        <?php echo $item->getFieldError('type') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('description') ?>
            <?php echo $item->req('description') ?>
            <?php echo $item->buildDescription() ?>
        </label>
        <?php echo $item->getFieldError('description') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
