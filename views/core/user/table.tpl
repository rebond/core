<table class="list">
    <tr>
        <th><?php echo $this->lang('email') ?></th>
        <th><?php echo $this->lang('first_name') ?></th>
        <th><?php echo $this->lang('last_name') ?></th>
        <th><?php echo $this->lang('avatar') ?></th>
        <th><?php echo $this->lang('is_admin') ?></th>
        <th><?php echo $this->lang('is_dev') ?></th>
        <th><?php echo $this->lang('status') ?></th>
        <th><?php echo $this->lang('actions') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td><a href="/user/edit/?id=<?php echo $item->getId() ?>"><?php echo $item->getEmail() ?></a></td>
        <td><?php echo $item->getFirstName() ?></td>
        <td><?php echo $item->getLastName() ?></td>
        <td class="no-padding"><img src="<?php echo $this->showFromModel($item->getAvatar(), 32, 32) ?>" alt="<?php echo $item->getAvatar()->getTitle() ?>" /></td>
        <td><span class="bool-<?php echo $item->getIsAdmin() ?>"><?php echo $item->getIsAdmin() ?></span></td>
        <td><span class="bool-<?php echo $item->getIsDev() ?>"><?php echo $item->getIsDev() ?></span></td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-<?php echo $item->getStatus() ?>" data-package="Core" data-entity="User" data-id="<?php echo $item->getId() ?>" data-status="<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></a>
            <?php } else { ?>
                <span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span>
            <?php } ?>
        </td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="/user/user-roles/?id=<?php echo $item->getId() ?>"><?php echo $this->lang('roles') ?></a> |
                <a href="/user/change-password/?id=<?php echo $item->getId() ?>"><?php echo $this->lang('password_change') ?></a> |
                <a href="/user/security/?id=<?php echo $item->getId() ?>"><?php echo $this->lang('security') ?></a>
            <?php } ?>
        </td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-3" data-package="Core" data-entity="User" data-id="<?php echo $item->getId() ?>" data-status="3"><?php echo $this->lang('delete') ?></a>
            <?php } else { ?>
                <a href="#" class="status-2" data-package="Core" data-entity="User" data-id="<?php echo $item->getId() ?>" data-status="2"><?php echo $this->lang('undelete') ?></a>
            <?php } ?>
        </td>
    </tr>
    <?php } ?>
</table>