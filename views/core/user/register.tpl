<?php echo $this->renderTitle('user', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('email') ?>
            <?php echo $item->req('email') ?>
            <?php echo $item->buildEmail() ?>
        </label>
        <?php echo $item->getFieldError('email') ?>
    </div>
    <div class="rb-form-item">
        <?php echo \Rebond\Services\Form::buildBoolean('withPassword', 'with_password', $withPassword) ?>
    </div>
    <div class="rb-form-item" id="password-form">
        <label>
            <?php echo $this->lang('password') ?>
            <?php echo $item->req('password') ?>
            <?php echo $item->buildPassword() ?>
        </label>
        <?php echo $item->getFieldError('password') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('first_name') ?>
            <?php echo $item->req('firstName') ?>
            <?php echo $item->buildFirstName() ?>
        </label>
        <?php echo $item->getFieldError('firstName') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('last_name') ?>
            <?php echo $item->req('lastName') ?>
            <?php echo $item->buildLastName() ?>
        </label>
        <?php echo $item->getFieldError('lastName') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('avatar') ?>
            <?php echo $item->req('avatar') ?>
            <?php echo $item->buildAvatar(false) ?>
        </label>
        <?php echo $item->getFieldError('avatar') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildIsAdmin() ?>
    </div>

    <?php if (!empty($canEditIsDev)) { ?>
    <div class="rb-form-item">
        <?php echo $item->buildIsDev() ?>
    </div>
    <?php } ?>

    <div class="rb-form-item">
        <?php echo \Rebond\Services\Form::buildBoolean('sendMail', 'send_email', $sendMail) ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="/user" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>