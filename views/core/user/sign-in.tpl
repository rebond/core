<img class="big-logo" src="/images/brand-logo.png" alt="Rebond" />
<form method="post" action="<?php echo $_SERVER['REQUEST_URI'] ?>">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('email') ?>
            <?php echo $item->buildEmail() ?>
        </label>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('password') ?>
            <input type="password" class="input" name="password" id="password" />
        </label>
    </div>
    <div class="rb-form-item">
        <?php echo \Rebond\Services\Form::buildBoolean('persistentCookie', 'sign_in_remember', false) ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId(), 'sign_in') ?>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
