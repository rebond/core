<h2>User</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('email') ?></th>
        <td><?php echo $item->getEmail() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('first_name') ?></th>
        <td><?php echo $item->getFirstName() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('last_name') ?></th>
        <td><?php echo $item->getLastName() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('avatar') ?></th>
        <td class="no-padding"><img src="<?php echo $this->showFromModel($item->getAvatar(), 128, 128) ?>" alt="<?php echo $item->getAvatar()->getTitle() ?>" /></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('is_admin') ?></th>
        <td><span class="bool-<?php echo $item->getIsAdmin() ?>"><?php echo $item->getIsAdmin() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('is_dev') ?></th>
        <td><span class="bool-<?php echo $item->getIsDev() ?>"><?php echo $item->getIsDev() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<a href="/user"><?php echo $this->lang('back_to_list') ?></a>