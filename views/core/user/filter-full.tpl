<div class="rb-filter">
    <div>
        <a href="/user/edit/" class="rb-btn"><?php echo $this->lang('user_new') ?></a>
    </div>

    <div class="status">
        <div><?php echo $this->lang('status') ?></div>
        <div class="rb-option rb-activator" data-active="<?php echo $status ?>" id="user-status">
            <a href="#active" data-menu="active"><?php echo $this->lang('active_or_inactive') ?></a>
            <a href="#deleted" data-menu="deleted"><?php echo $this->lang('deleted') ?></a>
        </div>
    </div>

    <div>
        <div><?php echo $this->lang('order') ?></div>
        <select class="input rb-select" id="order" data-hash="2">
            <option value="#<?php echo $status ?>/1/email-asc"><?php echo $this->lang('email') ?> (a->z)</option>
            <option value="#<?php echo $status ?>/1/email-desc"><?php echo $this->lang('email') ?> (z->a)</option>
            <option value="#<?php echo $status ?>/1/first_name-asc"><?php echo $this->lang('first_name') ?> (a->z)</option>
            <option value="#<?php echo $status ?>/1/first_name-desc"><?php echo $this->lang('first_name') ?> (z->a)</option>
            <option value="#<?php echo $status ?>/1/last_name-asc"><?php echo $this->lang('last_name') ?> (a->z)</option>
            <option value="#<?php echo $status ?>/1/last_name-desc"><?php echo $this->lang('last_name') ?> (z->a)</option>
            <option value="#<?php echo $status ?>/1/created_date-asc"><?php echo $this->lang('created_date') ?> (old->new)</option>
            <option value="#<?php echo $status ?>/1/created_date-desc"><?php echo $this->lang('created_date') ?> (new->old)</option>
        </select>
    </div>

    <?php echo $itemsPerPage ?>
    <?php echo $paging ?>
</div>