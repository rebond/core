<h2>Gadget</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('page') ?></th>
        <td><?php echo $item->getPage() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('component') ?></th>
        <td><?php echo $item->getComponent() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('col') ?></th>
        <td><?php echo $item->getCol() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('filter') ?></th>
        <td><?php echo $item->getFilter() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('custom_filter') ?></th>
        <td><?php echo $item->getCustomFilter() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('display_order') ?></th>
        <td><?php echo $item->getDisplayOrder() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<a href="#"><?php echo $this->lang('back_to_list') ?></a>