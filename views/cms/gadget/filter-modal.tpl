<input type="text" class="input" id="search-filter" value="<?php echo $search ?>" placeholder="<?php echo $this->lang('search') ?>" />
<input type="hidden" id="content-id" value="<?php echo $id ?>" />

<div id="search-result">
    <?php
    if (count($results) > 0) {
        foreach ($results as $result) {
            $version = ($result->getAppId() == 0) ? '' : ' data-version="' . $result->getAppId() . '"';
            echo '<a href="#" class="option" data-filter="' . $result->getId() . '"' . $version . '>' . $result->getTitle() . '</a>';
        }
    } else {
        echo $this->lang('result_none');
    }
    ?>
</div>

