<table class="list gadget">
    <tr>
        <th>&nbsp;</th>
        <th><?php echo $this->lang('component') ?></th>
        <th><?php echo $this->lang('col') ?></th>
        <th><?php echo $this->lang('filter') ?></th>
        <th><?php echo $this->lang('display_order') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
        <tr>
            <td>
                <a href="#"
                   class="status-<?php echo $item->getStatus() ?>"
                   data-package="Cms"
                   data-entity="Gadget"
                   data-id="<?php echo $item->getId() ?>"
                   data-status="<?php echo $item->getStatus() ?>">
                    <?php echo $item->getStatusValue() ?>
                </a>
            </td>
            <td>
                <b><?php echo $this->lang($item->getComponent()->getModule()->getTitle()) ?></b>
                <br /><?php echo $this->lang($item->getComponent()->getTitle()) ?>
            </td>
            <td>
                <?php echo \Rebond\Services\Form::buildList('col_' . $item->getId(), $columnOptions, $item->getCol(), true, null, 'column') ?>
            </td>
            <td>
                <?php if ($item->getComponent()->getType() == \Rebond\Enums\Cms\Component::SINGLE_ELEMENT) { ?>
                    <div id="filter-info-<?php echo $item->getId() ?>">
                        <?php echo $item->getFilterInfo()->getTitle() ?>
                    </div>
                    <a href="#" class="gadget-filter" data-id="<?php echo $item->getId() ?>">
                        <?php echo $this->lang('select') ?></a> |
                    <a id="filter-info-edit-<?php echo $item->getId() ?>"
                       target="_blank"
                       data-module="<?php echo $item->getComponent()->getModule() ?>"
                       href="/content/edit/?module=<?php echo $item->getComponent()->getModule() ?>&id=<?php echo $item->getFilterInfo()->getAppId() ?>">
                        <?php echo ($item->getFilterInfo()->getId() == 0) ? $this->lang('new') : $this->lang('edit'); ?>
                    </a>

                <?php } else if ($item->getComponent()->getType() == \Rebond\Enums\Cms\Component::FILTERED_CARDS) { ?>
                    <div id="filter-info-<?php echo $item->getId() ?>">
                        <?php echo $item->getFilterInfo()->getTitle() ?>
                    </div>
                    <a href="#" class="gadget-filter" data-id="<?php echo $item->getId() ?>">
                        <?php echo $this->lang('select') ?>
                    </a>

                <?php } else if ($item->getComponent()->getType() == \Rebond\Enums\Cms\Component::CUSTOM_LISTING) { ?>
                    <input type="text" class="input custom-filter" id="cl_<?php echo $item->getId() ?>" value="<?php echo $item->getCustomFilter() ?>" />
                <?php } ?>
            </td>
            <td>
                <input type="text" class="input display-order" data-id="<?php echo $item->getId() ?>" value="<?php echo $item->getDisplayOrder() ?>" maxlength="3" />
            </td>
            <td>
                <a class="status-3" href="#" data-package="Cms" data-entity="Gadget" data-id="<?php echo $item->getId() ?>" data-status="3">
                    <?php echo $this->lang('delete') ?>
                </a>
            </td>
        </tr>
    <?php } ?>
    <?php foreach ($inactiveItems as $item) { ?>
        <tr class="inactive">
            <td>
                <span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span>
                <?php if ($item->getComponent()->getStatus() == \Rebond\Enums\Core\Status::INACTIVE) { ?>
                    <span class="enum"><?php echo $this->lang('component_inactive') ?></span>
                <?php } else if ($item->getComponent()->getModule()->getStatus() == \Rebond\Enums\Core\Status::INACTIVE) { ?>
                    <span class="enum"><?php echo $this->lang('module_inactive') ?></span>
                <?php } ?>
            </td>
            <td><b><?php echo $item->getComponent()->getModule() ?></b><br /><?php echo $item->getComponent() ?></td>
            <td><?php echo $this->lang('column') ?> <?php echo $item->getCol() ?></td>
            <td>
                <?php if (in_array($item->getComponent()->getType(), [\Rebond\Enums\Cms\Component::SINGLE_ELEMENT, \Rebond\Enums\Cms\Component::FILTERED_CARDS])) { ?>
                    <?php echo $item->getFilterInfo()->getTitle() ?>
                <?php } else if ($item->getComponent()->getType() == \Rebond\Enums\Cms\Component::CUSTOM_LISTING) { ?>
                    <?php echo $item->getCustomFilter() ?>
                <?php } ?>
            </td>
            <td><?php echo $item->getDisplayOrder() ?></td>
            <td>
                <a class="status-3" href="#" data-package="Cms" data-entity="Gadget" data-id="<?php echo $item->getId() ?>" data-status="3">
                    <?php echo $this->lang('delete') ?>
                </a>
            </td>
        </tr>
    <?php } ?>
</table>