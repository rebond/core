<h2><?php echo $title ?> (<?php echo $pageTitle ?>)</h2>
<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-filter">
        <div>
            <img src="/images/layouts/<?php echo $layoutFilename ?>.png" width="50" />
        </div>
        <div>
            <div><?php echo $this->lang('component') ?></div>
            <?php echo $item->buildFullComponent() ?>
        </div>
        <div>
            <div><?php echo $this->lang('column') ?></div>
            <?php echo \Rebond\Services\Form::buildList('col', $columnOptions, $selectedColumn) ?>
        </div>
        <div>
            <div>&nbsp;</div>
            <?php echo $this->buildSubmit() ?>
        </div>
    </div>
</form>
