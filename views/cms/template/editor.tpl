<?php echo $this->renderTitle('template', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('title') ?>
            <?php echo $item->req('title') ?>
            <?php echo $item->buildTitle() ?>
        </label>
        <?php echo $item->getFieldError('title') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('summary') ?>
            <?php echo $item->req('summary') ?>
            <?php echo $item->buildSummary() ?>
        </label>
        <?php echo $item->getFieldError('summary') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('filename') ?>
            <?php echo $item->req('filename') ?>
            <?php echo $item->buildFilename() ?>
        </label>
        <?php echo $item->getFieldError('filename') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('menu') ?>
            <?php echo $item->req('menu') ?>
            <?php echo $item->buildMenu() ?>
        </label>
        <?php echo $item->getFieldError('menu') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('menu_level') ?>
            <?php echo $item->req('menuLevel') ?>
            <?php echo $item->buildMenuLevel() ?>
        </label>
        <?php echo $item->getFieldError('menuLevel') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildInBreadcrumb() ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('side_nav') ?>
            <?php echo $item->req('sideNav') ?>
            <?php echo $item->buildSideNav() ?>
        </label>
        <?php echo $item->getFieldError('sideNav') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('side_nav_level') ?>
            <?php echo $item->req('sideNavLevel') ?>
            <?php echo $item->buildSideNavLevel() ?>
        </label>
        <?php echo $item->getFieldError('sideNavLevel') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildInFooter() ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('footer_level') ?>
            <?php echo $item->req('footerLevel') ?>
            <?php echo $item->buildFooterLevel() ?>
        </label>
        <?php echo $item->getFieldError('footerLevel') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="/cms/template" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
