<h2>Template</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <td><?php echo $this->lang($item->getTitle()) ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('summary') ?></th>
        <td><?php echo $item->getSummary() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('filename') ?></th>
        <td><?php echo $item->getFilename() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('menu') ?></th>
        <td><span class="enum menu-<?php echo $item->getMenu() ?>"><?php echo $item->getMenuValue() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('menu_level') ?></th>
        <td><?php echo $item->getMenuLevel() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('in_breadcrumb') ?></th>
        <td><span class="bool-<?php echo $item->getInBreadcrumb() ?>"><?php echo $item->getInBreadcrumb() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('side_nav') ?></th>
        <td><span class="enum sideNav-<?php echo $item->getSideNav() ?>"><?php echo $item->getSideNavValue() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('side_nav_level') ?></th>
        <td><?php echo $item->getSideNavLevel() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('in_footer') ?></th>
        <td><span class="bool-<?php echo $item->getInFooter() ?>"><?php echo $item->getInFooter() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('footer_level') ?></th>
        <td><?php echo $item->getFooterLevel() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<a href="/cms/template"><?php echo $this->lang('back_to_list') ?></a>