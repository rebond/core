<table class="list">
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('filename') ?></th>
        <th><?php echo $this->lang('menu') . ' (' . $this->lang('depth') . ')' ?></th>
        <th><?php echo $this->lang('in_breadcrumb') ?></th>
        <th><?php echo $this->lang('side_nav') . ' (' . $this->lang('depth') . ')' ?></th>
        <th><?php echo $this->lang('in_footer') . ' (' . $this->lang('depth') . ')' ?></th>
        <th><?php echo $this->lang('status') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td><a href="/cms/template-edit/?id=<?php echo $item->getId() ?>"><?php echo $this->lang($item->getTitle()) ?></a></td>

        <td><?php echo $item->getFilename() ?></td>
        <td>
            <span class="enum menu-<?php echo $item->getMenu() ?>"><?php echo $item->getMenuValue() ?></span>
            <?php echo $item->getMenuLevel() ?>
        </td>

        <td><span class="bool-<?php echo $item->getInBreadcrumb() ?>"><?php echo $item->getInBreadcrumb() ?></span></td>
        <td>
            <span class="enum sideNav-<?php echo $item->getSideNav() ?>"><?php echo $item->getSideNavValue() ?></span>
            <?php echo $item->getSideNavLevel() ?>
        </td>
        <td>
            <span class="bool-<?php echo $item->getInFooter() ?>"><?php echo $item->getInFooter() ?></span>
            <?php echo $item->getFooterLevel() ?>
        </td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-<?php echo $item->getStatus() ?>" data-package="Cms" data-entity="Template" data-id="<?php echo $item->getId() ?>" data-status="<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></a>
            <?php } else { ?>
                <span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span>
            <?php } ?>
        </td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-3" data-package="Cms" data-entity="Template" data-id="<?php echo $item->getId() ?>" data-status="3"><?php echo $this->lang('delete') ?></a>
            <?php } else { ?>
                <a href="#" class="status-2" data-package="Cms" data-entity="Template" data-id="<?php echo $item->getId() ?>" data-status="2"><?php echo $this->lang('undelete') ?></a>
            <?php } ?>
        </td>
    </tr>
    <?php } ?>
</table>