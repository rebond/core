<?php echo $this->renderTitle('page', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <?php echo $item->buildFriendlyUrlPath() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('parent') ?>
            <?php echo $item->req('parent') ?>
            <?php echo $item->buildParent() ?>
        </label>
        <?php echo $item->getFieldError('parent') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('title') ?>
            <?php echo $item->req('title') ?>
            <?php echo $item->buildTitle() ?>
        </label>
        <?php echo $item->getFieldError('title') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('friendly_url') ?>
            <?php echo $item->req('friendlyUrl') ?>
            <div class="text-info"><?php echo $item->getModel()->getFriendlyUrlPath() ?></div>
            <?php echo $item->buildFriendlyUrl() ?>
        </label>
        <?php echo $item->getFieldError('friendlyUrl') ?>
        <?php if ($item->getModel()->getId() != 0) { ?>
            <div class="text-info">
                <a href="<?php echo $siteUrl . $item->getModel()->getFriendlyUrlPath() . $item->getModel()->getFriendlyUrl() ?>" target="_blank"><?php echo $this->lang('view') ?></a> |
                <a href="<?php echo $siteUrl . $item->getModel()->getFriendlyUrlPath() . $item->getModel()->getFriendlyUrl() ?>/?preview=1" target="_blank"><?php echo $this->lang('preview') ?></a>
            </div>
        <?php } ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('display_order') ?>
            <?php echo $item->req('displayOrder') ?>
            <?php echo $item->buildDisplayOrder() ?>
        </label>
        <?php echo $item->getFieldError('displayOrder') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildInNavHeader() ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildInNavSide() ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildInSitemap() ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildInBreadcrumb() ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildInNavFooter() ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('layout') ?>
            <?php echo $item->req('layout') ?>
            <?php echo $item->buildLayout() ?>
        </label>
        <?php echo $item->getFieldError('layout') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('redirect') ?>
            <?php echo $item->req('redirect') ?>
            <?php echo $item->buildRedirect() ?>
        </label>
        <?php echo $item->getFieldError('redirect') ?>
    </div>
<?php if ($isDev) { ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('template') ?>
            <?php echo $item->req('template') ?>
            <?php echo $item->buildTemplate() ?>
        </label>
        <?php echo $item->getFieldError('template') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('class') ?>
            <?php echo $item->req('class') ?>
            <?php echo $item->buildClass() ?>
        </label>
        <?php echo $item->getFieldError('class') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('permission') ?>
            <?php echo $item->req('permission') ?>
            <?php echo $item->buildPermission() ?>
        </label>
        <?php echo $item->getFieldError('permission') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('css') ?>
            <?php echo $item->req('css') ?>
            <?php echo $item->buildCss() ?>
        </label>
        <?php echo $item->getFieldError('css') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('js') ?>
            <?php echo $item->req('js') ?>
            <?php echo $item->buildJs() ?>
        </label>
        <?php echo $item->getFieldError('js') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('status') ?>
            <?php echo $item->req('status') ?>
            <?php echo $item->buildStatus() ?>
        </label>
        <?php echo $item->getFieldError('status') ?>
    </div>
<?php } ?>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="/page" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
