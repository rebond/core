<table class="list">
    <tr>
        <th><?php echo $this->lang('parent') ?></th>
        <th><?php echo $this->lang('template') ?></th>
        <th><?php echo $this->lang('layout') ?></th>
        <th><?php echo $this->lang('title') ?></th>
        <th><?php echo $this->lang('friendly_url') ?></th>
        <th><?php echo $this->lang('redirect') ?></th>
        <th><?php echo $this->lang('class') ?></th>
        <th><?php echo $this->lang('permission') ?></th>
        <th><?php echo $this->lang('display_order') ?></th>
        <th><?php echo $this->lang('status') ?></th>
        <th></th>
    </tr>
    <?php foreach ($items as $item) { ?>
    <tr>
        <td><?php echo ($item->getId() != 1) ? $item->getParent() : ''; ?></td>
        <td><?php echo $item->getTemplate() ?></td>
        <td><?php echo $item->getLayout() ?></td>
        <td><a href="#&id=<?php echo $item->getId() ?>"><?php echo $item->getTitle() ?></a></td>

        <td><?php echo $item->getFriendlyUrl() ?></td>

        <td><?php echo $item->getRedirect() ?></td>

        <td><?php echo $item->getClass() ?></td>

        <td><?php echo $item->getPermission() ?></td>

        <td><?php echo $item->getDisplayOrder() ?></td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-<?php echo $item->getStatus() ?>" data-package="Cms" data-entity="Page" data-id="<?php echo $item->getId() ?>" data-status="<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></a>
            <?php } else { ?>
                <span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span>
            <?php } ?>
        </td>
        <td>
            <?php if ($item->getStatus() != \Rebond\Enums\Core\Status::DELETED) { ?>
                <a href="#" class="status-3" data-package="Cms" data-entity="Page" data-id="<?php echo $item->getId() ?>" data-status="3"><?php echo $this->lang('delete') ?></a>
            <?php } else { ?>
                <a href="#" class="status-2" data-package="Cms" data-entity="Page" data-id="<?php echo $item->getId() ?>" data-status="2"><?php echo $this->lang('undelete') ?></a>
            <?php } ?>
        </td>
    </tr>
    <?php } ?>
</table>