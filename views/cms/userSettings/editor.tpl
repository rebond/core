<?php echo $this->renderTitle('user_settings', $item->getModel()->getId()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('media_view') ?>
            <?php echo $item->req('mediaView') ?>
            <?php echo $item->buildMediaView() ?>
        </label>
        <?php echo $item->getFieldError('mediaView') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('media_paging') ?>
            <?php echo $item->req('mediaPaging') ?>
            <?php echo $item->buildMediaPaging() ?>
        </label>
        <?php echo $item->getFieldError('mediaPaging') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('content_paging') ?>
            <?php echo $item->req('contentPaging') ?>
            <?php echo $item->buildContentPaging() ?>
        </label>
        <?php echo $item->getFieldError('contentPaging') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('paging') ?>
            <?php echo $item->req('paging') ?>
            <?php echo $item->buildPaging() ?>
        </label>
        <?php echo $item->getFieldError('paging') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="#" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
