<h2>Filter</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <td><?php echo $item->getTitle() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('module') ?></th>
        <td><?php echo $item->getModule() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('display_order') ?></th>
        <td><?php echo $item->getDisplayOrder() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<a href="/content/filter"><?php echo $this->lang('back_to_list') ?></a>