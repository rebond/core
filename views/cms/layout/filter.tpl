<div class="rb-filter">
    <div>
        <a href="/cms/layout-edit/" class="rb-btn"><?php echo $this->lang('new') . ' ' . $this->lang('layout') ?></a>
    </div>
    <div class="paging">
        <div class="items">
            <span id="item-count"><?php echo $count ?></span> <?php echo $this->lang('items') ?>
        </div>
    </div>
</div>