<h2>Layout</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <td><?php echo $this->lang($item->getTitle()) ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('summary') ?></th>
        <td><?php echo $item->getSummary() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('filename') ?></th>
        <td><?php echo $item->getFilename() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('columns') ?></th>
        <td><?php echo $item->getColumns() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<a href="/cms/layout"><?php echo $this->lang('back_to_list') ?></a>