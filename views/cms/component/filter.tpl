<div class="rb-filter">
    <div>
        <a href="/cms/component-edit/" class="rb-btn"><?php echo $this->lang('new') . ' ' . $this->lang('component') ?></a>
    </div>

    <div class="status">
        <div><?php echo $this->lang('status') ?></div>
        <div class="rb-option rb-activator" data-active="<?php echo $status ?>" id="component-status">
            <a href="#active" data-menu="active"><?php echo $this->lang('active_or_inactive') ?></a>
            <a href="#deleted" data-menu="deleted"><?php echo $this->lang('deleted') ?></a>
        </div>
    </div>

    <?php echo $itemsPerPage ?>
    <?php echo $paging ?>
</div>