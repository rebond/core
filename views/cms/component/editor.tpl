<?php echo $this->renderTitle('component', $item->getModel()->getId(), $item->getModel()) ?>
<form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form">
    <?php echo $item->buildId() ?>
    <?php echo $item->buildToken() ?>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('module') ?>
            <?php echo $item->req('module') ?>
            <?php echo $item->buildModule() ?>
        </label>
        <?php echo $item->getFieldError('module') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('title') ?>
            <?php echo $item->req('title') ?>
            <?php echo $item->buildTitle() ?>
        </label>
        <?php echo $item->getFieldError('title') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('summary') ?>
            <?php echo $item->req('summary') ?>
            <?php echo $item->buildSummary() ?>
        </label>
        <?php echo $item->getFieldError('summary') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('method') ?>
            <?php echo $item->req('method') ?>
            <?php echo $item->buildMethod() ?>
        </label>
        <?php echo $item->getFieldError('method') ?>
    </div>
    <div class="rb-form-item">
        <label>
            <?php echo $this->lang('type') ?>
            <?php echo $item->req('type') ?>
            <?php echo $item->buildType() ?>
            <span class="input" id="type-disabled">Generic</span>
        </label>
        <?php echo $item->getFieldError('type') ?>
    </div>
    <div class="rb-form-item">
        <?php echo $item->buildCanBeCached() ?>
    </div>
    <div class="rb-form-item">
        <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
        <a href="/cms/component" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
        <?php echo $item->getFieldError('token') ?>
    </div>
</form>
