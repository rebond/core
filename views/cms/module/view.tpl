<h2>Module</h2>
<table class="dual">
    <tr>
        <th><?php echo $this->lang('name') ?></th>
        <td><?php echo $item->getName() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('title') ?></th>
        <td><?php echo $this->lang($item->getTitle()) ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('summary') ?></th>
        <td><?php echo $item->getSummary() ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('workflow') ?></th>
        <td><span class="enum workflow-<?php echo $item->getWorkflow() ?>"><?php echo $item->getWorkflowValue() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('has_filter') ?></th>
        <td><span class="bool-<?php echo $item->getHasFilter() ?>"><?php echo $item->getHasFilter() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('has_content') ?></th>
        <td><span class="bool-<?php echo $item->getHasContent() ?>"><?php echo $item->getHasContent() ?></span></td>
    </tr>
    <tr>
        <th><?php echo $this->lang('status') ?></th>
        <td><span class="enum status-<?php echo $item->getStatus() ?>"><?php echo $item->getStatusValue() ?></span></td>
    </tr>
</table>
<a href="#"><?php echo $this->lang('back_to_list') ?></a>